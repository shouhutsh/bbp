package com.example.xmlParse.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午10:43
 * To change this template use File | Settings | File Templates.
 */
public class DBHelper extends SQLiteOpenHelper implements SQLiteHelper {
	private final static String DB_NAME = "client.db";
	private final static int    DB_VERSION = 1;

	private final static String TB_NAME = "event";
	private final static String PAR_ID = "id";
	private final static String PAR_NAME = "name";
	private final static String PAR_TIME = "time";
	private final static String PAR_DUTY = "duty";

	private final String CREATE = "create table event" +
								"(id int auto_increment primary key," +
								"name varchar(20)," +
								"time bigint," +
								"duty bolb)";

	private final String DROP   = "drop table if exits event";
	private final String SELECT = "select * from event where duty=1 and time!=0";
	private final String UPDATE = "update event set duty=0";
	//NOTE need parameters
	private final String INSERT = "insert into event(name,time, duty)values(%s, %d, 1)";
	private final String DELETE = "id=?";


	public DBHelper(Context context){
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db){
		db.execSQL(CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		db.execSQL(DROP);
		onCreate(db);
	}

	@Override
	public long insert(String name, long time){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(PAR_NAME, name);
		cv.put(PAR_TIME, time);
		cv.put(PAR_DUTY, true);

		return db.insert(TB_NAME, null, cv);
	}

	@Override
	public void delete(int id){
		SQLiteDatabase db = this.getWritableDatabase();
		String[] where = {String.valueOf(id)};

		db.delete(TB_NAME, DELETE, where);
	}

	@Override
	public void update(){
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL(UPDATE);
	}

	@Override
	public Cursor select(){
		SQLiteDatabase db = this.getReadableDatabase();
		return db.rawQuery(SELECT, null);
	}

}

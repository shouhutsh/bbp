package com.example.xmlParse.WebServer;

import android.content.Context;
import android.database.Cursor;
import com.example.xmlParse.MyActivity;
import com.example.xmlParse.SQLite.DBHelper;
import com.example.xmlParse.SQLite.SQLiteHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午11:23
 * To change this template use File | Settings | File Templates.
 */
public class InfoExchange extends MyActivity {
	private Context context;
	public InfoExchange(Context context){
		this.context = context;
	}

	public void acceptXML(){
		try {
			URL url = url = new URL("http://10.0.2.2:9090/xmlCreate");

			HttpURLConnection huc = (HttpURLConnection)url.openConnection();

			if(HttpURLConnection.HTTP_OK == huc.getResponseCode()){
				InputStream in = huc.getInputStream();

				StringBuilder event = new StringBuilder(1024);
				int c;
				while((c = in.read()) != -1){
					event.append((char)c);
				}

				JSONObject jsonInsert = new JSONObject(event.toString());
				JSONObject jsonEvent  = jsonInsert.getJSONObject("insert");
				JSONArray  jsonArray = jsonEvent.getJSONArray("event");

				SQLiteHelper helper = new DBHelper(context);

				for(int i=0; i < jsonArray.length(); ){
//					event e = new event();
//					e.setName(jsonArray.getString(i++));
//					e.setTime(jsonArray.getLong(i++));
//
//					e.saveToSQLite(helper);
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		} catch (JSONException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	public void sendXML(){
		try{
			URL url = new URL("http://10.0.2.2:9090/xmlCreate");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			HttpURLConnection.setFollowRedirects(true);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setRequestMethod("POST");	//设置为Post方法
			con.setRequestProperty("Content-Type", "multipart/form-data");	//这话似乎没影响
			con.connect();

			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(con.getOutputStream()));

			JSONObject jsonInsert = new JSONObject();
			JSONObject jsonEvent = new JSONObject();
			JSONArray  jsonArray  = new JSONArray();

			SQLiteHelper helper = new DBHelper(context);
			Cursor cursor = helper.select();
			cursor.moveToFirst();
			while(! cursor.isAfterLast()){
//				event e = new event(cursor);
//				jsonArray.put(e.getName()).put(e.getTime());

				cursor.moveToNext();
			}
			jsonEvent.put("event", jsonArray);
			jsonInsert.put("insert", jsonEvent);

			out.writeUTF(jsonInsert.toString());
			out.flush();
			out.close();

			//接受服务器发送过来的信息
			if (HttpURLConnection.HTTP_OK == con.getResponseCode()) {
				helper.update();
			}
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}

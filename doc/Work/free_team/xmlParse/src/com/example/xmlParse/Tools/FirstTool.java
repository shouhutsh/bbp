package com.example.xmlParse.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.xmlParse.SQLite.SQLiteDatabaseHelper;
import com.example.xmlParse.SQLite.SQLiteHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 上午11:13
 * To change this template use File | Settings | File Templates.
 */
public class FirstTool extends SQLiteTool{
	final static String F_ID="f_id";
	final static String U_ID="u_id";
	final static String NAME="name";
	final static String STATE="state";
	final static String TYPE="type";

	String f_id = null;
	String u_id = null;
	String name = null;
	String state = null;
	String type = null;

	public FirstTool() {
	}

	public FirstTool(Cursor cursor){
		f_id = cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(NAME));
		state =cursor.getString(cursor.getColumnIndexOrThrow(STATE));
		type = cursor.getString(cursor.getColumnIndexOrThrow(TYPE));
	}

	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(SQLiteDatabaseHelper helper){
		if(state != null && state.equals("0")){			//0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(helper);

			//这里是需要新添加的数据
			state = "1";									//1 代表S，意思是待同步

			ContentValues contentValues = new ContentValues();
			contentValues.put(U_ID, u_id);
			contentValues.put(NAME, name);
			contentValues.put(STATE,state);
			contentValues.put(TYPE, type);

			helper.insert(contentValues);
			f_id = helper.last_insert_rowid();
		}else if(state != null && state.equals("1")){	//1 代表S，意思是待同步，则这时直接修改即可
			ContentValues contentValues = new ContentValues();
			contentValues.put(U_ID, u_id);
			contentValues.put(NAME, name);
			contentValues.put(STATE,state);
			contentValues.put(TYPE, type);

			//判断这是不是一个新的数据
			if(f_id != null){
				helper.update(contentValues, F_ID+"=?", new String[]{f_id});
			}else{
				helper.insert(contentValues);
				f_id = helper.last_insert_rowid();
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(SQLiteDatabaseHelper helper){
		if(state != null && state.equals("0")){
			ContentValues contentValues = new ContentValues();
			contentValues.put(STATE, "2");				//2 代表D，意思是需要被删除
			helper.update(contentValues, F_ID+"=?", new String[]{f_id});
		}else if(state != null && state.equals("1")){
			if(f_id != null)
				helper.delete(F_ID+"=?", new String[]{f_id});
		}
	}

	@Override
	public String getF_id() {
		return f_id;
	}

	@Override
	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}
}

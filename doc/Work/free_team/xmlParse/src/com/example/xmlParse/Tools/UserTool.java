package com.example.xmlParse.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import com.example.xmlParse.SQLite.SQLiteDatabaseHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午3:14
 * To change this template use File | Settings | File Templates.
 */
public final class UserTool{
	final static String U_ID = "u_id";
	final static String NAME = "name";
	final static String PASSWORD="password";

	static String u_id = null;
	static String name = null;
	static String password = null;

	public UserTool() {
	}

	//不需要从数据库中读出，因为需要用户输入
	/*
	public UserTool(Cursor cursor){
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(NAME));
		password = cursor.getString(cursor.getColumnIndexOrThrow(PASSWORD));
	}*/


	static public void save(SQLiteDatabaseHelper helper) {
		//To change body of implemented methods use File | Settings | File Templates.
		ContentValues contentValues = new ContentValues();
		contentValues.put(U_ID, u_id);
		contentValues.put(NAME, name);
		contentValues.put(PASSWORD,password);

		helper.insert(contentValues);
	}

	static public void remove(SQLiteDatabaseHelper helper) {
		//To change body of implemented methods use File | Settings | File Templates.
		helper.delete(U_ID+"=?", new String[]{u_id});
	}


	static public String getU_id() {
		return u_id;
	}

	static public void setU_id(String u_id) {
		UserTool.u_id = u_id;
	}

	static public String getPassword() {
		return password;
	}

	static public void setPassword(String password) {
		UserTool.password = password;
	}

	static public String getName() {
		return name;
	}

	static public void setName(String name) {
		UserTool.name = name;
	}
}

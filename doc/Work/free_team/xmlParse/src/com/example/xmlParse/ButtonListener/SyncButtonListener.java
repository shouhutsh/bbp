package com.example.xmlParse.ButtonListener;

import android.content.Context;
import android.view.View;
import com.example.xmlParse.WebServer.InfoExchange;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午11:21
 * To change this template use File | Settings | File Templates.
 */
public class SyncButtonListener implements View.OnClickListener {
	private Context context;
	public SyncButtonListener(Context context){
		this.context = context;
	}

	@Override
	public void onClick(View v){
		InfoExchange ie = new InfoExchange(context);
		ie.acceptXML();
		ie.sendXML();
	}
}

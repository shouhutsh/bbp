package com.example.xmlParse;

import android.app.Activity;
import android.app.TabActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.Toast;
import com.example.xmlParse.ButtonListener.CommonButtonListener;
import com.example.xmlParse.ButtonListener.SyncButtonListener;
import com.example.xmlParse.SQLite.FirstHelper;
import com.example.xmlParse.SQLite.SQLiteDatabaseHelper;
import com.example.xmlParse.SQLite.UserHelper;
import com.example.xmlParse.Tools.FirstTool;
import com.example.xmlParse.Tools.SQLiteTool;
import com.example.xmlParse.Tools.UserTool;


public class MyActivity extends TabActivity implements TabHost.OnTabChangeListener{

	private TabHost.TabSpec ts1, ts2;
	private TabHost tabHost;
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);



		{
			SQLiteDatabaseHelper helper = null;
			helper = new UserHelper(getApplicationContext());

			UserTool.setU_id("5");

			UserTool.setName("user1");
			UserTool.setPassword("123");

			UserTool.save(helper);
			UserTool.remove(helper);


			SQLiteTool first = new FirstTool();
			helper = new FirstHelper(getApplicationContext());
			first.setU_id("5");
			first.setName("eat");
			first.setState("1");
			first.setType("0");

			first.save(helper);
			first.remove(helper);

		}




/*
		tabHost = this.getTabHost();
		LayoutInflater.from(this).inflate(R.layout.main, tabHost.getTabContentView());

		{
			Button b1 = (Button) findViewById(R.id.b1);
			Button b2 = (Button) findViewById(R.id.b2);

			Button sync = (Button) findViewById(R.id.sync);

			b1.setOnClickListener(new CommonButtonListener(b1.getText().toString(), MyActivity.this));
			b2.setOnClickListener(new CommonButtonListener(b2.getText().toString(), MyActivity.this));
			sync.setOnClickListener(new SyncButtonListener(MyActivity.this));
		}

		ts1 = tabHost.newTabSpec("tabOne");
		ts1.setIndicator(getString(R.string.health));
		ts1.setContent(R.id.health);

		ts2 = tabHost.newTabSpec("tabTwo");
		ts2.setIndicator(getString(R.string.lift));
		ts2.setContent(R.id.life);

		tabHost.addTab(ts1);
		tabHost.addTab(ts2);
		tabHost.setOnTabChangedListener(this);
		*/
	}

	@Override
	public void onTabChanged(String s) {
		//To change body of implemented methods use File | Settings | File Templates.
		if(s.equals("tabOne")){
			Toast.makeText(this, "test one", Toast.LENGTH_LONG).show();
		}else if(s.equals("tabTwo")){
			Toast.makeText(this, "test two", Toast.LENGTH_SHORT).show();
		}
	}
}

/*
public class MyActivity extends Activity {


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button b1 = (Button) findViewById(R.id.b1);
		Button b2 = (Button) findViewById(R.id.b2);

		Button sync = (Button) findViewById(R.id.sync);

		b1.setOnClickListener(new CommonButtonListener(b1.getText().toString(), MyActivity.this));
		b2.setOnClickListener(new CommonButtonListener(b2.getText().toString(), MyActivity.this));
		sync.setOnClickListener(new SyncButtonListener(MyActivity.this));
	}
}
*/
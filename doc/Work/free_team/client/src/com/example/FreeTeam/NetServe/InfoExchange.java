package com.example.FreeTeam.NetServe;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.FreeTeam.SQLite.*;
import com.example.FreeTeam.Tools.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-29
 * Time: 下午1:12
 * To change this template use File | Settings | File Templates.
 */
public class InfoExchange {

	JSONObject info = new JSONObject();

	JSONObject insert = new JSONObject();
	JSONObject delete = new JSONObject();

	private Context context = null;

	public InfoExchange(Context context) {
		this.context = context;
	}

	public void sync() {
		try {
			JSONArray user = new JSONArray();
			user.put(UserTool.getU_id()).put(UserTool.getName()).put(UserTool.getPassword());
			info.put("user", user);

			createInsert();
			createDelete();

			URL url = new URL("http://10.0.2.2:9090/deal");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			HttpURLConnection.setFollowRedirects(true);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setRequestMethod("POST");    //设置为Post方法
			con.setRequestProperty("Content-Type", "multipart/form-data");    //这话似乎没影响
			con.connect();

			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(con.getOutputStream()));
			out.writeUTF(info.toString());
			out.flush();
			out.close();
			//接受服务器发送过来的信息
			if (HttpURLConnection.HTTP_OK == con.getResponseCode()) {
				DataInputStream in = new DataInputStream(new BufferedInputStream(con.getInputStream()));
				parse(in.readUTF());
				in.close();

				SQLiteDatabaseHelper sqLiteDatabaseHelper = null;
				sqLiteDatabaseHelper = new FirstHelper(context);
				sqLiteDatabaseHelper.delete("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
				sqLiteDatabaseHelper = new SecondHelper(context);
				sqLiteDatabaseHelper.delete("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
				sqLiteDatabaseHelper = new ThirdHelper(context);
				sqLiteDatabaseHelper.delete("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
				sqLiteDatabaseHelper = new RecordHelper(context);
				sqLiteDatabaseHelper.delete("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
				sqLiteDatabaseHelper = new RecordButtonHelper(context);
				sqLiteDatabaseHelper.delete("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
			}
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}


	private void parse(String msg) throws JSONException, SQLException {
		info = new JSONObject(msg);

		if (info.has("delete")) {            //这里执行删除操作
			deleteOperate();
		}
		if (info.has("insert")) {                //这里是插入操作
			insertOperate();
		}
	}

	private void insertOperate() throws JSONException, SQLException {
		String key = "insert";
		SQLiteTool sqLiteTool = null;
		JSONArray tableArray = null;
		SQLiteDatabaseHelper sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
		SQLiteDatabase st = sqLiteDatabaseHelper.getWritableDatabase();

		insert = info.getJSONObject(key);

		/**
		 * 注意：因为目前只是测试阶段，因此并未执行操作
		 * 		只是将值取出而已
		 * 		由于外键约束，因此必须按顺序来，因此应
		 * 		保证传输的数据里必须有以下表字段
		 */
		if (insert.has("first")) {        //这里对应要执行操作的表
			tableArray = insert.getJSONArray("first");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new FirstTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("insert into first(f_id,u_id,name,state,type)values("
						+ sqLiteTool.getF_id() + "," + sqLiteTool.getU_id() + ",'" + sqLiteTool.getName() + "',"
						+ sqLiteTool.getState() + "," + sqLiteTool.getType() + ")");
			}
		}
		if (insert.has("second")) {
			tableArray = insert.getJSONArray("second");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new SecondTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("insert into second(s_id,f_id,u_id,name,state)values("
						+ sqLiteTool.getS_id() + "," + sqLiteTool.getF_id() + "," + sqLiteTool.getU_id() + ",'"
						+ sqLiteTool.getName() + "'," + sqLiteTool.getState() + ")");
			}
		}
		if (insert.has("third")) {
			tableArray = insert.getJSONArray("third");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new ThirdTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("insert into third(t_id,s_id,u_id,name,state)values("
						+ sqLiteTool.getT_id() + "," + sqLiteTool.getS_id() + "," + sqLiteTool.getU_id() + ",'"
						+ sqLiteTool.getName() + "'," + sqLiteTool.getState() + ")");
			}
		}
		if (insert.has("record")) {
			tableArray = insert.getJSONArray("record");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new RecordTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("insert into record(r_id,u_id,start_time,end_time,f_id,s_id,t_id,amount,unit,remark,state)values("
						+ sqLiteTool.getR_id() + "," + sqLiteTool.getU_id() + "," + sqLiteTool.getStart() + "," + sqLiteTool.getEnd() + ","
						+ sqLiteTool.getF_id() + "," + sqLiteTool.getS_id() + "," + sqLiteTool.getT_id() + "," + sqLiteTool.getAmount() + ","
						+ sqLiteTool.getUnit() + ",'" + sqLiteTool.getRemark() + "'," + sqLiteTool.getState() + ")");
			}
		}
		if (insert.has("record_button")) {
			tableArray = insert.getJSONArray("record_button");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new RecordButtonTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("insert into record_button(rb_id,u_id,f_id,s_id,t_id,state)values("
						+ sqLiteTool.getRb_id() + "," + sqLiteTool.getU_id() + "," + sqLiteTool.getF_id() + ","
						+ sqLiteTool.getS_id() + "," + sqLiteTool.getT_id() + "," + sqLiteTool.getState() + ")");
			}
		}
	}

	private void deleteOperate() throws JSONException, SQLException {
		String key = "delete";
		JSONArray tableArray = null;
		SQLiteTool sqLiteTool = null;
		SQLiteDatabaseHelper sqLiteDatabaseHelper = new SQLiteDatabaseHelper(context);
		SQLiteDatabase st = sqLiteDatabaseHelper.getWritableDatabase();

		insert = info.getJSONObject(key);
		/**
		 * 注意：因为目前只是测试阶段，因此并未执行操作
		 * 		只是将值取出而已
		 * 		由于有外键依赖，因此不能随意删除类目，
		 * 		以后再写级联删除
		 */
		if (delete.has("first")) {        //这里对应要执行操作的表
			tableArray = insert.getJSONArray("first");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new FirstTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("delete from first where f_id=" + sqLiteTool.getF_id() + " and u_id=" + sqLiteTool.getU_id());
			}
		}
		if (delete.has("second")) {
			tableArray = insert.getJSONArray("second");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new SecondTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("delete from second where f_id=" + sqLiteTool.getF_id() + " and u_id=" + sqLiteTool.getU_id());
			}
		}
		if (delete.has("third")) {
			tableArray = insert.getJSONArray("third");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new ThirdTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("delete from third where f_id=" + sqLiteTool.getF_id() + " and u_id=" + sqLiteTool.getU_id());
			}
		}
		if (delete.has("record")) {
			tableArray = insert.getJSONArray("record");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new RecordTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("delete from record where f_id=" + sqLiteTool.getF_id() + " and u_id=" + sqLiteTool.getU_id());
			}
		}
		if (delete.has("record_button")) {
			tableArray = insert.getJSONArray("record_button");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new RecordButtonTool().valueOf(tableArray.getJSONArray(i));
				st.execSQL("delete from record_button where f_id=" + sqLiteTool.getF_id() + " and u_id=" + sqLiteTool.getU_id());
			}
		}
	}

	private void createInsert() throws JSONException {
		Cursor cursor = null;
		JSONArray jsonArray = null;
		SQLiteTool sqLiteTool = null;
		SQLiteDatabaseHelper helper = null;

		//一级类
		jsonArray = new JSONArray();
		helper = new FirstHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new FirstTool(cursor);
			sqLiteTool.save(helper, "0");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getF_id()).put(sqLiteTool.getU_id()).put(sqLiteTool.getName()).put(sqLiteTool.getType());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		insert.put("first", jsonArray);


		//二级类
		jsonArray = new JSONArray();
		helper = new SecondHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new SecondTool(cursor);
			sqLiteTool.save(helper, "0");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getS_id()).put(sqLiteTool.getF_id()).put(sqLiteTool.getU_id()).put(sqLiteTool.getName());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		insert.put("second", jsonArray);


		//三级类
		jsonArray = new JSONArray();
		helper = new ThirdHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new ThirdTool(cursor);
			sqLiteTool.save(helper, "0");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getT_id()).put(sqLiteTool.getS_id()).put(sqLiteTool.getU_id()).put(sqLiteTool.getName());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		insert.put("third", jsonArray);


		//记录类
		jsonArray = new JSONArray();
		helper = new RecordHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new RecordTool(cursor);
			sqLiteTool.save(helper, "0");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getR_id()).put(sqLiteTool.getU_id()).put(sqLiteTool.getStart()).put(sqLiteTool.getEnd())
					.put(sqLiteTool.getF_id()).put(sqLiteTool.getS_id()).put(sqLiteTool.getT_id())
					.put(sqLiteTool.getAmount()).put(sqLiteTool.getUnit()).put(sqLiteTool.getRemark());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		insert.put("record", jsonArray);


		//按钮类
		jsonArray = new JSONArray();
		helper = new RecordButtonHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new RecordButtonTool(cursor);
			sqLiteTool.save(helper, "0");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getRb_id()).put(sqLiteTool.getU_id()).put(sqLiteTool.getF_id())
					.put(sqLiteTool.getS_id()).put(sqLiteTool.getT_id());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		insert.put("record_button", jsonArray);

		info.put("insert", insert);
	}

	private void createDelete() throws JSONException {
		Cursor cursor = null;
		JSONArray jsonArray = null;
		SQLiteTool sqLiteTool = null;
		SQLiteDatabaseHelper helper = null;

		//一级类
		jsonArray = new JSONArray();
		helper = new FirstHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new FirstTool(cursor);
			sqLiteTool.save(helper, "2");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getF_id()).put(sqLiteTool.getU_id());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		delete.put("first", jsonArray);


		//二级类
		jsonArray = new JSONArray();
		helper = new SecondHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new SecondTool(cursor);
			sqLiteTool.save(helper, "2");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getS_id()).put(sqLiteTool.getU_id());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		delete.put("second", jsonArray);


		//三级类
		jsonArray = new JSONArray();
		helper = new ThirdHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new ThirdTool(cursor);
			sqLiteTool.save(helper, "2");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getT_id()).put(sqLiteTool.getU_id());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		delete.put("third", jsonArray);


		//记录类
		jsonArray = new JSONArray();
		helper = new RecordHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new RecordTool(cursor);
			sqLiteTool.save(helper, "2");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getR_id()).put(sqLiteTool.getU_id());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		delete.put("record", jsonArray);


		//按钮类
		jsonArray = new JSONArray();
		helper = new RecordButtonHelper(context);
		cursor = helper.select("u_id=? and state=?", new String[]{UserTool.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			sqLiteTool = new RecordButtonTool(cursor);
			sqLiteTool.save(helper, "2");

			JSONArray item = new JSONArray();
			item.put(sqLiteTool.getRb_id()).put(sqLiteTool.getU_id());
			jsonArray.put(item);

			cursor.moveToNext();
		}
		delete.put("record_button", jsonArray);

		info.put("delete", delete);
	}
}

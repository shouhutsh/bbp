package com.example.FreeTeam;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.Toast;
import com.example.FreeTeam.NetServe.InfoExchange;
import com.example.FreeTeam.SQLite.*;
import com.example.FreeTeam.Tools.*;

public class MyActivity extends Activity {
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.main);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebtn);

		ImageButton button1 = (ImageButton)findViewById(R.id.login);
		ImageButton button2 = (ImageButton)findViewById(R.id.sync);
		button1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//To change body of implemented methods use File | Settings | File Templates.
				Toast.makeText(getApplicationContext(), "模拟用户登陆", Toast.LENGTH_SHORT).show();

				//模拟用户登录
				UserTool.setU_id("2");
				UserTool.setName("user2");
				UserTool.setPassword("2");

			}
		});
		button2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//To change body of implemented methods use File | Settings | File Templates.
				if(UserTool.getU_id() == null){
					Toast.makeText(getApplicationContext(), R.string.W_Login, Toast.LENGTH_SHORT).show();
				}else{
					//模拟与服务器交互
					InfoExchange infoExchange = new InfoExchange(getApplicationContext());
					infoExchange.sync();
					Toast.makeText(getApplicationContext(), R.string.S_Sync, Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}

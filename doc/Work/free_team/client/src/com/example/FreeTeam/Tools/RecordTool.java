package com.example.FreeTeam.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import com.example.FreeTeam.SQLite.SQLiteDatabaseHelper;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-25
 * Time: 下午8:39
 * To change this template use File | Settings | File Templates.
 */
public class RecordTool extends SQLiteTool {
	final static String R_ID = "r_id";
	final static String U_ID = "u_id";
	final static String START= "start_time";
	final static String END  = "end_time";
	final static String F_ID = "f_id";
	final static String S_ID = "s_id";
	final static String T_ID = "t_id";
	final static String AMOUNT="amount";
	final static String UNIT = "unit";
	final static String REMARK="remark";
	final static String STATE= "state";

	String r_id = null;
	String u_id = null;
	String start= null;
	String end  = null;
	String f_id = null;
	String s_id = null;
	String t_id = null;
	String amount=null;
	String unit = null;
	String remark=null;
	String state= null;

	public RecordTool() {
		empty();
	}

	public RecordTool(Cursor cursor){
		r_id = cursor.getString(cursor.getColumnIndexOrThrow(R_ID));
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		start= cursor.getString(cursor.getColumnIndexOrThrow(START));
		end  = cursor.getString(cursor.getColumnIndexOrThrow(END));
		f_id = cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		s_id = cursor.getString(cursor.getColumnIndexOrThrow(S_ID));
		t_id = cursor.getString(cursor.getColumnIndexOrThrow(T_ID));
		amount=cursor.getString(cursor.getColumnIndexOrThrow(AMOUNT));
		unit = cursor.getString(cursor.getColumnIndexOrThrow(UNIT));
		remark=cursor.getString(cursor.getColumnIndexOrThrow(REMARK));
		state= cursor.getString(cursor.getColumnIndexOrThrow(STATE));
	}


	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(SQLiteDatabaseHelper helper, String newState){
		ContentValues contentValues = new ContentValues();
		contentValues.put(U_ID, u_id);
		contentValues.put(START, start);
		contentValues.put(END, end);
		contentValues.put(F_ID, f_id);
		contentValues.put(S_ID, s_id);
		contentValues.put(T_ID, t_id);
		contentValues.put(AMOUNT, amount);
		contentValues.put(UNIT, unit);
		contentValues.put(REMARK, remark);

		if(state != null && state.equals("0")){			//0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(helper);

			//这里是需要新添加的数据
			state = newState;									//1 代表S，意思是待同步
			contentValues.put(STATE, state);

			helper.insert(contentValues);
			r_id = helper.last_insert_rowid();
		}else if(state != null && state.equals("1")){	//1 代表S，意思是待同步，则这时直接修改即可
			state = newState;
			contentValues.put(STATE, state);
			//判断这是不是一个新的数据
			if(r_id != null){
				helper.update(contentValues, R_ID+"=?", new String[]{r_id});
			}else{
				helper.insert(contentValues);
				r_id = helper.last_insert_rowid();
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(SQLiteDatabaseHelper helper){
		if(state != null && state.equals("0")){
			ContentValues contentValues = new ContentValues();
			contentValues.put(STATE, "2");				//2 代表D，意思是需要被删除
			helper.update(contentValues, R_ID+"=?", new String[]{r_id});
		}else if(state != null && state.equals("1")){
			if(r_id != null)
				helper.delete(R_ID+"=?", new String[]{r_id});
		}

//		empty();
	}

	@Override
	public void empty(){
		r_id = null;
		u_id = null;
		start= null;
		end  = null;
		f_id = null;
		s_id = null;
		t_id = null;
		amount=null;
		unit = null;
		remark=null;
		state= null;
	}

	@Override
	public SQLiteTool valueOf(JSONArray jsonArray) throws JSONException {
		r_id = jsonArray.getString(0);
		u_id = jsonArray.getString(1);
		start= jsonArray.getString(2);
		end  = jsonArray.getString(3);
		f_id = jsonArray.getString(4);
		s_id = jsonArray.getString(5);
		t_id = jsonArray.getString(6);
		amount=jsonArray.getString(7);
		unit = jsonArray.getString(8);
		remark=jsonArray.getString(9);

		state = "0";
		return this;
	}

	@Override
	public String getR_id() {
		return r_id;
	}

	@Override
	public void setR_id(String r_id) {
		this.r_id = r_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getStart() {
		return start;
	}

	@Override
	public void setStart(String start) {
		this.start = start;
	}

	@Override
	public String getEnd() {
		return end;
	}

	@Override
	public void setEnd(String end) {
		this.end = end;
	}

	@Override
	public String getF_id() {
		return f_id;
	}

	@Override
	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	@Override
	public String getS_id() {
		return s_id;
	}

	@Override
	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	@Override
	public String getT_id() {
		return t_id;
	}

	@Override
	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	@Override
	public String getAmount() {
		return amount;
	}

	@Override
	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String getUnit() {
		return unit;
	}

	@Override
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}
}

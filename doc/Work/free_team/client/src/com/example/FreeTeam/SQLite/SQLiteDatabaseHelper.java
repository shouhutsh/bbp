package com.example.FreeTeam.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 上午11:41
 * To change this template use File | Settings | File Templates.
 */
public class SQLiteDatabaseHelper extends SQLiteOpenHelper {
	private final static String DB_NAME = "client.db";
	private final static int 	DB_VERSION = 1;

	private final static String TB_Name1 = "user";
	private final static String TB_Name2 = "first";
	private final static String TB_Name3 = "second";
	private final static String TB_Name4 = "third";
	private final static String TB_Name5 = "record";
	private final static String TB_Name6 = "record_button";


	final static String CREATE1="CREATE TABLE IF NOT EXISTS "+TB_Name1+" ( \n" +
			"    u_id     INTEGER        NOT NULL\n" +
			"                            PRIMARY KEY,\n" +
			"    name     VARCHAR( 50 )  NOT NULL,\n" +
			"    password VARCHAR( 50 )  NOT NULL \n" +
			")";
	final static String CREATE2="CREATE TABLE IF NOT EXISTS "+TB_Name2+" ( \n" +
			"    f_id  INTEGER        PRIMARY KEY\n" +
			"                         NOT NULL,\n" +
			"    u_id  INTEGER        NOT NULL\n" +
			"                         REFERENCES user ( u_id ),\n" +
			"    name  VARCHAR( 50 )  NOT NULL,\n" +
			"    state INTEGER        NOT NULL\n" +
			"                         DEFAULT '1',\n" +
			"    type  INTEGER        NOT NULL\n" +
			"                         DEFAULT '0' \n" +
			")";
	final static String CREATE3="CREATE TABLE IF NOT EXISTS "+TB_Name3+" ( \n" +
			"    s_id  INTEGER        PRIMARY KEY\n" +
			"                         NOT NULL,\n" +
			"    f_id  INTEGER        NOT NULL\n" +
			"                         REFERENCES first ( f_id ),\n" +
			"    u_id  INTEGER        NOT NULL\n" +
			"                         REFERENCES user ( u_id ),\n" +
			"    name  VARCHAR( 50 )  NOT NULL,\n" +
			"    state INTEGER        NOT NULL\n" +
			"                         DEFAULT '1' \n" +
			")";
	final static String CREATE4="CREATE TABLE IF NOT EXISTS "+TB_Name4+" ( \n" +
			"    t_id  INTEGER        PRIMARY KEY\n" +
			"                         NOT NULL,\n" +
			"    s_id  INTEGER        NOT NULL\n" +
			"                         REFERENCES second ( s_id ),\n" +
			"    u_id  INTEGER        NOT NULL\n" +
			"                         REFERENCES user ( u_id ),\n" +
			"    name  VARCHAR( 50 )  NOT NULL,\n" +
			"    state INTEGER        NOT NULL\n" +
			"                         DEFAULT '1' \n" +
			")";
	final static String CREATE5="CREATE TABLE IF NOT EXISTS "+TB_Name5+" ( \n" +
			"    r_id       INTEGER         PRIMARY KEY\n" +
			"                               NOT NULL,\n" +
			"    u_id       INTEGER         NOT NULL\n" +
			"                               REFERENCES user ( u_id ),\n" +
			"    start_time TIMESTAMP       NOT NULL\n" +
			"                               DEFAULT 'CURRENT_TIMESTAMP',\n" +
			"    end_time   TIMESTAMP       DEFAULT 'CURRENT_TIMESTAMP',\n" +
			"    f_id       INTEGER         NOT NULL\n" +
			"                               REFERENCES first ( f_id ),\n" +
			"    s_id       INTEGER         NOT NULL\n" +
			"                               REFERENCES second ( s_id ),\n" +
			"    t_id       INTEGER         NOT NULL\n" +
			"                               REFERENCES third ( t_id ),\n" +
			"    amount     FLOAT,\n" +
			"    unit       INTEGER         NOT NULL\n" +
			"                               DEFAULT '0',\n" +
			"    remark     VARCHAR( 200 ),\n" +
			"    state      INTEGER         NOT NULL\n" +
			"                               DEFAULT '1' \n" +
			")";
	final static String CREATE6="CREATE TABLE IF NOT EXISTS "+TB_Name6+" ( \n" +
			"    rb_id INTEGER PRIMARY KEY\n" +
			"                  NOT NULL,\n" +
			"    u_id  INTEGER NOT NULL\n" +
			"                  REFERENCES user ( u_id ),\n" +
			"    f_id  INTEGER NOT NULL\n" +
			"                  REFERENCES first ( f_id ),\n" +
			"    s_id  INTEGER NOT NULL\n" +
			"                  REFERENCES second ( s_id ),\n" +
			"    t_id  INTEGER NOT NULL\n" +
			"                  REFERENCES third ( t_id ),\n" +
			"    state INTEGER        NOT NULL\n" +
			")";
	private final static String DROP="DROP TABLE IF EXISTS ";

	public SQLiteDatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		//To change body of implemented methods use File | Settings | File Templates.
		sqLiteDatabase.execSQL(CREATE1);
		sqLiteDatabase.execSQL(CREATE2);
		sqLiteDatabase.execSQL(CREATE3);
		sqLiteDatabase.execSQL(CREATE4);
		sqLiteDatabase.execSQL(CREATE5);
		sqLiteDatabase.execSQL(CREATE6);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
		//To change body of implemented methods use File | Settings | File Templates.
		sqLiteDatabase.execSQL(DROP+TB_Name1);
		sqLiteDatabase.execSQL(DROP+TB_Name2);
		sqLiteDatabase.execSQL(DROP+TB_Name3);
		sqLiteDatabase.execSQL(DROP+TB_Name4);
		sqLiteDatabase.execSQL(DROP+TB_Name5);
		sqLiteDatabase.execSQL(DROP+TB_Name6);

		this.onCreate(sqLiteDatabase);
	}

	//公共实现函数
	public void insert(ContentValues contentValues){

	}

	public void delete(String where, String[] args){

	}

	public void update(ContentValues contentValues, String where, String[] args){

	}

	public Cursor select(String where, String[] args){
		return null;
	}

	public String last_insert_rowid(){
		return null;
	}
}

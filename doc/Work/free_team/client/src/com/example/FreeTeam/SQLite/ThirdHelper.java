package com.example.FreeTeam.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-25
 * Time: 下午8:12
 * To change this template use File | Settings | File Templates.
 */
public class ThirdHelper extends SQLiteDatabaseHelper {
	private final static String TB_Name = "third";

	public ThirdHelper(Context context) {
		super(context);
	}

	@Override
	public void insert(ContentValues contentValues) {
		//To change body of implemented methods use File | Settings | File Templates.
		this.getWritableDatabase().insert(TB_Name, null, contentValues);
	}

	@Override
	public void delete(String where, String[] args) {
		//To change body of implemented methods use File | Settings | File Templates.
		this.getWritableDatabase().delete(TB_Name, where, args);
	}

	@Override
	public void update(ContentValues contentValues, String where, String[] args) {
		//To change body of implemented methods use File | Settings | File Templates.
		this.getWritableDatabase().update(TB_Name, contentValues, where, args);
	}

	@Override
	public Cursor select(String where, String[] args) {
		return this.getReadableDatabase().query(TB_Name, null, where, args, null, null, null);
	}

	@Override
	public String last_insert_rowid() {
		Cursor cursor = this.getReadableDatabase().rawQuery("select last_insert_rowid() from "+TB_Name, null);
		if(cursor.moveToFirst()){
			return cursor.getString(0);
		}
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}

	@Override
	public String toString(){
		return TB_Name;
	}
}

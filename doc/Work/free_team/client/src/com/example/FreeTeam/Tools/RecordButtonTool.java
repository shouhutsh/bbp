package com.example.FreeTeam.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import com.example.FreeTeam.SQLite.SQLiteDatabaseHelper;
import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-25
 * Time: 下午9:03
 * To change this template use File | Settings | File Templates.
 */
public class RecordButtonTool extends SQLiteTool {
	final static String RB_ID= "rb_id";
	final static String U_ID = "u_id";
	final static String F_ID = "f_id";
	final static String S_ID = "s_id";
	final static String T_ID = "t_id";
	final static String STATE= "state";

	String rb_id= null;
	String u_id = null;
	String f_id = null;
	String s_id = null;
	String t_id = null;
	String state= null;

	public RecordButtonTool() {
		empty();
	}

	public RecordButtonTool(Cursor cursor){
		rb_id = cursor.getString(cursor.getColumnIndexOrThrow(RB_ID));
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		f_id = cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		s_id = cursor.getString(cursor.getColumnIndexOrThrow(S_ID));
		t_id = cursor.getString(cursor.getColumnIndexOrThrow(T_ID));
		state= cursor.getString(cursor.getColumnIndexOrThrow(STATE));
	}


	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(SQLiteDatabaseHelper helper, String newState){
		ContentValues contentValues = new ContentValues();
		contentValues.put(U_ID, u_id);
		contentValues.put(F_ID, f_id);
		contentValues.put(S_ID, s_id);
		contentValues.put(T_ID, t_id);

		if(state != null && state.equals("0")){			//0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(helper);

			//这里是需要新添加的数据
			state = newState;									//1 代表S，意思是待同步
			contentValues.put(STATE, state);

			helper.insert(contentValues);
			rb_id = helper.last_insert_rowid();
		}else if(state != null && state.equals("1")){	//1 代表S，意思是待同步，则这时直接修改即可
			state = newState;
			contentValues.put(STATE, state);
			//判断这是不是一个新的数据
			if(rb_id != null){
				helper.update(contentValues, RB_ID+"=?", new String[]{rb_id});
			}else{
				helper.insert(contentValues);
				rb_id = helper.last_insert_rowid();
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(SQLiteDatabaseHelper helper){
		if(state != null && state.equals("0")){
			ContentValues contentValues = new ContentValues();
			contentValues.put(STATE, "2");				//2 代表D，意思是需要被删除
			helper.update(contentValues, RB_ID+"=?", new String[]{rb_id});
		}else if(state != null && state.equals("1")){
			if(rb_id != null)
				helper.delete(RB_ID+"=?", new String[]{rb_id});
		}

//		empty();
	}

	@Override
	public void empty(){
		rb_id= null;
		u_id = null;
		f_id = null;
		s_id = null;
		t_id = null;
		state= null;
	}

	@Override
	public SQLiteTool valueOf(JSONArray jsonArray) throws JSONException {
		rb_id= jsonArray.getString(0);
		u_id = jsonArray.getString(1);
		f_id = jsonArray.getString(2);
		s_id = jsonArray.getString(3);
		t_id = jsonArray.getString(4);

		state = "0";
		return this;
	}

	@Override
	public String getRb_id() {
		return rb_id;
	}

	@Override
	public void setRb_id(String rb_id) {
		this.rb_id = rb_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getF_id() {
		return f_id;
	}

	@Override
	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	@Override
	public String getS_id() {
		return s_id;
	}

	@Override
	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	@Override
	public String getT_id() {
		return t_id;
	}

	@Override
	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}
}

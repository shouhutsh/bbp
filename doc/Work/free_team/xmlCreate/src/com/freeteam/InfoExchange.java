package com.freeteam;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午11:48
 * To change this template use File | Settings | File Templates.
 */
public class InfoExchange {
	private HttpServletRequest request;
	private HttpServletResponse response;

	private Connection connection;
	private Statement st;

	InfoExchange(HttpServletRequest request, HttpServletResponse response){
		this.request = request;
		this.response = response;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=UTF-8", "root", "123456");
			st = connection.createStatement();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		} catch (SQLException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

	}

	public void acceptXML(){
		String msg;

		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");

			DataInputStream in = new DataInputStream(new BufferedInputStream(request.getInputStream()));

			String s = in.readUTF();

			JSONObject jsonInsert = new JSONObject(s);
			JSONObject jsonEvent  = jsonInsert.getJSONObject("insert");
			JSONArray  jsonArray = jsonEvent.getJSONArray("event");

			for(int i=0; i < jsonArray.length(); ){

				msg = String.format("insert into event(name,time, duty)values('%s',%s, true)", jsonArray.getString(i++), jsonArray.getLong(i++));
System.out.println(msg);
				st.execute(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	public void sendXML(){
		try{
			OutputStream out = response.getOutputStream();

			JSONObject jsonInsert = new JSONObject();
			JSONObject jsonEvent = new JSONObject();
			JSONArray  jsonArray  = new JSONArray();

			ResultSet resultSet = st.executeQuery("select * from event where duty=1 and time=0");
			while(resultSet.next()){
				jsonArray.put(resultSet.getString(2)).put(resultSet.getString(3));
			}
			jsonEvent.put("event", jsonArray);
			jsonInsert.put("insert", jsonEvent);

			out.write(jsonInsert.toString().getBytes());
			out.flush();
			out.close();

			//OK, 我假定当执行到这里时，服务器向客户端传送的消息都能正常接收
			//因此将其清除，即把duty字段置为false
			st.execute("update event set duty=0 where time=0");
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}
}

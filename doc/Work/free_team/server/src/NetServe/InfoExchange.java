package NetServe;

import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Tools.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午11:48
 * To change this template use File | Settings | File Templates.
 */
public class InfoExchange {
	private HttpServletRequest request;
	private HttpServletResponse response;

	private Connection connection;
	private Statement st;

	private JSONObject info = new JSONObject();
	private JSONObject insert = new JSONObject();
	private JSONObject delete = new JSONObject();

	final static String[] TABLES = {"first", "second", "third",        //注意，由于表间有外键依赖，因此
									"record", "record_button"};        //表的顺序不能更换

	public InfoExchange(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/server?useUnicode=true&characterEncoding=UTF-8", "root", "123456");
			st = connection.createStatement();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		} catch (SQLException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

	}

	public void sync() {
		String msg;

		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");

			DataInputStream in = new DataInputStream(new BufferedInputStream(request.getInputStream()));
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(response.getOutputStream()));

			UserTool.setU_id(null);

			parse(in.readUTF());
			in.close();

			if(UserTool.getU_id() != null){
				sendInfo(UserTool.getU_id());
				out.writeUTF(info.toString());
			}
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	private void sendInfo(String u_id) throws JSONException, SQLException {
			createInsert(u_id);
			createDelete(u_id);
			info.put("insert", insert);
			info.put("delete", delete);
	}

	private void parse(String msg) throws JSONException, SQLException {
		info = new JSONObject(msg);

		if(info.has("user")){
			JSONArray user = info.getJSONArray("user");
			ResultSet resultSet = st.executeQuery("select * from user where u_id="+user.getString(0));
			resultSet.next();
			if(resultSet.getString("name").equals(user.getString(1)) && resultSet.getString("password").equals(user.getString(2))){
				UserTool.setU_id(resultSet.getString("u_id"));
				if (info.has("delete")) {            //这里执行删除操作
					deleteOperate();
				}
				if (info.has("insert")) {                //这里是插入操作
					insertOperate();
				}
			}
		}
	}

	private void insertOperate() throws JSONException, SQLException {
		String key = "insert";
		MySQLTool mySQLTool = null;
		JSONArray tableArray = null;

		insert = info.getJSONObject(key);

		/**
		 * 注意：因为目前只是测试阶段，因此并未执行操作
		 * 		只是将值取出而已
		 * 		由于外键约束，因此必须按顺序来，因此应
		 * 		保证传输的数据里必须有以下表字段
		 */
		if (insert.has("first")) {        //这里对应要执行操作的表
			tableArray = insert.getJSONArray("first");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new FirstTool().valueOf(tableArray.getJSONArray(i));
				st.execute("insert into first(f_id,u_id,name,state,type)values("
						+mySQLTool.getF_id()+","+mySQLTool.getU_id()+",'"+mySQLTool.getName()+"',"
						+mySQLTool.getState()+","+mySQLTool.getType()+")");
			}
		}
		if (insert.has("second")) {
			tableArray = insert.getJSONArray("second");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new SecondTool().valueOf(tableArray.getJSONArray(i));
				st.execute("insert into second(s_id,f_id,u_id,name,state)values("
						+mySQLTool.getS_id()+","+mySQLTool.getF_id()+","+mySQLTool.getU_id()+",'"
						+mySQLTool.getName()+"',"+mySQLTool.getState()+")");
			}
		}
		if (insert.has("third")) {
			tableArray = insert.getJSONArray("third");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new ThirdTool().valueOf(tableArray.getJSONArray(i));
				st.execute("insert into third(t_id,s_id,u_id,name,state)values("
						+mySQLTool.getT_id()+","+mySQLTool.getS_id()+","+mySQLTool.getU_id()+",'"
						+mySQLTool.getName()+"',"+mySQLTool.getState()+")");
			}
		}
		if (insert.has("record")) {
			tableArray = insert.getJSONArray("record");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new RecordTool().valueOf(tableArray.getJSONArray(i));
				st.execute("insert into record(r_id,u_id,start_time,end_time,f_id,s_id,t_id,amount,unit,remark,state)values("
						+mySQLTool.getR_id()+","+mySQLTool.getU_id()+","+mySQLTool.getStart()+","+mySQLTool.getEnd()+","
						+mySQLTool.getF_id()+","+mySQLTool.getS_id()+","+mySQLTool.getT_id()+","+mySQLTool.getAmount()+","
						+mySQLTool.getUnit()+",'"+mySQLTool.getRemark()+"',"+mySQLTool.getState()+")");
			}
		}
		if (insert.has("record_button")) {
			tableArray = insert.getJSONArray("record_button");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new RecordButtonTool().valueOf(tableArray.getJSONArray(i));
				st.execute("insert into record_button(rb_id,u_id,f_id,s_id,t_id,state)values("
						+mySQLTool.getRb_id()+","+mySQLTool.getU_id()+","+mySQLTool.getF_id()+","
						+mySQLTool.getS_id()+","+mySQLTool.getT_id()+","+mySQLTool.getState()+")");
			}
		}
	}

	private void deleteOperate() throws JSONException, SQLException {
		String key = "delete";
		MySQLTool mySQLTool = null;
		JSONArray tableArray = null;

		insert = info.getJSONObject(key);
		/**
		 * 注意：因为目前只是测试阶段，因此并未执行操作
		 * 		只是将值取出而已
		 * 		由于有外键依赖，因此不能随意删除类目，
		 * 		以后再写级联删除
		 */
		if (delete.has("first")) {        //这里对应要执行操作的表
			tableArray = insert.getJSONArray("first");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new FirstTool().valueOf(tableArray.getJSONArray(i));
//				st.execute("delete from first where f_id="+mySQLTool.getF_id()+" and u_id="+mySQLTool.getU_id());
			}
		}
		if (delete.has("second")) {
			tableArray = insert.getJSONArray("second");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new SecondTool().valueOf(tableArray.getJSONArray(i));
//				st.execute("delete from second where f_id="+mySQLTool.getF_id()+" and u_id="+mySQLTool.getU_id());
			}
		}
		if (delete.has("third")) {
			tableArray = insert.getJSONArray("third");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new ThirdTool().valueOf(tableArray.getJSONArray(i));
//				st.execute("delete from third where f_id="+mySQLTool.getF_id()+" and u_id="+mySQLTool.getU_id());
			}
		}
		if (delete.has("record")) {
			tableArray = insert.getJSONArray("record");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new RecordTool().valueOf(tableArray.getJSONArray(i));
				st.execute("delete from record where f_id="+mySQLTool.getF_id()+" and u_id="+mySQLTool.getU_id());
			}
		}
		if (delete.has("record_button")) {
			tableArray = insert.getJSONArray("record_button");
			for (int i = 0; i < tableArray.length(); ++i) {
				mySQLTool = new RecordButtonTool().valueOf(tableArray.getJSONArray(i));
				st.execute("delete from record_button where f_id="+mySQLTool.getF_id()+" and u_id="+mySQLTool.getU_id());
			}
		}
	}

	private void createInsert(String u_id) throws SQLException, JSONException {
		for (String table : TABLES) {
			insert.put(table, create(table, u_id, "1", "0"));
		}
	}

	private void createDelete(String u_id) throws SQLException, JSONException {
		for (String table : TABLES) {
			delete.put(table, create(table, u_id, "2", "2"));
		}
	}

	private JSONArray create(String table, String u_id, String oldState, String newState) throws SQLException, JSONException {
		MySQLTool mySQLTool = null;
		JSONArray jsonArray = new JSONArray();
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("select * from " + table + " where state=" + oldState + " and u_id=" + u_id);

		if (table.equals("first")) {
			while (resultSet.next()) {
				mySQLTool = new FirstTool(resultSet);
				jsonArray.put(mySQLTool.toJSONArray());
				mySQLTool.save(st, newState);
			}
		} else if (table.equals("second")) {
			while (resultSet.next()) {
				mySQLTool = new SecondTool(resultSet);
				jsonArray.put(mySQLTool.toJSONArray());
				mySQLTool.save(st, newState);
			}
		} else if (table.equals("third")) {
			while (resultSet.next()) {
				mySQLTool = new ThirdTool(resultSet);
				jsonArray.put(mySQLTool.toJSONArray());
				mySQLTool.save(st, newState);
			}
		} else if (table.equals("record")) {
			while (resultSet.next()) {
				mySQLTool = new RecordTool(resultSet);
				jsonArray.put(mySQLTool.toJSONArray());
				mySQLTool.save(st, newState);
			}
		} else if (table.equals("record_button")) {
			while (resultSet.next()) {
				mySQLTool = new RecordButtonTool(resultSet);
				jsonArray.put(mySQLTool.toJSONArray());
				mySQLTool.save(st, newState);
			}
		}

		statement.close();
		resultSet.close();

		return jsonArray;
	}

}

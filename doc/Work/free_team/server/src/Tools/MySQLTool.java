package Tools;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午1:08
 * To change this template use File | Settings | File Templates.
 */
public abstract class MySQLTool {
	public void save(Statement statement, String newState) throws SQLException {
		//To change body of created methods use File | Settings | File Templates.
	}

	public void remove(Statement statement) throws SQLException {
		//To change body of created methods use File | Settings | File Templates.
	}

	public void empty(){

	}

	public MySQLTool valueOf(JSONArray jsonArray) throws JSONException {
		return null;
	}

	public JSONArray toJSONArray(){
		return null;
	}

	//其他共有函数
	public String getU_id() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setU_id(String u_id) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getName() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setName(String name) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getState() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setState(String state) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getType() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setType(String type) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getF_id() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setF_id(String f_id) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getS_id() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setS_id(String s_id) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getT_id() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setT_id(String t_id) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getR_id() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setR_id(String r_id) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getStart() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setStart(String start) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getEnd() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setEnd(String end) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getAmount() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setAmount(String amount) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getUnit() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setUnit(String unit) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getRemark() {
		return null;  //To change body of created methods use File | Settings | File Templates.
	}

	public void setRemark(String remark) {
		//To change body of created methods use File | Settings | File Templates.
	}

	public String getRb_id() {
		return null;
	}

	public void setRb_id(String rb_id) {

	}

	protected String last_insert_id(Statement statement) throws SQLException {
		ResultSet resultSet = statement.executeQuery("select last_insert_id()");
		if(resultSet.next())
			return resultSet.getString(1);
		return null;
	}
/*
	public void save(SQLiteDatabaseHelper helper);

	public void remove(SQLiteDatabaseHelper helper);

	public String getF_id();

	public void setF_id(String f_id);

	public String getU_id();

	public void setU_id(String u_id);

	public String getName();

	public void setName(String name);

	public String getState();

	public void setState(String state);

	public String getType();

	public void setType(String type);
*/
}

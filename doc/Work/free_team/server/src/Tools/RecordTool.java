package Tools;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Statement;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-25
 * Time: 下午8:39
 * To change this template use File | Settings | File Templates.
 */
public class RecordTool extends MySQLTool {
	final static String R_ID = "r_id";
	final static String U_ID = "u_id";
	final static String START= "start_time";
	final static String END  = "end_time";
	final static String F_ID = "f_id";
	final static String S_ID = "s_id";
	final static String T_ID = "t_id";
	final static String AMOUNT="amount";
	final static String UNIT = "unit";
	final static String REMARK="remark";
	final static String STATE= "state";

	String r_id = null;
	String u_id = null;
	String start= null;
	String end  = null;
	String f_id = null;
	String s_id = null;
	String t_id = null;
	String amount=null;
	String unit = null;
	String remark=null;
	String state= null;

	public RecordTool() {
		empty();
	}

	public RecordTool(ResultSet resultSet) throws SQLException {
		r_id = resultSet.getString(R_ID);
		u_id = resultSet.getString(U_ID);
		start= resultSet.getString(START);
		end  = resultSet.getString(END);
		f_id = resultSet.getString(F_ID);
		s_id = resultSet.getString(S_ID);
		t_id = resultSet.getString(T_ID);
		amount=resultSet.getString(AMOUNT);
		unit = resultSet.getString(UNIT);
		remark=resultSet.getString(REMARK);
		state= resultSet.getString(STATE);
	}

	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(Statement statement, String newState) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {            //0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(statement);
			//这里是需要新添加的数据
			state = newState;                                    //1 代表S，意思是待同步

			sql = "insert into record(u_id,start_time,end_time,f_id,s_id,t_id,amount,unit,remark,state)values" +
					"("+u_id+","+start+","+end+","+f_id+","+s_id+","+t_id+","+amount+","+unit+",'"+remark+"',"+state+")";
			statement.execute(sql);

			r_id = last_insert_id(statement);
		} else if (state != null && state.equals("1")) {    //1 代表S，意思是待同步，则这时直接修改即可
			state = newState;
			//判断这是不是一个新的数据
			if (r_id != null) {
				sql = "update record set u_id="+u_id+",start_time='"+start+"',end_time='"+end+"',f_id="+f_id+
						",s_id="+s_id+",t_id="+t_id+",amount="+amount+",unit="+unit+",remark='"+remark+"',state="+state+" where f_id="+f_id;
				statement.execute(sql);
			} else {
				sql = "insert into record(u_id,start_time,end_time,f_id,s_id,t_id,amount,unit,remark,state)values" +
						"("+u_id+","+start+","+end+","+f_id+","+s_id+","+t_id+","+amount+","+unit+",'"+remark+"',"+state+")";
				statement.execute(sql);

				r_id = last_insert_id(statement);
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(Statement statement) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {
			sql = "update record set state=2 where r_id="+r_id+" and u_id="+u_id;
			statement.executeUpdate(sql);
		} else if (state != null && state.equals("1")) {
			if (r_id != null){
				sql = "delete from record where r_id="+r_id;
				statement.execute(sql);
			}
		}
	}

	@Override
	public void empty(){
		r_id = null;
		u_id = null;
		start= null;
		end  = null;
		f_id = null;
		s_id = null;
		t_id = null;
		amount=null;
		unit = null;
		remark=null;
		state= null;
	}

	@Override
	public MySQLTool valueOf(JSONArray jsonArray) throws JSONException {
		r_id = jsonArray.getString(0);
		u_id = jsonArray.getString(1);
		start= jsonArray.getString(2);
		end  = jsonArray.getString(3);
		f_id = jsonArray.getString(4);
		s_id = jsonArray.getString(5);
		t_id = jsonArray.getString(6);
		amount=jsonArray.getString(7);
		unit = jsonArray.getString(8);
		remark=jsonArray.getString(9);

		state = "0";
		return this;
	}

	@Override
	public JSONArray toJSONArray(){
		return new JSONArray().put(r_id).put(u_id).put(start).put(end).put(f_id)
				.put(s_id).put(t_id).put(amount).put(unit).put(remark);
	}

	@Override
	public String getR_id() {
		return r_id;
	}

	@Override
	public void setR_id(String r_id) {
		this.r_id = r_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getStart() {
		return start;
	}

	@Override
	public void setStart(String start) {
		this.start = start;
	}

	@Override
	public String getEnd() {
		return end;
	}

	@Override
	public void setEnd(String end) {
		this.end = end;
	}

	@Override
	public String getF_id() {
		return f_id;
	}

	@Override
	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	@Override
	public String getS_id() {
		return s_id;
	}

	@Override
	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	@Override
	public String getT_id() {
		return t_id;
	}

	@Override
	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	@Override
	public String getAmount() {
		return amount;
	}

	@Override
	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String getUnit() {
		return unit;
	}

	@Override
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Override
	public String getRemark() {
		return remark;
	}

	@Override
	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}
}

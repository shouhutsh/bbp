package Tools;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Statement;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 上午11:13
 * To change this template use File | Settings | File Templates.
 */
public class FirstTool extends MySQLTool {
	final static String F_ID = "f_id";
	final static String U_ID = "u_id";
	final static String NAME = "name";
	final static String STATE = "state";
	final static String TYPE = "type";

	String f_id = null;
	String u_id = null;
	String name = null;
	String state = null;
	String type = null;

	public FirstTool() {
		empty();
	}

	public FirstTool(ResultSet resultSet) throws SQLException {
		f_id = resultSet.getString(F_ID);
		u_id = resultSet.getString(U_ID);
		name = resultSet.getString(NAME);
		state= resultSet.getString(STATE);
		type = resultSet.getString(TYPE);
	}

	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(Statement statement, String newState) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {            //0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(statement);
			//这里是需要新添加的数据
			state = newState;                                    //1 代表S，意思是待同步

			sql = "insert into first(u_id,name,state,type)values("+u_id+",'"+name+"',"+state+","+type+")";
			statement.executeQuery(sql);

			f_id = last_insert_id(statement);
		} else if (state != null && state.equals("1")) {    //1 代表S，意思是待同步，则这时直接修改即可
			state = newState;
			//判断这是不是一个新的数据
			if (f_id != null) {
				sql = "update first set u_id="+u_id+",name='"+name+"',state=0,type="+type+",state="+state+" where f_id="+f_id+" and u_id="+u_id;
				statement.execute(sql);
			} else {
				sql = "insert into first(u_id,name,state,type)values("+u_id+",'"+name+"',"+state+","+type+")";
				statement.execute(sql);

				f_id = last_insert_id(statement);
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(Statement statement) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {
			sql = "update first set state=2 where f_id="+f_id;
			statement.executeUpdate(sql);
		} else if (state != null && state.equals("1")) {
			if (f_id != null){
				sql = "delete from first where f_id="+f_id;
				statement.execute(sql);
			}
		}

//		empty();
	}

	@Override
	public void empty(){
		f_id = null;
		u_id = null;
		name = null;
		state = null;
		type = null;
	}


	@Override
	public MySQLTool valueOf(JSONArray jsonArray) throws JSONException {
		//注意这里基本都是硬编码，因此必须保证一致
		f_id = jsonArray.getString(0);
		u_id = jsonArray.getString(1);
		name = jsonArray.getString(2);
		type = jsonArray.getString(3);

		state = "0";		//设置为N，不需操作
		return this;
	}

	@Override
	public JSONArray toJSONArray(){
		return new JSONArray().put(f_id).put(u_id).put(name).put(type);
	}


	@Override
	public String getF_id() {
		return f_id;
	}

	@Override
	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String getType() {
		return type;
	}

	@Override
	public void setType(String type) {
		this.type = type;
	}
}

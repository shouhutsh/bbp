package Tools;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Statement;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午3:14
 * To change this template use File | Settings | File Templates.
 */
public final class UserTool{
	final static String U_ID = "u_id";
	final static String NAME = "name";
	final static String PASSWORD="password";

	static String u_id = null;
	static String name = null;
	static String password = null;

	public UserTool() {
		empty();
	}

	//不需要从数据库中读出，因为需要用户输入
	/*
	public UserTool(Cursor cursor){
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(NAME));
		password = cursor.getString(cursor.getColumnIndexOrThrow(PASSWORD));
	}*/


	static public void save(Statement statement) throws SQLException {
		//To change body of implemented methods use File | Settings | File Templates.
		String sql = "insert into user(name,password) values("+name+","+password+")";
		statement.execute(sql);
	}

	static public void update(Statement statement) throws SQLException {
		//To change body of implemented methods use File | Settings | File Templates.
		String sql = "update user set name='"+name+"',password='"+password+"' where u_id="+u_id;
		statement.execute(sql);

		ResultSet resultSet = statement.executeQuery("select last_insert_id()");
		if(resultSet.next())
			u_id = resultSet.getString(0);
//		empty();
	}

	static public void empty(){
		u_id = null;
		name = null;
		password = null;
	}

	static public String getU_id() {
		return u_id;
	}

	static public void setU_id(String u_id) {
		UserTool.u_id = u_id;
	}

	static public String getPassword() {
		return password;
	}

	static public void setPassword(String password) {
		UserTool.password = password;
	}

	static public String getName() {
		return name;
	}

	static public void setName(String name) {
		UserTool.name = name;
	}
}

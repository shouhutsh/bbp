package Tools;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-25
 * Time: 下午7:46
 * To change this template use File | Settings | File Templates.
 */
public class SecondTool extends MySQLTool {
	final static String S_ID="s_id";
	final static String F_ID="f_id";
	final static String U_ID="u_id";
	final static String NAME="name";
	final static String STATE="state";

	String s_id = null;
	String f_id = null;
	String u_id = null;
	String name = null;
	String state= null;

	public SecondTool() {
		empty();
	}

	public SecondTool(ResultSet resultSet) throws SQLException {
		s_id = resultSet.getString(S_ID);
		f_id = resultSet.getString(F_ID);
		u_id = resultSet.getString(U_ID);
		name = resultSet.getString(NAME);
		state =resultSet.getString(STATE);
	}

	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(Statement statement, String newState) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {            //0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(statement);
			//这里是需要新添加的数据
			state = newState;                                    //1 代表S，意思是待同步

			sql = "insert into second(f_id,u_id,name,state)values("+f_id+","+u_id+",'"+name+"',"+state+")";
			statement.execute(sql);

			s_id = last_insert_id(statement);
		} else if (state != null && state.equals("1")) {    //1 代表S，意思是待同步，则这时直接修改即可
			state = newState;
			//判断这是不是一个新的数据
			if (s_id != null) {
				sql = "update second set f_id="+f_id+",u_id="+u_id+",name='"+name+"',state="+newState+" where s_id="+s_id+" and u_id="+u_id;
				statement.execute(sql);
			} else {
				sql = "insert into second(f_id,u_id,name,state)values("+f_id+","+u_id+",'"+name+"',"+state+")";
				statement.execute(sql);

				s_id = last_insert_id(statement);
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(Statement statement) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {
			sql = "update second set state=2 where s_id="+s_id;
			statement.executeUpdate(sql);
		} else if (state != null && state.equals("1")) {
			if (s_id != null){
				sql = "delete from second where s_id="+s_id;
				statement.execute(sql);
			}
		}

//		empty();
	}


	@Override
	public void empty(){
		s_id = null;
		f_id = null;
		u_id = null;
		name = null;
		state= null;
	}


	@Override
	public MySQLTool valueOf(JSONArray jsonArray) throws JSONException {
		s_id = jsonArray.getString(0);
		f_id = jsonArray.getString(1);
		u_id = jsonArray.getString(2);
		name = jsonArray.getString(3);

		state = "0";
		return this;
	}

	@Override
	public JSONArray toJSONArray(){
		return new JSONArray().put(s_id).put(f_id).put(u_id).put(name);
	}

	@Override
	public String getS_id() {
		return s_id;
	}

	@Override
	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	@Override
	public String getF_id() {
		return f_id;
	}

	@Override
	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}
}

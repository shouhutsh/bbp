package Tools;

import org.json.JSONArray;
import org.json.JSONException;

import java.sql.Statement;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-25
 * Time: 下午7:46
 * To change this template use File | Settings | File Templates.
 */
public class ThirdTool extends MySQLTool {
	final static String T_ID="t_id";
	final static String S_ID="s_id";
	final static String U_ID="u_id";
	final static String NAME="name";
	final static String STATE="state";

	String t_id = null;
	String s_id = null;
	String u_id = null;
	String name = null;
	String state= null;

	public ThirdTool() {
		empty();
	}

	public ThirdTool(ResultSet resultSet) throws SQLException {
		t_id = resultSet.getString(T_ID);
		s_id = resultSet.getString(S_ID);
		u_id = resultSet.getString(U_ID);
		name = resultSet.getString(NAME);
		state =resultSet.getString(STATE);
	}

	//这里可能需要更改..
	//假定这里只存数据库中没有的数据
	@Override
	public void save(Statement statement, String newState) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {            //0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
			this.remove(statement);
			//这里是需要新添加的数据
			state = newState;                                    //1 代表S，意思是待同步

			sql = "insert into third(s_id,u_id,name,state)values("+s_id+","+u_id+",'"+name+"',"+state+")";
			statement.execute(sql);

			t_id = last_insert_id(statement);
		} else if (state != null && state.equals("1")) {    //1 代表S，意思是待同步，则这时直接修改即可
			state = newState;
			//判断这是不是一个新的数据
			if (t_id != null) {
				sql = "update third set s_id="+s_id+",u_id="+u_id+",name='"+name+"',state="+newState+" where t_id="+t_id+" and u_id="+u_id;
				statement.execute(sql);
			} else {
				sql = "insert into third(s_id,u_id,name,state)values("+s_id+","+u_id+",'"+name+"',"+state+")";
				statement.execute(sql);

				t_id = last_insert_id(statement);
			}
		}
	}

	//这里也可能需要修改。。
	//假定这里只删除数据库中有的数据
	//并且以ID 为索引
	@Override
	public void remove(Statement statement) throws SQLException {
		String sql;

		if (state != null && state.equals("0")) {
			sql = "update third set state=2 where t_id="+t_id;
			statement.executeUpdate(sql);
		} else if (state != null && state.equals("1")) {
			if (t_id != null){
				sql = "delete from third where t_id="+t_id;
				statement.execute(sql);
			}
		}

//		empty();
	}


	@Override
	public void empty(){
		t_id = null;
		s_id = null;
		u_id = null;
		name = null;
		state= null;
	}


	@Override
	public MySQLTool valueOf(JSONArray jsonArray) throws JSONException {
		t_id = jsonArray.getString(0);
		s_id = jsonArray.getString(1);
		u_id = jsonArray.getString(2);
		name = jsonArray.getString(3);

		state = "0";
		return this;
	}

	@Override
	public JSONArray toJSONArray(){
		return new JSONArray().put(t_id).put(s_id).put(u_id).put(name);
	}

	@Override
	public String getT_id() {
		return t_id;
	}
	@Override
	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	@Override
	public String getS_id() {
		return s_id;
	}

	@Override
	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	@Override
	public String getU_id() {
		return u_id;
	}

	@Override
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getState() {
		return state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}
}

package com.example.xmlParse.ButtonListener;

import android.content.Context;
import android.view.View;
import com.example.xmlParse.SQLite.DBHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午11:15
 * To change this template use File | Settings | File Templates.
 */
public class CommonButtonListener implements View.OnClickListener {
	private String msg;
	Context context;

	public CommonButtonListener(String msg, Context context){
		this.msg = msg;
		this.context = context;
	}
	@Override
	public void onClick(View v){
		DBHelper helper = new DBHelper(context);
		helper.insert(msg, System.currentTimeMillis());
	}
}

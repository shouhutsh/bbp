package com.example.xmlParse;

import android.app.Activity;
import android.app.TabActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.Toast;
import com.example.xmlParse.ButtonListener.CommonButtonListener;
import com.example.xmlParse.ButtonListener.SyncButtonListener;


public class MyActivity extends TabActivity implements TabHost.OnTabChangeListener{

	private TabHost.TabSpec ts1, ts2;
	private TabHost tabHost;
	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		tabHost = this.getTabHost();
		LayoutInflater.from(this).inflate(R.layout.main, tabHost.getTabContentView());

		{
			Button b1 = (Button) findViewById(R.id.b1);
			Button b2 = (Button) findViewById(R.id.b2);

			Button sync = (Button) findViewById(R.id.sync);

			b1.setOnClickListener(new CommonButtonListener(b1.getText().toString(), MyActivity.this));
			b2.setOnClickListener(new CommonButtonListener(b2.getText().toString(), MyActivity.this));
			sync.setOnClickListener(new SyncButtonListener(MyActivity.this));
		}

		ts1 = tabHost.newTabSpec("tabOne");
		ts1.setIndicator(getString(R.string.health));
		ts1.setContent(R.id.health);

		ts2 = tabHost.newTabSpec("tabTwo");
		ts2.setIndicator(getString(R.string.lift));
		ts2.setContent(R.id.life);

		tabHost.addTab(ts1);
		tabHost.addTab(ts2);
		tabHost.setOnTabChangedListener(this);
	}

	@Override
	public void onTabChanged(String s) {
		//To change body of implemented methods use File | Settings | File Templates.
		if(s.equals("tabOne")){
			Toast.makeText(this, "test one", Toast.LENGTH_LONG).show();
		}else if(s.equals("tabTwo")){
			Toast.makeText(this, "test two", Toast.LENGTH_SHORT).show();
		}
	}
}

/*
public class MyActivity extends Activity {


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Button b1 = (Button) findViewById(R.id.b1);
		Button b2 = (Button) findViewById(R.id.b2);

		Button sync = (Button) findViewById(R.id.sync);

		b1.setOnClickListener(new CommonButtonListener(b1.getText().toString(), MyActivity.this));
		b2.setOnClickListener(new CommonButtonListener(b2.getText().toString(), MyActivity.this));
		sync.setOnClickListener(new SyncButtonListener(MyActivity.this));
	}
}
*/
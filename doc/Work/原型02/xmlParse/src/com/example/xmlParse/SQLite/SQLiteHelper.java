package com.example.xmlParse.SQLite;

import android.database.Cursor;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-15
 * Time: 上午11:38
 * To change this template use File | Settings | File Templates.
 */
public interface SQLiteHelper {

	public long insert(String name, long time);

	public void delete(int id);

	public void update();

	public Cursor select();

}

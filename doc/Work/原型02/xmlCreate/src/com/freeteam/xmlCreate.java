package com.freeteam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: 上午11:47
 * To change this template use File | Settings | File Templates.
 */
public class xmlCreate extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getHeader("user-agent").toLowerCase().indexOf("android") != -1){
			InfoExchange ie = new InfoExchange(request, response);
			ie.acceptXML();
		}
		else{
			PrintWriter out = response.getWriter();
			out.println("PC operation");
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getHeader("user-agent").toLowerCase().indexOf("android") != -1){
			InfoExchange ie = new InfoExchange(request, response);
			ie.sendXML();
		}
		else{
			PrintWriter out = response.getWriter();
			out.println("PC operation");
		}
	}
}

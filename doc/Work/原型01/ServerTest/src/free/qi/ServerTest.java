package free.qi;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ServerTest {
	public static void main(String[] args) {
		try {
			System.out.println("Server is runnig.");
			ServerSocket server = new ServerSocket(12345);

			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "l920915");
			Statement st = con.createStatement();

			while (true) {
				System.out.println("Server is listening...");
				Socket client = server.accept();

				String msg, sql;
				DataInputStream dis = new DataInputStream(client.getInputStream());
				DataOutputStream dos = new DataOutputStream(client.getOutputStream());


				ResultSet rst = st.executeQuery("select * from user");
				rst.next();
				long last_sync = rst.getLong("last_sync");
				System.out.println("LAST_SYNC:" + last_sync);
				dos.writeLong(System.currentTimeMillis());
				dos.writeLong(last_sync);
				/**
				 * XXX ACCEPT
				 */
				sql = String.format("select * from %s where %s=%d", "event", "time", 0);
				System.out.println(sql);
				ResultSet rset = st.executeQuery(sql);
				while (rset.next()) {
					msg = rset.getString("name");
					msg += ",";
					msg += rset.getString("time");
					System.out.println("NEW MSG:" + msg);
					dos.writeUTF(msg);
				}
				st.executeUpdate("delete from event where time=0");
				dos.writeUTF("EOF");
				/**
				 * dos end
				 */

				try {
					while (!"EOF".equals((msg = dis.readUTF()))) {
						System.out.println(msg);
						String[] s = msg.split(",");
						msg = String.format("insert into event values(null,'%s',%s)", s[0], s[1]);
						System.out.println(msg);
						st.executeUpdate(msg);
					}
				} catch (EOFException e) {
					;        //Do nothing
				}
				/**
				 * dis end
				 */

				sql = String.format("update user set last_sync=%d where id=1", System.currentTimeMillis());
				System.out.println(sql);

				st.executeUpdate(sql);

				dos.close();
				dis.close();
				client.close();
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}
}


/************************************************/
/*
 * Test1 OK

System.out.println("Server is listening...");
Socket client = server.accept();

try{
	DataInputStream dis = new DataInputStream(client.getInputStream());
	DataOutputStream dos = new DataOutputStream(client.getOutputStream());

	dos.writeUTF("FROM SERVER");
	String str = dis.readUTF();
	System.out.println("READ-->"+str);

	String[] s = str.split(",");
	str = String.format("insert into event values(null,'%s',%s)", s[0],s[1]);
	st.execute(str);

	client.close();
	dos.close();
	dis.close();
}catch(Exception e){
	e.printStackTrace();
}
*/
/** Test2 OK
 * 
BufferedWriter out = new BufferedWriter(new OutputStreamWriter(  
            client.getOutputStream())); 

out.write("FROM SERVER");
System.out.println("SEND-->"+"FROM SERVER");

out.close();
client.close();
*/

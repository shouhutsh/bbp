package free.qi;

import java.io.*;
import java.net.*;

public class Client {
	public static void main(String[] args){
		try{
			InetAddress localIP = InetAddress.getLocalHost();

				Socket socket = new Socket(localIP.getHostAddress(), 12345);
				BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
				
				PrintWriter    sout = new PrintWriter(socket.getOutputStream());
				BufferedReader sin  = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				String readline;
				readline = sysin.readLine();
				while(!readline.equals("quit"))
				{
					sout.print(readline);
					sout.flush();
					
					System.out.println("Client send " + readline);
					System.out.println();
					readline = sysin.readLine();
				}
				sysin.close();
				sout.close();
				sin.close();
		}catch(Exception e){
			System.err.println(e);
		}
	}
}

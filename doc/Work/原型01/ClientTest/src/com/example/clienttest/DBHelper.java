package com.example.clienttest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private final static String DB_NAME = "client.db";
	private final static int    DB_VERSION = 1;
	private final static String TB_NAME = "event";
	private final static String TB_PAR_ID = "id";
	private final static String TB_PAR_NAME = "name";
	private final static String TB_PAR_TIME = "time";
	private final static String SPLITE = ",";
	
	public DBHelper(Context context){
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db){
		String sql = String.format(
				"create table %s" +
				"(%s int auto_increment primary key," +
				"%s varchar(20)," +
				"%s bigint not null)",
				TB_NAME, TB_PAR_ID, TB_PAR_NAME, TB_PAR_TIME);
		db.execSQL(sql);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		String sql = String.format("drop table if exits %s", TB_NAME);
		db.execSQL(sql);
		onCreate(db);
	}

	public Cursor selectForSync(long last_sync)
	{
		///Here will be change.
		SQLiteDatabase db=this.getReadableDatabase();
		String sql = String.format("select * from %s where %s>%s", TB_NAME, TB_PAR_TIME, last_sync);
		
		System.out.println(sql);
		Cursor cursor=db.rawQuery(sql, null);
		return cursor;
	}
	
	public long insert(String msg)
	{
		String pars[] = msg.split(SPLITE);
		SQLiteDatabase db=this.getWritableDatabase();
		ContentValues cv=new ContentValues(); 
		cv.put(TB_PAR_NAME, pars[0]);
		cv.put(TB_PAR_TIME, pars[1]);		//XXX
		long row=db.insert(TB_NAME, null, cv);
		return row;
	}
/*
	public void delete(int id)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String where=FIELD_ID+"=?";
		String[] whereValue={Integer.toString(id)};
		db.delete(TABLE_NAME, where, whereValue);
	}
	public void update(int id,String Title)
	{
		SQLiteDatabase db=this.getWritableDatabase();
		String where=FIELD_ID+"=?";
		String[] whereValue={Integer.toString(id)};
		ContentValues cv=new ContentValues(); 
		cv.put(FIELD_TITLE, Title);
		db.update(TABLE_NAME, cv, where, whereValue);
	}
*/

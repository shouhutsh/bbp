package com.example.clienttest;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button b1 = (Button)findViewById(R.id.button1);
		Button b2 = (Button)findViewById(R.id.button2);
		Button b3 = (Button)findViewById(R.id.button3);
		Button b4 = (Button)findViewById(R.id.button4);
		Button b5 = (Button)findViewById(R.id.button5);
		Button b6 = (Button)findViewById(R.id.button6);
		Button sync_button = (Button)findViewById(R.id.sync_button);
		
		b1.setOnClickListener(new ButtonListener(b1.getText().toString()));
		b2.setOnClickListener(new ButtonListener(b2.getText().toString()));
		b3.setOnClickListener(new ButtonListener(b3.getText().toString()));
		b4.setOnClickListener(new ButtonListener(b4.getText().toString()));
		b5.setOnClickListener(new ButtonListener(b5.getText().toString()));
		b6.setOnClickListener(new ButtonListener(b6.getText().toString()));

		sync_button.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				sync();
			}
		});
	}

	private class ButtonListener implements OnClickListener{
		private String msg;
		private String split = ",";
		
		ButtonListener(){
			msg = null;
		}
		ButtonListener(String msg){
			this.msg = msg;
		}
		@Override
		public void onClick(View v){
			msg += split+System.currentTimeMillis();
			DBHelper db = new DBHelper(MainActivity.this);
			System.out.println("INSERT:"+msg);
			db.insert(msg);
		}
			
	}
	
	
	private void sync(){
		String msg;
		List<String> msgs = new ArrayList<String>(50);
		DBHelper db = new DBHelper(MainActivity.this);

		try {
			Socket socket = new Socket("10.0.2.2", 12345);
			DataInputStream dis = new DataInputStream(
					socket.getInputStream());
			DataOutputStream dos = new DataOutputStream(
					socket.getOutputStream());
		
			long server_time = dis.readLong();
			long last_sync = dis.readLong();
			long client_time = System.currentTimeMillis();
			/**
			 * XXX ACCEPT
			 */
			try{
				while(!"EOF".equals((msg = dis.readUTF()))){
					System.out.println(msg);
					db.insert(msg);
				}
			}catch(EOFException e){
				;		//Do nothing
			}
			/**
			 * dis end
			 */
			
			
			Cursor cursor = db.selectForSync(last_sync-(server_time-client_time));
			cursor.moveToFirst();
			while(!cursor.isAfterLast()){
				msg = cursor.getString(1);
				msg += ",";
				msg += cursor.getString(2);
				System.out.println("READ AND SEND :"+msg);
				msgs.add(msg);
				cursor.moveToNext();
			}
			for(String m : msgs)
				dos.writeUTF(m);
			dos.writeUTF("EOF");
			/**
			 * dos end
			 */

			dis.close();
			dos.close();
			socket.close();
				
		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

	}
}


/***************************************************************/

//Socket socket;
//	try {
//	socket = new Socket("10.0.2.2", 12345);
//	/*
//	 * Test1 OK
//	DataInputStream dis = new DataInputStream(
//			socket.getInputStream());
//	DataOutputStream dos = new DataOutputStream(
//			socket.getOutputStream());
//
//	dos.writeUTF(msg);
//	System.out.println("READ-->"+dis.readUTF());
//	
//	socket.close();
//	dos.close();
//	dis.close();
//	*/
//	
//	/**
//	 * Test2 OK
//	
//	BufferedReader in  = new BufferedReader(new InputStreamReader(
//					socket.getInputStream()));
//
//	System.out.println(in.readLine());
//	
//	in.close();
//	socket.close();
//	*/
//
//	InputStreamReader in = new InputStreamReader(socket.getInputStream());
//	char[] str = new char[20];
//	in.read(str);
//	System.out.println(str);
//	
//	in.close();
//	socket.close();
//} catch (UnknownHostException e) {
//	// TODO 自动生成的 catch 块
//	e.printStackTrace();
//} catch (IOException e) {
//	// TODO 自动生成的 catch 块
//	e.printStackTrace();
//}
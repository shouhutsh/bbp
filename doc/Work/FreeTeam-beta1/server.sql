-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.6.14 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win32
-- HeidiSQL 版本:                  8.1.0.4669
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 导出 server 的数据库结构
CREATE DATABASE IF NOT EXISTS `server` /*!40100 DEFAULT CHARACTER SET utf32 COLLATE utf32_unicode_ci */;
USE `server`;


-- 导出  表 server.first 结构
CREATE TABLE IF NOT EXISTS `first` (
  `f_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1' COMMENT 'N->不需做操作，S->需要同步，D->需要删除，默认为S',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT 'I->短暂性动作，S->持续性动作，默认为I',
  PRIMARY KEY (`f_id`,`u_id`),
  KEY `FK_first_user` (`u_id`),
  CONSTRAINT `FK_first_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 server.record 结构
CREATE TABLE IF NOT EXISTS `record` (
  `r_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) DEFAULT NULL,
  `f_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL,
  `amount` float DEFAULT NULL,
  `unit` int(11) DEFAULT '0' COMMENT 'O->其他，ML->毫升，G->克，''N''->个数',
  `remark` varchar(200) DEFAULT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`r_id`,`u_id`),
  KEY `FK_record_user` (`u_id`),
  KEY `FK_record_first` (`f_id`),
  KEY `FK_record_second` (`s_id`),
  KEY `FK_record_third` (`t_id`),
  CONSTRAINT `FK_record_first` FOREIGN KEY (`f_id`) REFERENCES `first` (`f_id`),
  CONSTRAINT `FK_record_second` FOREIGN KEY (`s_id`) REFERENCES `second` (`s_id`),
  CONSTRAINT `FK_record_third` FOREIGN KEY (`t_id`) REFERENCES `third` (`t_id`),
  CONSTRAINT `FK_record_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 server.record_button 结构
CREATE TABLE IF NOT EXISTS `record_button` (
  `rb_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) NOT NULL,
  `f_id` int(11) NOT NULL,
  `s_id` int(11) NOT NULL,
  `t_id` int(11) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`rb_id`,`u_id`),
  KEY `FK_record_button_user` (`u_id`),
  KEY `FK_record_button_first` (`f_id`),
  KEY `FK_record_button_second` (`s_id`),
  KEY `FK_record_button_third` (`t_id`),
  CONSTRAINT `FK_record_button_first` FOREIGN KEY (`f_id`) REFERENCES `first` (`f_id`),
  CONSTRAINT `FK_record_button_second` FOREIGN KEY (`s_id`) REFERENCES `second` (`s_id`),
  CONSTRAINT `FK_record_button_third` FOREIGN KEY (`t_id`) REFERENCES `third` (`t_id`),
  CONSTRAINT `FK_record_button_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 server.second 结构
CREATE TABLE IF NOT EXISTS `second` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `f_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`s_id`,`u_id`),
  KEY `FK_second_first` (`f_id`),
  KEY `FK_second_user` (`u_id`),
  CONSTRAINT `FK_second_first` FOREIGN KEY (`f_id`) REFERENCES `first` (`f_id`),
  CONSTRAINT `FK_second_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 server.third 结构
CREATE TABLE IF NOT EXISTS `third` (
  `t_id` int(11) NOT NULL AUTO_INCREMENT,
  `s_id` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `state` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`t_id`,`u_id`),
  KEY `FK_third_second` (`s_id`),
  KEY `FK_third_user` (`u_id`),
  CONSTRAINT `FK_third_second` FOREIGN KEY (`s_id`) REFERENCES `second` (`s_id`),
  CONSTRAINT `FK_third_user` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。


-- 导出  表 server.user 结构
CREATE TABLE IF NOT EXISTS `user` (
  `u_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

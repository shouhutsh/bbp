package edu.zut.cs.bbp.server.util.factory.impl;

import edu.zut.cs.bbp.server.dao.impl.RecordButtonDao;
import edu.zut.cs.bbp.server.domain.impl.RecordButton;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Qi_2 on 14-4-21.
 */
@SuppressWarnings("ALL")
@Component
public class RecordButtonTable extends Table {

	@Autowired
	RecordButtonDao rbd;

	@Override
	public void insertEntities(JSONObject insert) {
		RecordButton rb;
		JSONArray rbs = null;

		try{
			rbs = insert.getJSONArray("record_button");
			for(int i = 0; i < rbs.length(); ++i){
				rb = (RecordButton) new RecordButton().valueOf(rbs.getJSONArray(i));
//				rbd.save(rb, "0");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntities(JSONObject delete) {

	}

    @Override
    public JSONArray insertCreater(User user) {
        return null;
    }

    @Override
    public JSONArray deleteCreater(User user) {
        return null;
    }

}

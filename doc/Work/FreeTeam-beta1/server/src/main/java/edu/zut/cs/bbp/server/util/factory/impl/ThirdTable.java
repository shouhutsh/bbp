package edu.zut.cs.bbp.server.util.factory.impl;

import edu.zut.cs.bbp.server.dao.impl.ThirdDao;
import edu.zut.cs.bbp.server.domain.impl.Third;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Qi_2 on 14-4-21.
 */
@SuppressWarnings("ALL")
@Component
public class ThirdTable extends Table {

	@Autowired
	ThirdDao td;

	@Override
	public void insertEntities(JSONObject insert) {
		Third t;
		JSONArray ts = null;
		try{
			ts = insert.getJSONArray("third");
			for(int i = 0; i < ts.length(); ++i){
				t = (Third) new Third().valueOf(ts.getJSONArray(i));
//				td.save(t, "0");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntities(JSONObject delete) {

	}

    @Override
    public JSONArray insertCreater(User user) {
        return null;
    }

    @Override
    public JSONArray deleteCreater(User user) {
        return null;
    }

}

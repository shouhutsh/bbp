package edu.zut.cs.bbp.server.domain.impl;

import edu.zut.cs.bbp.server.domain.Base;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
public class Second extends Base {
    private String s_id;
    private String f_id;
    private String u_id;
    private String name;
    private String state;

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getF_id() {
        return f_id;
    }

    public void setF_id(String f_id) {
        this.f_id = f_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean isHaveSync() {
        return (state != null && state .equals("0"));
    }

    @Override
    public boolean isNeedSync() {
        return (state !=null && state.equals("1"));
    }

    @Override
    public boolean isNewData() {
        return false;
    }

    @Override
    public Base valueOf(Map map) {
        s_id = String.valueOf(map.get("s_id"));
        f_id = String.valueOf(map.get("f_id"));
        u_id = String.valueOf(map.get("u_id"));
        name = String.valueOf(map.get("name"));
        state= String.valueOf(map.get("state"));
        return this;
    }

    @Override
    public JSONArray toJSONArray() {
        return new JSONArray().put(s_id).put(f_id).put(u_id).put(name).put(state);
    }

    @Override
    public Second valueOf(JSONArray jsonArray) throws JSONException {
        s_id = jsonArray.getString(0);
        f_id = jsonArray.getString(1);
        u_id = jsonArray.getString(2);
        name = jsonArray.getString(3);
        state = "0";
        return this;
    }
}

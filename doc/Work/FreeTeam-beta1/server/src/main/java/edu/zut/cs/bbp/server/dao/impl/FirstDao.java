package edu.zut.cs.bbp.server.dao.impl;

import edu.zut.cs.bbp.server.dao.BaseDao;
import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.First;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by Qi_2 on 14-4-16.
 */
@Transactional
@Repository("firstDao")
public class FirstDao extends BaseDao {
	private static final String INSERT = "INSERT INTO first(u_id,name,state,type)VALUES(?,?,?,?)";
	private static final String DELETE = "DELETE FROM first WHERE f_id=?";
	private static final String UPDATE = "UPDATE first set name=?,state=?,type=? WHERE f_id=?";
	private static final String SELECT_ONE = "SELECT * FROM first WHERE f_id=?";

	private static final String INSERT_FULL = "INSERT INTO first(f_id,u_id,name,state,type)VALUES(?,?,?,?,?)";
	private static final String SELECT_ALL = "SELECT * FROM first WHERE u_id=? AND state=?";

	@Override
	public void save(Base b, String newState){
		First f = (First) b;

		if(f.isHaveSync()){
			// FIXME 这里只是为了与之前项目一致，后期重构之后需要进行修改
			delete(f);
			f.setState(newState);

			insertNewEntity(f);
		}else if(f.isNeedSync()){
			f.setState(newState);
			if(f.isNewData()){
				jdbcTemplate.update(UPDATE, f.getName(),
						f.getState(), f.getType(), f.getF_id());
			}else{
				insertNewEntity(f);
			}
		}
	}

	@Override
	public First getOne(String id){
		Map map = jdbcTemplate.queryForMap(SELECT_ONE, id);
		return new First().valueOf(map);
	}

	public SqlRowSet getAll(String u_id, String state){
		return jdbcTemplate.queryForRowSet(SELECT_ALL, u_id, state);
	}

	@Override
	public void delete(Base b){
		First f = (First) b;
		if(f.isHaveSync()){
			// FIXME 2表示为待删除，只是为了与前期工程相同
			f.setState("2");
			jdbcTemplate.update(UPDATE, f.getName(),
					f.getState(), f.getType(), f.getF_id());
		}else{
			jdbcTemplate.update(DELETE, new String[]{f.getF_id()});
		}
	}

	public void simpleSave(First f){
		jdbcTemplate.update(INSERT_FULL, f.getF_id(),f.getU_id(),
						f.getName(), f.getState(), f.getType());
	}

	private void insertNewEntity(First first){
		final First f = first;
		//插入新数据,为了安全返回自增ID所以写的比较复杂
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement ps = con.prepareStatement(INSERT);
				ps.setString(1, f.getU_id());
				ps.setString(2, f.getName());
				ps.setString(3, f.getState());
				ps.setString(4, f.getType());
				return ps;
			}
		}, keyHolder);
		f.setF_id(keyHolder.getKey().toString());
	}
}

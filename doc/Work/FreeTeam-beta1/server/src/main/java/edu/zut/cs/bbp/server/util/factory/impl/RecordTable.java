package edu.zut.cs.bbp.server.util.factory.impl;

import edu.zut.cs.bbp.server.dao.impl.RecordDao;
import edu.zut.cs.bbp.server.domain.impl.Record;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Qi_2 on 14-4-21.
 */
@SuppressWarnings("ALL")
@Component
public class RecordTable extends Table {

	@Autowired
	RecordDao rd;

	@Override
	public void insertEntities(JSONObject insert) {
		Record r;
		JSONArray rs = null;
		try{
			rs = insert.getJSONArray("record");
			for(int i = 0; i < rs.length(); ++i){
				r = (Record) new Record().valueOf(rs.getJSONArray(i));
//				rd.save(r, "0");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntities(JSONObject delete) {

	}

    @Override
    public JSONArray insertCreater(User user) {
        return null;
    }

    @Override
    public JSONArray deleteCreater(User user) {
        return null;
    }

}

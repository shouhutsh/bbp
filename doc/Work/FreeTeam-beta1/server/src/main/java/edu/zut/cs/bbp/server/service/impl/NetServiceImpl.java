package edu.zut.cs.bbp.server.service.impl;

import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.service.BaseService;
import edu.zut.cs.bbp.server.service.DataHelper;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 下午2:50
 * To change this template use File | Settings | File Templates.
 */
@Service("netService")
public class NetServiceImpl implements BaseService {

	private HttpServletRequest request;
	private HttpServletResponse response;

    @Autowired
    @Qualifier("jsonDataHelper")
	private JsonDataImpl helper;

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    @Override
	public void sync(){
		try{
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");

			DataInputStream in = new DataInputStream(new BufferedInputStream(request.getInputStream()));
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(response.getOutputStream()));

			if(helper.parse(in.readUTF())){
				out.writeUTF(helper.pack());
			}
			in.close();

			out.flush();
			out.close();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
}

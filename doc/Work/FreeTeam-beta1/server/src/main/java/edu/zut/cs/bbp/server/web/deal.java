package edu.zut.cs.bbp.server.web;

import edu.zut.cs.bbp.server.service.BaseService;
import edu.zut.cs.bbp.server.service.impl.NetServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-2
 * Time: ����11:47
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class deal extends HttpServlet {

    @Autowired
    NetServiceImpl netService;

    @RequestMapping("/hello")
    public String sayHello(){
        return "hello";
    }

	@RequestMapping("/deal")
	public void deal(HttpServletRequest request, HttpServletResponse response) throws IOException {
		if (request.getHeader("user-agent").toLowerCase().indexOf("android") != -1) {
			netService.setRequest(request);
            netService.setResponse(response);
			netService.sync();
		} else {
			PrintWriter out = response.getWriter();
			out.println("PC operation");
		}
	}
}

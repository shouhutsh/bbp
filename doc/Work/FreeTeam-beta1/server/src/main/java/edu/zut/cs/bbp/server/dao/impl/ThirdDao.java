package edu.zut.cs.bbp.server.dao.impl;

import edu.zut.cs.bbp.server.dao.BaseDao;
import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.Third;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
@Transactional
@Repository("thirdDao")
public class ThirdDao extends BaseDao {
    private static final String INSERT = "INSERT INTO third(s_id,u_id,name,state) values(?,?,?,?)";
    private static final String DELETE = "DELETE FROM third where t_id =?";
    private static final String UPDATE = "UPDATE third set s_id=?,name=?,state=? WHERE t_id=?";
    private static final String SELECT_ONE = "SELECT * FROM third WHERE t_id =?";

    private static final String INSERT_FULL = "INSERT INTO third(t_id,s_id,u_id,name,state) values(?,?,?,?)";
    private static final String SELECT_ALL = "SELECT * FROM third WHERE u_id=? AND state=?";

    @Override
    public void save(Base base, String newState) {
        Third third = (Third)base;
        if (third.isHaveSync()){
            delete(third);
            third.setState(newState);
            insertNewEntity(third);
        }else if (third.isNeedSync()){
          third.setState(newState);
            if (third.isNewData())
            {
                jdbcTemplate.update(UPDATE,third.getS_id(),third.getU_id(),third.getState(),third.getT_id());
            }else {
                insertNewEntity(third);
            }
        }

    }

    public void simpleSave(Third third){
        jdbcTemplate.update(INSERT_FULL, third.getT_id(),third.getS_id(),third.getU_id(),third.getState(),third.getT_id());
    }

    private void insertNewEntity(Third third) {
        final Third third1 = third;
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(INSERT);
                ps.setString(1,third1.getS_id());
                ps.setString(2,third1.getU_id());
                ps.setString(3,third1.getName());
                ps.setString(4,third1.getState());
                return ps;
            }
        },keyHolder);
        third.setT_id(keyHolder.getKey().toString());

    }

    @Override
    public Third getOne(String id) {
        Map map = jdbcTemplate.queryForMap(SELECT_ONE,id);
        return new Third().valueOf(map);
    }
    public SqlRowSet getAll(String u_id, String state){
        return jdbcTemplate.queryForRowSet(SELECT_ALL, u_id, state);
    }
    @Override
    public void delete(Base base) {
        Third third = (Third)base;
        if(third.isHaveSync()){
            third.setState("2");
            jdbcTemplate.update(UPDATE,third.getS_id(),third.getName(),third.getState(),third.getT_id());
        }else {
            jdbcTemplate.update(DELETE,new String[]{third.getT_id()});
        }
    }
}

package edu.zut.cs.bbp.server.service.impl;

import edu.zut.cs.bbp.server.dao.impl.UserDao;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.service.DataHelper;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 下午3:15
 * To change this template use File | Settings | File Templates.
 */
@Component("jsonDataHelper")
public class JsonDataImpl implements DataHelper {
	// FIXME 这里可以根据parse解析出来的用户信息来pack，因此需要user的domain
	private User user;

	@Autowired
	UserDao userDao;

	@Override
	public boolean parse(String msg) throws JSONException {
		JSONObject info = new JSONObject(msg);

		if(info.has("user")){
			return data(info);
		}else if(info.has("login")){
			return login(info);
		}else if(info.has("register")){
			return register(info);
		}
		return false;
	}

	@Override
	public String pack() throws JSONException {
		assert (user != null);

        JSONObject info = new JSONObject();
        JSONObject insert = new JSONObject();
        JSONObject delete = new JSONObject();

        for(int i = 0; i < Table.TABLES.length; ++i){
            insert.put(Table.TABLES[i], Table.getTable(i).insertCreater(user));
        }
        for(int i = 0; i < Table.TABLES.length; ++i){
            delete.put(Table.TABLES[i], Table.getTable(i).deleteCreater(user));
        }

        info.put("insert", insert).put("delete", delete).put("user", user.toJSONArray());
        return info.toString();
	}


	private boolean data(JSONObject info) throws JSONException {
		JSONObject insert = info.getJSONObject("insert");
		JSONObject delete = info.getJSONObject("delete");

		UserDao userDao = new UserDao();
		User client = (User) new User().valueOf(info.getJSONArray("user"));
		User server = (User) userDao.getOne(client.getU_id());

		if(server.getPassword().equals(client.getPassword())){
			for(int i = 0; i < Table.TABLES.length; ++i){
				if(insert.has(Table.TABLES[i]))
					Table.getTable(i).insertEntities(insert);
			}
			for(int i = 0; i < Table.TABLES.length; ++i){
				if(insert.has(Table.TABLES[i]))
					Table.getTable(i).deleteEntities(delete);
			}
			user = server;
			return true;
		}
		return false;
	}
	private boolean login(JSONObject info) throws JSONException {
		// FIXME 之前的json格式里login应该只有u_id和password
		User client = new User();
		client.setU_id(info.getJSONArray("login").getString(0));
		client.setPassword(info.getJSONArray("login").getString(1));

		User server = (User) userDao.getOne(client.getU_id());

		if(server.getPassword().equals(client.getPassword())){
			user = server;
			return true;
		}
		return false;
	}
	private boolean register(JSONObject info) throws JSONException {
		// FIXME 这里应只有name和password
		User client = new User();
		client.setName(info.getJSONArray("register").getString(0));
		client.setPassword(info.getJSONArray("register").getString(1));

		userDao.save(client, null);
		user = client;
		return true;
	}
}

package edu.zut.cs.bbp.server.domain.impl;

import edu.zut.cs.bbp.server.domain.Base;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
public class User extends Base {
    private String u_id;
    private String name;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean isHaveSync() {
        return false;
    }

    @Override
    public boolean isNeedSync() {
        return false;
    }

    @Override
    public boolean isNewData() {
        return false;
    }

    @Override
    public Base valueOf(Map map) {
        return null;
    }

    @Override
    public JSONArray toJSONArray() {
        return null;
    }

    @Override
    public Base valueOf(JSONArray jsonArray) throws JSONException {
        return null;
    }
}

package edu.zut.cs.bbp.server.domain.impl;

import edu.zut.cs.bbp.server.domain.Base;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
public class Third extends Base {
    private String t_id;
    private String s_id;
    private String u_id;
    private String name;
    private String state;

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean isHaveSync() {
        return (state !=null && state.equals("0"));
    }

    @Override
    public boolean isNeedSync() {
        return (state !=null && state.equals("1"));
    }

    @Override
    public boolean isNewData() {
        return false;
    }

    @Override
    public Third valueOf(Map map) {
        t_id = String.valueOf(map.get("t_id"));
        s_id = String.valueOf(map.get("s_id"));
        u_id = String.valueOf(map.get("u_id"));
        name = String.valueOf(map.get("name"));
        state= String.valueOf(map.get("state"));
        return this;
    }

    @Override
    public JSONArray toJSONArray() {
        return new JSONArray().put(t_id).put(s_id).put(u_id).put(name).put(state);
    }

    @Override
    public Third valueOf(JSONArray jsonArray) throws JSONException {
        t_id = jsonArray.getString(0);
        s_id = jsonArray.getString(1);
        u_id = jsonArray.getString(2);
        name = jsonArray.getString(3);
        state = "0";
        return this;
    }
}

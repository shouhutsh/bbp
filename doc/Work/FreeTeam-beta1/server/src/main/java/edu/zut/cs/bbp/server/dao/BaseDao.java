package edu.zut.cs.bbp.server.dao;

import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by Qi_2 on 14-4-16.
 */
public abstract class BaseDao {
	@Autowired
	protected JdbcTemplate jdbcTemplate;

	abstract public void save(Base b, String newState);

	abstract public Base getOne(String id);

	abstract public void delete(Base b);

    // TODO 需要实现这两个抽象函数
//    abstract public void simpleSave(Base b);
//
//    abstract public SqlRowSet getAll(User u);
}

package edu.zut.cs.bbp.server.domain.impl;

import edu.zut.cs.bbp.server.domain.Base;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 上午11:13
 * To change this template use File | Settings | File Templates.
 */
public class First extends Base {
    public static final String F_ID = "f_id";
    public static final String U_ID = "u_id";
    public static final String NAME = "name";
    public static final String STATE= "state";
    public static final String TYPE = "type";

    String f_id;
	String u_id;
	String name;
	String state;
	String type;

	@Override
	public boolean isHaveSync(){
		return (state!=null && state.equals("0"));
	}

	@Override
	public boolean isNeedSync(){
		return (state != null && state.equals("1"));
	}

	@Override
	public boolean isNewData() {
		return false;
	}

	@Override
	public First valueOf(Map map){
		f_id = String.valueOf(map.get("f_id"));
		u_id = String.valueOf(map.get("u_id"));
		name = String.valueOf(map.get("name"));
		state= String.valueOf(map.get("state"));
		type = String.valueOf(map.get("type"));

		return this;
	}

	@Override
	public JSONArray toJSONArray(){
		return new JSONArray().put(f_id).put(u_id).put(name).put(type);
	}

	@Override
	public First valueOf(JSONArray jsonArray) throws JSONException {
		//注意这里基本都是硬编码，因此必须保证一致
		f_id = jsonArray.getString(0);
		u_id = jsonArray.getString(1);
		name = jsonArray.getString(2);
		type = jsonArray.getString(3);

		state = "0";		//设置为N，不需操作
		return this;
	}

    public First valueOf(SqlRowSet rowSet){
        f_id = rowSet.getString(F_ID);
        u_id = rowSet.getString(U_ID);
        name = rowSet.getString(NAME);
        state= rowSet.getString(STATE);
        type = rowSet.getString(TYPE);
        return this;
    }

	public String getF_id() {
		return f_id;
	}

	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}

package edu.zut.cs.bbp.server.dao.impl;

import edu.zut.cs.bbp.server.dao.BaseDao;
import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * Created by Qi_2 on 14-4-21.
 */
@Transactional
@Repository("userDao")
public class UserDao extends BaseDao {
	private static final String SELECT_ONE = "SELECT * FROM user WHERE u_id=?";

	@Override
	public void save(Base b, String newState) {

	}

	@Override
	public User getOne(String id) {
		Map map = jdbcTemplate.queryForMap(SELECT_ONE, id);
		return (User) new User().valueOf(map);
	}

	@Override
	public void delete(Base b) {

	}

	public void simpleSave(User user){

	}
}

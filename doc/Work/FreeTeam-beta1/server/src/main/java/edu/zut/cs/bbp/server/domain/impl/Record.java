package edu.zut.cs.bbp.server.domain.impl;

import edu.zut.cs.bbp.server.domain.Base;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
public class Record extends Base {
    private String r_id;
    private String u_id;
    private String start_time;
    private String end_time;
    private String f_id;
    private String s_id;
    private String t_id;
    private String amount;
    private String unit;
    private String remark;
    private String state;

    public String getR_id() {
        return r_id;
    }

    public void setR_id(String r_id) {
        this.r_id = r_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getF_id() {
        return f_id;
    }

    public void setF_id(String f_id) {
        this.f_id = f_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean isHaveSync() {
        return (state != null && state.equals("0"));
    }

    @Override
    public boolean isNeedSync() {
        return (state != null && state.equals("1"));
    }

    @Override
    public boolean isNewData() {
        return false;
    }

    @Override
    public Record valueOf(Map map) {
        r_id = String.valueOf(map.get("r_id"));
        u_id = String.valueOf(map.get("u_id"));
        start_time = String.valueOf(map.get("start_time"));
        end_time = String.valueOf(map.get("end_time"));
        f_id = String.valueOf(map.get("f_id"));
        s_id = String.valueOf(map.get("s_id"));
        t_id = String.valueOf(map.get("t_id"));
        amount = String.valueOf(map.get("amount"));
        unit = String.valueOf(map.get("unit"));
        remark = String.valueOf(map.get("remark"));
        state = String.valueOf(map.get("state"));
        return this;
    }

    @Override
    public JSONArray toJSONArray() {
        return new JSONArray().put(r_id).put(u_id).put(start_time).put(end_time).put(f_id).put(s_id).put(t_id).put(amount).put(unit).put(remark).put(state);
    }

    @Override
    public Record valueOf(JSONArray jsonArray) throws JSONException {
        r_id = jsonArray.getString(0);
        u_id = jsonArray.getString(1);
        start_time= jsonArray.getString(2);
        end_time  = jsonArray.getString(3);
        f_id = jsonArray.getString(4);
        s_id = jsonArray.getString(5);
        t_id = jsonArray.getString(6);
        amount=jsonArray.getString(7);
        unit = jsonArray.getString(8);
        remark=jsonArray.getString(9);
        state= "0";
        return this;
    }
}

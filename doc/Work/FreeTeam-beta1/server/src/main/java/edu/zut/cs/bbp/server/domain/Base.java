package edu.zut.cs.bbp.server.domain;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午1:08
 * To change this template use File | Settings | File Templates.
 */
public abstract class Base {

	abstract public boolean isHaveSync();

	abstract public boolean isNeedSync();

	abstract public boolean isNewData();

	abstract public Base valueOf(Map map);

	abstract public JSONArray toJSONArray();

	abstract public Base valueOf(JSONArray jsonArray) throws JSONException;
}

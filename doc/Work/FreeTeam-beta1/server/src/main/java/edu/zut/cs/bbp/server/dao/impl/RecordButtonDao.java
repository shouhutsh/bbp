package edu.zut.cs.bbp.server.dao.impl;

import edu.zut.cs.bbp.server.dao.BaseDao;
import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.RecordButton;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
@Transactional
@Repository("recordButtonDao")
public class RecordButtonDao extends BaseDao {
    private static final String INSERT = "INSERT INTO record_button(u_id,f_id,s_id,t_id,state)VALUES(?,?,?,?,?)";
    private static final String DELETE = "DELETE FROM record_button WHERE rb_id=?";
    private static final String UPDATE = "UPDATE record_button set f_id=?,s_id=?,t_id=/,state=? WHERE rb_id=?";
    private static final String SELECT_ONE = "SELECT * FROM record_button WHERE rb_id=?";

    private static final String INSERT_FULL = "INSERT INTO record_button(rb_id,u_id,f_id,s_id,t_id,state)VALUES(?,?,?,?,?)";
    private static final String SELECT_ALL = "SELECT * FROM record_button WHERE  u_id=? AND state=?";

    @Override
    public void save(Base base, String newState) {
        RecordButton recordButton = (RecordButton) base;
        if (recordButton.isHaveSync()) {
            delete(recordButton);
            recordButton.setState(newState);
            insertNewEntity(recordButton);
        } else if (recordButton.isNeedSync()) {
            recordButton.setState(newState);
            if (recordButton.isNewData()) {
                jdbcTemplate.update(UPDATE,recordButton.getF_id(),recordButton.getS_id(),recordButton.getT_id(),recordButton.getState(),recordButton.getRb_id());

            }else {
                insertNewEntity(recordButton);
            }
        }

    }

    private void insertNewEntity(RecordButton recordButton) {
        final RecordButton rb = recordButton;
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(INSERT);
                ps.setString(1,rb.getU_id());
                ps.setString(2,rb.getF_id());
                ps.setString(3,rb.getState());
                ps.setString(4,rb.getT_id());
                ps.setString(5,rb.getState());
                return ps;
            }
        },keyHolder);
        rb.setRb_id(keyHolder.getKey().toString());
    }
    public void simpleSave(RecordButton recordButton){
        jdbcTemplate.update(INSERT_FULL, recordButton.getF_id(),recordButton.getS_id(),recordButton.getT_id(),recordButton.getState(),recordButton.getRb_id());
    }

    @Override
    public RecordButton getOne(String id) {
        Map map = jdbcTemplate.queryForMap(SELECT_ONE,id);
        return new RecordButton().valueOf(map) ;
    }
    public SqlRowSet getAll(String u_id, String state){
        return jdbcTemplate.queryForRowSet(SELECT_ALL, u_id, state);
    }
    @Override
    public void delete(Base base) {
        RecordButton recordButton = (RecordButton)base;
        if (recordButton.isHaveSync()){
            recordButton.setState("2");
            jdbcTemplate.update(UPDATE,recordButton.getF_id(),recordButton.getS_id(),recordButton.getT_id(),recordButton.getState(),recordButton.getRb_id());
        }else {
            jdbcTemplate.update(DELETE,new String[]{recordButton.getRb_id()});
        }
    }
}

package edu.zut.cs.bbp.server.util.factory.impl;

import edu.zut.cs.bbp.server.dao.impl.UserDao;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Qi_2 on 14-4-20.
 */
@SuppressWarnings("ALL")
@Component
public class UserTable extends Table {

	@Autowired
	UserDao ud;

	@Override
	public void insertEntities(JSONObject insert) {
		User u;
		JSONArray us = null;
		try{
			us = insert.getJSONArray("user");
			for(int i = 0; i < us.length(); ++i){
				u = (User) new User().valueOf(us.getJSONArray(i));
//				ud.save(u, "0");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntities(JSONObject delete) {

	}

    @Override
    public JSONArray insertCreater(User user) {
        return null;
    }

    @Override
    public JSONArray deleteCreater(User user) {
        return null;
    }

}

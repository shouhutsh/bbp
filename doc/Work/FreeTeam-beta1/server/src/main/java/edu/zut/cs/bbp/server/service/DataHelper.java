package edu.zut.cs.bbp.server.service;

import edu.zut.cs.bbp.server.domain.impl.User;
import org.json.JSONException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 下午3:13
 * To change this template use File | Settings | File Templates.
 */
public interface DataHelper {
	public boolean parse(String msg) throws JSONException;
	public String pack() throws JSONException;
}

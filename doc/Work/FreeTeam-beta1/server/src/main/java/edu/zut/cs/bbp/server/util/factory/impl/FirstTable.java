package edu.zut.cs.bbp.server.util.factory.impl;

import edu.zut.cs.bbp.server.dao.impl.FirstDao;
import edu.zut.cs.bbp.server.domain.impl.First;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

/**
 * Created by Qi_2 on 14-4-20.
 */
@SuppressWarnings("ALL")
@Component
public class FirstTable extends Table {

	@Autowired
	FirstDao fd;

	@Override
	public void insertEntities(JSONObject insert){
		First f;
		JSONArray fs = null;
		// FIXME 这里的表名为硬编码
		try {
			fs = insert.getJSONArray("first");
			for(int i = 0; i < fs.length(); ++i){
				f = new First().valueOf(fs.getJSONArray(i));
				fd.simpleSave(f);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntities(JSONObject delete) {
        First f;
        JSONArray fs = null;
        // FIXME 这里的表名为硬编码
        try {
            fs = delete.getJSONArray("first");
            for(int i = 0; i < fs.length(); ++i){
                f = new First().valueOf(fs.getJSONArray(i));
                fd.delete(f);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
	}

    @Override
    public JSONArray insertCreater(User user) {
        JSONArray fs = new JSONArray();

        SqlRowSet rowSet = fd.getAll(user.getU_id(), "1");
        while(rowSet.next()){
            First f = new First().valueOf(rowSet);
            fs.put(f.toJSONArray());
        }

        return fs;
    }

    @Override
    public JSONArray deleteCreater(User user) {
        JSONArray fs = new JSONArray();

        SqlRowSet rowSet = fd.getAll(user.getU_id(), "2");
        while(rowSet.next()){
            First f = new First().valueOf(rowSet);
            fs.put(f.toJSONArray());
        }

        return fs;
    }
}

package edu.zut.cs.bbp.server.dao.impl;

import edu.zut.cs.bbp.server.dao.BaseDao;
import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.First;
import edu.zut.cs.bbp.server.domain.impl.Record;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
@Transactional
@Repository("recordDao")
public class RecordDao extends BaseDao {
    private static final String INSERT = "INSERT INTO record(u_id,start_time,end_time,f_id,s_id,t_id,amount,unit,remark,state) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static final String DELETE = "DELETE FROM record WHERE r_id=?";
    private static final String UPDATE = "UPDATE record set start_time=?,end_time=?,f_id=?,s_id=?,t_id=?,amount=?,unit=?,remark=?,state=?WHERE r_id=?";
    private static final String SELECT_ONE = "SELECT * FROM record WHERE r_id=?";

    private static final String INSERT_FULL = "INSERT INTO record(r_id,u_id,start_time,end_time,f_id,s_id,t_id,amount,unit,remark,state) VALUES(?,?,?,?,?,?,?,?,?,?)";
    private static final String SELECT_ALL = "SELECT * FROM record WHERE u_id=? AND state=?";

    @Override
    public void save(Base base, String newState) {
        Record record = (Record)base;
        if (record.isHaveSync()){
            delete(record);
            record.setState(newState);
            insertNewEntity(record);
        }else if (record.isNeedSync()){
            record.setState(newState);
            if(record.isNewData()){
                jdbcTemplate.update(UPDATE,record.getU_id(),record.getStart_time(),record.getEnd_time(),record.getF_id(),record.getS_id(),record.getT_id(),record.getAmount(),record.getUnit(),record.getRemark(),record.getState());
            }else {
                insertNewEntity(record);
            }
        }
    }

    private void insertNewEntity(Record record) {
        final Record record1 = record;
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(INSERT);
                ps.setString(1,record1.getU_id());
                ps.setString(2,record1.getStart_time());
                ps.setString(3,record1.getEnd_time());
                ps.setString(4,record1.getF_id());
                ps.setString(5,record1.getS_id());
                ps.setString(6,record1.getT_id());
                ps.setString(7,record1.getAmount());
                ps.setString(8,record1.getUnit());
                ps.setString(9,record1.getRemark());
                ps.setString(10,record1.getState());
                return ps;
            }
        },keyHolder);
        record1.setR_id(keyHolder.getKey().toString());
    }

    public void simpleSave(First f){
        jdbcTemplate.update(INSERT_FULL, f.getF_id(),f.getU_id(),
                f.getName(), f.getState(), f.getType());
    }

    @Override
    public Record getOne(String id) {
        Map map = jdbcTemplate.queryForMap(SELECT_ONE, id);
        return new Record().valueOf(map);
    }

    public SqlRowSet getAll(String u_id, String state){
        return jdbcTemplate.queryForRowSet(SELECT_ALL, u_id, state);
    }
    @Override
    public void delete(Base base) {
        Record record = (Record)base;
        if (record.isHaveSync()){
            record.setState("2");
            jdbcTemplate.update(UPDATE,record.getU_id(),record.getStart_time(),record.getEnd_time(),record.getF_id(),record.getS_id(),record.getT_id(),record.getAmount(),record.getUnit(),record.getRemark(),record.getState(),record.getR_id());
        }else{
            jdbcTemplate.update(DELETE,new String[]{record.getR_id()});
        }
    }
}

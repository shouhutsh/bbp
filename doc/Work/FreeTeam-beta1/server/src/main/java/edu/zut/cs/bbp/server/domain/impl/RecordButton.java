package edu.zut.cs.bbp.server.domain.impl;

import edu.zut.cs.bbp.server.domain.Base;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
public class RecordButton extends Base {
    private String rb_id;
    private String u_id;
    private String f_id;
    private String s_id;
    private String t_id;
    private String state;

    public String getRb_id() {
        return rb_id;
    }

    public void setRb_id(String rb_id) {
        this.rb_id = rb_id;
    }

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public String getF_id() {
        return f_id;
    }

    public void setF_id(String f_id) {
        this.f_id = f_id;
    }

    public String getS_id() {
        return s_id;
    }

    public void setS_id(String s_id) {
        this.s_id = s_id;
    }

    public String getT_id() {
        return t_id;
    }

    public void setT_id(String t_id) {
        this.t_id = t_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean isHaveSync() {
        return (state!=null && state.equals("0"));
    }

    @Override
    public boolean isNeedSync() {
        return (state!=null && state.equals("1"));
    }

    @Override
    public boolean isNewData() {
        return false;
    }

    @Override
    public RecordButton valueOf(Map map) {
        rb_id= String.valueOf(map.get("rb_id"));
        u_id = String.valueOf(map.get("u_id"));
        f_id = String.valueOf(map.get("f_id"));
        s_id = String.valueOf(map.get("s_id"));
        t_id = String.valueOf(map.get("t_id"));
        state= String.valueOf(map.get("state"));
        return this;
    }

    @Override
    public JSONArray toJSONArray() {
        return new JSONArray().put(rb_id).put(u_id).put(f_id).put(s_id).put(t_id).put(state);
    }

    @Override
    public RecordButton valueOf(JSONArray jsonArray) throws JSONException {
        rb_id= jsonArray.getString(0);
        u_id = jsonArray.getString(1);
        f_id = jsonArray.getString(2);
        s_id = jsonArray.getString(3);
        t_id = jsonArray.getString(4);
        state= "0";
        return this;
    }
}

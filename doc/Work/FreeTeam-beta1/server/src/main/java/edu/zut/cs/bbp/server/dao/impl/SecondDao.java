package edu.zut.cs.bbp.server.dao.impl;

import edu.zut.cs.bbp.server.dao.BaseDao;
import edu.zut.cs.bbp.server.domain.Base;
import edu.zut.cs.bbp.server.domain.impl.Second;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by ZL on 14-4-21.
 */
@Transactional
@Repository("secondDao")
public class SecondDao extends BaseDao {
    private static final String INSERT = "INSERT INTO second(f_id,u_id,name,state) values(?,?,?,?)";
    private static final String DELETE = "DELETE FROM second where s_id =?";
    private static final String UPDATE = "UPDATE second set f_id=?,name=?,state=? WHERE s_id=?";
    private static final String SELECT_ONE = "SELECT * FROM second where s_id = ?";

    private static final String INSERT_FULL = "INSERT INTO second(s_id,f_id,u_id,name,state) values(?,?,?,?)";
    private static final String SELECT_ALL = "SELECT * FROM second WHERE u_id=? AND state=?";

    @Override
    public void save(Base base, String newState) {
        Second second = (Second)base;
        if (second.isHaveSync())
        {
            delete(second);
            second.setState(newState);
            insertNewEntity(second);
        }else if(second.isNeedSync())
        {
            second.setState(newState);
            if (second.isNewData()){
                jdbcTemplate.update(UPDATE,second.getF_id(),second.getName(),second.getState(),second.getS_id());
            }else {
                insertNewEntity(second);
            }
        }
    }
    public void simpleSave(Second second){
        jdbcTemplate.update(INSERT_FULL, second.getS_id(),second.getF_id(),second.getName(),second.getState(),second.getS_id());
    }
    private void insertNewEntity(Second second) {
        final Second second1 = second;
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(INSERT);
                ps.setString(1,second1.getF_id());
                ps.setString(2,second1.getU_id());
                ps.setString(3,second1.getName());
                ps.setString(4,second1.getState());
                return ps;
            }
        },keyHolder);
        second.setS_id(keyHolder.getKey().toString());
        System.out.println(second.getS_id());
    }

    @Override
    public Second getOne(String id) {
        Map map = jdbcTemplate.queryForMap(SELECT_ONE,id);
        return (Second) new Second().valueOf(map);
    }

    public SqlRowSet getAll(String u_id, String state){
        return jdbcTemplate.queryForRowSet(SELECT_ALL, u_id, state);
    }
    @Override
    public void delete(Base base) {
           Second second = (Second)base;
        if(second.isHaveSync()){
            second.setState("2");
            jdbcTemplate.update(UPDATE,second.getF_id(),second.getU_id(),second.getName(),second.getState());
        }else {
            jdbcTemplate.update(DELETE,new String[]{second.getS_id()});
        }
    }

}

package edu.zut.cs.bbp.server.util.factory;

import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.impl.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Created by Qi_2 on 14-4-20.
 */
public abstract class Table implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

	// 注意此处不要调整顺序，若一定要调整，注意需要改变getTable函数
	public static final String[] TABLES = {"first", "second", "third", "record_button", "record"};

	public static Table getTable(int table){

		switch (table){
			case 0:
				return (FirstTable) context.getBean("firstTable");
			case 1:
				return (SecondTable) context.getBean("secondTable");
			case 2:
				return (ThirdTable) context.getBean("thirdTable");
			case 3:
				return (RecordButtonTable) context.getBean("recordButtonTable");
			case 4:
				return (RecordTable) context.getBean("recordTable");
			default:
				throw new RuntimeException("没有找到该表");
		}
	}

	public abstract void insertEntities(JSONObject insert);

	public abstract void deleteEntities(JSONObject delete);

    public abstract JSONArray insertCreater(User user);
    public abstract JSONArray deleteCreater(User user);
}

package edu.zut.cs.bbp.server.util.factory.impl;

import edu.zut.cs.bbp.server.dao.impl.SecondDao;
import edu.zut.cs.bbp.server.domain.impl.Second;
import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.util.factory.Table;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Qi_2 on 14-4-20.
 */
@SuppressWarnings("ALL")
@Component
public class SecondTable extends Table {

	@Autowired
	SecondDao secondDao;

	@Override
	public void insertEntities(JSONObject insert) {
		Second second;
		JSONArray seconds = null;

		try {
			seconds = insert.getJSONArray("second");
			for(int i = 0; i < seconds.length(); ++i){
				second = new Second().valueOf(seconds.getJSONArray(i));
//				secondDao.save(second, "0");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEntities(JSONObject delete) {

	}

    @Override
    public JSONArray insertCreater(User user) {
        return null;
    }

    @Override
    public JSONArray deleteCreater(User user) {
        return null;
    }

}

package edu.zut.cs.bbp.server.dao;

import edu.zut.cs.bbp.server.dao.impl.RecordDao;
import edu.zut.cs.bbp.server.domain.impl.Record;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by ZL on 14-4-22.
 */
public class RecordDaoTest {
    Record record;
    RecordDao recordDao;
    ApplicationContext ctx ;
    @Before
    public void init(){
        ctx = new ClassPathXmlApplicationContext("file:src/test/config/daoTest.xml");
        record = new Record();
        System.currentTimeMillis();
        record.setU_id("1");
        record.setStart_time(""+System.currentTimeMillis()+"");
        record.setEnd_time("" + System.currentTimeMillis() + "");
        record.setF_id("1");
        record.setS_id("1");
        record.setT_id("1");
        record.setAmount("1000");
        record.setUnit("1");
        record.setRemark("ok");
        record.setState("1");
    }
    @Test
    public void test() throws JSONException {
        recordDao = (RecordDao) ctx.getBean("recordDao");
        recordDao.save(record,"1");

        JSONArray jsonArray = record.toJSONArray();
        Record record1 = new Record().valueOf(jsonArray);

        record = recordDao.getOne(record.getR_id());
        Assert.assertEquals("ok",record.getRemark());

        SqlRowSet rowSet = recordDao.getAll(record.getU_id(), record.getState());
        while(rowSet.next()){
            System.out.println(rowSet.getString("r_id"));
        }

        recordDao.delete(record);


    }
}

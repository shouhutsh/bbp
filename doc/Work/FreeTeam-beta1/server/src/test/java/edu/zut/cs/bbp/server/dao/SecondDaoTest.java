package edu.zut.cs.bbp.server.dao;

import edu.zut.cs.bbp.server.dao.impl.SecondDao;
import edu.zut.cs.bbp.server.domain.impl.First;
import edu.zut.cs.bbp.server.domain.impl.Second;
import junit.framework.Assert;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by Qi_2 on 14-4-17.
 */
public class SecondDaoTest {
	Second second;
    SecondDao secondDao;
    ApplicationContext ctx = null;

	@Before
	public void init(){
        ctx = new ClassPathXmlApplicationContext("file:src/test/config/daoTest.xml");
		secondDao = (SecondDao)ctx.getBean("secondDao");
        second = new Second();
        second.setF_id("1");
        second.setU_id("1");
        second.setName("fruit");
        second.setState("1");
	}

	@Test
	public void test() throws JSONException {
		secondDao.save(second,"1");

		JSONArray json = second.toJSONArray();
        Second second1 = new Second().valueOf(json);
		Assert.assertEquals(second1.getS_id(),second.getS_id());

        second = secondDao.getOne(second1.getS_id());
		Assert.assertEquals("fruit", second.getName());

        SqlRowSet rowSet = secondDao.getAll(second.getU_id(), second.getState());
        while(rowSet.next()){
            System.out.println(rowSet.getString("name"));
        }

        secondDao.delete(second);
	}
}

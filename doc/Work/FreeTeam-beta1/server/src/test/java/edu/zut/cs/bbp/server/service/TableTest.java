package edu.zut.cs.bbp.server.service;

import edu.zut.cs.bbp.server.domain.impl.User;
import edu.zut.cs.bbp.server.service.impl.JsonDataImpl;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Qi_2 on 14-4-22.
 */
public class TableTest {

	//这是从上学期的工程里抽取出来的数据流
	//注意需要一个u_id为 1 的用户，由于
	//主键ID因此也只能插入一次
	String msg = "{\"delete\":{\"record_button\":[],\"record\":[],\"second\":[],\"first\":[],\"third\":[]},\"insert\":{\"record_button\":[[\"1\",\"1\",\"1\",\"1\",\"1\"],[\"2\",\"1\",\"2\",\"2\",\"2\"]],\"record\":[],\"second\":[[\"1\",\"1\",\"1\",\"fruit\"],[\"2\",\"2\",\"1\",\"water\"]],\"first\":[[\"1\",\"1\",\"eat\",\"0\"],[\"2\",\"1\",\"drink\",\"0\"]],\"third\":[[\"1\",\"1\",\"1\",\"apple\"],[\"2\",\"2\",\"1\",\"coffie\"]]},\"user\":[\"1\",\"test\",\"1\"]}";
	DataHelper jsonDataImpl;
    User u;

	@Before
	public void init(){
		ApplicationContext context = new ClassPathXmlApplicationContext("file:src/main/config/serviceConfig.xml");

		jsonDataImpl = (JsonDataImpl) context.getBean("jsonDataHelper");

	}

	@Test
	public void testParse(){
		try {

			jsonDataImpl.parse(msg);

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

    @Test
    public void testPack(){
        try {
            System.out.println(jsonDataImpl.pack());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

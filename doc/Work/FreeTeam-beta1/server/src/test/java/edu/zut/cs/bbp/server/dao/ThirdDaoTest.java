package edu.zut.cs.bbp.server.dao;

import edu.zut.cs.bbp.server.dao.impl.FirstDao;
import edu.zut.cs.bbp.server.dao.impl.ThirdDao;
import edu.zut.cs.bbp.server.domain.impl.First;
import edu.zut.cs.bbp.server.domain.impl.Third;
import junit.framework.Assert;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by Qi_2 on 14-4-17.
 */
public class ThirdDaoTest {
	Third third;
    ThirdDao thirdDao;
    ApplicationContext ctx;

	@Before
	public void init(){
        ctx = new ClassPathXmlApplicationContext("file:src/test/config/daoTest.xml");
		thirdDao = (ThirdDao)ctx.getBean("thirdDao");

		third = new Third();
        third.setS_id("1");
        third.setName("apple");
        third.setU_id("1");
        third.setState("1");
	}

	@Test
	public void test() throws JSONException {
		thirdDao.save(third,"1");

		JSONArray json = third.toJSONArray();
		Third third1 = new Third().valueOf(json);
		Assert.assertEquals(third1.getT_id(),third.getT_id());

		third = thirdDao.getOne(third.getT_id());
		Assert.assertEquals("apple", third.getName());

        SqlRowSet rowSet = thirdDao.getAll(third.getU_id(), third.getState());
        while(rowSet.next()){
            System.out.println(rowSet.getString("name"));
        }

        thirdDao.delete(third);
	}
}

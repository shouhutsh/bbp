package edu.zut.cs.bbp.server.dao;

import edu.zut.cs.bbp.server.dao.impl.RecordButtonDao;
import edu.zut.cs.bbp.server.dao.impl.RecordDao;
import edu.zut.cs.bbp.server.domain.impl.RecordButton;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by ZL on 14-4-22.
 */
public class RecordButtondaoTest {
    RecordButton rb;
    RecordButtonDao recordButtonDao;
    ApplicationContext ctx ;
    @Before
    public void init()
    {
        ctx = new ClassPathXmlApplicationContext("file:src/test/config/daoTest.xml");
        rb = new RecordButton();
        rb.setU_id("1");
        rb.setF_id("1");
        rb.setS_id("1");
        rb.setT_id("1");
        rb.setState("1");
    }
    @Test
    public void test() throws JSONException {
        recordButtonDao = (RecordButtonDao) ctx.getBean("recordButtonDao");
        recordButtonDao.save(rb,"1");

        JSONArray jsonArray = rb.toJSONArray();
        RecordButton button = new RecordButton().valueOf(jsonArray);
        Assert.assertEquals(button.getRb_id(),rb.getRb_id());

        rb = recordButtonDao.getOne(rb.getRb_id());
        Assert.assertEquals("1",rb.getS_id());

        SqlRowSet rowSet = recordButtonDao.getAll(rb.getU_id(), rb.getState());
        while(rowSet.next()){
            System.out.println(rowSet.getString("rb_id"));
        }

        recordButtonDao.delete(rb);
    }
}

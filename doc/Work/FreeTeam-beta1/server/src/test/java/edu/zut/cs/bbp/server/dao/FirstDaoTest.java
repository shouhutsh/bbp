package edu.zut.cs.bbp.server.dao;

import edu.zut.cs.bbp.server.dao.impl.FirstDao;
import edu.zut.cs.bbp.server.domain.impl.First;
import junit.framework.Assert;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.support.rowset.SqlRowSet;

/**
 * Created by Qi_2 on 14-4-17.
 */
public class FirstDaoTest {
	First first;
	FirstDao firstDao;

	@Before
	public void init(){
		ApplicationContext context = new ClassPathXmlApplicationContext("file:src/test/config/daoTest.xml");
		firstDao = (FirstDao)context.getBean("firstDao");

		first = new First();
		first.setU_id("1");
		first.setName("eat");
		first.setState("1");
		first.setType("0");
	}

	@Test
	public void test() throws JSONException {
		firstDao.save(first, "1");

		JSONArray json = first.toJSONArray();
		First f = new First().valueOf(json);
		Assert.assertEquals(f.getF_id(), first.getF_id());

		first = firstDao.getOne(first.getF_id());
		Assert.assertEquals("eat", first.getName());

        SqlRowSet rowSet = firstDao.getAll(first.getU_id(), first.getState());
        while(rowSet.next()){
            System.out.println(rowSet.getString("name"));
        }

		firstDao.delete(first);
	}
}

package test.edu.zut.cs.bbp.client.dao;

import android.test.AndroidTestCase;
import junit.framework.Assert;
import main.edu.ztu.cs.bbp.client.dao.impl.FirstDao;
import main.edu.ztu.cs.bbp.client.domain.impl.First;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午10:51
 * To change this template use File | Settings | File Templates.
 */
public class FirstDaoTest extends AndroidTestCase {
	First first;
	FirstDao firstDao;
	private void init(){
		first = new First();
		firstDao = new FirstDao(getContext());

//		first.setF_id("1");		// 有f_id 则插入新数据，否则更新
		first.setU_id("1");
		first.setName("drink");
		first.setState("1");
		first.setType("0");
	}

	public void test(){
		init();

		firstDao.save(first, "1");
		First f = firstDao.getOne(first.getF_id());
		Assert.assertEquals(f.getF_id(), first.getF_id());
	}
}

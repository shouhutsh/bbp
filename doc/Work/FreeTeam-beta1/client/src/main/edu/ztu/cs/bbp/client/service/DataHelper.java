package main.edu.ztu.cs.bbp.client.service;

import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午11:31
 * To change this template use File | Settings | File Templates.
 */
public interface DataHelper {
	abstract public void parse(String msg) throws JSONException;

	abstract public String pack(String msg);
}
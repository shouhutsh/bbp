package main.edu.ztu.cs.bbp.client.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.Record;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 下午12:16
 * To change this template use File | Settings | File Templates.
 */
public class RecordDao extends SQLiteDatabaseDao {
	private final static String where="SELECT * FROM record WHERE u_id=? AND state=?";

	public RecordDao(Context context) {
		super(context);
		setTB_Name("record");
	}

	@Override
	public void save(SQLiteTool s,String newState){
		Record r = (Record) s;
		ContentValues contentValues=new ContentValues();
		contentValues.put(r.U_ID,r.getU_id());
		contentValues.put(r.START,r.getStart());
		contentValues.put(r.END,r.getEnd());
		contentValues.put(r.F_ID,r.getF_id());
		contentValues.put(r.S_ID,r.getS_id());
		contentValues.put(r.T_ID,r.getT_id());
		contentValues.put(r.AMOUNT,r.getAmount());
		contentValues.put(r.UNIT,r.getUnit());
		contentValues.put(r.REMARK,r.getRemark());

		if (r.isHaveSync()){
			this.remove(r);

			r.setState(newState);
			contentValues.put(r.STATE,r.getState());

			insert(contentValues);

			r.setR_id(last_insert_rowid());
		}else if(r.isNeedSync()){
			r.setState(newState);
			contentValues.put(r.STATE,r.getState());

			if (r.isOldData()){
				update(contentValues,r.R_ID+"=?",new String[]{r.getR_id()});
			}else{
				insert(contentValues);
				r.setR_id(last_insert_rowid());
			}
		}
	}

	public void remove(SQLiteTool s){
		Record r = (Record) s;

		if (r.isHaveSync()){
			ContentValues contentValues=new ContentValues();
			contentValues.put(r.STATE,"2");
			update(contentValues,r.R_ID+"=?",new String[]{r.getR_id()});
		}else if(r.isNeedSync()){
			if(r.isOldData())
				delete(r.R_ID,new String[]{r.getR_id()});
		}
	}

	@Override
	public Cursor getAll(String state) {
		String[] columns=new String[]{User.getU_id(),state};

		Cursor c=select(where,columns);
		return c;
	}

	@Override
	public void simpleSave(SQLiteTool s) {
		Record r = (Record) s;
		ContentValues contentValues=new ContentValues();
		contentValues.put(r.R_ID,r.getR_id());
		contentValues.put(r.U_ID,r.getU_id());
		contentValues.put(r.START,r.getStart());
		contentValues.put(r.END,r.getEnd());
		contentValues.put(r.F_ID,r.getF_id());
		contentValues.put(r.S_ID,r.getS_id());
		contentValues.put(r.T_ID,r.getT_id());
		contentValues.put(r.AMOUNT,r.getAmount());
		contentValues.put(r.UNIT,r.getUnit());
		contentValues.put(r.REMARK,r.getRemark());
		contentValues.put(r.STATE,r.getState());

		insert(contentValues);
	}

	public Record getOne(String id){
		Record record = new Record();
		Cursor c=select(Record.R_ID+"=?",new String[]{id});
		if (c.moveToFirst()){
			record.valueOf(c);
		}
		return record;
	}

}

package main.edu.ztu.cs.bbp.client.domain.impl;

import android.database.Cursor;
import android.util.Log;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import org.json.JSONArray;
import org.json.JSONException;


import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午10:05
 * To change this template use File | Settings | File Templates.
 */
public class Second extends SQLiteTool {
	public final static String S_ID="s_id";
	public final static String F_ID="f_id";
	public final static String U_ID="u_id";
	public final static String NAME="name";
	public final static String STATE="state";

	String s_id = null;
	String f_id = null;
	String u_id = null;
	String name = null;
	String state= null;

	@Override
	public boolean isHaveSync() {
		//0 代表N，意思是已同步过，需要先将旧数据置删除位后添加新数据
		return (state != null && state.equals("0"));
	}

	@Override
	public boolean isNeedSync() {
		//1 代表S，意思是待同步
		return (state != null && state.equals("1"));
	}

	@Override
	public boolean isOldData() {

		return s_id!=null;
	}

	@Override
	public Second valueOf(Map map) {
		s_id = String.valueOf(map.get(S_ID));
		f_id = String.valueOf(map.get(F_ID));
		u_id = String.valueOf(map.get(U_ID));
		name = String.valueOf(map.get(NAME));
		/*state = String.valueOf(map.get(STATE));*/

		return this;
	}

	@Override
	public Second valueOf(Cursor cursor) {

		s_id = cursor.getString(cursor.getColumnIndexOrThrow(S_ID));
		f_id = cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(NAME));
		state= cursor.getString(cursor.getColumnIndexOrThrow(STATE));

		return this;
	}

	@Override
	public Second valueOf(JSONArray jsonArray) {
		// FIXME 注意这里基本都是硬编码，因此必须保证次序一致
		try {
			s_id = jsonArray.getString(0);
			f_id = jsonArray.getString(1);
			u_id = jsonArray.getString(2);
			name = jsonArray.getString(3);
			state = "0";		//设置为N，不需操作
		} catch (JSONException e) {
			Log.e("ERROR", "读取Json错误");
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public JSONArray toJSONArray() {

		return new JSONArray().put(s_id).put(f_id).put(u_id).put(name);
	}

	public String getS_id() {
		return s_id;
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String getF_id() {
		return f_id;
	}

	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}

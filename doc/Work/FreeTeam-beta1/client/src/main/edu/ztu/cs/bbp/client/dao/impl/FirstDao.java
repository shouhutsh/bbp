package main.edu.ztu.cs.bbp.client.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.First;

import javax.sql.RowSet;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午12:02
 * To change this template use File | Settings | File Templates.
 */
public class FirstDao extends SQLiteDatabaseDao {

	private final static String where="SELECT * FROM first WHERE u_id=? AND state=?";

	public FirstDao(Context context) {
		super(context);
		setTB_Name("first");
	}

	@Override
	public void save(SQLiteTool s, String newState) {
		First f = (First) s;

		ContentValues contentValues = new ContentValues();
		contentValues.put(f.U_ID, f.getU_id());
		contentValues.put(f.NAME, f.getName());
		contentValues.put(f.TYPE, f.getType());

		if (f.isHaveSync()) {
			this.remove(f);

			f.setState(newState);
			contentValues.put(f.STATE, f.getState());

			insert(contentValues);
			f.setF_id(last_insert_rowid());
		} else if (f.isNeedSync()) {
			f.setState(newState);
			contentValues.put(f.STATE, f.getState());

			if (f.isOldData()) {
				update(contentValues, f.F_ID + "=?", new String[]{f.getF_id()});
			} else {
				insert(contentValues);
				f.setF_id(last_insert_rowid());
			}
		}
	}

	@Override
	public First getOne(String id){
		First f = new First();
		Cursor c = select(First.F_ID+"=?", new String[]{id});
		if(c.moveToFirst()){
			f.valueOf(c);
		}
		c.close();
		return f;
	}

	@Override
	public void remove(SQLiteTool s) {
		First f = (First) s;
		
		if (f.isHaveSync()) {
			ContentValues contentValues = new ContentValues();
			contentValues.put(f.STATE, "2");                //2 代表D，意思是需要被删除
			update(contentValues, f.F_ID + "=?", new String[]{f.getF_id()});
		} else if (f.isNeedSync()) {
			if (f.isOldData())
				delete(f.F_ID + "=?", new String[]{f.getF_id()});
		}
	}



	public Cursor getAll(String state){
		String[] columns=new String[]{User.getU_id(),state};

		Cursor c=select(where,columns);
		return c;
	}

	@Override
	public void simpleSave(SQLiteTool s) {
		First f = (First)s;
		ContentValues contentValues = new ContentValues();
		contentValues.put(f.F_ID, f.getF_id());
		contentValues.put(f.U_ID, f.getU_id());
		contentValues.put(f.NAME, f.getName());
		contentValues.put(f.TYPE, f.getType());
		contentValues.put(f.STATE,f.getState());

		insert(contentValues);
	}

}

package main.edu.ztu.cs.bbp.client.service;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午11:25
 * To change this template use File | Settings | File Templates.
 */
public interface NetService {

	abstract public String login(String id, String pw) throws JSONException, IOException;

	abstract public String register(String name, String pw) throws JSONException, IOException;

	abstract public void sync() throws JSONException, IOException;
}

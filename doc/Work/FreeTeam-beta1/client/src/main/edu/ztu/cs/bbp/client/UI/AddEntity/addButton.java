package main.edu.ztu.cs.bbp.client.UI.AddEntity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import edu.zut.cs.bbp.client.R;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.dao.impl.FirstDao;
import main.edu.ztu.cs.bbp.client.dao.impl.RecordButtonDao;
import main.edu.ztu.cs.bbp.client.dao.impl.SecondDao;
import main.edu.ztu.cs.bbp.client.dao.impl.ThirdDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.*;

import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-30
 * Time: 下午5:46
 * To change this template use File | Settings | File Templates.
 */
public class addButton extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.button);
	}
	// TODO 设定 三个 下拉列表的数据，并设置监听函数
	@Override
	public void onResume(){
		super.onResume();

		Cursor cursor = null;
		SQLiteDatabaseDao helper = null;

		final RecordButton recordButton = new RecordButton();
		recordButton.setU_id(User.getU_id());

		final List<First> firsts = new LinkedList<First>();
		final List<Second> seconds = new LinkedList<Second>();
		final List<Third> thirds = new LinkedList<Third>();

		firsts.clear();
		seconds.clear();
		thirds.clear();

		helper = new FirstDao(getApplicationContext());
		cursor = helper.select("u_id=?", new String[]{User.getU_id()});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			firsts.add(new First().valueOf(cursor));
			cursor.moveToNext();
		}

		if(recordButton.getF_id() != null){
			helper = new SecondDao(getApplicationContext());
			cursor = helper.select("u_id=? and f_id=?", new String[]{User.getU_id(), recordButton.getF_id()});
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				seconds.add(new Second().valueOf(cursor));
				cursor.moveToNext();
			}
		}

		if(recordButton.getS_id() != null){
			helper = new ThirdDao(getApplicationContext());
			cursor = helper.select("u_id=? and s_id=?", new String[]{User.getU_id(), recordButton.getS_id()});
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				thirds.add(new Third().valueOf(cursor));
				cursor.moveToNext();
			}
		}
		Spinner firstChose = (Spinner) findViewById(R.id.firstChose);
		final Spinner secondChose = (Spinner) findViewById(R.id.secondChose);
		final Spinner thirdChose = (Spinner) findViewById(R.id.thirdChose);

		/**
		 * 这里是下拉列表的监听事件
		 */
		final ArrayAdapter<First> firstAdapter = new ArrayAdapter<First>(getApplicationContext(),
				android.R.layout.simple_spinner_item, firsts);
		firstChose.setAdapter(firstAdapter);

		final ArrayAdapter<Second> secondAdapter = new ArrayAdapter<Second>(getApplicationContext(),
				android.R.layout.simple_spinner_item, seconds);
		secondChose.setAdapter(secondAdapter);

		final ArrayAdapter<Third> thirdAdapter = new ArrayAdapter<Third>(getApplicationContext(),
				android.R.layout.simple_spinner_item, thirds);
		thirdChose.setAdapter(thirdAdapter);

		/**
		 * 这里是下拉框条目点击事件
		 */
		firstChose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				//To change body of implemented methods use File | Settings | File Templates.
				recordButton.setF_id(firstAdapter.getItem(i).getF_id());
				recordButton.setS_id(null);
				recordButton.setT_id(null);
				seconds.clear();
				thirds.clear();

				SQLiteDatabaseDao helper = new SecondDao(getApplicationContext());
				Cursor cursor = helper.select("u_id=? and f_id=?", new String[]{User.getU_id(), recordButton.getF_id()});
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					seconds.add(new Second().valueOf(cursor));
					cursor.moveToNext();
				}
				secondChose.setAdapter(secondAdapter);
			}


			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});
		secondChose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				//To change body of implemented methods use File | Settings | File Templates.
				recordButton.setS_id(secondAdapter.getItem(i).getS_id());
				recordButton.setT_id(null);
				thirds.clear();
				SQLiteDatabaseDao helper = new ThirdDao(getApplicationContext());
				Cursor cursor = helper.select("u_id=? and s_id=?", new String[]{User.getU_id(), recordButton.getS_id()});
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					thirds.add(new Third().valueOf(cursor));
					cursor.moveToNext();
				}
				thirdChose.setAdapter(thirdAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});
		thirdChose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				//To change body of implemented methods use File | Settings | File Templates.
				recordButton.setT_id(thirdAdapter.getItem(i).getT_id());
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapterView) {
				//To change body of implemented methods use File | Settings | File Templates.
			}
		});

	}

	public void toFirstAddButton(View v){
		Log.i("CLICK", "to first add button");
		Intent intent = new Intent(this, firstActivity.class);
		startActivity(intent);
	}

	public void toSecondAddButton(View v){
		Log.i("CLICK", "to second add button");
		final RecordButton recordButton = new RecordButton();
		Intent intent = new Intent(this, secondActivity.class);
		intent.putExtra("f_id", recordButton.getF_id());
		// TODO 注意将 【选定 first】 的id传过去
		intent.setClass(getApplicationContext(), secondActivity.class);

		startActivity(intent);
	}

	public void toThirdAddButton(View v){
		Log.i("CLICK", "to third add button");
		final RecordButton recordButton = new RecordButton();
		Intent intent = new Intent(this, thirdActivity.class);
		// TODO 注意将 【选定 second】 的id传过去
		intent.putExtra("s_id", recordButton.getS_id());
		intent.setClass(getApplicationContext(), thirdActivity.class);

		startActivity(intent);
	}

	public void submitButton(View v){
		Log.i("CLICK", "submit button");
		final RecordButton recordButton = new RecordButton();
		RecordButtonDao helper=new RecordButtonDao(getApplicationContext());
		// TODO 将这个 新的 record button 记录在数据库
		if(recordButton.getF_id() == null ||
				recordButton.getS_id() == null ||
				recordButton.getT_id() == null){
			Toast.makeText(getApplicationContext(), "不能为空值！", Toast.LENGTH_LONG).show();
		}else{
			recordButton.setState("1");
			helper.save(recordButton, "1");
			Toast.makeText(getApplicationContext(), "创建成功！", Toast.LENGTH_LONG).show();
			finish();
		}
	}
}

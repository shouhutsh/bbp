package main.edu.ztu.cs.bbp.client.UI.AddEntity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import edu.zut.cs.bbp.client.R;
import main.edu.ztu.cs.bbp.client.dao.impl.ThirdDao;
import main.edu.ztu.cs.bbp.client.domain.impl.Third;

import java.util.ArrayList;
import java.util.List;

//import main.edu.ztu.cs.bbp.client.domain.impl.User;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-29
 * Time: 下午3:58
 * To change this template use File | Settings | File Templates.
 */
public class thirdActivity extends Activity {

	private List<String> list = new ArrayList<String>();
	private EditText editText;
	private String str1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.third);

		// TODO
	}

	public void thirdAddButton(View v){
		Log.i("CLICK", "third add button");

		// TODO 将 新的 third 写入数据库
		//	editText = (EditText) findViewById(R.id.editText);
		str1 = editText.getText().toString();
		Third third = new Third();
		ThirdDao helper = new ThirdDao(getApplicationContext());
		//			third.setU_id(User.getU_id());
		third.setName(str1);
		helper.save(third, "1");
		helper.close();

		if (!str1.equals("")) {

			Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_SHORT).show();
			finish();
		} else {
			Toast.makeText(getApplicationContext(), "事件不能为空！", Toast.LENGTH_SHORT).show();
		}


	}
}

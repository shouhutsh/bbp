package main.edu.ztu.cs.bbp.client.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午3:28
 * To change this template use File | Settings | File Templates.
 */
public class UserDao {
	private SQLiteDatabaseDao db;

	public UserDao(Context context) {
		db = new SQLiteDatabaseDao(context) {
			@Override
			public void save(SQLiteTool s, String newState) { }

			@Override
			public SQLiteTool getOne(String id) { return null; }

			@Override
			public void remove(SQLiteTool s) { }

            @Override
            public Cursor getAll(String state) {
                return null;
            }

            @Override
            public void simpleSave(SQLiteTool s) {

            }
        };

		db.setTB_Name("user");
	}

	public void save() {
		remove();

		ContentValues contentValues = new ContentValues();
		contentValues.put(User.U_ID, User.getU_id());
		contentValues.put(User.NAME, User.getName());
		contentValues.put(User.PASSWORD, User.getPassword());

		db.insert(contentValues);
	}

	public void getOne(){
		// TODO 我得想想这个应该根据什么来检索，可能是用户名吧，或者账号ID
	}

	public void remove() {
		db.delete(User.U_ID + "=?", new String[]{User.getU_id()});
	}

}

package main.edu.ztu.cs.bbp.client.service.impl;

import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.dao.impl.*;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.*;
import main.edu.ztu.cs.bbp.client.service.DataHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParsePosition;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-29
 * Time: 下午1:12
 * To change this template use File | Settings | File Templates.
 */
public class NetServiceImpl implements main.edu.ztu.cs.bbp.client.service.NetService {
	Context context;

	public NetServiceImpl(Context context){
		this.context = context;
	}

	private void init(String u, HttpURLConnection con) throws IOException {

		URL url = new URL(u);

		con = (HttpURLConnection) url.openConnection();
		HttpURLConnection.setFollowRedirects(true);
		con.setDoInput(true);
		con.setDoOutput(true);
		con.setRequestMethod("POST");    //设置为Post方法
		con.setRequestProperty("Content-Type", "multipart/form-data");    //这话似乎没影响
		con.connect();
	}

	@Override
	public String login(String id, String pw) throws JSONException, IOException {

		JSONObject login = new JSONObject();
		JSONArray user = new JSONArray();
		user.put(id).put(pw);
		login.put("login",user);
		HttpURLConnection con = null;
		init("http://",con);
		DataOutputStream out = new DataOutputStream(new BufferedOutputStream(con.getOutputStream()));
		out.writeUTF(login.toString());
		out.flush();
		out.close();
		//接受服务器发送过来的信息
		if (HttpURLConnection.HTTP_OK == con.getResponseCode()) {
			DataInputStream in = new DataInputStream(new BufferedInputStream(con.getInputStream()));
			login = new JSONObject(in.readUTF());
			if (login.has("user")) {
				return login.getString("user");
			}
		}
		return null;

	}

	@Override
	public String register(String name, String pw) throws JSONException, IOException {
		JSONObject register = new JSONObject();
		JSONArray user = new JSONArray();
		user.put(name).put(pw);
		register.put("register",user);
		HttpURLConnection con = null;
		init("http://",con);

		DataOutputStream out = new DataOutputStream(new BufferedOutputStream(con.getOutputStream()));
		out.writeUTF(register.toString());
		out.flush();
		out.close();
		if (HttpURLConnection.HTTP_OK == con.getResponseCode()) {
			DataInputStream in = new DataInputStream(new BufferedInputStream(con.getInputStream()));
			register = new JSONObject(in.readUTF());
			if (register.has("user")) {
				return register.getString("user");
			}
		}
		return null;
	}

	@Override
	public void sync() throws JSONException, IOException {
		DataHelper helper = new DataHelperImpl(context);


		JSONObject info = new JSONObject();
		try {
			JSONArray user = new JSONArray();
			user.put(User.getU_id()).put(User.getName()).put(User.getPassword());
			info.put("user",user);
			createInsert();
			createDelete();


			HttpURLConnection con = null;
			init("http://",con);
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(con.getOutputStream()));
			out.writeUTF(info.toString());
			out.flush();
			out.close();
			//接受服务器发送过来的信息
			if (HttpURLConnection.HTTP_OK == con.getResponseCode()) {
				DataInputStream in = new DataInputStream(new BufferedInputStream(con.getInputStream()));
				helper.parse(in.readUTF());
				in.close();

				SQLiteDatabaseDao SQLiteDatabaseDao = null;
				SQLiteDatabaseDao = new FirstDao(context);
				SQLiteDatabaseDao.delete("u_id=? and state=?", new String[]{User.getU_id(), "2"});
				SQLiteDatabaseDao = new SecondDao(context);
				SQLiteDatabaseDao.delete("u_id=? and state=?", new String[]{User.getU_id(), "2"});
				SQLiteDatabaseDao = new ThirdDao(context);
				SQLiteDatabaseDao.delete("u_id=? and state=?", new String[]{User.getU_id(), "2"});
				SQLiteDatabaseDao = new RecordDao(context);
				SQLiteDatabaseDao.delete("u_id=? and state=?", new String[]{User.getU_id(), "2"});
				SQLiteDatabaseDao = new RecordButtonDao(context);
				SQLiteDatabaseDao.delete("u_id=? and state=?", new String[]{User.getU_id(), "2"});
			}
		} catch (Exception e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}


		}



	private void createInsert() throws JSONException {
		Cursor cursor = null;
		JSONArray jsonArray = null;

		SQLiteDatabaseDao helper = null;
		JSONObject info = new JSONObject();
		JSONObject insert = new JSONObject();

		//一级类
		helper =new FirstDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			First first = new First().valueOf(cursor);
			helper.save(first, "0");

			jsonArray.put(first.toJSONArray());

			cursor.moveToNext();
		}
		insert.put("first", jsonArray);


		//二级类
		helper = new SecondDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Second second = new Second().valueOf(cursor);
			helper.save(second, "0");

			jsonArray.put(second.toJSONArray());

			cursor.moveToNext();
		}
		insert.put("second", jsonArray);


		//三级类
		helper = new ThirdDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Third third = new Third().valueOf(cursor);
			helper.save(third, "0");
			jsonArray.put(third.toJSONArray());

			cursor.moveToNext();
		}
		insert.put("third", jsonArray);


		//记录类
		helper = new RecordDao(context);
		jsonArray = new JSONArray();

		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Record record = new Record().valueOf(cursor);
			helper.save(record, "0");


			jsonArray.put(record.toJSONArray());

			cursor.moveToNext();
		}
		insert.put("record", jsonArray);


		//按钮类
		helper = new RecordButtonDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			RecordButton record_Button  = new RecordButton().valueOf(cursor);
			helper.save(record_Button, "0");


			jsonArray.put(record_Button);

			cursor.moveToNext();
		}
		insert.put("record_button", jsonArray);

		info.put("insert", insert);
	}

	private void createDelete() throws JSONException {
		Cursor cursor = null;
		JSONArray jsonArray = null;
		SQLiteDatabaseDao helper = null;
		JSONObject info = new JSONObject();
		JSONObject delete =new JSONObject();

		//一级类
		helper =new FirstDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			First first = new First().valueOf(cursor);
			helper.save(first, "0");

			jsonArray.put(first.toJSONArray());

			cursor.moveToNext();
		}
		delete.put("first", jsonArray);


		//二级类
		helper = new SecondDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Second second = new Second().valueOf(cursor);
			helper.save(second, "0");

			jsonArray.put(second.toJSONArray());

			cursor.moveToNext();
		}
		delete.put("second", jsonArray);


		//三级类
		helper = new ThirdDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Third third = new Third().valueOf(cursor);
			helper.save(third, "0");
			jsonArray.put(third.toJSONArray());

			cursor.moveToNext();
		}
		delete.put("third", jsonArray);


		//记录类
		helper = new RecordDao(context);
		jsonArray = new JSONArray();

		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "2"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Record record = new Record().valueOf(cursor);
			helper.save(record, "0");


			jsonArray.put(record.toJSONArray());

			cursor.moveToNext();
		}
		delete.put("record", jsonArray);


		//按钮类
		helper = new RecordButtonDao(context);
		jsonArray = new JSONArray();
		cursor = helper.select("u_id=? and state=?", new String[]{User.getU_id(), "1"});
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			RecordButton record_Button  = new RecordButton().valueOf(cursor);
			helper.save(record_Button, "0");


			jsonArray.put(record_Button);

			cursor.moveToNext();
		}
		delete.put("record_button", jsonArray);

		info.put("delete", delete);
	}


}

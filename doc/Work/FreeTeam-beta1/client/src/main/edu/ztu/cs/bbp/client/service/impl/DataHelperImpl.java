package main.edu.ztu.cs.bbp.client.service.impl;

import android.content.Context;
import main.edu.ztu.cs.bbp.client.dao.impl.*;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.impl.*;
import main.edu.ztu.cs.bbp.client.service.DataHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLClientInfoException;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 下午12:10
 * To change this template use File | Settings | File Templates.
 */
public class DataHelperImpl implements DataHelper {
	private Context context;

	public DataHelperImpl(Context context) {
		this.context = context;
	}

	@Override
	public void parse(String msg) throws JSONException {
		JSONObject info = new JSONObject(msg);

		if (info.has("delete")) {            //这里执行删除操作
			deleteOperate(info);
		}
		if (info.has("insert")) {                //这里是插入操作
			insertOperate(info);
		}
	}

	@Override
	public String pack(String msg) {
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}

	private void deleteOperate(JSONObject info) throws JSONException {


		info = new JSONObject();
		JSONObject delete = info.getJSONObject("delete");
		JSONArray tableArray = null;
		SQLiteTool sqLiteTool = null;

		if (delete.has("first")) {
			FirstDao firstDao = new FirstDao(context);
			tableArray = delete.getJSONArray("first");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new First().valueOf(tableArray.getJSONArray(i));
				firstDao.remove((First) sqLiteTool);
			}
		}

		if (delete.has("second")){
			SecondDao secondDao = new SecondDao(context);
			tableArray = delete.getJSONArray("second");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new Second().valueOf(tableArray.getJSONArray(i));
				secondDao.remove((Second) sqLiteTool);
			}
		}

		if (delete.has("third")){
			ThirdDao thirdDao = new ThirdDao(context);
			tableArray = delete.getJSONArray("third");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new Third().valueOf(tableArray.getJSONArray(i));
				thirdDao.remove((Third) sqLiteTool);
			}
		}

		if (delete.has("record")){
			RecordDao recordDao = new RecordDao(context);
			tableArray = delete.getJSONArray("record");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new Record().valueOf(tableArray.getJSONArray(i));
				recordDao.remove((Record) sqLiteTool);
			}

		}

		if (delete.has("record_button")){
			RecordButtonDao recordButtonDao = new RecordButtonDao(context);
			tableArray = delete.getJSONArray("record_button");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new RecordButton().valueOf(tableArray.getJSONArray(i));
			    recordButtonDao.remove((RecordButton) sqLiteTool);


			}
		}
	}

	private void insertOperate(JSONObject info) throws JSONException {

		info = new JSONObject();
		JSONObject insert = info.getJSONObject("insert");
		JSONArray tableArray = null;
		SQLiteTool sqLiteTool = null;

		if (insert.has("first")) {
			FirstDao firstDao = new FirstDao(context);
			tableArray = insert.getJSONArray("first");
			for (int i = 0; i < tableArray.length(); ++i) {
				sqLiteTool = new First().valueOf(tableArray.getJSONArray(i));

				firstDao.save((First) sqLiteTool, "0");
			}
		}

		if (insert.has("second")){
			SecondDao secondDao = new SecondDao(context);
			tableArray = insert.getJSONArray("second");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new Second().valueOf(tableArray.getJSONArray(i));
				secondDao.save((Second) sqLiteTool, "0");
			}
		}

		if (insert.has("third")){
			ThirdDao thirdDao = new ThirdDao(context);
			tableArray = insert.getJSONArray("third");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new Third().valueOf(tableArray.getJSONArray(i));
				thirdDao.save((Third) sqLiteTool, "0");
			}
		}

		if (insert.has("record")){
			RecordDao recordDao = new RecordDao(context);
			tableArray = insert.getJSONArray("record");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new Record().valueOf(tableArray.getJSONArray(i));
				recordDao.save((Record) sqLiteTool, "0");
			}

		}

		if (insert.has("record_button")){
			RecordButtonDao recordButtonDao = new RecordButtonDao(context);
			tableArray = insert.getJSONArray("record_button");
			for (int i = 0; i < tableArray.length(); ++i){
				sqLiteTool = new RecordButton().valueOf(tableArray.getJSONArray(i));
				recordButtonDao.save((RecordButton) sqLiteTool, "0");


			}
		}
	}

}

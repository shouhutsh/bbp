package main.edu.ztu.cs.bbp.client.domain.impl;

import android.database.Cursor;
import android.util.Log;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import org.json.JSONArray;
import org.json.JSONException;

import java.security.interfaces.RSAMultiPrimePrivateCrtKey;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午10:57
 * To change this template use File | Settings | File Templates.
 */
public class Record extends SQLiteTool {
	public final static String R_ID = "r_id";
	public final static String U_ID = "u_id";
	public final static String START = "start_time";
	public final static String END = "end_time";
	public final static String F_ID = "f_id";
	public final static String S_ID = "s_id";
	public final static String T_ID = "t_id";
	public final static String AMOUNT = "amount";
	public final static String UNIT = "unit";
	public final static String REMARK = "remark";
	public final static String STATE = "state";

	String r_id = null;
	String u_id = null;
	String start = null;
	String end = null;
	String f_id = null;
	String s_id = null;
	String t_id = null;
	String amount = null;
	String unit = null;
	String remark = null;
	String state = null;


	@Override
	public boolean isHaveSync() {
		return (state != null && state.equals("0"));
	}

	@Override
	public boolean isNeedSync() {
		return state != null && state.equals("1");
	}

	@Override
	public boolean isOldData() {
		return (r_id != null);
	}

	@Override
	public Record valueOf(Map map) {

		r_id = String.valueOf(map.get(R_ID));
		u_id = String.valueOf(map.get(U_ID));
		start = String.valueOf(map.get(START));
		end = String.valueOf(map.get(END));
		f_id = String.valueOf(map.get(F_ID));
		s_id = String.valueOf(map.get(S_ID));
		t_id = String.valueOf(map.get(T_ID));
		amount = String.valueOf(map.get(AMOUNT));
		unit = String.valueOf(map.get(UNIT));
		remark = String.valueOf(map.get(REMARK));
		state = String.valueOf(STATE);

		return this;
	}

	@Override
	public Record valueOf(Cursor cursor) {
		r_id = cursor.getString(cursor.getColumnIndexOrThrow(R_ID));
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		start = cursor.getString(cursor.getColumnIndexOrThrow(START));
		end = cursor.getString(cursor.getColumnIndexOrThrow(END));
		f_id = cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		s_id = cursor.getString(cursor.getColumnIndexOrThrow(S_ID));
		t_id = cursor.getString(cursor.getColumnIndexOrThrow(T_ID));
		amount = cursor.getString(cursor.getColumnIndexOrThrow(AMOUNT));
		unit = cursor.getString(cursor.getColumnIndexOrThrow(UNIT));
		remark = cursor.getString(cursor.getColumnIndexOrThrow(REMARK));
		state = cursor.getString(cursor.getColumnIndexOrThrow(STATE));

		return this;
	}

	@Override
	public Record valueOf(JSONArray jsonArray) {
		try {
			r_id = jsonArray.getString(0);
			u_id = jsonArray.getString(1);
			start = jsonArray.getString(2);
			end = jsonArray.getString(3);
			f_id=jsonArray.getString(4);
			s_id=jsonArray.getString(5);
			t_id=jsonArray.getString(6);
			amount=jsonArray.getString(7);
			unit=jsonArray.getString(8);
			remark=jsonArray.getString(9);

			state = "0";        //设置为N，不需操作
		} catch (JSONException e) {
			Log.e("ERROR", "读取Json错误");
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public JSONArray toJSONArray() {
		return new JSONArray().put(r_id).put(u_id).put(start).put(end).put(f_id)
				.put(s_id).put(t_id).put(amount).put(unit).put(remark);
	}

	public String getR_id() {
		return r_id;
	}

	public void setR_id(String r_id) {
		this.r_id = r_id;
	}

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getF_id() {
		return f_id;
	}

	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	public String getS_id() {
		return s_id;
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String getT_id() {
		return t_id;
	}

	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}

package main.edu.ztu.cs.bbp.client.domain.impl;

import android.database.Cursor;
import android.util.Log;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午10:55
 * To change this template use File | Settings | File Templates.
 */
public class Third extends SQLiteTool {
	public final static String T_ID="t_id";
	public final static String S_ID="s_id";
	public final static String U_ID="u_id";
	public final static String NAME="name";
	public final static String STATE="state";

	String t_id = null;
	String s_id = null;
	String u_id = null;
	String name = null;
	String state= null;

	@Override
	public boolean isHaveSync() {

		return (state !=null && state.equals("0"));
	}

	@Override
	public boolean isNeedSync() {

		return (state !=null && state.equals("1"));
	}

	@Override
	public boolean isOldData() {
		return t_id != null;
	}

	@Override
	public Third valueOf(Map map) {
		t_id=String.valueOf(map.get(T_ID));
		s_id=String.valueOf(map.get(S_ID));
		u_id=String.valueOf(map.get(U_ID));
		name=String.valueOf(map.get(NAME));

		return this;
	}

	@Override
	public Third valueOf(Cursor cursor) {

		t_id=cursor.getString(cursor.getColumnIndexOrThrow(T_ID));
		s_id=cursor.getString(cursor.getColumnIndexOrThrow(S_ID));
		u_id=cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		name=cursor.getString(cursor.getColumnIndexOrThrow(NAME));
		state=cursor.getString(cursor.getColumnIndexOrThrow(STATE));

		return this;
	}

	@Override
	public Third valueOf(JSONArray jsonArray) {
		try {
			t_id = jsonArray.getString(0);
			s_id = jsonArray.getString(1);
			u_id = jsonArray.getString(2);
			name = jsonArray.getString(3);

			state = "0";		//设置为N，不需操作
		} catch (JSONException e) {
			Log.e("ERROR", "读取Json错误");
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public JSONArray toJSONArray() {
		return new JSONArray().put(t_id).put(s_id).put(u_id).put(name);
	}

	public String getT_id() {
		return t_id;
	}

	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	public String getS_id() {
		return s_id;
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}

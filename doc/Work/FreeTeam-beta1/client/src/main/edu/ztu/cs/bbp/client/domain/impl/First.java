package main.edu.ztu.cs.bbp.client.domain.impl;

import android.database.Cursor;
import android.util.Log;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 上午11:13
 * To change this template use File | Settings | File Templates.
 */
public class First extends SQLiteTool {
	public final static String F_ID = "f_id";
	public final static String U_ID = "u_id";
	public final static String NAME = "name";
	public final static String STATE= "state";
	public final static String TYPE = "type";

	String f_id = null;
	String u_id = null;
	String name = null;
	String state= null;
	String type = null;

	@Override
	public First valueOf(JSONArray jsonArray) {
		// FIXME 注意这里基本都是硬编码，因此必须保证次序一致
		try {
			f_id = jsonArray.getString(0);
			u_id = jsonArray.getString(1);
			name = jsonArray.getString(2);
			type = jsonArray.getString(3);

			state = "0";		//设置为N，不需操作
		} catch (JSONException e) {
			Log.e("ERROR", "读取Json错误");
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public First valueOf(Map map){
		f_id = String.valueOf(map.get(F_ID));
		u_id = String.valueOf(map.get(U_ID));
		name = String.valueOf(map.get(NAME));
		state= String.valueOf(map.get(STATE));
		type = String.valueOf(map.get(TYPE));

		return this;
	}

	@Override
	public First valueOf(Cursor cursor){
		f_id = cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		u_id = cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		name = cursor.getString(cursor.getColumnIndexOrThrow(NAME));
		state= cursor.getString(cursor.getColumnIndexOrThrow(STATE));
		type = cursor.getString(cursor.getColumnIndexOrThrow(TYPE));

		return this;
	}

	@Override
	public JSONArray toJSONArray(){
		return new JSONArray().put(f_id).put(u_id).put(name).put(type);
	}

	@Override
	public boolean isHaveSync(){
		return (state!=null && state.equals("0"));
	}

	@Override
	public boolean isNeedSync(){
		return (state != null && state.equals("1"));
	}

	@Override
	public boolean isOldData() {
		return f_id != null;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getF_id() {
		return f_id;
	}

	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}

package main.edu.ztu.cs.bbp.client.domain;

import android.database.Cursor;
import org.json.JSONArray;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午1:08
 * To change this template use File | Settings | File Templates.
 */
public abstract class SQLiteTool {

	abstract public boolean isHaveSync();

	abstract public boolean isNeedSync();

	abstract public boolean isOldData();

	abstract public SQLiteTool valueOf(Map map);

	abstract public SQLiteTool valueOf(Cursor cursor);

	abstract public SQLiteTool valueOf(JSONArray jsonArray);

	abstract public JSONArray toJSONArray();
}

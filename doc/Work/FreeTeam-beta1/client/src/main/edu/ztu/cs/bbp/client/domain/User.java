package main.edu.ztu.cs.bbp.client.domain;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-25
 * Time: 下午3:14
 * To change this template use File | Settings | File Templates.
 */
public final class User {
	public final static String U_ID = "u_id";
	public final static String NAME = "name";
	public final static String PASSWORD="password";

	static String u_id = null;
	static String name = null;
	static String password = null;

	static public String getU_id() {
		return u_id;
	}

	static public void setU_id(String u_id) {
		User.u_id = u_id;
	}

	static public String getPassword() {
		return password;
	}

	static public void setPassword(String password) {
		User.password = password;
	}

	static public String getName() {
		return name;
	}

	static public void setName(String name) {
		User.name = name;
	}
}

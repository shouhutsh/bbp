package main.edu.ztu.cs.bbp.client.domain.impl;

import android.database.Cursor;
import android.util.Log;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午10:58
 * To change this template use File | Settings | File Templates.
 */
public class RecordButton extends SQLiteTool {
	public final static String RB_ID= "rb_id";
	public final static String U_ID = "u_id";
	public final static String F_ID = "f_id";
	public final static String S_ID = "s_id";
	public final static String T_ID = "t_id";
	public final static String STATE= "state";

	String rb_id= null;
	String u_id = null;
	String f_id = null;
	String s_id = null;
	String t_id = null;
	String state= null;

	@Override
	public RecordButton valueOf(Map map) {
		rb_id=String.valueOf(map.get(RB_ID));
		u_id=String.valueOf(map.get(U_ID));
		f_id=String.valueOf(map.get(F_ID));
		s_id=String.valueOf(map.get(S_ID));
		t_id=String.valueOf(map.get(T_ID));

		return this;
	}

	@Override
	public RecordButton valueOf(Cursor cursor) {

		rb_id=cursor.getString(cursor.getColumnIndexOrThrow(RB_ID));
		u_id=cursor.getString(cursor.getColumnIndexOrThrow(U_ID));
		f_id=cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		f_id=cursor.getString(cursor.getColumnIndexOrThrow(F_ID));
		s_id=cursor.getString(cursor.getColumnIndexOrThrow(S_ID));
		t_id=cursor.getString(cursor.getColumnIndexOrThrow(T_ID));

		return this;
	}

	@Override
	public boolean isHaveSync() {
		return (state != null && state.equals("0"));
	}

	@Override
	public boolean isNeedSync() {
		return (state != null && state.equals("1"));
	}

	@Override
	public boolean isOldData() {
		return (rb_id != null);
	}

	@Override
	public RecordButton valueOf(JSONArray jsonArray) {
		try {
			rb_id = jsonArray.getString(0);
			u_id = jsonArray.getString(1);
			f_id = jsonArray.getString(2);
			s_id = jsonArray.getString(3);
			t_id = jsonArray.getString(4);

			state = "0";		//设置为N，不需操作
		} catch (JSONException e) {
			Log.e("ERROR", "读取Json错误");
			e.printStackTrace();
		}
		return this;
	}

	@Override
	public JSONArray toJSONArray() {
		return new JSONArray().put(rb_id).put(u_id).put(f_id).put(s_id).put(t_id);
	}

	public String getRb_id() {
		return rb_id;
	}

	public void setRb_id(String rb_id) {
		this.rb_id = rb_id;
	}

	public String getU_id() {
		return u_id;
	}

	public void setU_id(String u_id) {
		this.u_id = u_id;
	}

	public String getF_id() {
		return f_id;
	}

	public void setF_id(String f_id) {
		this.f_id = f_id;
	}

	public String getS_id() {
		return s_id;
	}

	public void setS_id(String s_id) {
		this.s_id = s_id;
	}

	public String getT_id() {
		return t_id;
	}

	public void setT_id(String t_id) {
		this.t_id = t_id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}

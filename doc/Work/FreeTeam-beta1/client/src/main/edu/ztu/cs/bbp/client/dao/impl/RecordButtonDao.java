package main.edu.ztu.cs.bbp.client.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.RecordButton;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 下午2:09
 * To change this template use File | Settings | File Templates.
 */
public class RecordButtonDao extends SQLiteDatabaseDao {
	private final static String where="SELECT * FROM record_button WHERE u_id=? AND state=?";

	public RecordButtonDao(Context context) {
		super(context);
		setTB_Name("record_button");
	}

	public void save(SQLiteTool s, String newState) {
		RecordButton rb =(RecordButton) s;

		ContentValues contentValues=new ContentValues();
		contentValues.put(rb.U_ID,rb.getU_id());
		contentValues.put(rb.F_ID,rb.getF_id());
		contentValues.put(rb.S_ID,rb.getS_id());
		contentValues.put(rb.T_ID,rb.getT_id());

		if(rb.isHaveSync()){
			this.remove(rb);

			rb.setState(newState);
			contentValues.put(rb.STATE,rb.getState());

			insert(contentValues);
			rb.setRb_id(last_insert_rowid());

		}else if(rb.isNeedSync()){

			rb.setState(newState);
			contentValues.put(rb.STATE,rb.getState());

			if(rb.isOldData()){
				update(contentValues,rb.RB_ID+"=?",new String[]{rb.getRb_id()});

			}else{
				insert(contentValues);
				rb.setRb_id(last_insert_rowid());
			}
		}
	}

	public RecordButton getOne(String id) {
		RecordButton recordButton = new RecordButton();
		Cursor c = select(RecordButton.F_ID+"=?", new String[]{id});
		if(c.moveToFirst()){
			recordButton.valueOf(c);
		}
		c.close();
		return recordButton;
	}

	public void remove(SQLiteTool s) {
		RecordButton rb =(RecordButton) s;

		if(rb.isHaveSync()){
			ContentValues contentValues=new ContentValues();
			contentValues.put(rb.STATE,"2");
			update(contentValues,rb.RB_ID+"=?",new String[]{rb.getRb_id()});
		}else if(rb.isNeedSync()){
			if (rb.isOldData())
				delete(rb.RB_ID,new String[]{rb.getRb_id()});
		}
	}

	@Override
	public Cursor getAll(String state) {
		String[] columns=new String[]{User.getU_id(),state};

		Cursor c=select(where,columns);
		return c;
	}

	@Override
	public void simpleSave(SQLiteTool s) {
		RecordButton rb =(RecordButton) s;

		ContentValues contentValues=new ContentValues();
		contentValues.put(rb.RB_ID,rb.getRb_id());
		contentValues.put(rb.U_ID,rb.getU_id());
		contentValues.put(rb.F_ID,rb.getF_id());
		contentValues.put(rb.S_ID,rb.getS_id());
		contentValues.put(rb.T_ID,rb.getT_id());
		contentValues.put(rb.STATE,rb.getState());

		insert(contentValues);
	}

}
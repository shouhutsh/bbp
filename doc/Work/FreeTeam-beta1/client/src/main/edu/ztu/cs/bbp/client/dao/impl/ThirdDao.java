package main.edu.ztu.cs.bbp.client.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.Third;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午11:25
 * To change this template use File | Settings | File Templates.
 */
public class ThirdDao extends SQLiteDatabaseDao{
	private final static String where="SELECT * FROM third WHERE u_id=? AND state=?";

	public ThirdDao(Context context) {
		super(context);
		setTB_Name("third");
	}

	public void save(SQLiteTool s, String newState){
		Third t = (Third) s;
		ContentValues contentValues = new ContentValues();

		contentValues.put(t.S_ID,t.getS_id());
		contentValues.put(t.U_ID,t.getU_id());
		contentValues.put(t.NAME,t.getName());

		if (t.isHaveSync()){
			this.remove(t);

			t.setState(newState);
			contentValues.put(t.STATE, t.getState());

			insert(contentValues);

			t.setT_id(last_insert_rowid());
		}else if (t.isNeedSync()){
			t.setState(newState);
			contentValues.put(t.STATE,t.getState());

			if (t.isOldData()){
				update(contentValues,t.T_ID+"=?",new String[]{t.getT_id()});
			}else{
				insert(contentValues);
				t.setT_id(last_insert_rowid());
			}
		}
	}

	public void remove(SQLiteTool s){
		Third t = (Third) s;
		if (t.isHaveSync()){
			ContentValues contentValues=new ContentValues();
			contentValues.put(t.STATE,"2");
			update(contentValues,t.T_ID+"=?",new String[]{t.getT_id()});
		}else if (t.isNeedSync()){
			if(t.isOldData()){
				delete(t.T_ID+"=?",new String[]{t.getT_id()});
			}
		}

	}

	public Third getOne(String id){
		Third t=new Third();
		Cursor c=select(Third.T_ID+"=?",new String[]{id});
		if(c.moveToFirst()){
			t.valueOf(c);
		}
		c.close();
		return t;
	}

	@Override
	public Cursor getAll(String state) {
		String[] columns=new String[]{User.getU_id(),state};

		Cursor c=select(where,columns);
		return c;
	}

	@Override
	public void simpleSave(SQLiteTool s) {
		Third t = (Third) s;

		ContentValues contentValues = new ContentValues();
		contentValues.put(t.T_ID,t.getT_id());
		contentValues.put(t.S_ID,t.getS_id());
		contentValues.put(t.U_ID,t.getU_id());
		contentValues.put(t.NAME,t.getName());
		contentValues.put(t.STATE,t.getState());

		insert(contentValues);
	}
}

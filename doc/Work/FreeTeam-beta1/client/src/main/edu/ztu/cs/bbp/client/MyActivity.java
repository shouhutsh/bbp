package main.edu.ztu.cs.bbp.client;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import edu.zut.cs.bbp.client.R;
import main.edu.ztu.cs.bbp.client.UI.AddEntity.addButton;
import main.edu.ztu.cs.bbp.client.UI.User.userLogin;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.service.impl.NetServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class MyActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
		setContentView(R.layout.main);
		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titlebtn);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (User.getU_id() != null) {
			final List<SQLiteTool> buttons = new ArrayList<SQLiteTool>();
			// TODO 显示用户设定的按钮

			ListView listView = (ListView) findViewById(R.id.listview);
			final ArrayAdapter<SQLiteTool> adapter = new ArrayAdapter<SQLiteTool>(this, android.R.layout.simple_list_item_1, buttons);
			listView.setAdapter(adapter);


			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

					// TODO 短按 添加事件

				}
			});

			listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
				@Override
				public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

					// TODO 长按 查看详情

					return true;
				}
			});

		}
	}


	public void toLoginButton(View v){
		Log.i(null, "ToLogin Button");

		Intent intent = new Intent();

		intent.setClass(getApplicationContext(), userLogin.class);

		startActivity(intent);
	}

	public void addButton(View v){
		Log.i(null, "Add Button");

		// FIXME 测试界面时暂不需要用户登录
		User.setU_id("1");
		User.setName("1");
		User.setPassword("1");
//		if (User.getU_id() == null) {
//			Toast.makeText(getApplicationContext(), R.string.W_Login, Toast.LENGTH_SHORT).show();
//			return;
//		}

		Intent intent = new Intent();

		intent.setClass(getApplicationContext(), addButton.class);

		startActivity(intent);
	}

	public void syncButton(View v){
		Log.i(null, "Sync Button");

		// FIXME 待考虑：同服务器交互时可能有异常，而且同步之后可能需要做一些处理

		if (User.getU_id() == null) {
			Toast.makeText(getApplicationContext(), R.string.W_Login, Toast.LENGTH_SHORT).show();
			return;
		}

		NetServiceImpl netServiceImpl = new NetServiceImpl(getApplicationContext());
		//netServiceImpl.sync();
		Toast.makeText(getApplicationContext(), R.string.S_Sync, Toast.LENGTH_SHORT).show();
	}
}

package main.edu.ztu.cs.bbp.client.UI.User;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import edu.zut.cs.bbp.client.R;

/**
 * Created with IntelliJ IDEA.
 * User: Qi_2
 * Date: 13-12-31
 * Time: 上午11:16
 * To change this template use File | Settings | File Templates.
 */
public class userLogin extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		// TODO
	}

	public void loginButton(View v){
		Log.i("CLICK", "on click login button");
		// TODO 调用 service的 login 函数，同服务器验证
	}

	public void toRegisterButton(View v){
		Log.i("CLICK", "on click toRegister button");

		Intent intent = new Intent(this, userRegister.class);
		startActivity(intent);
	}
}

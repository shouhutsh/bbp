package main.edu.ztu.cs.bbp.client.UI.AddEntity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import edu.zut.cs.bbp.client.R;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.dao.impl.FirstDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.First;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-28
 * Time: 下午3:17
 * To change this template use File | Settings | File Templates.
 */
public class firstActivity extends Activity {

	private List<String> list = new ArrayList<String>();
	private Spinner mySpinner;
	private ArrayAdapter<String> adapter;
	private EditText editText;
	private String i,str1,str2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first);

		list.add(getString(R.string.RI));
		list.add(getString(R.string.RS));

		mySpinner = (Spinner) findViewById(R.id.spinner);
		//第二步：为下拉列表定义一个适配器，这里就用到里前面定义的list。
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
		//第三步：为适配器设置下拉列表下拉时的菜单样式。
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		//第四步：将适配器添加到下拉列表上
		mySpinner.setAdapter(adapter);
		//第五步：为下拉列表设置各种事件的响应，这个事响应菜单被选中
		mySpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@SuppressWarnings("unchecked")
			public void onItemSelected(AdapterView arg0, View arg1, int arg2, long arg3) {
				str2 = adapter.getItem(arg2).toString();
				                /* 将mySpinner 显示*/
				arg0.setVisibility(View.VISIBLE);
			}

			@SuppressWarnings("unchecked")
			public void onNothingSelected(AdapterView arg0) {
				// TODO Auto-generated method stub

				arg0.setVisibility(View.VISIBLE);
			}
		});
	        /*下拉菜单弹出的内容选项触屏事件处理*/
		mySpinner.setOnTouchListener(new Spinner.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				                /* 将mySpinner 隐藏，不隐藏也可以，看自己爱好*/
				v.setVisibility(View.INVISIBLE);
				return false;
			}
		});
		        /*下拉菜单弹出的内容选项焦点改变事件处理*/
		mySpinner.setOnFocusChangeListener(new Spinner.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				v.setVisibility(View.VISIBLE);
			}
		});
	}

	public void firstAddButton(View v){
		Log.i("CLICK", "first add button");

		// TODO 将 新的 first 写入数据库

		editText = (EditText) findViewById(R.id.editText);
		str1 = editText.getText().toString();


		if (!str1.equals("")) {

			if (str2 == getString(R.string.RI)) {
				i = "0";
			} else if (str2 == getString(R.string.RS)) {
				i = "1";
			}


			First first = new First();
			FirstDao helper = new FirstDao(getApplicationContext());
			first.setU_id(User.getU_id());
			first.setName(str1);
			first.setType(i);
			first.setState("1");

			helper.save(first, "1");
			helper.close();
			Toast.makeText(getApplicationContext(),"事件添加成功！",Toast.LENGTH_SHORT).show();
			finish();
		}else {
			Toast.makeText(getApplicationContext(),"事件不能为空！",Toast.LENGTH_SHORT).show();
		}
	}

}

package main.edu.ztu.cs.bbp.client.UI.AddEntity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
//import edu.zut.cs.bbp.client.R;
import edu.zut.cs.bbp.client.R;
import main.edu.ztu.cs.bbp.client.dao.impl.SecondDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.impl.Second;
//import main.edu.ztu.cs.bbp.client.domain.impl.User;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-29
 * Time: 下午3:58
 * To change this template use File | Settings | File Templates.
 */
public class secondActivity extends Activity {

	private List<String> list = new ArrayList<String>();
	private EditText editText;
	private String str1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);

		// TODO
	}

	public void secondAddButton(View v){
		Log.i("CLICK", "second add button");

		// TODO 将 新的 second 写入数据库
		editText = (EditText) findViewById(R.id.editText);
		str1 = editText.getText().toString();

		Second second = new Second();
		SecondDao helper = new SecondDao(getApplicationContext());
		//			second.setU_id(User.getU_id());
		second.setName(str1);
		helper.save(second, "1");
		helper.close();

		if (!str1.equals("")) {

			Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
			finish();
		} else {
			Toast.makeText(getApplicationContext(), "事件不能为空！", Toast.LENGTH_SHORT).show();
		}


	}
}

package main.edu.ztu.cs.bbp.client.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import main.edu.ztu.cs.bbp.client.dao.SQLiteDatabaseDao;
import main.edu.ztu.cs.bbp.client.domain.SQLiteTool;
import main.edu.ztu.cs.bbp.client.domain.User;
import main.edu.ztu.cs.bbp.client.domain.impl.Second;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-4-19
 * Time: 上午10:06
 * To change this template use File | Settings | File Templates.
 */
public class SecondDao extends SQLiteDatabaseDao {

	private final static String where="SELECT * FROM second WHERE u_id=? AND state=?";

	public SecondDao(Context context) {
		super(context);
		setTB_Name("second");
	}

	public void save(SQLiteTool sql,String newState){
		Second s = (Second) sql;
		ContentValues contentValues=new ContentValues();
		contentValues.put(s.F_ID,s.getF_id());
		contentValues.put(s.U_ID,s.getU_id());
		contentValues.put(s.NAME,s.getName());

		if (s.isHaveSync()){
			this.remove(s);

			s.setState(newState);//0：已同步		1：未同步
			contentValues.put(s.STATE, s.getState());

			insert(contentValues);

			s.setF_id(last_insert_rowid());
		} else if (s.isNeedSync()) {
			s.setState(newState);
			contentValues.put(s.STATE, s.getState());

			if (s.isOldData()) {
				update(contentValues, s.S_ID + "=?", new String[]{s.getS_id()});
			} else {
				insert(contentValues);
				s.setF_id(last_insert_rowid());
			}
		}
	}

	public void remove(SQLiteTool sql){
		Second s = (Second) sql;
		if (s.isHaveSync()){
			ContentValues contentValues=new ContentValues();
			contentValues.put(s.STATE,"2");        //2 代表D，意思是需要被删除

			update(contentValues,s.S_ID+"=?",new String[]{s.getS_id()});
		}else if(s.isNeedSync()){
			if(s.getS_id()!=null){
				delete(s.S_ID+"=?",new String[]{s.getS_id()});
			}
		}
	}

	public Second getOne(String id){
		Second s = new Second();
		Cursor c = select(Second.S_ID+"=?", new String[]{id});
		if(c.moveToFirst()){
			s.valueOf(c);
		}
		c.close();
		return s;
	}

	@Override
	public Cursor getAll(String state) {
		String[] columns=new String[]{User.getU_id(),state};

		Cursor c=select(where,columns);
		return c;
	}

	@Override
	public void simpleSave(SQLiteTool s) {
		Second second=(Second)s;

		ContentValues contentValues=new ContentValues();
		contentValues.put(second.S_ID,second.getS_id());
		contentValues.put(second.F_ID,second.getF_id());
		contentValues.put(second.U_ID,second.getU_id());
		contentValues.put(second.NAME,second.getName());
		contentValues.put(second.STATE,second.getState());

		insert(contentValues);
	}

}

package edu.zut.cs.bbp.creater;

import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public class DaoGenerator {

    /**
     *
     *
     * 使用GreentDao创建android端数据库
     *      依据《domain（一修）》
     *
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "edu.zut.cs.bbp.main");

        // 添加基础实体
        Entity user = addUser(schema);
        Entity event= addEvent(schema, user);
        // 添加依赖User的实体组
        Entity diary= addDiary(schema, user);
        Entity first= addFirst(schema, user);
        Entity first2Diary = addFirst2Diary(schema, user, first, diary);
        Entity intellect   = addIntellect(schema, user);
        Entity media = addMedia(schema, user, diary);
        Entity milestone   = addMilestone(schema, user);
        Entity mile2Diary  = addMile2Diary(schema, user, milestone, diary);
        // 添加依赖Event的实体组
        addBB(schema, user, event);
        addDrink(schema, user, event);
        addEat(schema, user, event);
        addHeight(schema, user, event);
        addNN(schema, user, event);
        addSleep(schema, user, event);
        addWeight(schema, user, event);

        new de.greenrobot.daogenerator.DaoGenerator()
                .generateAll(schema,"../client/src-gen");
    }

    /**
     * 基础创建基本条目
     *
     * @param entity
     */
    private static void base(Entity entity){
        entity.addLongProperty("id").primaryKey().autoincrement();
        entity.addLongProperty("dateCreated");
        entity.addLongProperty("dateModified");
        entity.addBooleanProperty("deleted");
    }

    private static void baseUser(Entity entity, Entity user){
        entity.addStringProperty("id").primaryKey().notNull();
        entity.addLongProperty("dateCreated");
        entity.addLongProperty("dateModified");
        entity.addBooleanProperty("deleted");
        entity.addIntProperty("state");

        Property userId = entity.addLongProperty("userId").getProperty();
        entity.addToOne(user, userId);
    }

    private static void baseEvent(Entity entity, Entity user, Entity event){
        baseUser(entity, user);

        Property eventId = entity.addStringProperty("eventId").getProperty();
        entity.addToOne(event, eventId);

        entity.addLongProperty("time");
        entity.addStringProperty("remark");
    }

    /**
     * 基础实体User、Event
     *
     * @param schema
     * @return
     */
    private static Entity addUser(Schema schema) {
        Entity user = schema.addEntity("User");

        base(user);

        user.addStringProperty("nickname");
        user.addStringProperty("name").notNull();
        user.addStringProperty("password").notNull();
        user.addLongProperty("birthday");
        user.addLongProperty("lastUpload");

        return user;
    }

    private static Entity addEvent(Schema schema, Entity user) {
        Entity event = schema.addEntity("Event");

        baseUser(event, user);

        event.addStringProperty("name").notNull();
        event.addStringProperty("unit");

        // 建立父子关系
        Property parentId = event.addStringProperty("parentId").getProperty();
        event.addToOne(event, parentId).setName("parent");
//        event.addToMany(event, parentId).setName("childrens");

        return event;
    }

    /**
     * 基于User的实体
     *
     * @param schema
     * @param user
     * @return
     */
    private static Entity addDiary(Schema schema, Entity user){
        Entity diary = schema.addEntity("Diary");

        baseUser(diary, user);

        diary.addStringProperty("title");
        diary.addStringProperty("content");

        return diary;
    }

    private static Entity addFirst(Schema schema, Entity user){
        Entity first = schema.addEntity("First");

        baseUser(first, user);

        first.addStringProperty("name");

        return first;
    }

    private static Entity addFirst2Diary(Schema schema, Entity user, Entity first, Entity diary){
        Entity first2Diary = schema.addEntity("First2Diary");

        baseUser(first2Diary, user);

        Property firstId = first2Diary.addStringProperty("firstId").getProperty();
        first2Diary.addToOne(first, firstId);
        Property diaryId = first2Diary.addStringProperty("diaryId").getProperty();
        first2Diary.addToOne(diary, diaryId);

        first2Diary.addLongProperty("time");

        return first2Diary;
    }

    private static Entity addIntellect(Schema schema, Entity user){
        Entity intellect = schema.addEntity("Intellect");

        baseUser(intellect, user);

        intellect.addStringProperty("name");
        intellect.addLongProperty("suitable");
        intellect.addBooleanProperty("complete");
        intellect.addLongProperty("time");

        return intellect;
    }

    private static Entity addMedia(Schema schema, Entity user, Entity diary){
        Entity media = schema.addEntity("Media");

        baseUser(media, user);

        Property diaryId = media.addStringProperty("diaryId").getProperty();
        media.addToOne(diary, diaryId);

        media.addStringProperty("path");

        // 建立与Diary的一对多关系
        diary.addToMany(media, diaryId).setName("mediaSet");

        return media;
    }

    private static Entity addMilestone(Schema schema, Entity user){
        Entity milestone = schema.addEntity("Milestone");

        baseUser(milestone, user);

        milestone.addStringProperty("name");

        return milestone;
    }

    private static Entity addMile2Diary(Schema schema, Entity user, Entity milestone, Entity diary){
        Entity mile2Diary = schema.addEntity("Mile2Diary");

        baseUser(mile2Diary, user);

        Property mileId = mile2Diary.addStringProperty("mileId").getProperty();
        mile2Diary.addToOne(milestone, mileId);
        Property diaryId= mile2Diary.addStringProperty("diaryId").getProperty();
        mile2Diary.addToOne(diary, diaryId);

        mile2Diary.addLongProperty("time");

        return mile2Diary;
    }

    /**
     * 基于Event实体
     *
     * @param schema
     * @param user
     * @param event
     * @return
     */
    private static Entity addBB(Schema schema, Entity user, Entity event){
        Entity BB = schema.addEntity("BB");

        baseEvent(BB, user, event);

        return BB;
    }

    private static Entity addDrink(Schema schema, Entity user, Entity event){
        Entity drink = schema.addEntity("Drink");

        baseEvent(drink, user, event);

        drink.addLongProperty("amount");

        return drink;
    }

    private static Entity addEat(Schema schema, Entity user, Entity event){
        Entity eat = schema.addEntity("Eat");

        baseEvent(eat, user, event);

        eat.addLongProperty("amount");

        return eat;
    }

    private static Entity addHeight(Schema schema, Entity user, Entity event){
        Entity height = schema.addEntity("Height");

        baseEvent(height, user, event);

        height.addLongProperty("height");

        return height;
    }

    private static Entity addNN(Schema schema, Entity user, Entity event){
        Entity NN = schema.addEntity("NN");

        baseEvent(NN, user, event);

        return NN;
    }

    private static Entity addSleep(Schema schema, Entity user, Entity event){
        Entity sleep = schema.addEntity("Sleep");

        baseEvent(sleep, user, event);

        sleep.addLongProperty("endTime");

        return sleep;
    }

    private static Entity addWeight(Schema schema, Entity user, Entity event){
        Entity weight = schema.addEntity("Weight");

        baseEvent(weight, user, event);

        weight.addFloatProperty("weight");

        return weight;
    }
}

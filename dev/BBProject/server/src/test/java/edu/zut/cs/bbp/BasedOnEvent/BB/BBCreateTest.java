package edu.zut.cs.bbp.BasedOnEvent.BB;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnEvent.BB.domain.BB;
import edu.zut.cs.bbp.BasedOnEvent.BB.service.BBManager;
import edu.zut.cs.bbp.BasedOnEvent.drink.domain.Drink;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class BBCreateTest {


    @Autowired
    BBManager bbManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Event e = new Event();
        e.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            BB bb = new BB();
            bb.setId(IdCreateTest.getId(null));
            bb.setUser(u);
            bb.setEvent(e);

            this.bbManager.save(bb);
        }
    }

}

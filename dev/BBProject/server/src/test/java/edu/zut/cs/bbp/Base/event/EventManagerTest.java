package edu.zut.cs.bbp.Base.event;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.event.service.EventManager;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class EventManagerTest {

    @Autowired
    EventManager eventManager;
    ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() {

    }

//    @Test
//    public void getChildren(){
//        List<Event> events = eventManager.getChildrens("14034983087470000000000001000000", 1L);
//        assert (null != events);
//    }

    @Test
    public void test() throws IOException {
//        Event e = eventManager.findById("14040446195930000000000001000000");
//
//        String s = mapper.writeValueAsString(e);

        Event e;
        String s = "{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":\"12\",\"name\":\"THIRD_2\",\"parent\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":\"7\",\"name\":\"SECOND_2\",\"parent\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":\"2\",\"name\":\"FIRST_2\",\"parent\":null,\"state\":1,\"unit\":null,\"user\":{\"birthday\":null,\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"lastUpload\":null,\"name\":\"Name_0\",\"nickname\":null,\"password\":\"Password_0\"}},\"state\":1,\"unit\":null,\"user\":{\"birthday\":null,\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"lastUpload\":null,\"name\":\"Name_0\",\"nickname\":null,\"password\":\"Password_0\"}},\"state\":1,\"unit\":null,\"user\":{\"birthday\":null,\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"lastUpload\":null,\"name\":\"Name_0\",\"nickname\":null,\"password\":\"Password_0\"}}";

        e = mapper.readValue(s, Event.class);

        System.out.println(s);
    }
}

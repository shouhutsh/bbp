package edu.zut.cs.bbp.BasedOnUser.first;

import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.first.service.FirstManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class FirstCreateTest {


    @Autowired
    FirstManager firstManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);

        int num = 10;
        for(int i = 1; i < num; ++i){
            First f = new First();
            f.setId(IdCreateTest.getId(null));
            f.setUser(u);
            f.setName("NAME_" + i);

            this.firstManager.save(f);
        }
    }

}

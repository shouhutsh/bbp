package edu.zut.cs.bbp.Base.event;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.event.web.spring.controller.EventController;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml",
        "classpath:/applicationContext-controller.xml"})
public class EventControllerTest {

    @Autowired
    EventController eventController;

    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    RestTemplate template = new RestTemplate();

    @Before
    public void setUp(){
        request = new MockHttpServletRequest();
        request.setCharacterEncoding("UTF-8");
        response = new MockHttpServletResponse();
    }

    /*@Test
    public void testGetTree(){
        String id = "0";
        String uid= "1";
        List<Event> events;

        while(id != null) {
            request.setParameter("id", id);
            request.setParameter("uid", uid);
            request.setMethod(HttpMethod.GET.name());

            events = eventController.getChildrens(request, response);

            if(events.size() == 0)
                break;

            id = events.get(0).getId()+"";

            assert (null != id);
        }
    }*/
    @Test
    public void testGetTree(){
        String id = "14034983087470000000000001000000";
        String uid= "1";
        List<Event> events;

        while(id != null) {
            request.setParameter("id", id);
            request.setParameter("uid", uid);
            request.setMethod(HttpMethod.GET.name());

            events = eventController.getChildren(request, response);

            if(events.size() == 0)
                break;

            System.out.print(events);
        }
    }
    @Test
    public void saveSecond() throws UnsupportedEncodingException {
        String id = "14034983087470000000000001000000";
        String name = "吃饭";
        request.setParameter("parentId", id);
        request.setParameter("name", name);
        request.setMethod(HttpMethod.GET.name());
        //assert(eventController.addChildren(request, response)!=null);
    }
}

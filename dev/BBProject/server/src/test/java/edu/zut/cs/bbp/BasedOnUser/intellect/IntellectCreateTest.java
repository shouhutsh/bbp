package edu.zut.cs.bbp.BasedOnUser.intellect;

import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.intellect.dao.IntellectDao;
import edu.zut.cs.bbp.BasedOnUser.intellect.domain.Intellect;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class IntellectCreateTest {


    @Autowired
    IntellectDao intellectDao;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);

        int num = 10;
        for(int i = 1; i < num; ++i){
            Intellect in = new Intellect();
            in.setId(IdCreateTest.getId(null));
            in.setUser(u);
            in.setName("INTELLECT_" + i);
            in.setSuitable(1L);

            this.intellectDao.save(in);
        }
    }

}

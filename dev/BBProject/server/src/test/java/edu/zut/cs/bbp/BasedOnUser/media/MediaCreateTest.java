package edu.zut.cs.bbp.BasedOnUser.media;

import edu.zut.cs.bbp.Base.base.domain.State;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.media.domain.Media;
import edu.zut.cs.bbp.BasedOnUser.media.service.MediaManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class MediaCreateTest {


    @Autowired
    MediaManager mediaManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Diary d = new Diary();
        d.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            Media m = new Media();
            m.setId(IdCreateTest.getId(null));
            m.setUser(u);
            m.setDiary(d);
            m.setState(State.CLEAN);
            m.setPath("PATH_" + i);

            this.mediaManager.save(m);
        }
    }

}

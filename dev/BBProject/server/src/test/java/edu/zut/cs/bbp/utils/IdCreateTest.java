package edu.zut.cs.bbp.utils;

import edu.zut.cs.bbp.Base.user.domain.User;
import org.junit.Test;

/**
 * Created by Qi_2 on 2014/6/23 0023.
 */
public final class IdCreateTest {

    public static String getId(String uid){
        StringBuilder id = new StringBuilder(32);


        char[] userId = new char[13];
        for(int i = 0; i < userId.length; ++i)
            userId[i] = '0';

        if(null != uid)
            uid.getChars(0, uid.length(), userId, 13-uid.length());

        id.append(String.valueOf(System.currentTimeMillis()).substring(0, 13));
        id.append(userId);
        id.append("00");
        id.append("0000");

        return id.toString();
    }
}

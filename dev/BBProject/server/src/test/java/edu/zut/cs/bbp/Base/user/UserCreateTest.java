package edu.zut.cs.bbp.Base.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.Base.user.service.UserManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class UserCreateTest {


    @Autowired
    UserManager userManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        int num = 10;
        for(int i = 1; i < num; ++i){
            User u = new User();
            u.setName("NAME_"+i);
            u.setPassword("PASSWORD_"+i);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(System.out, u);
            this.userManager.save(u);
        }
    }

}

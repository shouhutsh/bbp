package edu.zut.cs.bbp.BasedOnEvent.drink;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnEvent.drink.domain.Drink;
import edu.zut.cs.bbp.BasedOnEvent.drink.service.DrinkManager;
import edu.zut.cs.bbp.BasedOnEvent.eat.domain.Eat;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class DrinkCreateTest {


    @Autowired
    DrinkManager drinkManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Event e = new Event();
        e.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            Drink d = new Drink();
            d.setId(IdCreateTest.getId(null));
            d.setUser(u);
            d.setEvent(e);
            d.setAmount(i * 100L);

            this.drinkManager.save(d);
        }
    }

}

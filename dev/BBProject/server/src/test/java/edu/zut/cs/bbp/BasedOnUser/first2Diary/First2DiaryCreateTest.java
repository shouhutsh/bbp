package edu.zut.cs.bbp.BasedOnUser.first2Diary;

import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.first.service.FirstManager;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.domain.First2Diary;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.service.First2DiaryManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Administrator on 2014/6/24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class First2DiaryCreateTest {

    @Autowired
    First2DiaryManager first2DiaryManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        First f = new First();
        f.setId("1");
        Diary d = new Diary();
        d.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            First2Diary fd = new First2Diary();
            fd.setId(IdCreateTest.getId(null));
            fd.setUser(u);

            fd.setFirst(f);
            fd.setDiary(d);

            this.first2DiaryManager.save(fd);
        }
    }
}

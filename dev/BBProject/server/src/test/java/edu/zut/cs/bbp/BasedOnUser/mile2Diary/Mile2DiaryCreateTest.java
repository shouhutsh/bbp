package edu.zut.cs.bbp.BasedOnUser.mile2Diary;

import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.domain.First2Diary;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.domain.Mile2Diary;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.service.Mile2DiaryManager;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Administrator on 2014/6/24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class Mile2DiaryCreateTest {

    @Autowired
    Mile2DiaryManager mile2DiaryManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Milestone m = new Milestone();
        m.setId("1");
        Diary d = new Diary();
        d.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            Mile2Diary fd = new Mile2Diary();
            fd.setId(IdCreateTest.getId(null));
            fd.setUser(u);

            fd.setMilestone(m);
            fd.setDiary(d);

            this.mile2DiaryManager.save(fd);
        }
    }
}

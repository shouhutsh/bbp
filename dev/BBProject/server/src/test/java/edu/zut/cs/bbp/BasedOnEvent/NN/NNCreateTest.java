package edu.zut.cs.bbp.BasedOnEvent.NN;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnEvent.BB.domain.BB;
import edu.zut.cs.bbp.BasedOnEvent.NN.domain.NN;
import edu.zut.cs.bbp.BasedOnEvent.NN.service.NNManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class NNCreateTest {


    @Autowired
    NNManager nnManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Event e = new Event();
        e.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            NN nn = new NN();
            nn.setId(IdCreateTest.getId(null));
            nn.setUser(u);
            nn.setEvent(e);

            this.nnManager.save(nn);
        }
    }

}

package edu.zut.cs.bbp.BasedOnEvent.weight;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnEvent.height.domain.Height;
import edu.zut.cs.bbp.BasedOnEvent.weight.domain.Weight;
import edu.zut.cs.bbp.BasedOnEvent.weight.service.WeightManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class WeightCreateTest {


    @Autowired
    WeightManager weightManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Event e = new Event();
        e.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            Weight w = new Weight();
            w.setId(IdCreateTest.getId(null));
            w.setUser(u);
            w.setEvent(e);
            w.setWeight(i * 5.5F);

            this.weightManager.save(w);
        }
    }

}

package edu.zut.cs.bbp.utils;

import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.Base.base.domain.State;
import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.user.domain.User;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Administrator on 2014/7/1.
 */
public class JacksonTest {

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void test() throws IOException {
        Event[] es;

//        String str = "[{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":\"6\",\"name\":\"SECOND_1\",\"state\":1,\"unit\":null,\"user\":{\"birthday\":null,\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"lastUpload\":null,\"name\":\"Name_0\",\"nickname\":null,\"password\":\"Password_0\"},\"parent\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":\"1\",\"name\":\"FIRST_1\",\"state\":1,\"unit\":null,\"user\":{\"birthday\":null,\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"lastUpload\":null,\"name\":\"Name_0\",\"nickname\":null,\"password\":\"Password_0\"},\"parent\":null}}]";
//        es = mapper.readValue(str, Event[].class);

//        Event p = new Event();
//        p.setId("Parent");
//        Event e = new Event();
//        e.setId("EVENT");
//        e.setName("NAME");
//        e.setParent(e);
//
//        String s = mapper.writeValueAsString(e);
//
//        assert(null != s);

        String s = "{\"content\":[],\"size\":100,\"number\":99,\"sort\":[{\"direction\":\"ASC\",\"property\":\"id\",\"ignoreCase\":false,\"ascending\":true}],\"numberOfElements\":0,\"totalElements\":35,\"totalPages\":1,\"firstPage\":false,\"lastPage\":true}";

        JsonNode node = mapper.readTree(s);
        es = mapper.readValue(node.get("content"), Event[].class);

    }
}

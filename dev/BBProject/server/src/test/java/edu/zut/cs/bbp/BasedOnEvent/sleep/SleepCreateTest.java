package edu.zut.cs.bbp.BasedOnEvent.sleep;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnEvent.eat.domain.Eat;
import edu.zut.cs.bbp.BasedOnEvent.sleep.domain.Sleep;
import edu.zut.cs.bbp.BasedOnEvent.sleep.service.SleepManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class SleepCreateTest {


    @Autowired
    SleepManager sleepManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        Event e = new Event();
        e.setId("1");

        int num = 10;
        for(int i = 1; i < num; ++i){
            Eat eat = new Eat();
            Sleep s = new Sleep();
            s.setId(IdCreateTest.getId(null));
            s.setUser(u);
            s.setEvent(e);
            s.setEndTime(new Date());

            this.sleepManager.save(s);
        }
    }

}

package edu.zut.cs.bbp.BasedOnUser.milestone;

import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.service.Mile2DiaryManager;
import edu.zut.cs.bbp.BasedOnUser.milestone.dao.MilestoneDao;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;
import edu.zut.cs.bbp.BasedOnUser.milestone.service.MilestoneManager;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class MilestoneCreateTest {


    @Autowired
    MilestoneManager milestoneManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);

        int num = 10;
        for(int i = 1; i < num; ++i){
            Milestone m = new Milestone();
            m.setId(IdCreateTest.getId(null));
            m.setUser(u);
            m.setName("NAME_" + i);

            this.milestoneManager.save(m);
        }
    }

}

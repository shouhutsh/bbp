package edu.zut.cs.bbp.BasedOnUser.diary;

import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.diary.service.DiaryManager;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

/**
 * Created by Qi_2 on 2014/5/13 0013.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class DiaryCreateTest {


    @Autowired
    DiaryManager diaryManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate() throws IOException {
        User u = new User();
        u.setId(1L);
        int num = 10;
        for(int i = 1; i < num; ++i){
            Diary d = new Diary();
            First first = new First();
            first.setName("first"+i);
            Milestone milestone = new Milestone();
            milestone.setName("milestone"+i);
            d.setId(IdCreateTest.getId(null));
            d.setUser(u);
            d.setTitle("TITLE_" + i);

            this.diaryManager.save(d);
        }
    }
}

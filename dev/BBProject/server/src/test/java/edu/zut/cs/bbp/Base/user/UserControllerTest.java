package edu.zut.cs.bbp.Base.user;

import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.Base.user.web.spring.controller.UserController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import scala.util.parsing.json.JSONArray;
import scala.util.parsing.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by ZL on 14-6-24.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml",
        "classpath:/applicationContext-controller.xml"})
public class UserControllerTest {
    @Autowired
    UserController userController;

    private static MockHttpServletRequest request;
    private static MockHttpServletResponse response;

    RestTemplate template = new RestTemplate();

    @Before
    public void setUp(){
        request = new MockHttpServletRequest();
        request.setCharacterEncoding("UTF-8");
        response = new MockHttpServletResponse();
    }
   // @Test
   /* public void register()
    {
        String s = "{\"id\":null,\"name\":\"NAME_1\",\"password\":\"PASSWORD_1\"}";

        request.setContent(s.getBytes());
        userController.register(request,response);
    }*/
}

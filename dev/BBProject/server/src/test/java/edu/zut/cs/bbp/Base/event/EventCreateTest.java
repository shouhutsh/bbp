package edu.zut.cs.bbp.Base.event;

import edu.zut.cs.bbp.Base.base.domain.State;
import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.event.service.EventManager;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.utils.IdCreateTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-dao.xml",
        "classpath:/applicationContext-service.xml" })
public class EventCreateTest {

    @Autowired
    EventManager eventManager;

    @Before
    public void setUp() {

    }

    @Test
    public void testGenerate(){
        User u = new User();
        u.setId(1L);

        int num = 5;
        for(int i = 1; i < num; ++i){
            Event e = new Event();
            e.setId(IdCreateTest.getId("1"));
            e.setUser(u);
            e.setName("NAME_" + i);
            e.setState(State.NEW);

            this.eventManager.save(e);
        }
    }
}

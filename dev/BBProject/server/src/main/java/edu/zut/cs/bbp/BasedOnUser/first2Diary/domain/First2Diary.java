package edu.zut.cs.bbp.BasedOnUser.first2Diary.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2014/6/24.
 */
@Entity
@Table(name = "T_First2Diary")
public class First2Diary extends BaseUserEntityDomain {

    @ManyToOne(cascade = { CascadeType.REMOVE })
    @JoinColumn(name = "First_Id", nullable = false)
    private First first;

    @ManyToOne(cascade = { CascadeType.REMOVE })
    @JoinColumn(name = "Diary_Id", nullable = false)
    private Diary diary;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Time", nullable = false)
    private Date time = new Date();


    public First getFirst() {
        return first;
    }

    public void setFirst(First first) {
        this.first = first;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}

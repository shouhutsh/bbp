package edu.zut.cs.bbp.BasedOnEvent.eat.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.eat.dao.EatDao;
import edu.zut.cs.bbp.BasedOnEvent.eat.domain.Eat;
import edu.zut.cs.bbp.BasedOnEvent.eat.service.EatManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("eatManager")
public class EatManagerImpl extends BaseUserManagerImpl<Eat, String>
            implements EatManager{

    EatDao eatDao;

    @Autowired
    public void setEatDao(EatDao eatDao) {
        this.eatDao = eatDao;
        this.dao = this.eatDao;
    }
}

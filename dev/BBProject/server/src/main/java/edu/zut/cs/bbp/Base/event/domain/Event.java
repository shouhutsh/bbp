package edu.zut.cs.bbp.Base.event.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;

import javax.persistence.*;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
@Entity
@Table(name = "T_Event")
public class Event extends BaseUserEntityDomain {

    @Column(name = "Name", nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "PARENT_ID")
    Event parent;

    @Column(name = "Unit")
    private String unit;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Event getParent() {
        return parent;
    }

    public void setParent(Event parent) {
        this.parent = parent;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

}

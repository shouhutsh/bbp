package edu.zut.cs.bbp.Base.base.web.spring.controller;

import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.Base.user.domain.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/6/23 0023.
 */
public class BaseUserEntityController<T extends BaseUserEntityDomain, PK extends Serializable, M extends BaseUserManager<T, PK>>
        extends MultiActionController {
    protected M manager;
    protected PK id;
    protected T model;
    protected Page<T> page;

    protected int pageNumber = 0;
    protected int pageSize = 20;

    protected Pageable pageable = new PageRequest(pageNumber, pageSize,
            new Sort(Sort.Direction.ASC, "id"));

    /**
     *
     * @param model
     * @return
     *
     * 设置唯一主键ID
     *  主键ID为32位字符串
     *      前13位为时间戳（精确到毫秒）
     *      再13位为用户ID（理论支持亿亿的用户）
     *      再 2位为终端类型（0--WEB，1--Android...理论支持100种设备）
     *      再 4位暂不使用
     *
     *      则理论上此主键ID能保证亿亿个用户同时使用100种设备在毫秒级的精度上不冲突
     */
    public T create(T model) {
        if(null==model.getId() || "0".equals(model.getId())){
            StringBuilder id = new StringBuilder(32);

            char[] userId = new char[13];
            for(int i = 0; i < userId.length; ++i) userId[i] = '0';
            User u = model.getUser();
            if(null != u.getId()){
                String uid = String.valueOf(u.getId());
                uid.getChars(0, uid.length(), userId, 13-uid.length());
            }

            id.append(String.valueOf(System.currentTimeMillis()).substring(0, 13));
            id.append(userId);
            id.append("00");
            id.append("0000");

            model.setId(id.toString());
        }
        this.model = this.manager.save(model);
        return this.model;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public void delete(@PathVariable String id) throws IOException {
        model = manager.findById((PK) id);
        model.setDeleted(true);
        this.manager.save(model);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Page<T> get(HttpServletRequest request, HttpServletResponse response) {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        if (StringUtils.isNotBlank(page)) {
            this.pageNumber = Integer.valueOf(page) - 1;
        } else {
            this.pageNumber = 0;
        }
        if (StringUtils.isNotBlank(limit)) {
            this.pageSize = Integer.valueOf(limit);
        }
        this.pageable = new PageRequest(this.pageNumber, this.pageSize,
                new Sort(Sort.Direction.ASC, "id"));
        this.page = this.manager.findAll(this.pageable);
        logger.info(this.page);
        return this.page;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public T get(@PathVariable String id) {
        this.model = this.manager.findById((PK) id);
        return this.model;
    }

    public T update(String id, T model) {
        model.setId(id);
        model.setDateModified(new Date());
        this.model = this.manager.save(model);
        return this.model;
    }

}

package edu.zut.cs.bbp.BasedOnEvent.BB.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.BB.dao.BBDao;
import edu.zut.cs.bbp.BasedOnEvent.BB.domain.BB;
import edu.zut.cs.bbp.BasedOnEvent.BB.service.BBManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("bbManager")
public class BBManagerImpl extends BaseUserManagerImpl<BB, String>
            implements BBManager {

    BBDao bbDao;

    @Autowired
    public void setBbDao(BBDao bbDao) {
        this.bbDao = bbDao;
        this.dao = this.bbDao;
    }
}

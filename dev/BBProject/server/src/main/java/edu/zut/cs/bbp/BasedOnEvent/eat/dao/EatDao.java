package edu.zut.cs.bbp.BasedOnEvent.eat.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnEvent.eat.domain.Eat;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface EatDao extends BaseUserDao<Eat, String> {

}

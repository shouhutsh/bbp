package edu.zut.cs.bbp.BasedOnEvent.drink.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnEvent.drink.domain.Drink;
import edu.zut.cs.bbp.BasedOnEvent.drink.service.DrinkManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/drink")
public class DrinkController extends
        BaseUserEntityController<Drink, String, DrinkManager> {

    DrinkManager drinkManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setDrinkManager(DrinkManager drinkManager) {
        this.drinkManager = drinkManager;
        this.manager = this.drinkManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Drink create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Drink.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Drink update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Drink.class));
    }
}

package edu.zut.cs.bbp.BasedOnUser.first2Diary.service;

import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.domain.First2Diary;

/**
 * Created by Administrator on 2014/6/24.
 */
public interface First2DiaryManager extends BaseUserManager<First2Diary, String> {
}

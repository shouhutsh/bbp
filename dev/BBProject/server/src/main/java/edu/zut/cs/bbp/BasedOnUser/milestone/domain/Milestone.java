package edu.zut.cs.bbp.BasedOnUser.milestone.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEntityDomain;
import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Milestone")
public class Milestone extends BaseUserEntityDomain {

    @Column(name = "Name", nullable = false)
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

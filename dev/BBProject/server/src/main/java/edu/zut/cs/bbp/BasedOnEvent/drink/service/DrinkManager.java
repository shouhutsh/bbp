package edu.zut.cs.bbp.BasedOnEvent.drink.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnEvent.drink.domain.Drink;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface DrinkManager extends BaseUserManager<Drink, String> {

}

package edu.zut.cs.bbp.Base.event.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.Base.event.domain.Event;

import java.util.List;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
public interface EventManager extends BaseUserManager<Event, String> {
    public List<Event> getRoot();
    public List<Event> getChildrens(String id, Long uid);
}

package edu.zut.cs.bbp.BasedOnEvent.height.web.spring.controller;

import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.BasedOnEvent.eat.domain.Eat;
import edu.zut.cs.bbp.BasedOnEvent.eat.service.EatManager;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;

import edu.zut.cs.bbp.BasedOnEvent.height.domain.Height;
import edu.zut.cs.bbp.BasedOnEvent.height.service.HeightManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/height")
public class HeightController extends
        BaseUserEntityController<Height, String, HeightManager> {

    HeightManager heightManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setHeightManager(HeightManager heightManager) {
        this.heightManager = heightManager;
        this.manager = this.heightManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Height create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Height.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Height update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Height.class));
    }
    @RequestMapping(method = RequestMethod.GET, value = "/heightChart.html")
    public String heightChart(HttpServletRequest request, HttpServletResponse response){
        return "baseOnEvent/heightChart";
    }
    @ResponseBody
    @RequestMapping(value = "/getHeightData.json", method = RequestMethod.GET, produces = "application/json")
    public List<Height> getHeightData(HttpServletRequest request,
                                    HttpServletResponse response) {
        List<Height> result = new ArrayList<>();
        result = heightManager.findAll();
        return result;
    }
}

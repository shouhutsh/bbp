package edu.zut.cs.bbp.BasedOnEvent.weight.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.weight.dao.WeightDao;
import edu.zut.cs.bbp.BasedOnEvent.weight.domain.Weight;
import edu.zut.cs.bbp.BasedOnEvent.weight.service.WeightManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("weightManager")
public class WeightManagerImpl extends BaseUserManagerImpl<Weight, String>
            implements WeightManager{

    WeightDao weightDao;

    @Autowired
    public void setWeightDao(WeightDao weightDao) {
        this.weightDao = weightDao;
        this.dao = this.weightDao;
    }
}

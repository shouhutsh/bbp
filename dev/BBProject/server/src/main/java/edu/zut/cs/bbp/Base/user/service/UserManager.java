package edu.zut.cs.bbp.Base.user.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.user.domain.User;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
public interface UserManager extends BaseManager<User, Long> {

}


package edu.zut.cs.bbp.BasedOnUser.first2Diary.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.domain.First2Diary;

/**
 * Created by Administrator on 2014/6/24.
 */
public interface First2DiaryDao extends BaseUserDao<First2Diary, String> {
}

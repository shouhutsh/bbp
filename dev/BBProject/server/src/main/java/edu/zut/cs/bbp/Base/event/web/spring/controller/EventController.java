package edu.zut.cs.bbp.Base.event.web.spring.controller;

import edu.zut.cs.bbp.Base.base.domain.State;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.event.service.EventManager;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.Base.user.service.UserManager;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
@Controller
@RequestMapping("/event")
public class EventController extends
        BaseUserEntityController<Event, String, EventManager> {

    EventManager eventManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    UserManager userManager ;

    @Autowired
    public void setEventManager(EventManager eventManager) {
        this.eventManager = eventManager;
        this.manager = this.eventManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Event create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Event.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Event update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Event.class));
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String index(HttpServletRequest request, HttpServletResponse response,@PathVariable String url) {
        return "event/"+url;
    }

    /**
     * 得到孩子节点
     * @param request
     * @param response
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getChildren.json", method = RequestMethod.GET, produces = "application/json")
    public List<Event> getChildren(HttpServletRequest request,
                           HttpServletResponse response) {
        List<Event> result = null;
        String nodeId = request.getParameter("id");
        String userId = request.getParameter("uid");
        if (StringUtils.isBlank(nodeId)
                || StringUtils.equalsIgnoreCase("0", nodeId)) {
            result = this.eventManager.getRoot();
        } else {
            Event node = this.eventManager.findById(nodeId);
            result = this.manager.getChildrens(node.getId(), Long.valueOf(userId));

        }
        return result;
    }


    @RequestMapping(value = "/insert.json", method = RequestMethod.POST, consumes = "application/json")
    public void insert(HttpServletRequest request, HttpServletResponse response) throws IOException {

        // FIXME 没有考虑异常情况
        Event[] events =  mapper.readValue(request.getInputStream(), Event[].class);

        for(Event e : events){
            e.setState(State.CLEAN);
            this.manager.save(e);
        }

        response.getWriter().write(0);
    }

}

package edu.zut.cs.bbp.Base.user.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.Base.user.service.UserManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
@Controller
@RequestMapping("/user")
public class UserController extends
        BaseEntityController<User, Long, UserManager> {


    UserManager userManager;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
        this.manager = this.userManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public User create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), User.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public User update(@PathVariable Long id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), User.class));
    }

   @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String request(HttpServletRequest request, HttpServletResponse response, @PathVariable String url) {
        return "user/" + url;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login.html")
    public ModelAndView loginCheck(@RequestParam(value = "userId", required = true) String userId,
                                   @RequestParam(value = "password", required = true) String password,
                                   HttpServletRequest request) {
        User user = null;
        user = userManager.findById(Long.parseLong(userId));
        String msg = null;
        if (user.getPassword().equals(password)) {
            request.getSession().setAttribute("user", user);
            return new ModelAndView(new RedirectView("welcome.html"));
        } else {
            ModelAndView mav = new ModelAndView("user/login");
            mav.setViewName("user/login");
            mav.addObject("msg", "登录失败，用户名或密码错误");
            return mav;
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/updatePwd.json", produces = "application/json")
    @ResponseBody
    public List<String> updatePwd(@RequestParam(value = "oldPassword", required = false) String oldPassword,
                            @RequestParam(value = "newPassword", required = false) String newPassword,
                            HttpServletRequest request,HttpServletResponse response) throws IOException {
        List<String> list = new ArrayList<>();
        String msg = null;


//  /     User user = (User) request.getSession().getAttribute("user");
        User user = userManager.findById(1L);
        if (!user.getPassword().equals(oldPassword)) {
            msg = "原密码不对";
            list.add(msg);
            return list;

        } else {
            user.setPassword(newPassword);
            userManager.save(user);
            msg = "密码修改成功";
            list.add(msg);
            return list;
        }
    }
    @RequestMapping(method = RequestMethod.POST, value = "/rbUser.json", consumes = "application/json", produces = "application/json")
    @ResponseBody
    public User rbUser(HttpServletRequest request, HttpServletResponse response) {

        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/test.html")
    public ModelAndView test(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("user/login");
        return mav;
    }

}

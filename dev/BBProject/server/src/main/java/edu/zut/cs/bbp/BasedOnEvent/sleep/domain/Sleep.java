package edu.zut.cs.bbp.BasedOnEvent.sleep.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEventEntityDomain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Sleep")
public class Sleep extends BaseEventEntityDomain {

    /**
     * 睡觉结束时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "END_TIME")
    private Date endTime = new Date();


    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}

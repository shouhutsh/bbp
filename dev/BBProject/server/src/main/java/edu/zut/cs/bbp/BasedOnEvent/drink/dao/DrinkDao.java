package edu.zut.cs.bbp.BasedOnEvent.drink.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnEvent.drink.domain.Drink;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface DrinkDao extends BaseUserDao<Drink, String> {

}

package edu.zut.cs.bbp.BasedOnEvent.weight.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnEvent.weight.domain.Weight;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface WeightDao extends BaseUserDao<Weight, String> {

}

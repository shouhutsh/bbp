package edu.zut.cs.bbp.Base.event.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.Base.event.dao.EventDao;
import edu.zut.cs.bbp.Base.event.domain.Event;
import edu.zut.cs.bbp.Base.event.service.EventManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
@Service("eventManager")
public class EventManagerImpl extends BaseUserManagerImpl<Event, String>
            implements EventManager{

    EventDao eventDao;

    @Autowired
    public void setEventDao(EventDao eventDao) {
        this.eventDao = eventDao;
        this.dao = this.eventDao;
    }

    @Override
    public List<Event> getRoot() {
        return this.eventDao.getRoot();
    }

    @Override
    public List<Event> getChildrens(String id, Long uid) {
        return this.eventDao.getChildrens(id, uid);
    }

}

package edu.zut.cs.bbp.BasedOnEvent.sleep.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnEvent.sleep.domain.Sleep;
import edu.zut.cs.bbp.BasedOnEvent.sleep.service.SleepManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/sleep")
public class SleepController extends
        BaseUserEntityController<Sleep, String, SleepManager> {

    SleepManager sleepManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setSleepManager(SleepManager sleepManager) {
        this.sleepManager = sleepManager;
        this.manager = this.sleepManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Sleep create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Sleep.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Sleep update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Sleep.class));
    }
}

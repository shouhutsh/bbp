package edu.zut.cs.bbp.BasedOnEvent.height.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnEvent.height.domain.Height;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface HeightManager extends BaseUserManager<Height, String> {

}

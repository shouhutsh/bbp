package edu.zut.cs.bbp.BasedOnUser.intellect.web.spring.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.intellect.domain.Intellect;
import edu.zut.cs.bbp.BasedOnUser.intellect.service.IntellectManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/intellect")
public class IntellectController extends
        BaseUserEntityController<Intellect, String, IntellectManager> {

    IntellectManager intellectManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setIntellectManager(IntellectManager intellectManager) {
        this.intellectManager = intellectManager;
        this.manager = this.intellectManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Intellect create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Intellect.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Intellect update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Intellect.class));
    }
}

package edu.zut.cs.bbp.BasedOnUser.milestone.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface MilestoneDao extends BaseUserDao<Milestone, String> {

}

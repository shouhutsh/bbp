package edu.zut.cs.bbp.Base.user.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.user.domain.User;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
public interface UserDao extends BaseDao<User, Long> {
}

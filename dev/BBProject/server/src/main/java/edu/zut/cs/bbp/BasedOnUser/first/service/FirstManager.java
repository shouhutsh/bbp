package edu.zut.cs.bbp.BasedOnUser.first.service;

import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface FirstManager extends BaseUserManager<First, String> {

}

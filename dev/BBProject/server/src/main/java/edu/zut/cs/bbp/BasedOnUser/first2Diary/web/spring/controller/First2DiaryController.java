package edu.zut.cs.bbp.BasedOnUser.first2Diary.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.domain.First2Diary;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.service.First2DiaryManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Administrator on 2014/6/24.
 */
@Controller
@RequestMapping("/first2Diary")
public class First2DiaryController extends
        BaseUserEntityController<First2Diary, String, First2DiaryManager>{

    First2DiaryManager first2DiaryManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setFirst2DiaryManager(First2DiaryManager first2DiaryManager) {
        this.first2DiaryManager = first2DiaryManager;
        this.manager = this.first2DiaryManager;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public First2Diary create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), First2Diary.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public First2Diary update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), First2Diary.class));
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String request(HttpServletRequest request, HttpServletResponse response, @PathVariable String url) {
        return "baseOnUser/" + url;
    }
}

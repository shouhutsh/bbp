package edu.zut.cs.bbp.BasedOnEvent.NN.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.NN.dao.NNDao;
import edu.zut.cs.bbp.BasedOnEvent.NN.domain.NN;
import edu.zut.cs.bbp.BasedOnEvent.NN.service.NNManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("nnManager")
public class NNManagerImpl extends BaseUserManagerImpl<NN, String>
            implements NNManager{

    NNDao nnDao;

    @Autowired
    public void setNnDao(NNDao nnDao) {
        this.nnDao = nnDao;
        this.dao = this.nnDao;
    }
}

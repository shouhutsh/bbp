package edu.zut.cs.bbp.BasedOnUser.mile2Diary.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2014/6/24.
 */
@Entity
@Table(name = "T_Mile2Diary")
public class Mile2Diary extends BaseUserEntityDomain {

    @ManyToOne(cascade = { CascadeType.REMOVE })
    @JoinColumn(name = "Mile_Id", nullable = false)
    private Milestone milestone;

    @ManyToOne(cascade = { CascadeType.REMOVE })
    @JoinColumn(name = "Diary", nullable = false)
    private Diary diary;


    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Time", nullable = false)
    private Date time = new Date();


    public Milestone getMilestone() {
        return milestone;
    }

    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}

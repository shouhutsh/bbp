package edu.zut.cs.bbp.BasedOnEvent.NN.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnEvent.NN.domain.NN;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface NNManager extends BaseUserManager<NN, String> {

}

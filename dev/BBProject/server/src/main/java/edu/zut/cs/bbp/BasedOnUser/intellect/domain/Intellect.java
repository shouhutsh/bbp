package edu.zut.cs.bbp.BasedOnUser.intellect.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEntityDomain;
import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Intellect")
public class Intellect extends BaseUserEntityDomain {

    @Column(name = "Name", nullable = false)
    private String name;

    /**
     * 适合年龄字段：
     *      应该是时间段
     *      例如：六个月，一岁
     *      因此假定单位为一个月
     */
    @Column(name = "Suitable", nullable = false)
    private Long suitable;

    @Column(name = "Complete", nullable = false)
    private boolean complete = false;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "Time", nullable = false)
    private Date time = new Date();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSuitable() {
        return suitable;
    }

    public void setSuitable(Long suitable) {
        this.suitable = suitable;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}

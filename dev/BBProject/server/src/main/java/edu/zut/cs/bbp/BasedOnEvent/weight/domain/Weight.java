package edu.zut.cs.bbp.BasedOnEvent.weight.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEventEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Weight")
public class Weight extends BaseEventEntityDomain {

    /**
     * 体重单位为Kg
     */
    @Column(name = "Weight")
    private float weight;


    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}

package edu.zut.cs.bbp.BasedOnEvent.drink.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.drink.dao.DrinkDao;
import edu.zut.cs.bbp.BasedOnEvent.drink.domain.Drink;
import edu.zut.cs.bbp.BasedOnEvent.drink.service.DrinkManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service
public class DrinkManagerImpl extends BaseUserManagerImpl<Drink, String>
            implements DrinkManager{

    DrinkDao drinkDao;

    @Autowired
    public void setDrinkDao(DrinkDao drinkDao) {
        this.drinkDao = drinkDao;
        this.dao = this.drinkDao;
    }
}

package edu.zut.cs.bbp.BasedOnEvent.height.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnEvent.height.domain.Height;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface HeightDao extends BaseUserDao<Height, String> {

}

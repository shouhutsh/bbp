package edu.zut.cs.bbp.BasedOnUser.diary.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.diary.service.DiaryManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/diary")
public class DiaryController extends
        BaseUserEntityController<Diary, String, DiaryManager> {

    DiaryManager diaryManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setDiaryManager(DiaryManager diaryManager) {
        this.diaryManager = diaryManager;
        this.manager = this.diaryManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Diary create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Diary.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Diary update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Diary.class));
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String request(HttpServletRequest request, HttpServletResponse response, @PathVariable String url) {
        return "baseOnUser/diary/" + url;
    }
}

package edu.zut.cs.bbp.Base.base.domain;

import edu.zut.cs.bbp.Base.user.domain.User;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@MappedSuperclass
public class BaseUserEntityDomain extends BaseDomain {

    private static final long serialVersionUID = -6163675074532129459L;


    /**
     * 实体主键
     */
    @Id
    @Column(name = "ID", length = 32)
    String id;

    /**
     * 实体创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATECREATED")
    protected Date dateCreated = new Date();

    /**
     * 实体修改时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DATEMODIFED")
    protected Date dateModified = new Date();

    /**
     * 实体是否被删除
     */
    @Column(name = "DELETED")
    protected Boolean deleted = false;

    /**
     * 实体状态
     */
    @Column(name = "State", nullable = false)
    private Integer state = State.CLEAN;

    /**
     * 建立 User 外键
     */
    @ManyToOne(cascade = { CascadeType.REMOVE }, targetEntity = User.class)
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
    private User user;


    @Override
    public boolean equals(Object obj) {
        if (null != obj) {
            if (obj instanceof BaseEntityDomain) {
                BaseEntityDomain domain = (BaseEntityDomain) obj;
                if (this.id.equals(domain.id)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        if (this.id == null) {
            this.id = "0";
        }
        return HashCodeBuilder.reflectionHashCode(this.id);
    }

    public Boolean isDeleted() {
        return deleted;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this,
                ToStringStyle.MULTI_LINE_STYLE);
    }

    public String getId() {
        return id;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public Integer getState() {
        return state;
    }

    public User getUser() {
        return user;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

package edu.zut.cs.bbp.BasedOnUser.diary.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.Base.base.dao.BaseDao;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface DiaryDao extends BaseUserDao<Diary, String> {

}

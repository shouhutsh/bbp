package edu.zut.cs.bbp.BasedOnEvent.sleep.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.sleep.dao.SleepDao;
import edu.zut.cs.bbp.BasedOnEvent.sleep.domain.Sleep;
import edu.zut.cs.bbp.BasedOnEvent.sleep.service.SleepManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("sleepManager")
public class SleepManagerImpl extends BaseUserManagerImpl<Sleep, String>
            implements SleepManager{

    SleepDao sleepDao;

    @Autowired
    public void setSleepDao(SleepDao sleepDao) {
        this.sleepDao = sleepDao;
        this.dao = this.sleepDao;
    }
}

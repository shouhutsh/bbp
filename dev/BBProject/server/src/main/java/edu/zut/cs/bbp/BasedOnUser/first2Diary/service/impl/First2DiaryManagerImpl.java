package edu.zut.cs.bbp.BasedOnUser.first2Diary.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.dao.First2DiaryDao;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.domain.First2Diary;
import edu.zut.cs.bbp.BasedOnUser.first2Diary.service.First2DiaryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2014/6/24.
 */
@Service("first2DiaryManager")
public class First2DiaryManagerImpl extends BaseUserManagerImpl<First2Diary, String>
            implements First2DiaryManager{

    First2DiaryDao first2DiaryDao;

    @Autowired
    public void setFirst2DiaryDao(First2DiaryDao first2DiaryDao) {
        this.first2DiaryDao = first2DiaryDao;
        this.dao = this.first2DiaryDao;
    }
}

package edu.zut.cs.bbp.BasedOnUser.first.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface FirstDao extends BaseUserDao<First, String> {
}

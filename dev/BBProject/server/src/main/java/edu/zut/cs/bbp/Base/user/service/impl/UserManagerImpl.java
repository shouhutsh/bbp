package edu.zut.cs.bbp.Base.user.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.user.dao.UserDao;
import edu.zut.cs.bbp.Base.user.domain.User;
import edu.zut.cs.bbp.Base.user.service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/5/12 0012.
 */
@Service("userManager")
public class UserManagerImpl extends BaseManagerImpl<User, Long>
            implements UserManager{

    UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao){
        this.userDao = userDao;
        this.dao = this.userDao;
    }

}

package edu.zut.cs.bbp.BasedOnEvent.NN.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnEvent.NN.domain.NN;
import edu.zut.cs.bbp.BasedOnEvent.NN.service.NNManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/nn")
public class NNController extends
        BaseUserEntityController<NN, String, NNManager> {

    NNManager nnManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setNnManager(NNManager nnManager) {
        this.nnManager = nnManager;
        this.manager = this.nnManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public NN create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), NN.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public NN update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), NN.class));
    }
}

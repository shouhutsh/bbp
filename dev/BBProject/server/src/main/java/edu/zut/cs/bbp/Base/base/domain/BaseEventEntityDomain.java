package edu.zut.cs.bbp.Base.base.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import edu.zut.cs.bbp.Base.event.domain.Event;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@MappedSuperclass
public class BaseEventEntityDomain extends BaseUserEntityDomain {

    private static final long serialVersionUID = -6163675075289534675L;

    /**
     * 建立 Event 外键
     */
    @JsonManagedReference
    @ManyToOne(cascade = {CascadeType.REMOVE }, targetEntity = Event.class)
    @JoinColumn(name = "EVENT_ID", referencedColumnName = "ID", nullable = false)
    private Event event;

    /**
     * 事件发生时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TIME", nullable = false)
    private Date time = new Date();

    /**
     * 事件备注
     */
    @Column(name = "Remark")
    private String remark;


    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}

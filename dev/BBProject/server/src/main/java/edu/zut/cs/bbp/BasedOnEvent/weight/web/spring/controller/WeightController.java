package edu.zut.cs.bbp.BasedOnEvent.weight.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnEvent.weight.domain.Weight;
import edu.zut.cs.bbp.BasedOnEvent.weight.service.WeightManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/weight")
public class WeightController extends
        BaseUserEntityController<Weight, String, WeightManager> {

    WeightManager weightManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setWeightManager(WeightManager weightManager) {
        this.weightManager = weightManager;
        this.manager = this.weightManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Weight create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Weight.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Weight update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Weight.class));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/weightChart.html")
    public String heightChart(HttpServletRequest request, HttpServletResponse response){
        return "baseOnEvent/weightChart";
    }
    @ResponseBody
    @RequestMapping(value = "/getWeightData.json", method = RequestMethod.GET, produces = "application/json")
    public List<Weight> getWeightData(HttpServletRequest request,
                                      HttpServletResponse response) {
        List<Weight> result = new ArrayList<>();
        result = weightManager.findAll();
        return result;
    }
}

package edu.zut.cs.bbp.BasedOnUser.mile2Diary.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.domain.Mile2Diary;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.service.Mile2DiaryManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Administrator on 2014/6/24.
 */
@Controller
@RequestMapping("/mile2Diary")
public class Mile2DiaryController extends
        BaseUserEntityController<Mile2Diary, String, Mile2DiaryManager>{

    Mile2DiaryManager mile2DiaryManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setMile2DiaryManager(Mile2DiaryManager mile2DiaryManager) {
        this.mile2DiaryManager = mile2DiaryManager;
        this.manager = this.mile2DiaryManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Mile2Diary create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Mile2Diary.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Mile2Diary update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Mile2Diary.class));
    }
    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String request(HttpServletRequest request, HttpServletResponse response, @PathVariable String url) {
        return "baseOnUser/" + url;
    }
}

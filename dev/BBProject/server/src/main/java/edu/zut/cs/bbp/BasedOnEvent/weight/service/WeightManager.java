package edu.zut.cs.bbp.BasedOnEvent.weight.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnEvent.weight.domain.Weight;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface WeightManager extends BaseUserManager<Weight, String> {

}

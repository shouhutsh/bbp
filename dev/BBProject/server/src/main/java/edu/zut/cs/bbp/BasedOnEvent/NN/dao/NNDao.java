package edu.zut.cs.bbp.BasedOnEvent.NN.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnEvent.NN.domain.NN;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface NNDao extends BaseUserDao<NN, String> {

}

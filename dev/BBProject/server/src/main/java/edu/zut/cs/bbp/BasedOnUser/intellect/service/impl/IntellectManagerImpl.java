package edu.zut.cs.bbp.BasedOnUser.intellect.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.intellect.dao.IntellectDao;
import edu.zut.cs.bbp.BasedOnUser.intellect.domain.Intellect;
import edu.zut.cs.bbp.BasedOnUser.intellect.service.IntellectManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("intellectManager")
public class IntellectManagerImpl extends BaseUserManagerImpl<Intellect, String>
            implements IntellectManager {

    IntellectDao intellectDao;

    @Autowired
    public void setIntellectDao(IntellectDao intellectDao) {
        this.intellectDao = intellectDao;
        this.dao = this.intellectDao;
    }
}

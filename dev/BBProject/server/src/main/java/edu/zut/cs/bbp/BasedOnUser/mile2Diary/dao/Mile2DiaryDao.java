package edu.zut.cs.bbp.BasedOnUser.mile2Diary.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.domain.Mile2Diary;

/**
 * Created by Administrator on 2014/6/24.
 */
public interface Mile2DiaryDao extends BaseUserDao<Mile2Diary, String> {
}

package edu.zut.cs.bbp.BasedOnUser.mile2Diary.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.dao.Mile2DiaryDao;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.domain.Mile2Diary;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.service.Mile2DiaryManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2014/6/24.
 */
@Service("mile2DiaryManager")
public class Mile2DiaryManagerImpl extends BaseUserManagerImpl<Mile2Diary, String>
            implements Mile2DiaryManager{

    Mile2DiaryDao mile2DiaryDao;

    @Autowired
    public void setMile2DiaryDao(Mile2DiaryDao mile2DiaryDao) {
        this.mile2DiaryDao = mile2DiaryDao;
        this.dao = this.mile2DiaryDao;
    }
}

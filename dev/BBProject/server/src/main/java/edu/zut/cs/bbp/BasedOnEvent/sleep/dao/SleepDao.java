package edu.zut.cs.bbp.BasedOnEvent.sleep.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.BasedOnEvent.sleep.domain.Sleep;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface SleepDao extends BaseUserDao<Sleep, String> {

}

package edu.zut.cs.bbp.BasedOnUser.media.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnUser.media.domain.Media;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface MediaManager extends BaseUserManager<Media, String> {

}

package edu.zut.cs.bbp.BasedOnUser.media.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;

import javax.persistence.*;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Media")
public class Media extends BaseUserEntityDomain {

    @Column(name = "Path")
    private String path;

    @ManyToOne
    @JsonManagedReference
    @JoinColumn(name = "DIARY_ID")
    private Diary diary;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Diary getDiary() {
        return diary;
    }

    public void setDiary(Diary diary) {
        this.diary = diary;
    }
}

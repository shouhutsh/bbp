package edu.zut.cs.bbp.BasedOnEvent.height.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnEvent.height.dao.HeightDao;
import edu.zut.cs.bbp.BasedOnEvent.height.domain.Height;
import edu.zut.cs.bbp.BasedOnEvent.height.service.HeightManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("heightManager")
public class HeightManagerImpl extends BaseUserManagerImpl<Height, String>
            implements HeightManager{

    HeightDao heightDao;

    @Autowired
    public void setHeightDao(HeightDao heightDao) {
        this.heightDao = heightDao;
        this.dao = this.heightDao;
    }
}

package edu.zut.cs.bbp.BasedOnUser.first.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.first.dao.FirstDao;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.first.service.FirstManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("firstManager")
public class FirstManagerImpl extends BaseUserManagerImpl<First, String>
            implements FirstManager {

    FirstDao firstDao;

    @Autowired
    public void setFirstDao(FirstDao firstDao) {
        this.firstDao = firstDao;
        this.dao = this.firstDao;
    }
}

package edu.zut.cs.bbp.BasedOnUser.media.web.spring.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.media.domain.Media;
import edu.zut.cs.bbp.BasedOnUser.media.service.MediaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/19 0019.
 */
@Controller
@RequestMapping("/file")
public class MediaController extends
		BaseUserEntityController<Media, String, MediaManager> {

	MediaManager mediaManager;
	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	public void setMediaManager(MediaManager mediaManager) {
		this.mediaManager = mediaManager;
		this.manager = this.mediaManager;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public Media create(HttpServletRequest request) throws IOException {
		return super.create(mapper.readValue(request.getInputStream(), Media.class));
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public Media update(@PathVariable String id, HttpServletRequest request) throws IOException {
		return super.update(id, mapper.readValue(request.getInputStream(), Media.class));
	}

	@RequestMapping(value = "/image/{imgID}.do", method = RequestMethod.GET)
	public @ResponseBody byte[] upload1(@PathVariable Long imgID){
//        this.getClass().getClassLoader().getResource("/").getPath()
		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		File img = new File(path + "../image/" + imgID + ".jpg");
		if(img.exists()){
			try{
				return FileCopyUtils.copyToByteArray(img);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	@RequestMapping(value = "../audio/{audioID}.do",method = RequestMethod.GET, produces = "audio/mpeg")
	public @ResponseBody byte[] upload2(@PathVariable long audioID){
//		this.getClass().getClassLoader().getResource("/").getPath()
		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		File audio = new File(path + "../audio/" + audioID + ".mp3");
		if (audio.exists()){
			try{
				return FileCopyUtils.copyToByteArray(audio);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@RequestMapping(value = "/video/{videoID}.do",method = RequestMethod.GET,produces = "video/mp4")
	public @ResponseBody byte[] upload3(@PathVariable long videoID){
//		this.getClass().getClassLoader().getResource("/").getPath()
		String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		File video = new File(path + "../video/" + videoID + ".mp4");
		if (video.exists()){
			try{
				return FileCopyUtils.copyToByteArray(video);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	@RequestMapping(value = "/upload.do", method = RequestMethod.POST)
	public String upload(@RequestParam(value = "media", required = false) MultipartFile file, HttpServletRequest request, ModelMap model){

		String path = request.getSession().getServletContext().getRealPath("upload");
		String fileName = file.getOriginalFilename();
		File targetFile = new File(path, fileName);
		if(!targetFile.exists()){
			targetFile.mkdirs();
		}

		//保存
		try {
			file.transferTo(targetFile);
		} catch (Exception e) {
			model.addAttribute("fileUrl", "上传失败");
		}

		model.addAttribute("fileUrl", targetFile.getPath());
		return "file/result";
	}

}

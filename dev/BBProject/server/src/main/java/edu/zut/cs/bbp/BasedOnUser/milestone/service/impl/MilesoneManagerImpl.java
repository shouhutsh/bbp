package edu.zut.cs.bbp.BasedOnUser.milestone.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.milestone.dao.MilestoneDao;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;
import edu.zut.cs.bbp.BasedOnUser.milestone.service.MilestoneManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("milesoneManager")
public class MilesoneManagerImpl extends BaseUserManagerImpl<Milestone, String>
            implements MilestoneManager {

    MilestoneDao milestoneDao;

    @Autowired
    public void setMilestoneDao(MilestoneDao milestoneDao) {
        this.milestoneDao = milestoneDao;
        this.dao = this.milestoneDao;
    }
}

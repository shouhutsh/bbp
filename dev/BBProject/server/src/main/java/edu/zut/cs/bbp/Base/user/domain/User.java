package edu.zut.cs.bbp.Base.user.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEntityDomain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2014/5/12.
 */
@Entity
@Table(name="T_User")
public class User extends BaseEntityDomain {

    @Column(name = "NickName")
    private String nickname;

    @Column(name = "Name", nullable = false)
    private String name;

    // MD5 加密之后为32位
    @Column(name = "PASSWORD", nullable = false, length = 32)
    private String password;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LastUpload")
    private Date lastUpload = new Date();

    @Temporal(TemporalType.DATE)
    @Column(name = "Birthday")
    private Date birthday;


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLastUpload() {
        return lastUpload;
    }

    public void setLastUpload(Date lastUpload) {
        this.lastUpload = lastUpload;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}

package edu.zut.cs.bbp.BasedOnUser.diary.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import edu.zut.cs.bbp.Base.base.domain.BaseUserEntityDomain;
import edu.zut.cs.bbp.BasedOnUser.media.domain.Media;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Diary")
public class Diary extends BaseUserEntityDomain {

    @Column(name = "Title")
    private String title;

    @Column(name = "Content")
    private String content;

    @JsonBackReference
    @OneToMany(mappedBy = "diary")
    private Set<Media> mediaSet;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Set<Media> getMediaSet() {
        return mediaSet;
    }

    public void setMediaSet(Set<Media> mediaSet) {
        this.mediaSet = mediaSet;
    }
}

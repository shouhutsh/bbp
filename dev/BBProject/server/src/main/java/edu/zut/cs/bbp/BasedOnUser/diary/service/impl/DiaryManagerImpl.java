package edu.zut.cs.bbp.BasedOnUser.diary.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.diary.dao.DiaryDao;
import edu.zut.cs.bbp.BasedOnUser.diary.domain.Diary;
import edu.zut.cs.bbp.BasedOnUser.diary.service.DiaryManager;
import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("diaryManager")
public class DiaryManagerImpl extends BaseUserManagerImpl<Diary, String>
            implements DiaryManager {

    DiaryDao diaryDao;

    @Autowired
    public void setDiaryDao(DiaryDao diaryDao) {
        this.diaryDao = diaryDao;
        this.dao = this.diaryDao;
    }
}

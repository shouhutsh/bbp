package edu.zut.cs.bbp.BasedOnUser.first.web.spring.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.first.domain.First;
import edu.zut.cs.bbp.BasedOnUser.first.service.FirstManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/first")
public class FirstController extends
        BaseUserEntityController<First, String, FirstManager> {

    FirstManager firstManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setFirstManager(FirstManager firstManager) {
        this.firstManager = firstManager;
        this.manager = this.firstManager;
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public First create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), First.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public First update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), First.class));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String request(HttpServletRequest request, HttpServletResponse response, @PathVariable String url) {
        return "baseOnUser/" + url;
    }
    @ResponseBody
    @RequestMapping(value = "/getFirstData.json", method = RequestMethod.GET, produces = "application/json")
    public List<First> getFirstData(HttpServletRequest request,
                                      HttpServletResponse response) {
        List<First> result = new ArrayList<>();
        result = firstManager.findAll();
        return result;
    }
}

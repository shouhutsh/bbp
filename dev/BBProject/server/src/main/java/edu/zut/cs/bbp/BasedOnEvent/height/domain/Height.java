package edu.zut.cs.bbp.BasedOnEvent.height.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEventEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Height")
public class Height extends BaseEventEntityDomain {
    /**
     * 身高的单位为CM
     */
    @Column(name = "Height")
    private Long height;


    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }
}

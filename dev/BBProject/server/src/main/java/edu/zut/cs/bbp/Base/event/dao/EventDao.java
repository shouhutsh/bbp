package edu.zut.cs.bbp.Base.event.dao;

import edu.zut.cs.bbp.Base.base.dao.BaseDao;
import edu.zut.cs.bbp.Base.base.dao.BaseUserDao;
import edu.zut.cs.bbp.Base.event.domain.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Qi_2 on 2014/6/20 0020.
 */
public interface EventDao extends BaseUserDao<Event, String> {
    @Query(value = "SELECT e  FROM Event e  WHERE e.parent IS NULL")
    public List<Event> getRoot();
    @Query(value = "SELECT e FROM Event e WHERE e.parent.id=:id AND e.user.id=:uid")
    public List<Event> getChildrens(@Param("id") String id, @Param("uid") Long uid);
}

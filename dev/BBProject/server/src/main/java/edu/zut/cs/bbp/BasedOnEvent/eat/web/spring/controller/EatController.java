package edu.zut.cs.bbp.BasedOnEvent.eat.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnEvent.eat.domain.Eat;
import edu.zut.cs.bbp.BasedOnEvent.eat.service.EatManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/eat")
public class EatController extends
        BaseUserEntityController<Eat, String, EatManager> {

    EatManager eatManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setEatManager(EatManager eatManager) {
        this.eatManager = eatManager;
        this.manager = this.eatManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Eat create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Eat.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Eat update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Eat.class));
    }
    @RequestMapping(method = RequestMethod.POST, value = "/add.html")
    public String add(HttpServletRequest request, HttpServletResponse response) {
        Eat eat = null;
        try {
            eat = create(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
    @RequestMapping(method = RequestMethod.GET, value = "/eatList.html")
    @ResponseBody
    public List<Eat> eatList(HttpServletRequest request, HttpServletResponse response)
    {
        List<Eat> list = new ArrayList<Eat>();
        list = eatManager.findAll();
        return list;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/recipeChart.html")
    public String heightChart(HttpServletRequest request, HttpServletResponse response){
        return "baseOnEvent/recipeChart";
    }

}

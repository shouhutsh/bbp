package edu.zut.cs.bbp.BasedOnUser.milestone.web.spring.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseEntityController;
import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnUser.milestone.domain.Milestone;
import edu.zut.cs.bbp.BasedOnUser.milestone.service.MilestoneManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/milestone")
public class MilestoneController extends
        BaseUserEntityController<Milestone, String, MilestoneManager> {

    MilestoneManager milestoneManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setMilestoneManager(MilestoneManager milestoneManager) {
        this.milestoneManager = milestoneManager;
        this.manager = this.milestoneManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Milestone create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), Milestone.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Milestone update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), Milestone.class));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{url}.html")
    public String request(HttpServletRequest request, HttpServletResponse response, @PathVariable String url) {
        return "baseOnUser/" + url;
    }

    @ResponseBody
    @RequestMapping(value = "/getMilestoneData.json", method = RequestMethod.GET, produces = "application/json")
    public List<Milestone> getFirstData(HttpServletRequest request,
                                    HttpServletResponse response) {
        List<Milestone> result = new ArrayList<>();
        result = milestoneManager.findAll();
        return result;
    }


}

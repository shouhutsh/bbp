package edu.zut.cs.bbp.BasedOnUser.media.service.impl;

import edu.zut.cs.bbp.Base.base.service.impl.BaseManagerImpl;
import edu.zut.cs.bbp.Base.base.service.impl.BaseUserManagerImpl;
import edu.zut.cs.bbp.BasedOnUser.media.dao.MediaDao;
import edu.zut.cs.bbp.BasedOnUser.media.domain.Media;
import edu.zut.cs.bbp.BasedOnUser.media.service.MediaManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Service("mediaManager")
public class MediaManagerImpl extends BaseUserManagerImpl<Media, String>
            implements MediaManager{

    MediaDao mediaDao;

    @Autowired
    public void setMediaDao(MediaDao mediaDao) {
        this.mediaDao = mediaDao;
        this.dao = this.mediaDao;
    }
}

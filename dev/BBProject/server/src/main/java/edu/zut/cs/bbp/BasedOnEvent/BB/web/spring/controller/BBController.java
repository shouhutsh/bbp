package edu.zut.cs.bbp.BasedOnEvent.BB.web.spring.controller;

import edu.zut.cs.bbp.Base.base.web.spring.controller.BaseUserEntityController;
import edu.zut.cs.bbp.BasedOnEvent.BB.domain.BB;
import edu.zut.cs.bbp.BasedOnEvent.BB.service.BBManager;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Controller
@RequestMapping("/bb")
public class BBController extends
        BaseUserEntityController<BB, String, BBManager> {

    BBManager bbManager;
    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public void setBbManager(BBManager bbManager) {
        this.bbManager = bbManager;
        this.manager = this.bbManager;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public BB create(HttpServletRequest request) throws IOException {
        return super.create(mapper.readValue(request.getInputStream(), BB.class));
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public BB update(@PathVariable String id, HttpServletRequest request) throws IOException {
        return super.update(id, mapper.readValue(request.getInputStream(), BB.class));
    }
}

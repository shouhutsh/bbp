package edu.zut.cs.bbp.BasedOnEvent.drink.domain;

import edu.zut.cs.bbp.Base.base.domain.BaseEventEntityDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
@Entity
@Table(name = "T_Drink")
public class Drink extends BaseEventEntityDomain {

    /**
     * 喝以ML为单位
     */
    @Column(name = "Amount")
    private Long amount;


    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}

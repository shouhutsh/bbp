package edu.zut.cs.bbp.BasedOnUser.intellect.service;

import edu.zut.cs.bbp.Base.base.service.BaseManager;
import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnUser.intellect.domain.Intellect;

/**
 * Created by Qi_2 on 2014/6/22 0022.
 */
public interface IntellectManager extends BaseUserManager<Intellect, String> {

}

package edu.zut.cs.bbp.Base.base.domain;

/**
 * Created by Administrator on 2014/7/1.
 */
public class State {
    public static final Integer CLEAN = 0;
    public static final Integer NEW   = 1;
    public static final Integer OLD   = 2;
    public static final Integer DELETE= 3;
}

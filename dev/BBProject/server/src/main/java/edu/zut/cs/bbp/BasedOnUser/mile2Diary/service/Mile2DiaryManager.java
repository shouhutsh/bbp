package edu.zut.cs.bbp.BasedOnUser.mile2Diary.service;

import edu.zut.cs.bbp.Base.base.service.BaseUserManager;
import edu.zut.cs.bbp.BasedOnUser.mile2Diary.domain.Mile2Diary;

/**
 * Created by Administrator on 2014/6/24.
 */
public interface Mile2DiaryManager extends BaseUserManager<Mile2Diary, String> {
}

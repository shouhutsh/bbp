Ext.define('event.view.EventEdit', {
	extend : 'Ext.window.Window',
	alias : 'widget.eventEdit',
	title : '编辑事件信息',
	layout : 'fit',
	autoShow : true,
	initComponent : function() {
		this.items = [ {
			xtype : 'form',
			height : 300,
			width : 300,
			border : true,
			items : [ {
                xtype : 'textfield',
                name : 'name',
                fieldLabel : 'NAME'
            } ]
		} ];
		this.buttons = [ {
			text : '保存',
			action : 'save'
		}, {
			text : '取消',
			scope : this,
			handler : this.close
		} ];
		this.callParent(arguments);
	}
});

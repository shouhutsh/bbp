var pageSize = 20;

Ext.define('user.store.UserStore', {
	extend : 'Ext.data.Store',
	alias : 'widget.userStore',
	autoLoad : true,
	autoSync : true,// 需要同步
	model : 'user.model.UserModel',
	proxy : {
		type : 'rest',
		format : 'json',
		url : ctx + '/user/',
		headers : {
			'Content-Type' : 'application/json'
		},
		reader : {
			type : 'json',
			root : 'content',
			totalProperty : 'totalElements'
		},
		writer : {
			type : 'json',
			allowSingle : 'true'
		}
	},
	// 每页显示的记录行数
	pageSize : pageSize
});
Ext.application({
	requires : [ 'Ext.container.Viewport' ],
	name : 'user', // 命名空间
	appFolder : ctx + '/resources/js/app/user',
	controllers : [ 'UserController' ],
	// 这个所有加载完毕最后执行
	launch : function() {
		Ext.create('Ext.container.Viewport', {
			id : 'userGrid',
			title : '主面板',
			layout : 'fit',
			border : true,
			items : [ {
				xtype : 'userGrid'
			} ]
		});// create
	}// launch;
});// application;

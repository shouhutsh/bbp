Ext.define('event.controller.EventController', {
	extend : 'Ext.app.Controller',
	alias : 'widget.EventController',
	stores : [ 'EventStore' ],
	models : [ 'EventModel' ],
	views : [ 'EventGrid', 'EventEdit', 'EventAdd', 'EventSearch' ],
	init : function() {
		this.control({
			'eventGrid' : {
				itemdblclick : this.editEvent
			},
			'eventEdit button[action=save]' : {
				click : this.updateEvent
			},
            'eventGrid  button[action=addBtn]'  :{
                click : this.addView
            },
            'eventAdd button[action=save]' : {
                click : this.addEvent
            },
            // List页面 删除 按钮绑定事件
            'eventGrid button[action=delBtn]' : {
                click : this.destoryEvent
            },
            // List页面 更新 按钮绑定事件
            'eventGrid button[action=upBtn]' : {
                click : this.upEvent
            },
			// 添加查找
			'eventGrid [action=find]' : {
				click : this.searchEventView
			},
			'eventSearch [action=search]' : {
				click : this.searchEvent
			}
		});
	},
	editEvent : function(grid, record) {
		var view = Ext.widget('eventEdit');
		view.down('form').loadRecord(record);
	},
	updateEvent : function(button) {
		var win = button.up('window');
		form = win.down('form');
		record = form.getRecord();
		values = form.getValues();
		record.set(values);
		win.close();
	},
    addView : function() {
        Ext.widget('eventAdd');
    },
    /**
     * 添加
     */
    addEvent : function(button) {
        var win = button.up('eventAdd');
        form = win.down('form');
        values = form.getValues();
        if (confirm("确定保存记录吗？"))
        {
            eventStore.insert(0, values);
        }
        eventStore.reload();
        win.close();
    },
    /**
     * 删除
     */
    destoryEvent : function(o) {
        var grid = o.ownerCt.ownerCt;
        var sm = grid.getSelectionModel();
        var recs = sm.getSelection();
        var count = recs.length;
        var record;
        if (count != 0) {
            Ext.MessageBox.confirm("提示", "确定要删除吗?", function (button){
                if (button == "yes") {
                    for (var i = 0; i < count; i++) {
                        record = recs[i];
                        eventStore.remove(record);
                        grid.reconfigure(eventStore);
                    }
                }
            });
        } else {
            Ext.MessageBox.alert("提示", "请选择要删除的行!");
            return false;
        };
        eventStore.load();
    },
    /**
     * 修改 点击头部修改按钮 修改选中项
     *
     */
    upEvent : function() {
    	var grid = Ext.getCmp('eventList');
		var sm = grid.getSelectionModel();
		var recs = sm.getSelection();
		if (recs.length == 1) {
			var num = grid.getStore().indexOf(recs[0]);
			var view = Ext.widget('eventEdit');
			view.down('form').loadRecord(eventStore.getAt(num));
		} else {
			Ext.Msg.alert('提示', '请选择某一行，进行修改！');
		}
    },
	// 查找
	searchEventView : function(grid, record) {

		var view = Ext.widget('eventSearch');
		view.down('form');
	},

	searchEvent : function(button) {
		var win = button.up('eventSearch');
		form = win.down('form');
		var a = form.getForm().findField('id').getValue();
		index = eventStore.findBy(function(record, id) {
			return record.get('id') == '' + a;
		});
		if (index == -1) {
			Ext.MessageBox.alert("提示", "没有数据!!!");
		} else {
			var value1 = eventStore.getAt(index).get('name');
			form.getForm().findField('name').setValue(value1);
		}
	}
});

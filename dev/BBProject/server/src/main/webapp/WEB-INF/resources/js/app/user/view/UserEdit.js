Ext.define('user.view.UserEdit', {
	extend : 'Ext.window.Window',
	alias : 'widget.userEdit',
	title : '编辑课程基本信息',
	layout : 'fit',
	autoShow : true,
	initComponent : function() {
		this.items = [ {
			xtype : 'form',
			height : 300,
			width : 300,
			border : true,
			items : [ {
				xtype : 'textfield',
				name : 'name',
				fieldLabel : '用户名称'
			}, {
				xtype : 'textfield',
				name : 'password',
				fieldLabel : '用户密码'
			} ]
		} ];
		this.buttons = [ {
			text : '保存',
			action : 'save'
		}, {
			text : '取消',
			scope : this,
			handler : this.close
		} ];
		this.callParent(arguments);
	}
});

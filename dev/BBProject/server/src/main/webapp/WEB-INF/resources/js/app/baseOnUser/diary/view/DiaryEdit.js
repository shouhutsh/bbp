Ext.define('diary.view.DiaryEdit', {
	extend : 'Ext.window.Window',
	alias : 'widget.diaryEdit',
	title : '编辑事件信息',
	layout : 'fit',
	autoShow : true,
	initComponent : function() {
		this.items = [ {
			xtype : 'form',
			height : 300,
			width : 300,
			border : true,
			items : [ {
                xtype : 'textfield',
                name : 'title',
                fieldLabel : '标题'
            } ]
		} ];
		this.buttons = [ {
			text : '保存',
			action : 'save'
		}, {
			text : '取消',
			scope : this,
			handler : this.close
		} ];
		this.callParent(arguments);
	}
});

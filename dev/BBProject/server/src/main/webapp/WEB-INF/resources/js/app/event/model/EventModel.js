Ext.define('event.model.EventModel', {
	extend : 'Ext.data.Model',
	alias : 'widget.eventModel',
	fields : [ {
		name : 'id',
		type : 'string',
		sortable : true
	}, {
        name : 'name',
        type : 'string',
        sortable : true
    },{
        name : 'unit',
        type : 'string',
        sortable : true
    }/*, {
        name : 'parent',
        type : 'object',
        sortable : true
    }*/ ]
});
var pageSize = 20;

var userStore = Ext.create('user.store.UserStore');

// 创建多选
var selModel = Ext.create('Ext.selection.CheckboxModel');

Ext.define('user.view.UserGrid', {
	extend : 'Ext.grid.GridPanel',
	alias : 'widget.userGrid',
	title : '用户列表',
	store : userStore,
	columnLines : true,
	selModel : selModel,
	columns : [ {
		text : 'ID',
		width : 50,
		sortable : true,
		dataIndex : 'id'
	}, {
		text : '用户名称',
		width : 200,
		sortable : true,
		dataIndex : 'name'
	}, {
		text : '用户密码',
		width : 200,
		sortable : true,
		dataIndex : 'password'
	} ],

	viewConfig : {
		columnsText : '列',
		sortAscText : '升序',
		sortDescText : '降序'
	},
	dockedItems : [ {
		xtype : 'toolbar',
		items : [ {
			action : 'add',
			text : '添加'
		}, '-', {
			action : 'remove',
			text : '删除',
			disabled : true
		} ]
	} ],
	bbar : new Ext.PagingToolbar({
		pageSize : pageSize,// 每页显示的记录值
		store : userStore,
		displayInfo : true,
		firstTest : '首页',
		lastText : '尾页',
		nextText : '下页',
		prevText : '前页',
		beforePageText : '第',
		afterPageText : '页，共{0}页',
		displayMsg : '记录数：第{0}条 - 第{1}条，共 {2}条',
		emptyMsg : "没有记录"
	})
});

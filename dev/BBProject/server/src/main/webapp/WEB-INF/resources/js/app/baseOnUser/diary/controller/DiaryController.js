Ext.define('diary.controller.DiaryController', {
	extend : 'Ext.app.Controller',
	alias : 'widget.DiaryController',
	stores : [ 'DiaryStore' ],
	models : [ 'DiaryModel' ],
	views : [ 'DiaryGrid', 'DiaryEdit', 'DiaryAdd', 'DiarySearch' ],
	init : function() {
		this.control({
			'diaryGrid' : {
				itemdblclick : this.editDiary
			},
			'diaryEdit button[action=save]' : {
				click : this.updateDiary
			},
            'diaryGrid  button[action=addBtn]'  :{
                click : this.addView
            },
            'diaryAdd button[action=save]' : {
                click : this.addDiary
            },
            // List页面 删除 按钮绑定事件
            'diaryGrid button[action=delBtn]' : {
                click : this.destoryDiary
            },
            // List页面 更新 按钮绑定事件
            'diaryGrid button[action=upBtn]' : {
                click : this.upDiary
            },
			// 添加查找
			'diaryGrid [action=find]' : {
				click : this.searchDiaryView
			},
			'diarySearch [action=search]' : {
				click : this.searchDiary
			}
		});
	},
	editDiary : function(grid, record) {
		var view = Ext.widget('diaryEdit');
		view.down('form').loadRecord(record);
	},
	updateDiary : function(button) {
		var win = button.up('window');
		form = win.down('form');
		record = form.getRecord();
		values = form.getValues();
		record.set(values);
		win.close();
	},
    addView : function() {
        Ext.widget('diaryAdd');
    },
    /**
     * 添加
     */
    addDiary : function(button) {
        var win = button.up('diaryAdd');
        form = win.down('form');
        values = form.getValues();
        if (confirm("确定保存记录吗？"))
        {
            diaryStore.insert(0, values);
        }
        diaryStore.reload();
        win.close();
    },
    /**
     * 删除
     */
    destoryDiary : function(o) {
        var grid = o.ownerCt.ownerCt;
        var sm = grid.getSelectionModel();
        var recs = sm.getSelection();
        var count = recs.length;
        var record;
        if (count != 0) {
            Ext.MessageBox.confirm("提示", "确定要删除吗?", function (button){
                if (button == "yes") {
                    for (var i = 0; i < count; i++) {
                        record = recs[i];
                        diaryStore.remove(record);
                        grid.reconfigure(diaryStore);
                    }
                }
            });
        } else {
            Ext.MessageBox.alert("提示", "请选择要删除的行!");
            return false;
        };
        diaryStore.load();
    },
    /**
     * 修改 点击头部修改按钮 修改选中项
     *
     */
    upDiary : function() {
    	var grid = Ext.getCmp('diaryList');
		var sm = grid.getSelectionModel();
		var recs = sm.getSelection();
		if (recs.length == 1) {
			var num = grid.getStore().indexOf(recs[0]);
			var view = Ext.widget('diaryEdit');
			view.down('form').loadRecord(diaryStore.getAt(num));
		} else {
			Ext.Msg.alert('提示', '请选择某一行，进行修改！');
		}
    },
	// 查找
	searchDiaryView : function(grid, record) {

		var view = Ext.widget('diarySearch');
		view.down('form');
	},

	searchDiary : function(button) {
		var win = button.up('diarySearch');
		form = win.down('form');
		var a = form.getForm().findField('id').getValue();
		index = diaryStore.findBy(function(record, id) {
			return record.get('id') == '' + a;
		});
		if (index == -1) {
			Ext.MessageBox.alert("提示", "没有数据!!!");
		} else {
			var value1 = diaryStore.getAt(index).get('title');
			form.getForm().findField('title').setValue(value1);
		}
	}
});

/**
 * Created by ZL on 14-6-28.
 */
//ajax方式请求数据
function getData() {
    var arr = new Array();
    $.ajax({
        type: "get",
        url: basePath + "height/getHeightData.json",
        async: false,
        dataType: "json",
        success: function (json) {
            for (var i in json) {
                arr[i] = json[i].height;
            }
        },
        error: function () {
            alert("error");
        }
    });
    return arr;
}
require.config({

    paths: {

        echarts: 'http://echarts.baidu.com/build/echarts',

        'echarts/chart/bar': 'http://echarts.baidu.com/build/echarts',//这里需要注意的是除了mapchart使用的配置文件为echarts-map之外，
        //其他的图形引用的配置文件都为echarts，这也是一般的图形跟地图的区别

        'echarts/chart/line': 'http://echarts.baidu.com/build/echarts'

    }

});
require(

    [

        'echarts',

        'echarts/chart/bar',

        'echarts/chart/line'

    ], function (ec) {
        // 基于准备好的dom，初始化echarts图表
        var myChart = ec.init(document.getElementById('main'));

        var option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['身高']
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {show: true},
                    dataView: {show: true},
                    magicType: {show: true, type: ['line', 'bar']},
                    restore: {show: true},
                    saveAsImage: {show: true}
                }
            },
            xAxis: [
                {
                    type: 'category',
                    position: 'bottom',
                    boundaryGap: true,
                    axisLine: {    // 轴线
                        show: true,
                        lineStyle: {
                            color: 'green',
                            type: 'solid',
                            width: 2
                        }
                    },
                    axisTick: {    // 轴标记
                        show: true,
                        length: 10,
                        lineStyle: {
                            color: 'red',
                            type: 'solid',
                            width: 2
                        }
                    },
                    axisLabel: {
                        show: true,
                        interval: 'auto',    // {number}
                        rotate: 0,
                        margin: 8,
                        formatter: '{value}月',
                        textStyle: {
                            color: 'blue',
                            fontFamily: 'sans-serif',
                            fontSize: 15,
                            fontStyle: 'italic',
                            fontWeight: 'bold'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#483d8b',
                            type: 'dashed',
                            width: 1
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(144,238,144,0.3)', 'rgba(135,200,250,0.3)']
                        }
                    },
                    data: [
                        '1', '2', '3', '4', '5',
                        {
                            value: '6',
                            textStyle: {
                                color: 'red',
                                fontSize: 30,
                                fontStyle: 'normal',
                                fontWeight: 'bold'
                            }
                        },
                        '7', '8', '9', '10', '11', '12'
                    ]
                },
                {
                    type: 'category',
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    position: 'left',
                    min: 0,
                    max: 100,
                    precision: 1,
                    power: 10,
                    splitNumber: 10,
                    boundaryGap: [0, 0.1],
                    axisLine: {    // 轴线
                        show: true,
                        lineStyle: {
                            color: 'red',
                            type: 'dashed',
                            width: 2
                        }
                    },
                    axisTick: {    // 轴标记
                        show: true,
                        length: 10,
                        lineStyle: {
                            color: 'green',
                            type: 'solid',
                            width: 2
                        }
                    },
                    axisLabel: {
                        show: true,
                        interval: 'auto',    // {number}
                        rotate: 0,
                        margin: 18,
                        formatter: '{value} cm',    // Template formatter!
                        textStyle: {
                            color: '#1e90ff',
                            fontFamily: 'verdana',
                            fontSize: 10,
                            fontStyle: 'normal',
                            fontWeight: 'bold'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#483d8b',
                            type: 'dotted',
                            width: 2
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(205,92,92,0.3)', 'rgba(255,215,0,0.3)']
                        }
                    }
                },
                {
                    type: 'value',
                    precision: 1,
                    splitNumber: 10,
                    axisLabel: {
                        formatter: function (value) {
                            // Function formatter
                            return value + 'cm'
                        }
                    },
                    splitLine: {
                        show: false
                    }
                }
            ],
            series: [
                {
                    name: '身高',
                    type: 'bar',
                    data: getData()
                }
            ]
        };
        myChart.setOption(option);

    });
var pageSize = 20;

var diaryStore = Ext.create('diary.store.DiaryStore');

// 创建多选
var selModel = Ext.create('Ext.selection.CheckboxModel');

Ext.define('diary.view.DiaryGrid', {
	extend : 'Ext.grid.GridPanel',
	alias : 'widget.diaryGrid',
	id : 'diaryList',		// 新增
	title : '事件基本信息',
	store : diaryStore,
	columnLines : true,
	selModel : selModel,
	columns : [ {
		text : 'ID',
		width : 100,
		sortable : true,
		dataIndex : 'id'
	}, {
        text : '标题',
        width : 200,
        sortable : true,
        dataIndex : 'title'
    },{
    text : '内容',
        width : 200,
        sortable : true,
        dataIndex : 'content'
}/*, {
        text : 'PARENT',
        width : 200,
        sortable : true,
        dataIndex : 'parent.id'
    }*/ ],

    viewConfig : {
        columnsText : '列',
        sortAscText : '升序',
        sortDescText : '降序'
    },
    dockedItems : [ {
        xtype : 'toolbar',
        items : [ {
            action : 'addBtn',
            text : '添加',
            tooltip : '添加记录',
            iconCls : 'add'
        }, '-',{
            text : '修改',
            tooltip : '修改记录',
            iconCls : 'option',
            action : 'upBtn'
        },'-', {
            itemId : 'removeBtn',
            action : 'delBtn',
            text : '删除',
            tooltip : '删除所选记录',
            iconCls : 'remove'
        }, '-', {
			action : 'find',
			text : '查找(按ID查找)',
			tooltip : '查找记录',
			iconCls : 'find'
		} ]
    } ],
    bbar : new Ext.PagingToolbar({
        pageSize : pageSize,// 每页显示的记录值
        store : diaryStore,
        displayInfo : true,
        firstTest : '首页',
        lastText : '尾页',
        nextText : '下页',
        prevText : '前页',
        beforePageText : '第',
        afterPageText : '页，共{0}页',
        displayMsg : '记录数：第{0}条 - 第{1}条，共 {2}条',
        emptyMsg : "没有记录"
    })
});

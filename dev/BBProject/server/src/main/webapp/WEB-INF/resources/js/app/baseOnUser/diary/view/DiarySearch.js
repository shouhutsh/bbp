Ext.define('diary.view.DiarySearch', {
	extend : 'Ext.window.Window',
	alias : 'widget.diarySearch',
	title : '查找信息',
	layout : 'fit',
	autoShow : true,
	initComponent : function() {
		this.items = [ {
			xtype : 'form',
			height : 270,
			width : 300,
			border : true,
			items : [ {
				xtype : 'textfield',
				name : 'id',
				fieldLabel : 'ID'
			}, {
				xtype : 'textfield',
				name : 'title',
				fieldLabel : 'title'
			} ]
		} ];
		this.buttons = [ {
			text : '查找',
			action : 'search'
		}, {
			text : '取消',
			scope : this,
			handler : this.close
		} ];
		this.callParent(arguments);
	}
});

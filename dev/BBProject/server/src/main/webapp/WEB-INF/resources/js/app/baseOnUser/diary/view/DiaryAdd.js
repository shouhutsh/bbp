/**
 * Created by Qi_2 on 2014/5/31 0031.
 */
Ext.define('diary.view.DiaryAdd', {
    extend: 'Ext.window.Window',
    alias: 'widget.diaryAdd',
    title: '编辑日志基本信息',
    layout: 'fit',
    autoShow: true,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                height: 300,
                width: 300,
                border: true,
                items: [   {
                        xtype: 'textfield',
                        name: 'title',
                        fieldLabel: '标题'
                    }, {
                    xtype: 'textfield',
                    name: 'content',
                    fieldLabel: '备注'
                } ]
            }
        ];
        this.buttons = [
            {
                text: '保存',
                action: 'save'
            },
            {
                text: '取消',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});

Ext.define('user.model.UserModel', {
	extend : 'Ext.data.Model',
	alias : 'widget.userModel',
	fields : [ {
		name : 'id',
		type : 'int',
		sortable : true
	}, {
		name : 'name',
		type : 'string',
		sortable : true
	}, {
        name : 'password',
        type : 'string',
        sortable : true
    } ]
});
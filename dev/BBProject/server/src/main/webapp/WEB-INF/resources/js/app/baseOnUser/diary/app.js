Ext.application({
	requires : [ 'Ext.container.Viewport' ],
	name : 'diary', // 命名空间
	appFolder : ctx + '/resources/js/app/baseOnUser/diary',
	controllers : [ 'DiaryController' ],
	// 这个所有加载完毕最后执行
	launch : function() {
		Ext.create('Ext.container.Viewport', {
			id : 'diaryGrid',
			title : '主面板',
			layout : 'fit',
			border : true,
			items : [ {
				xtype : 'diaryGrid'
			} ]
		});// create
	}// launch;
});// application;

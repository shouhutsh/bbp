Ext.define('event.view.EventSearch', {
	extend : 'Ext.window.Window',
	alias : 'widget.eventSearch',
	title : '查找信息',
	layout : 'fit',
	autoShow : true,
	initComponent : function() {
		this.items = [ {
			xtype : 'form',
			height : 270,
			width : 300,
			border : true,
			items : [ {
				xtype : 'textfield',
				name : 'id',
				fieldLabel : 'ID'
			}, {
				xtype : 'textfield',
				name : 'name',
				fieldLabel : 'NAME'
			} ]
		} ];
		this.buttons = [ {
			text : '查找',
			action : 'search'
		}, {
			text : '取消',
			scope : this,
			handler : this.close
		} ];
		this.callParent(arguments);
	}
});

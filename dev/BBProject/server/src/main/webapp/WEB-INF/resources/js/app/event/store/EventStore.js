var pageSize = 20;

Ext.define('event.store.EventStore', {
	extend : 'Ext.data.Store',
	alias : 'widget.EventStore',
	autoLoad : true,
	autoSync : true,// 需要同步
	model : 'event.model.EventModel',
	proxy : {
		type : 'rest',
		format : 'json',
		url : ctx + '/event/',
		headers : {
			'Content-Type' : 'application/json'
		},
		reader : {
			type : 'json',
			root : 'content',
			totalProperty : 'totalElements'
		},
		writer : {
			type : 'json',
			allowSingle : 'true'
		}
	},
	// 每页显示的记录行数
	pageSize : pageSize
});
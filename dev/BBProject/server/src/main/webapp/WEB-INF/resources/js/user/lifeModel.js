/**
 * Created by ZL on 14-6-27.
 */
function save(event){
    $.ajax({
        type: "post",
        url: basePath + event+"/0.json",
        contentType: 'application/json;charset=utf-8',
        data: JSON.stringify(params),
        dataType: "json",
        success: function (result) {
            alert(successMsg);
        },
        error: function () {
            alert(errorMsg);
        }
    });
}
$(function () {
    var uid = $("#userIdInput").val();
    $(function ($) {
        var name = $("#id").val();
        //初始化一级表
        $.ajax({
            type: "get",
            url: basePath + "event/getChildren.json",
            dataType: "json",
            success: function (json) {
                $("#first").empty();
                $("#first").append("<option>一级表</option>");
                for (var p in json) {
                    $("#first").append("<option id=" + json[p].id + ">" + json[p].name + "</option>");
                }
            },
            error: function () {
                alert("error");
            }
        });
    });
//拉取二级表的数据
    $("#first").change(function () {
        //初始化div
        if ($("#first").val() == "吃" || $("#first").val() == "喝") {
            //清除
            var remark = $("#remark").val("");
            var date = $("#date").val("");
            var amount = $("#amount").val("");
            var endTime = $("#endTime").val("");

            $("#endTimeDiv").hide();
            $("#amountDiv").show();
            $("#unit").show();
        } else if ($("#first").val() == "拉" || $("#first").val() == "撒") {
            //清除
            var remark = $("#remark").val("");
            var date = $("#date").val("");
            var amount = $("#amount").val("");
            var endTime = $("#endTime").val("");

            $("#unit").hide();
            $("#endTimeDiv").hide();
        } else if ($("#first").val() == "睡") {
            //清除
            var remark = $("#remark").val("");
            var date = $("#date").val("");
            var amount = $("#amount").val("");
            var endTime = $("#endTime").val("");

            $("#unit").hide();
            $("#endTimeDiv").show();
        } else if ($("#first").val() == "身高" || $("#first").val() == "体重") {
            //清除
            var remark = $("#remark").val("");
            var date = $("#date").val("");
            var amount = $("#amount").val("");
            var endTime = $("#endTime").val("");

            $("#endTimeDiv").hide();
            $("#amountDiv").show();
            $("#unit").show();
        }
        else {

        }
        var firstId = $("#first").find("option:selected").attr("id");
        var param = {
            id: firstId,
            uid: uid
        };
        $.ajax({
            type: "get",
            url: basePath + "event/getChildren.json",
            data: param,
            dataType: "json",
            success: function (json) {
                $("#second").empty();
                $("#second").append("<option>二级表</option>");
                for (var p in json) {
                    $("#second").append("<option id=" + json[p].id + ">" + json[p].name + "</option>");
                }
                $("#second").append("<option id=" + "addSecond" + ">新增</option>")
            },
            error: function () {
                alert("error");
            }
        });
    });
//拉取三级表的数据
    $("#second").change(function () {
        if ($("#second").val() === "新增") {
            $("#addSecondModal").modal('show');
            $("#secondName").val("");
        }
        else {
            var secondId = $("#second").find("option:selected").attr("id");
            var param = {
                id: secondId,
                uid: uid
            };
            $.ajax({
                type: "get",
                url: basePath + "event/getChildren.json",
                dataType: "json",
                data: param,
                success: function (json) {
                    $("#third").empty();
                    $("#third").append("<option>三级表</option>");
                    for (var p in json) {
                        $("#third").append("<option id=" + json[p].id + ">" + json[p].name + "</option>");
                    }
                    $("#third").append("<option id=" + "addThird" + ">新增</option>");
                },
                error: function () {
                    alert("error");
                }


            });
        }
    });
//操作三级表
    $("#third").change(function () {
        if ($("#third").val() === "新增") {
            $("#addThirdModal").modal('show');
            $("#thirdName").val("");
        }
    });
//保存二级表
    $("#saveSecondBtn").click(function () {
        var firstId = $("#first").find("option:selected").attr("id");
        var secondName = $("#secondName").val();
        $.ajax({
            type: "post",
            url: basePath + "event/0.json",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                user: {id: uid},
                parent: {id: firstId},
                name: secondName
            }),
            success: function (json) {
                alert("添加成功");
                $("#second option:last").remove();
                $("#second option[text='新增']").remove();
                $("#second").append("<option id=" + result.id + ">" + result.name + "</option>");
                $("#second").append("<option id=" + "addSecond" + ">新增</option>");

            },
            error: function (json) {
                alert("添加失败");
            }

        });
        $("#addSecondModal").modal("hide");
        event.stopPropagation();

    });

//保存三级表
    $("#saveThirdBtn").click(function () {
        var secondId = $("#second").find("option:selected").attr("id");
        var thirdName = $("#thirdName").val();
        $.ajax({
            type: "post",
            url: basePath + "event/0.json?_d="+(new Date()),
            dataType: "json",
            async: false,
            cache:false,
            mode: "abort",
            contentType: "application/json",
            data: JSON.stringify({
                user: {id: uid},
                name: thirdName,
                parent: {id: secondId}
            }),
            success: function (result) {
                alert("添加成功");
                $("#third option[text='新增']").remove();
                $("#third").append("<option id=" + result.id + ">" + result.name + "</option>");
                $("#third").append("<option id=" + "addThird" + ">新增</option>");

            },
            error: function (json) {
                alert("添加失败");
            }
        });
        $("#addThirdModal").modal("hide");
        event.stopPropagation();

    });
    $("#saveBtn").click(function () {
        var successMsg = "记录添加成功";
        var errorMsg = "记录添加失败";
        var firstName = $("#first").find("option:selected").val();
        var firstId = $("#first").find("option:selected").attr("id");
        var remark = $("#remark").val();
        var date = $("#date").val();
        var amount = $("#amount").val();
        var endTime = $("#endTime").val();
        var params;
        if (firstName == "吃" || firstName == "喝") {
            params = {
                user: {id: uid},
                event: {id: firstId},
                amount: amount,
                //        date:date,
                remark: remark
            };
            if (firstName == "吃") {
                $.ajax({
                    type: "post",
                    url: basePath + "eat/0.json",
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (result) {
                        alert(successMsg);
                    },
                    error: function () {
                        alert(errorMsg);
                    }
                });
                event.stopPropagation();
            } else {
                $.ajax({
                    type: "post",
                    url: basePath + "drink/0.json",
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (result) {
                        alert(successMsg);
                    },
                    error: function () {
                        alert(errorMsg);
                    }
                });
                event.stopPropagation();
            }
        } else if (firstName == "拉" || firstName == "撒") {
            params = {
                user: {id: uid},
                event: {id: firstId},
                //        date:date,
                remark: remark
            };
            if (firstName == "拉") {
                $.ajax({
                    type: "post",
                    url: basePath + "bb/0.json",
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (result) {
                        alert(successMsg);
                    },
                    error: function () {
                        alert(errorMsg);
                    }
                });
                event.stopPropagation();

            } else {
                $.ajax({
                    type: "post",
                    url: basePath + "nn/0.json",
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (result) {
                        alert(successMsg);
                    },
                    error: function () {
                        alert(errorMsg);
                    }
                });
                event.stopPropagation();
            }
        } else if (firstName == "身高" || firstName == "体重") {
            params = {
                event: {id: firstId},
                user: {id: uid},
                amount: amount,
                //        date:date,
                remark: remark
            };
            if (firstName == "身高") {
                $.ajax({
                    type: "post",
                    url: basePath + "height/0.json",
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (result) {
                        alert(successMsg);
                    },
                    error: function () {
                        alert(errorMsg);
                    }
                });
                event.stopPropagation();
            } else {
                $.ajax({
                    type: "post",
                    url: basePath + "weight/0.json",
                    data: JSON.stringify(params),
                    dataType: "json",
                    success: function (result) {
                        alert(successMsg);
                    },
                    error: function () {
                        alert(errorMsg);
                    }
                });
                event.stopPropagation();
            }
        } else if (firstName == "睡") {
            params = {
                event: {id: firstId},
                user: {id: uid},
                //        date:date,
                // endTime：endTime,
                remark: remark
            };
            $.ajax({
                type: "post",
                url: basePath + "sleep/0.json",
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(params),
                dataType: "json",
                success: function (result) {
                    alert(successMsg);
                },
                error: function () {
                    alert(errorMsg);
                }
            });
            event.stopPropagation();
        }

    });
});

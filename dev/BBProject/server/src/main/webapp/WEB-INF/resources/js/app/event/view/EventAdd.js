/**
 * Created by Qi_2 on 2014/5/31 0031.
 */
Ext.define('event.view.EventAdd', {
    extend: 'Ext.window.Window',
    alias: 'widget.eventAdd',
    title: '编辑类别基本信息',
    layout: 'fit',
    autoShow: true,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                height: 300,
                width: 300,
                border: true,
                items: [   {
                        xtype: 'textfield',
                        name: 'name',
                        fieldLabel: 'NAME'
                    }/*, {
                    xtype: 'textfield',
                    name: 'parent.id',
                    fieldLabel: 'PARENT'
                }*/ ]
            }
        ];
        this.buttons = [
            {
                text: '保存',
                action: 'save'
            },
            {
                text: '取消',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    }
});

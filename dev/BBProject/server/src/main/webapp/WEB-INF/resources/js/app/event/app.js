Ext.application({
	requires : [ 'Ext.container.Viewport' ],
	name : 'event', // 命名空间
	appFolder : ctx + '/resources/js/app/event',
	controllers : [ 'EventController' ],
	// 这个所有加载完毕最后执行
	launch : function() {
		Ext.create('Ext.container.Viewport', {
			id : 'eventGrid',
			title : '主面板',
			layout : 'fit',
			border : true,
			items : [ {
				xtype : 'eventGrid'
			} ]
		});// create
	}// launch;
});// application;

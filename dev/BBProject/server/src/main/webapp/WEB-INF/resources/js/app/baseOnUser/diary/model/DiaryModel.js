Ext.define('diary.model.DiaryModel', {
	extend : 'Ext.data.Model',
	alias : 'widget.diaryModel',
	fields : [ {
		name : 'id',
		type : 'string',
		sortable : true
	}, {
        name : 'title',
        type : 'string',
        sortable : true
    },{
        name : 'content',
        type : 'string',
        sortable : true
    }/*, {
        name : 'parent',
        type : 'object',
        sortable : true
    }*/ ]
});
<%--
  Created by IntelliJ IDEA.
  User: ZL
  Date: 14-6-27
  Time: 下午2:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
%>
<html>
<head>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.min.js"></script>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
            rel="stylesheet" type="text/css"/>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap-theme.min.css"
            rel="stylesheet" type="text/css"/>

    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="application/javascript">
        $(function(){
            $("#saveBtn").click(function(){
                var name=$("#name").val();
                $.ajax({
                    type: "post",
                    url: basePath + "event/addChildren.json",
                    dataType: "json",
                    data: {
                        parentId: firstId,
                        name: name
                    },
                    success: function (json) {
                        alert("添加成功");
                    },
                    error: function (json) {
                        alert("addSecondError");
                    }

                });
            });
        });
    </script>
</head>
<body>
<div class="container">
    <div class="row clearfix">
        <div class="col-md-12 column" method="post">
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name">名称</label>
                    <div class="col-sm-2">
                        <input type="text" class="input" id="name">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary" id="saveBtn">保存</button>
                        <button type="reset" class="btn btn-default">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" import="java.util.*" %>
<%@ page import="edu.zut.cs.bbp.Base.user.domain.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta name="description"
          content="Bootstrap Version2.0 form error example from w3cschool.cc.">
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.min.js"></script>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
            rel="stylesheet" type="text/css"/>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap-theme.min.css"
            rel="stylesheet" type="text/css"/>
    <link href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap-datetimepicker.css" rel="stylesheet"
          type="text/css"/>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
    <script type="application/javascript" src="<%=path%>/resources/js/user/lifeModel.js"></script>
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="application/javascript" src="<%=path%>/resources/js/user/lifeModel.js/"></script>
</head>

<body>
<%
    User user = (User) request.getSession().getAttribute("user");
%>
<div hidden="hidden">
    <input id="userIdInput" value="${user.id}">
    <input id="passwordInput" value="${user.password}">
</div>
<fieldset>

    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">记一笔</h3>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="first">一级表</label>

                                <div class="col-sm-4">
                                    <select class="form-control" id="first" name="">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="second">二级表</label>

                                <div class="col-sm-4">
                                    <select class="form-control" id="second" name="">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="third">三级表</label>

                                <div class="col-sm-4">
                                    <select class="form-control" id="third" name="">
                                    </select>
                                </div>
                            </div>
                            <!-- time -->
                            <div class="form-group">
                                <div class="control-group">
                                    <label class="col-sm-2 control-label" for="time">时间</label>

                                    <div class=" col-sm-4 controls input-append date form_datetime"
                                         data-date-format="yyyy-MM-dd hh:mm:ss " data-link-field="dtp_input1">
                                        <input size="16" type="text" value="" class="input" id="time" name="time"
                                               readonly> <span
                                            class="add-on"><i class="icon-remove"></i></span> <span
                                            class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </div>
                            </div>
                            <!-- end time    the div for sleep -->
                            <div class="form-group" hidden="hidden" id="endTimeDiv">
                                <label class="col-sm-2 control-label" for="endTime">结束时间</label>

                                <div class=" col-sm-4 controls input-append date form_datetime"
                                     data-date-format="yyyy-MM-dd" data-link-field="dtp_input1">
                                    <input size="16" type="text" value="" class="input" id="endTime" name="endTime"
                                           readonly> <span
                                        class="add-on"><i class="icon-remove"></i></span> <span
                                        class="add-on"><i class="icon-th"></i></span>
                                </div>
                            </div>
                            <div class="form-group" hidden="hidden" id="amountDiv">
                                <label class="col-sm-2 control-label" for="amount">Amount</label>

                                <div class="col-sm-4">
                                    <input class="form-control" id="amount" name="">
                                    <span id="unit"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="remark">备注</label>

                                <div class="col-sm-4">
                                    <textarea class="form-control" class="form-control" rows="5" id="remark"
                                              cols="40"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveBtn">保存</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- 添加二级表Modal -->
    <div class="modal fade" id="addSecondModal" tabindex="-1" role="dialog"
         aria-labelledby="secondModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title">添加二级表</h4>
                </div>
                <div class="modal-body">
                    <label class="col-sm-4 control-label" for="secondName">二级表名称</label>

                    <div class="col-sm-2">
                        <input type="text" class="input" id="secondName">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="saveSecondBtn">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- 添加三级表Modal -->
    <div class="modal fade" id="addThirdModal" tabindex="-1" role="dialog"
         aria-labelledby="threeModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title">添加三级表</h4>
                </div>
                <div class="modal-body">
                    <label class="col-sm-4 control-label" for="thirdName">三级表名称</label>

                    <div class="col-sm-2">
                        <input type="text" class="input" id="thirdName">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="saveThirdBtn">保存</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</fieldset>

<script type="application/javascript">
    $(function () {
        $('.form_datetime').datetimepicker({
            language: 'zh-CN',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
    });

</script>
</body>
</html>
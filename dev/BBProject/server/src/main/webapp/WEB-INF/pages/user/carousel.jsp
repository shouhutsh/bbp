<%@ page import="edu.zut.cs.bbp.Base.user.domain.User" %>
<%--
  Created by IntelliJ IDEA.
  User: ZL
  Date: 14-6-30
  Time: 下午4:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
%>
<html>
<head>
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="application/javascript" src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.js"></script>
    <link href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.css"
          rel="stylesheet" type="text/css"/>

    <script type="application/javascript">
        $(function () {
            $('.carousel').carousel({
                interval: 2000
            });
            $("#click").click(function()
            {

            });
        });

    </script>
    <style type="text/css">
        .container {
            width: 850px;
            height: 800px;
        }
    </style>
</head>
<body>
<div class="container">
    <div id="myCarousel" class="carousel slide">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
        </ol>
        <!-- 旋转木马项目 -->
        <div class="carousel-inner">
            <div class="active item">
                <img src="<%=path%>/resources/images/baobao-1.jpg"/>

                <div class="carousel-caption">
                    <h1>在这里,开始记录宝宝的点滴</h1>
                </div>
            </div>
            <div class="item">
                <img src="<%=path%>/resources/images/baobao-2.jpg"/>

                <div class="carousel-caption">
                    <h1>记录宝宝的人生第一次</h1>
                </div>
            </div>
            <div class="item">
                <img src="<%=path%>/resources/images/baobao-3.jpg"/>

                <div class="carousel-caption">
                    <h1>记住生活得美好瞬间</h1>
                </div>
            </div>
            <div class="item">
                <img src="<%=path%>/resources/images/baobao-4.jpg"/>

                <div class="carousel-caption">
                    <h1><span id="startSpan">
                       <% User user = (User) request.getSession().getAttribute("user");
                           if (null == user) {%><a href="<%=path%>/user/login.html">现在开始记录吧</a><%}else {%>
                        <script type="application/javascript">

                        </script>
                        <%}%></span></h1>
                </div>
            </div>
        </div>
        <!-- 旋转木马导航 -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
        <a class="carousel-control right" href="#myCarousel"
           data-slide="next">&rsaquo;</a>
    </div>
</div>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
<script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
<script type="text/javascript"
	src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.min.js"></script>
<link
	href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://s1.bdstatic.com/r/www/cache/ecom/esl/1-6-10/esl.js"></script>
<script type="text/javascript">
        function getData()
        {
            var arr = new Array();
              $.ajax({
                type:"get",
                url:basePath+"height/getHeightData.json",
                contentType: 'application/json;charset=utf-8',
                async:false,
                dataType:"json",
                success:function(json){
                    for(var i in json)
                    {
                        arr[i] = json[i].height;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
            return arr;
        }
</script>
  </head>
<body style="height: 450px;">
	<div id="main"
		style="width:inherit;height: 450px; border: 1px solid #ccc; padding: 0px;"></div>
	<script type="text/javascript">
		require.config({

			paths : {

				echarts : 'http://echarts.baidu.com/build/echarts',

				'echarts/chart/bar' : 'http://echarts.baidu.com/build/echarts',//这里需要注意的是除了mapchart使用的配置文件为echarts-map之外，
				//其他的图形引用的配置文件都为echarts，这也是一般的图形跟地图的区别

				'echarts/chart/line' : 'http://echarts.baidu.com/build/echarts'

			}

		});
		require(

		[

		'echarts',

		'echarts/chart/bar',

		'echarts/chart/line'

		], function(ec) {
                    // 基于准备好的dom，初始化echarts图表
                    var myChart = ec.init(document.getElementById('main'));

                    var option = {
                        tooltip : {
                            trigger: 'axis'
                        },
                        legend: {
                            data:['身高']
                        },
                        toolbox: {
                            show : true,
                            feature : {
                                mark : {show: true},
                                dataView : {show: true},
                                magicType : {show: true, type: ['line', 'bar']},
                                restore : {show: true},
                                saveAsImage : {show: true}
                            }
                        },
                        xAxis : [
                            {
                                type : 'category',
                                position: 'bottom',
                                boundaryGap: true,
                                axisLine : {    // 轴线
                                    show: true,
                                    lineStyle: {
                                        color: 'green',
                                        type: 'solid',
                                        width: 2
                                    }
                                },
                                axisTick : {    // 轴标记
                                    show:true,
                                    length: 10,
                                    lineStyle: {
                                        color: 'red',
                                        type: 'solid',
                                        width: 2
                                    }
                                },
                                axisLabel : {
                                    show:true,
                                    interval: 'auto',    // {number}
                                    rotate: 0,
                                    margin: 8,
                                    formatter: '{value}月',
                                    textStyle: {
                                        color: 'blue',
                                        fontFamily: 'sans-serif',
                                        fontSize: 15,
                                        fontStyle: 'italic',
                                        fontWeight: 'bold'
                                    }
                                },
                                splitLine : {
                                    show:true,
                                    lineStyle: {
                                        color: '#483d8b',
                                        type: 'dashed',
                                        width: 1
                                    }
                                },
                                splitArea : {
                                    show: true,
                                    areaStyle:{
                                        color:['rgba(144,238,144,0.3)','rgba(135,200,250,0.3)']
                                    }
                                },
                                data : [
                                    '1','2','3','4','5',
                                    {
                                        value:'6',
                                        textStyle: {
                                            color: 'red',
                                            fontSize: 30,
                                            fontStyle: 'normal',
                                            fontWeight: 'bold'
                                        }
                                    },
                                    '7','8','9','10','11','12'
                                ]
                            },
                            {
                                type : 'category',
                                data : ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
                            }
                        ],
                        yAxis : [
                            {
                                type : 'value',
                                position: 'left',
                                min: 0,
                                max: 100,
                                precision: 1,
                                power: 10,
                                splitNumber: 10,
                                boundaryGap: [0,0.1],
                                axisLine : {    // 轴线
                                    show: true,
                                    lineStyle: {
                                        color: 'red',
                                        type: 'dashed',
                                        width: 2
                                    }
                                },
                                axisTick : {    // 轴标记
                                    show:true,
                                    length: 10,
                                    lineStyle: {
                                        color: 'green',
                                        type: 'solid',
                                        width: 2
                                    }
                                },
                                axisLabel : {
                                    show:true,
                                    interval: 'auto',    // {number}
                                    rotate: 0,
                                    margin: 18,
                                    formatter: '{value} cm',    // Template formatter!
                                    textStyle: {
                                        color: '#1e90ff',
                                        fontFamily: 'verdana',
                                        fontSize: 10,
                                        fontStyle: 'normal',
                                        fontWeight: 'bold'
                                    }
                                },
                                splitLine : {
                                    show:true,
                                    lineStyle: {
                                        color: '#483d8b',
                                        type: 'dotted',
                                        width: 2
                                    }
                                },
                                splitArea : {
                                    show: true,
                                    areaStyle:{
                                        color:['rgba(205,92,92,0.3)','rgba(255,215,0,0.3)']
                                    }
                                }
                            },
                            {
                                type : 'value',
                                precision: 1,
                                splitNumber: 10,
                                axisLabel : {
                                    formatter: function(value) {
                                        // Function formatter
                                        return value + 'cm'
                                    }
                                },
                                splitLine : {
                                    show: false
                                }
                            }
                        ],
                        series : [
                            {
                                name: '身高',
                                type: 'bar',
                                data:getData()
                            }
                        ]
                    };
                    myChart.setOption(option);

		});
	</script>


</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: ZL
  Date: 14-6-24
  Time: 上午10:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
%>
<html>
<head>
    <title></title>
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="text/javascript" src="<%=path%>/resources/js/datepicker/jquery.ui.core.js"></script>
    <script type="text/javascript" src="<%=path%>/resources/js/datepicker/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="<%=path%>/resources/js/datepicker/jquery.ui.widget.js"></script>
    <link
            href="<%=path%>/resources/js/datepicker/jquery.ui.all.css"
            rel="stylesheet" type="text/css" />
    <link
            href="<%=path%>/resources/js/datepicker/demos.css"
            rel="stylesheet" type="text/css" />
    <script>
        $(function() {
            $( "#datepicker" ).datepicker(
                    {
                        showOn: "button",
                        buttonImage: "<%=path%>/resources/js/datepicker/images/calendar.gif",
                        buttonImageOnly: true
                    }
            );
            $( "#format" ).change(function() {
                $( "#datepicker" ).datepicker( "option", "dateFormat", $( this ).val() );
                $.datepicker.setDefaults( $.datepicker.regional[ "" ] )
            });
        });
    </script>
</head>
<body>
<p>Date: <input type="text" id="datepicker"></p>
</body>
</html>

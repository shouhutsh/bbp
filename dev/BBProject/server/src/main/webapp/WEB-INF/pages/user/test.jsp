<%--
  Created by IntelliJ IDEA.
  User: ZL
  Date: 14-6-25
  Time: 下午8:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%
    String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <link href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <script type="text/javascript"
            src="<%=path%>/resources/js/jquery-validation-1.12.0/dist/jquery.validate.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/js/jquery-validation-1.12.0/src/localization/messages_zh.js"></script>
    <link href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap-datetimepicker.css" rel="stylesheet"
          type="text/css"/>

    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>

    <script>


    </script>
</head>
<body>
<div class="control-group">
    <label class="control-label">DateTime Picking</label>
    <div class="controls input-append date form_datetime"

         data-date-format="yyyy-MM-dd" data-link-field="dtp_input1" >
        <input size="16" type="text" value="" readonly> <span
            class="add-on"><i class="icon-remove"></i></span> <span
            class="add-on"><i class="icon-th"></i></span>
    </div>
    <input  id="dtp_input1"  /><br />
</div>
<script type="application/javascript">
    $(function(){
        $('.form_datetime').datetimepicker({
            language: 'zh-CN',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });});

</script>

</body>
</html>

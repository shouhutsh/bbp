<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
%>

<!DOCTYPE html >
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.min.js"></script>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
            rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://s1.bdstatic.com/r/www/cache/ecom/esl/1-6-10/esl.js"></script>
    <script type="text/javascript">
        function getData()
        {
            var arr = new Array();
            $.ajax({
                type:"get",
                url:basePath+"height/getHeightData.json",
                contentType: 'application/json;charset=utf-8',
                async:false,
                dataType:"json",
                success:function(json){
                    for(var i in json)
                    {
                        arr[i] = json[i].height;
                    }
                },
                error:function(){
                    alert("error");
                }
            });
            return arr;
        }
    </script>
</head>
<body style="height: 450px;">
<div id="main"
     style="width:inherit;height: 450px; border: 1px solid #ccc; padding: 0px;"></div>
<script type="text/javascript">
    require.config({

        paths : {

            echarts : 'http://echarts.baidu.com/build/echarts',

            'echarts/chart/pie' : 'http://echarts.baidu.com/build/echarts'//这里需要注意的是除了mapchart使用的配置文件为echarts-map之外，
            //其他的图形引用的配置文件都为echarts，这也是一般的图形跟地图的区别

        }

    });
    require(

            [

                'echarts',

                'echarts/chart/pie'

            ], function(ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('main'));

                var option = option = {
                    title : {
                        text: '宝宝食谱',
                        subtext: '仅供参考',
                        x:'center'
                    },
                    tooltip : {
                        trigger: 'item',
                        formatter: "{a} <br/>{b} : {c} ({d}%)"
                    },
                    legend: {
                        orient : 'vertical',
                        x : 'left',
                        data:['蔬菜','水果','大米','面食','糕点','其他']
                    },
                    toolbox: {
                        show : true,
                        feature : {
                            mark : {show: true},
                            dataView : {show: true, readOnly: false},
                            restore : {show: true},
                            saveAsImage : {show: true}
                        }
                    },
                    calculable : true,
                    series : [
                        {
                            name:'访问来源',
                            type:'pie',
                            radius : '55%',
                            center: ['50%', '60%'],
                            data:[
                                {value:600, name:'蔬菜'},
                                {value:500, name:'水果'},
                                {value:800, name:'大米'},
                                {value:1000, name:'面食'},
                                {value:400, name:'糕点'},
                                {value:700,name:'其他'}
                            ]
                        }
                    ]
                };
                // 为echarts对象加载数据
                myChart.setOption(option);
            });
</script>


</body>
</html>
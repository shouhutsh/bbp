<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="edu.zut.cs.bbp.Base.user.domain.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta name="description"
          content="Bootstrap Version2.0 form error example from w3cschool.cc.">
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.min.js"></script>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
            rel="stylesheet" type="text/css"/>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap-theme.min.css"
            rel="stylesheet" type="text/css"/>
    <script type="application/javascript" src="<%=path%>/resources/js/user/lifeModel.js"></script>
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="application/javascript">
        var uid = $("#userIdInput").val();

        $(function () {
            $(".collapse").click(function () {
                $('.collapse').collapse();
            });
            //获取第一次的信息
            $.ajax({
                type: "get",
                url: basePath + "first/getFirstData.json",
                //   data: {user: {id: uid}},
                dataType: "json",
                async: false,
                contentType: "application/json",
                success: function (json) {
                    $("#first").empty();
                    var i = 1;
                    for (var p in json) {
                        $("#firstDiv").append("<span> <input type='checkbox' id=" + json[p].id + " name=" + json[p].name + " /> " + json[p].name + "</span>")
                        if (i % 3 == 0) {
                            $("#firstDiv").append("<br>");
                        }
                        i++;
                    }

                },
                error: function () {
                    alert('获取数据失败');
                }
            });
            //获取里程碑的信息
            $.ajax({
                type: "get",
                url: basePath + "milestone/getMilestoneData.json",
                //   data: {user: {id: uid}},
                dataType: "json",
                async: false,
                contentType: "application/json",
                success: function (json) {
                    $("#milestone").empty();
                    var i = 1;
                    for (var p in json) {
                        $("#milestoneDiv").append("<span> <input type='checkbox' id=" + json[p].id + " name=" + json[p].name + " /> " + json[p].name + "</span>")
                        if (i % 3 == 0) {
                            $("#milestoneDiv").append("<br>");
                        }
                        i++;
                    }

                },
                error: function () {
                    alert('获取数据失败');
                }
            });
            //保存日志
            $("#saveBtn").click(function () {
                var uid = $("#userIdInput").val();
                var title = $("#title").val();
                var content = $("#content").val();
                $.ajax({
                    type: "post",
                    url: basePath + "diary/0.json",
                    contentType: 'application/json;charset=utf-8',
                    async: false,
                    data: JSON.stringify({
                        title: title,
                        content: content,
                        user: {id: uid},
                    }),
                    dataType: "json",
                    success: function (result) {
                        alert("添加记录成功");
                    },
                    error: function () {
                        alert("添加失败");
                    }

                });
                event.stopPropagation();

            });
        });
    </script>
    <style type="text/css">
        span {
            width: 100px;
            height: 20px;
        }

        input #title {
            width: 300px;;
        }

        input .checkbox {
            width: 40px;
            height: 15px;
            font-family: fantasy;
        }

        .panel {
            width: 500px;
        }

        td .left {
            width: 200px;
            padding-right: 40px;
        }

        td .right {
            width: 300px;

        }

        .container {
            width: 500px;
            margin-right: 40%;
        }
        .panel-default
        {
            width: 480px;
        }
    </style>
</head>
<body>
<%
    User user = (User) request.getSession().getAttribute("user");
%>
<div hidden="hidden">
    <input id="userIdInput" value="${user.id}">
    <input id="passwordInput" value="${user.password}">
</div>
<div class="container">
    <form id="form" class="form-horizontal" role="form" method="post">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">记日志</h3>
            </div>
            <div class="panel-body">
                <table>
                    <tbody>
                    <tr style="height: 50px">
                        <td class="left"><label for="title">标题</label></td>
                        <td class="right"><input name="title" id="title"><br>
                        </td>
                    </tr>
                    <tr>
                        <td class="left"><label for="content" class="control-label">备注</label>
                        </td>
                        <td class="right"><textarea cols="20" rows="5" id="content" name="content"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="media">上传</label></td>
                        <td><input type="file" id="media"></td>
                    </tr>
                    </tbody>
                </table>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    第一次
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div id="firstDiv">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    里程碑
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div id="milestoneDiv">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" id="saveBtn">保存</button>
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>

<%@ page import="edu.zut.cs.bbp.Base.user.domain.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%
    String path = request.getContextPath();
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>宝贝计划</title>
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <link rel="stylesheet" type="text/css"
          href="<%=path%>/resources/js/tree_themes/SimpleTree.css"/>
    <script type="text/javascript" src="<%=path%>/resources/js/SimpleTree.js"></script>
    <link href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.css"
          rel="stylesheet" type="text/css"/>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap-theme.css"
            rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" type="text/css"
          href="<%=path%>/resources/js/tree_themes/SimpleTree.css"/>
    <script type="text/javascript" src="<%=path%>/resources/js/SimpleTree.js"></script>
    <script type="text/javascript">
        $(function () {
            if ($("#userIDInput").val() == "") {
                $("#div1").show();
                $("#div2").hide();
            }
            else {
                $("#div2").show();
                $("#div1").hide();
            }
            $(".st_tree").SimpleTree({
                click: function (a) {
                    if (!$(a).attr("hasChild")) {
                        alert($(a).attr("ref"));
                    }
                }
            });
        });
    </script>
<style>
    .div{
        width: 85%;
        margin-left: 10%;
        margin-right: 10%;
    }
</style>
</head>
<body>
<input hidden="hidden" id="userIDInput" value="${user.id}">

<div class="div">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <div class="row clearfix">
                <div class="col-md-12 column">
                    <ul class="nav nav-tabs"
                        style="font-weight: bolder; border-bottom: 2px; border-bottom-style: solid; border-bottom-color: blue;">
                        <li class="active" style="width: 40%;"><a><span
                                style="font-size: 24px">宝贝计划</span>&nbsp;&nbsp;<span
                                style="font-size: 16; font-weight: inherit;">Baby Plan</span></a></li>
                        <li><a href="">首页</a></li>
                        <li><a href="">宝贝社区</a></li>
                        <% User user = (User) request.getSession().getAttribute("user");
                            if (null == user) {%>
                        <li><a href="<%=path%>/user/login.html" target="_self">登录</a></li>
                        <li><a href="<%=path%>/user/register.html" target="_self">注册</a></li>
                        <%} else {%>
                        <li><a href="">${user.id} </a></li>
                        <li><a href="<%=path%>/user/exit.html">退出</a></li>
                        <%}%>
                        <li class="disabled"><a href="">换肤</a></li>
                        <li class="dropdown pull-right"><a href=""
                                                           data-toggle="dropdown" class="dropdown-toggle">应用下载<strong
                                class="caret"></strong></a>
                            <ul class="dropdown-menu">
                                <li><a href="">宝贝计划 for Android</a></li>
                                <li><a href="">Another action</a></li>
                                <li class="divider"></li>
                                <li><a href="">Separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row clearfix col-md-12 column">
            <div class="row clearfix" id="div1" hidden="hidden">
                <jsp:include page="carousel.jsp"></jsp:include>
            </div>
            <div id="div2" hidden="hidden">
                <div class="col-md-12 column">
                    <div class="row clearfix">
                        <div class="col-md-3 column"  style="border: 1px; border-bottom-color: gray;">
                            <div>
                                <span style="font-size: 22px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;用户概况</span>
                            </div>
                            <div>
                                <div>
                                    <div></div>
										<span style="font-size: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                href="<%=path%>/user/lifeModel.html" target="mainView">生活模块</a></span>
                                </div>
                                <div>
                                    <div></div>
										<span style="font-size: 20px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a
                                                href="<%=path%>/user/healthModel.html" target="mainView">健康模块</a></span>
                                </div>
                                <div>
                                    <div class="st_tree">
                                        <ul>
                                            <li><a href="javascript:void(0)" style="font-size: 20px">个人设置</a></li>
                                            <ul>
                                                <li><a href="<%=path%>/user/personalCenter.html"
                                                       target="mainView">个人信息</a></li>
                                                <li><a href="<%=path%>/event/index.html" target="mainView">个性化设置</a>
                                                </li>
                                                <li><a href="javascript:void(0)">消息管理</a></li>
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    <div class="st_tree">
                                        <ul>
                                            <li><a href="javascript:void(0)" style="font-size: 20px">信息共享</a></li>
                                            <ul>
                                                <li><a href="<%=path%>/diary/addDiary.html" target="mainView">记录</a></li>
                                                <li><a href="<%=path%>/diary/index.html" target="mainView">查看</a></li>
                                                <li><a href="javascript:void(0)">消息管理</a></li>
                                            </ul>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-9 column">
                            <div>
                                <iframe name="mainView"
                                        style="border:none; width: 100%; height: 600px; float: inherit;margin-right: 10px;"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-12 column"
                     style="margin-top:2%;border-top: 2px; border-top-style: solid; border-top-color: blue;"
                     align="center">
                    <span>@Copyright归FreeTeam所有</span><br> <span><a
                        href="#">联系我们</a></span>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
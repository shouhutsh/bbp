<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<%
    String path = request.getContextPath();
%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>健康模块</title>
    <script type="text/javascript">
        var localObj = window.location;
        var contextPath = localObj.pathname.split("/")[1];
        var basePath = localObj.protocol + "//" + localObj.host + "/";
    </script>
    <script type="text/javascript" src="<%=path%>/resources/js/jquery-2.0.3.js"></script>
    <script type="text/javascript"
            src="<%=path%>/resources/style/bootstrap-3.1.1/dist/js/bootstrap.min.js"></script>
    <link
            href="<%=path%>/resources/style/bootstrap-3.1.1/dist/css/bootstrap.min.css"
            rel="stylesheet" type="text/css"/>
<script type="application/javascript">

</script>
    <style>
        .ulDiv
        {
            width: 10%;
            height: auto;
            float: left;
        }
        ul{
            height: 500px;
        }
        ul li{
            list-style: none;
            border: 1px solid #ccc;
            padding: 10px;
        }
        .dataDiv
        {
            width: 90%;
            height: 500px;
            border: 1px solid #ccc;
            padding: 0px;
            float: right;
        }

    </style>
</head>
<body>
<div>
    <div>
        <div>
            <div class="ulDiv">
                <ul id="select" class="list-group" >
                    <li id="height" class="list-group-item"><a href="<%=path%>/height/heightChart.html" target="dataShow">身高</a> </li>
                    <li id="weight" class="list-group-item"><a href="<%=path%>/weight/weightChart.html" target="dataShow">体重</a> </li>
                    <li id="recipe" class="list-group-item"><a href="<%=path%>/eat/recipeChart.html" target="dataShow">食谱</a> </li>
                </ul>

            </div>
            <div class="dataDiv">
                <iframe name="dataShow" style="width: 100%;border: none;height:500px; "></iframe>
            </div>
        </div>
    </div>
</div>
</body>
</html>
package edu.zut.cs.bbp.main;

import java.util.List;
import java.util.ArrayList;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.SqlUtils;
import de.greenrobot.dao.internal.DaoConfig;

import edu.zut.cs.bbp.main.Event;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table EVENT.
*/
public class EventDao extends AbstractDao<Event, String> {

    public static final String TABLENAME = "EVENT";

    /**
     * Properties of entity Event.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property Id = new Property(0, String.class, "id", true, "ID");
        public final static Property DateCreated = new Property(1, Long.class, "dateCreated", false, "DATE_CREATED");
        public final static Property DateModified = new Property(2, Long.class, "dateModified", false, "DATE_MODIFIED");
        public final static Property Deleted = new Property(3, Boolean.class, "deleted", false, "DELETED");
        public final static Property State = new Property(4, Integer.class, "state", false, "STATE");
        public final static Property UserId = new Property(5, Long.class, "userId", false, "USER_ID");
        public final static Property Name = new Property(6, String.class, "name", false, "NAME");
        public final static Property Unit = new Property(7, String.class, "unit", false, "UNIT");
        public final static Property ParentId = new Property(8, String.class, "parentId", false, "PARENT_ID");
    };

    private DaoSession daoSession;


    public EventDao(DaoConfig config) {
        super(config);
    }
    
    public EventDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
        this.daoSession = daoSession;
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'EVENT' (" + //
                "'ID' TEXT PRIMARY KEY NOT NULL ," + // 0: id
                "'DATE_CREATED' INTEGER," + // 1: dateCreated
                "'DATE_MODIFIED' INTEGER," + // 2: dateModified
                "'DELETED' INTEGER," + // 3: deleted
                "'STATE' INTEGER," + // 4: state
                "'USER_ID' INTEGER," + // 5: userId
                "'NAME' TEXT NOT NULL ," + // 6: name
                "'UNIT' TEXT," + // 7: unit
                "'PARENT_ID' TEXT);"); // 8: parentId
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'EVENT'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, Event entity) {
        stmt.clearBindings();
        stmt.bindString(1, entity.getId());
 
        Long dateCreated = entity.getDateCreated();
        if (dateCreated != null) {
            stmt.bindLong(2, dateCreated);
        }
 
        Long dateModified = entity.getDateModified();
        if (dateModified != null) {
            stmt.bindLong(3, dateModified);
        }
 
        Boolean deleted = entity.getDeleted();
        if (deleted != null) {
            stmt.bindLong(4, deleted ? 1l: 0l);
        }
 
        Integer state = entity.getState();
        if (state != null) {
            stmt.bindLong(5, state);
        }
 
        Long userId = entity.getUserId();
        if (userId != null) {
            stmt.bindLong(6, userId);
        }
        stmt.bindString(7, entity.getName());
 
        String unit = entity.getUnit();
        if (unit != null) {
            stmt.bindString(8, unit);
        }
 
        String parentId = entity.getParentId();
        if (parentId != null) {
            stmt.bindString(9, parentId);
        }
    }

    @Override
    protected void attachEntity(Event entity) {
        super.attachEntity(entity);
        entity.__setDaoSession(daoSession);
    }

    /** @inheritdoc */
    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.getString(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public Event readEntity(Cursor cursor, int offset) {
        Event entity = new Event( //
            cursor.getString(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1), // dateCreated
            cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2), // dateModified
            cursor.isNull(offset + 3) ? null : cursor.getShort(offset + 3) != 0, // deleted
            cursor.isNull(offset + 4) ? null : cursor.getInt(offset + 4), // state
            cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5), // userId
            cursor.getString(offset + 6), // name
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // unit
            cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8) // parentId
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, Event entity, int offset) {
        entity.setId(cursor.getString(offset + 0));
        entity.setDateCreated(cursor.isNull(offset + 1) ? null : cursor.getLong(offset + 1));
        entity.setDateModified(cursor.isNull(offset + 2) ? null : cursor.getLong(offset + 2));
        entity.setDeleted(cursor.isNull(offset + 3) ? null : cursor.getShort(offset + 3) != 0);
        entity.setState(cursor.isNull(offset + 4) ? null : cursor.getInt(offset + 4));
        entity.setUserId(cursor.isNull(offset + 5) ? null : cursor.getLong(offset + 5));
        entity.setName(cursor.getString(offset + 6));
        entity.setUnit(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setParentId(cursor.isNull(offset + 8) ? null : cursor.getString(offset + 8));
     }
    
    /** @inheritdoc */
    @Override
    protected String updateKeyAfterInsert(Event entity, long rowId) {
        return entity.getId();
    }
    
    /** @inheritdoc */
    @Override
    public String getKey(Event entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
    private String selectDeep;

    protected String getSelectDeep() {
        if (selectDeep == null) {
            StringBuilder builder = new StringBuilder("SELECT ");
            SqlUtils.appendColumns(builder, "T", getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T0", daoSession.getUserDao().getAllColumns());
            builder.append(',');
            SqlUtils.appendColumns(builder, "T1", daoSession.getEventDao().getAllColumns());
            builder.append(" FROM EVENT T");
            builder.append(" LEFT JOIN USER T0 ON T.'USER_ID'=T0.'ID'");
            builder.append(" LEFT JOIN EVENT T1 ON T.'PARENT_ID'=T1.'ID'");
            builder.append(' ');
            selectDeep = builder.toString();
        }
        return selectDeep;
    }
    
    protected Event loadCurrentDeep(Cursor cursor, boolean lock) {
        Event entity = loadCurrent(cursor, 0, lock);
        int offset = getAllColumns().length;

        User user = loadCurrentOther(daoSession.getUserDao(), cursor, offset);
        entity.setUser(user);
        offset += daoSession.getUserDao().getAllColumns().length;

        Event parent = loadCurrentOther(daoSession.getEventDao(), cursor, offset);
        entity.setParent(parent);

        return entity;    
    }

    public Event loadDeep(Long key) {
        assertSinglePk();
        if (key == null) {
            return null;
        }

        StringBuilder builder = new StringBuilder(getSelectDeep());
        builder.append("WHERE ");
        SqlUtils.appendColumnsEqValue(builder, "T", getPkColumns());
        String sql = builder.toString();
        
        String[] keyArray = new String[] { key.toString() };
        Cursor cursor = db.rawQuery(sql, keyArray);
        
        try {
            boolean available = cursor.moveToFirst();
            if (!available) {
                return null;
            } else if (!cursor.isLast()) {
                throw new IllegalStateException("Expected unique result, but count was " + cursor.getCount());
            }
            return loadCurrentDeep(cursor, true);
        } finally {
            cursor.close();
        }
    }
    
    /** Reads all available rows from the given cursor and returns a list of new ImageTO objects. */
    public List<Event> loadAllDeepFromCursor(Cursor cursor) {
        int count = cursor.getCount();
        List<Event> list = new ArrayList<Event>(count);
        
        if (cursor.moveToFirst()) {
            if (identityScope != null) {
                identityScope.lock();
                identityScope.reserveRoom(count);
            }
            try {
                do {
                    list.add(loadCurrentDeep(cursor, false));
                } while (cursor.moveToNext());
            } finally {
                if (identityScope != null) {
                    identityScope.unlock();
                }
            }
        }
        return list;
    }
    
    protected List<Event> loadDeepAllAndCloseCursor(Cursor cursor) {
        try {
            return loadAllDeepFromCursor(cursor);
        } finally {
            cursor.close();
        }
    }
    

    /** A raw-style query where you can pass any WHERE clause and arguments. */
    public List<Event> queryDeep(String where, String... selectionArg) {
        Cursor cursor = db.rawQuery(getSelectDeep() + where, selectionArg);
        return loadDeepAllAndCloseCursor(cursor);
    }
 
}

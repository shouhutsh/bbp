package edu.zut.cs.bbp.main;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.Diary;
import edu.zut.cs.bbp.main.First;
import edu.zut.cs.bbp.main.First2Diary;
import edu.zut.cs.bbp.main.Intellect;
import edu.zut.cs.bbp.main.Media;
import edu.zut.cs.bbp.main.Milestone;
import edu.zut.cs.bbp.main.Mile2Diary;
import edu.zut.cs.bbp.main.BB;
import edu.zut.cs.bbp.main.Drink;
import edu.zut.cs.bbp.main.Eat;
import edu.zut.cs.bbp.main.Height;
import edu.zut.cs.bbp.main.NN;
import edu.zut.cs.bbp.main.Sleep;
import edu.zut.cs.bbp.main.Weight;

import edu.zut.cs.bbp.main.UserDao;
import edu.zut.cs.bbp.main.EventDao;
import edu.zut.cs.bbp.main.DiaryDao;
import edu.zut.cs.bbp.main.FirstDao;
import edu.zut.cs.bbp.main.First2DiaryDao;
import edu.zut.cs.bbp.main.IntellectDao;
import edu.zut.cs.bbp.main.MediaDao;
import edu.zut.cs.bbp.main.MilestoneDao;
import edu.zut.cs.bbp.main.Mile2DiaryDao;
import edu.zut.cs.bbp.main.BBDao;
import edu.zut.cs.bbp.main.DrinkDao;
import edu.zut.cs.bbp.main.EatDao;
import edu.zut.cs.bbp.main.HeightDao;
import edu.zut.cs.bbp.main.NNDao;
import edu.zut.cs.bbp.main.SleepDao;
import edu.zut.cs.bbp.main.WeightDao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig userDaoConfig;
    private final DaoConfig eventDaoConfig;
    private final DaoConfig diaryDaoConfig;
    private final DaoConfig firstDaoConfig;
    private final DaoConfig first2DiaryDaoConfig;
    private final DaoConfig intellectDaoConfig;
    private final DaoConfig mediaDaoConfig;
    private final DaoConfig milestoneDaoConfig;
    private final DaoConfig mile2DiaryDaoConfig;
    private final DaoConfig bBDaoConfig;
    private final DaoConfig drinkDaoConfig;
    private final DaoConfig eatDaoConfig;
    private final DaoConfig heightDaoConfig;
    private final DaoConfig nNDaoConfig;
    private final DaoConfig sleepDaoConfig;
    private final DaoConfig weightDaoConfig;

    private final UserDao userDao;
    private final EventDao eventDao;
    private final DiaryDao diaryDao;
    private final FirstDao firstDao;
    private final First2DiaryDao first2DiaryDao;
    private final IntellectDao intellectDao;
    private final MediaDao mediaDao;
    private final MilestoneDao milestoneDao;
    private final Mile2DiaryDao mile2DiaryDao;
    private final BBDao bBDao;
    private final DrinkDao drinkDao;
    private final EatDao eatDao;
    private final HeightDao heightDao;
    private final NNDao nNDao;
    private final SleepDao sleepDao;
    private final WeightDao weightDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        userDaoConfig = daoConfigMap.get(UserDao.class).clone();
        userDaoConfig.initIdentityScope(type);

        eventDaoConfig = daoConfigMap.get(EventDao.class).clone();
        eventDaoConfig.initIdentityScope(type);

        diaryDaoConfig = daoConfigMap.get(DiaryDao.class).clone();
        diaryDaoConfig.initIdentityScope(type);

        firstDaoConfig = daoConfigMap.get(FirstDao.class).clone();
        firstDaoConfig.initIdentityScope(type);

        first2DiaryDaoConfig = daoConfigMap.get(First2DiaryDao.class).clone();
        first2DiaryDaoConfig.initIdentityScope(type);

        intellectDaoConfig = daoConfigMap.get(IntellectDao.class).clone();
        intellectDaoConfig.initIdentityScope(type);

        mediaDaoConfig = daoConfigMap.get(MediaDao.class).clone();
        mediaDaoConfig.initIdentityScope(type);

        milestoneDaoConfig = daoConfigMap.get(MilestoneDao.class).clone();
        milestoneDaoConfig.initIdentityScope(type);

        mile2DiaryDaoConfig = daoConfigMap.get(Mile2DiaryDao.class).clone();
        mile2DiaryDaoConfig.initIdentityScope(type);

        bBDaoConfig = daoConfigMap.get(BBDao.class).clone();
        bBDaoConfig.initIdentityScope(type);

        drinkDaoConfig = daoConfigMap.get(DrinkDao.class).clone();
        drinkDaoConfig.initIdentityScope(type);

        eatDaoConfig = daoConfigMap.get(EatDao.class).clone();
        eatDaoConfig.initIdentityScope(type);

        heightDaoConfig = daoConfigMap.get(HeightDao.class).clone();
        heightDaoConfig.initIdentityScope(type);

        nNDaoConfig = daoConfigMap.get(NNDao.class).clone();
        nNDaoConfig.initIdentityScope(type);

        sleepDaoConfig = daoConfigMap.get(SleepDao.class).clone();
        sleepDaoConfig.initIdentityScope(type);

        weightDaoConfig = daoConfigMap.get(WeightDao.class).clone();
        weightDaoConfig.initIdentityScope(type);

        userDao = new UserDao(userDaoConfig, this);
        eventDao = new EventDao(eventDaoConfig, this);
        diaryDao = new DiaryDao(diaryDaoConfig, this);
        firstDao = new FirstDao(firstDaoConfig, this);
        first2DiaryDao = new First2DiaryDao(first2DiaryDaoConfig, this);
        intellectDao = new IntellectDao(intellectDaoConfig, this);
        mediaDao = new MediaDao(mediaDaoConfig, this);
        milestoneDao = new MilestoneDao(milestoneDaoConfig, this);
        mile2DiaryDao = new Mile2DiaryDao(mile2DiaryDaoConfig, this);
        bBDao = new BBDao(bBDaoConfig, this);
        drinkDao = new DrinkDao(drinkDaoConfig, this);
        eatDao = new EatDao(eatDaoConfig, this);
        heightDao = new HeightDao(heightDaoConfig, this);
        nNDao = new NNDao(nNDaoConfig, this);
        sleepDao = new SleepDao(sleepDaoConfig, this);
        weightDao = new WeightDao(weightDaoConfig, this);

        registerDao(User.class, userDao);
        registerDao(Event.class, eventDao);
        registerDao(Diary.class, diaryDao);
        registerDao(First.class, firstDao);
        registerDao(First2Diary.class, first2DiaryDao);
        registerDao(Intellect.class, intellectDao);
        registerDao(Media.class, mediaDao);
        registerDao(Milestone.class, milestoneDao);
        registerDao(Mile2Diary.class, mile2DiaryDao);
        registerDao(BB.class, bBDao);
        registerDao(Drink.class, drinkDao);
        registerDao(Eat.class, eatDao);
        registerDao(Height.class, heightDao);
        registerDao(NN.class, nNDao);
        registerDao(Sleep.class, sleepDao);
        registerDao(Weight.class, weightDao);
    }
    
    public void clear() {
        userDaoConfig.getIdentityScope().clear();
        eventDaoConfig.getIdentityScope().clear();
        diaryDaoConfig.getIdentityScope().clear();
        firstDaoConfig.getIdentityScope().clear();
        first2DiaryDaoConfig.getIdentityScope().clear();
        intellectDaoConfig.getIdentityScope().clear();
        mediaDaoConfig.getIdentityScope().clear();
        milestoneDaoConfig.getIdentityScope().clear();
        mile2DiaryDaoConfig.getIdentityScope().clear();
        bBDaoConfig.getIdentityScope().clear();
        drinkDaoConfig.getIdentityScope().clear();
        eatDaoConfig.getIdentityScope().clear();
        heightDaoConfig.getIdentityScope().clear();
        nNDaoConfig.getIdentityScope().clear();
        sleepDaoConfig.getIdentityScope().clear();
        weightDaoConfig.getIdentityScope().clear();
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public EventDao getEventDao() {
        return eventDao;
    }

    public DiaryDao getDiaryDao() {
        return diaryDao;
    }

    public FirstDao getFirstDao() {
        return firstDao;
    }

    public First2DiaryDao getFirst2DiaryDao() {
        return first2DiaryDao;
    }

    public IntellectDao getIntellectDao() {
        return intellectDao;
    }

    public MediaDao getMediaDao() {
        return mediaDao;
    }

    public MilestoneDao getMilestoneDao() {
        return milestoneDao;
    }

    public Mile2DiaryDao getMile2DiaryDao() {
        return mile2DiaryDao;
    }

    public BBDao getBBDao() {
        return bBDao;
    }

    public DrinkDao getDrinkDao() {
        return drinkDao;
    }

    public EatDao getEatDao() {
        return eatDao;
    }

    public HeightDao getHeightDao() {
        return heightDao;
    }

    public NNDao getNNDao() {
        return nNDao;
    }

    public SleepDao getSleepDao() {
        return sleepDao;
    }

    public WeightDao getWeightDao() {
        return weightDao;
    }

}

package edu.zut.cs.bbp.main.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Administrator on 2014/6/30.
 */
public abstract class NetTemplate {

    public void execute(){
        try{
            URL url = new URL(getUrl());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod(getMethod());
            setHeader(connection);
            connection.connect();

            upload(connection);

            if(HttpURLConnection.HTTP_OK == connection.getResponseCode()){
                download(connection);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    abstract protected String getUrl();
    abstract protected String getMethod();

    protected void setHeader(HttpURLConnection connection){
        connection.setDoInput(true);
        connection.setDoOutput(true);
    }

    protected void upload(HttpURLConnection connection){

    }

    protected void download(HttpURLConnection connection){

    }

}

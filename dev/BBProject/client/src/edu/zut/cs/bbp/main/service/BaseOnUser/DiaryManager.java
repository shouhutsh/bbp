package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;
import edu.zut.cs.bbp.main.utils.LoginUser;

import java.util.List;

public class DiaryManager {

    private static final String TAG = DiaryManager.class.getSimpleName();

    private static DiaryManager diaryManager;
    private static Context context;
    private DaoSession daoSession;
    private DiaryDao diaryDao;

    private static User user;

    private DiaryManager() {
    }

    public static DiaryManager getInstance(Context context, final User user) {
        if (diaryManager == null) {
            diaryManager = new DiaryManager();
            if (DiaryManager.context == null) {
                DiaryManager.context = context.getApplicationContext();
            }
            diaryManager.daoSession = BaseApplication.getDaoSession(context);
            diaryManager.diaryDao = diaryManager.daoSession.getDiaryDao();
        }

        DiaryManager.user = user;
        return diaryManager;
    }

    public Diary findById(String id) {
        return diaryDao.load(id);
    }

    public List<Diary> findAll() {
        return diaryDao.queryBuilder()
                .where(DiaryDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Diary> findAll(Integer state) {
        return diaryDao.queryBuilder()
                .where(DiaryDao.Properties.UserId.eq(user.getId()), DiaryDao.Properties.State.eq(state))
                .list();
    }

    public List<Diary> query(String where, String... params) {
        return diaryDao.queryRaw(" WHERE " + where, params);
    }

    public long save(Diary diary) {
        return diaryDao.insertOrReplace(diary);
    }

    public void save(final List<Diary> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        diaryDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Diary diary = list.get(i);
                    diaryDao.insertOrReplace(diary);
                }
            }
        });
    }

    public void delete(String id) {
        diaryDao.deleteByKey(id);
    }

    public void delete(Diary diary) {
        diaryDao.delete(diary);
    }

    public void deleteAll() {
        diaryDao.deleteAll();
    }
}
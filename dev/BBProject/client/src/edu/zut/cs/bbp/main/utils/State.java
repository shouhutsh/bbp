package edu.zut.cs.bbp.main.utils;

/**
 * Created by Administrator on 2014/6/30.
 */
public class State {
    public static final int CLEAN = 0;
    public static final int NEW   = 1;
    public static final int OLD   = 2;
    public static final int DELETE= 3;
}

package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class BBManager {

    private static final String TAG = BBManager.class.getSimpleName();
    
    private static BBManager bbManager;
    private static Context context;
    private DaoSession daoSession;
    private BBDao bbDao;

    private static User user;

    private BBManager() {
    }

    public static BBManager getInstance(Context context, final User user) {
        if (bbManager == null) {
            bbManager = new BBManager();
            if (BBManager.context == null) {
                BBManager.context = context.getApplicationContext();
            }
            bbManager.daoSession = BaseApplication.getDaoSession(context);
            bbManager.bbDao = bbManager.daoSession.getBBDao();
        }

        BBManager.user = user;
        return bbManager;
    }

    public BB findById(String id) {
        return bbDao.load(id);
    }

    // 得到所有状态的实体
    public List<BB> findAll(){
        return bbDao.queryBuilder()
                .where(BBDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<BB> findAll(Integer state){
        return bbDao.queryBuilder()
                .where(BBDao.Properties.UserId.eq(user.getId()), BBDao.Properties.State.eq(state))
                .list();
    }

    public List<BB> findAll(Integer state, final Event event){
        return bbDao.queryBuilder()
                .where(BBDao.Properties.UserId.eq(user.getId()),
                        BBDao.Properties.State.eq(state),
                        BBDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public List<BB> query(String where, String... params) {
        return bbDao.queryRaw(" WHERE "+where, params);
    }

    public long save(BB bb) {
        return bbDao.insertOrReplace(bb);
    }

    public void save(final List<BB> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        bbDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    BB bb = list.get(i);
                    bbDao.insertOrReplace(bb);
                }
            }
        });
    }

    public void delete(String id) {
        bbDao.deleteByKey(id);
    }

    public void delete(BB bb) {
        bbDao.delete(bb);
    }

    public void deleteAll() {
        bbDao.deleteAll();
    }
}
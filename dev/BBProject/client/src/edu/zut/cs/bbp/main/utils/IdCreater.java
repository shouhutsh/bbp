package edu.zut.cs.bbp.main.utils;

import edu.zut.cs.bbp.main.User;

/**
 * Created by Administrator on 2014/6/27.
 */
public class IdCreater {
    public static String get(User user){
        StringBuilder id = new StringBuilder(32);

        char[] userId = new char[13];
        for(int i = 0; i < userId.length; ++i) userId[i] = '0';
        if(null != user.getId()){
            String uid = String.valueOf(user.getId());
            uid.getChars(0, uid.length(), userId, 13-uid.length());
        }

        id.append(String.valueOf(System.currentTimeMillis()).substring(0, 13));
        id.append(userId);
        id.append("01");        // 终端类型：Android
        id.append("0000");

        return id.toString();
    }
}

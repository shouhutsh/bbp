package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class FirstManager {

    private static final String TAG = FirstManager.class.getSimpleName();
    
    private static FirstManager firstManager;
    private static Context context;
    private DaoSession daoSession;
    private FirstDao firstDao;

    private static User user;

    private FirstManager() {
    }

    public static FirstManager getInstance(Context context, final User user) {
        if (firstManager == null) {
            firstManager = new FirstManager();
            if (FirstManager.context == null) {
                FirstManager.context = context.getApplicationContext();
            }
            firstManager.daoSession = BaseApplication.getDaoSession(context);
            firstManager.firstDao = firstManager.daoSession.getFirstDao();
        }

        FirstManager.user = user;
        return firstManager;
    }

    public First findById(String id) {
        return firstDao.load(id);
    }

    public List<First> findAll(){
        return firstDao.queryBuilder()
                .where(FirstDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<First> findAll(Integer state){
        return firstDao.queryBuilder()
                .where(FirstDao.Properties.UserId.eq(user.getId()), FirstDao.Properties.State.eq(state))
                .list();
    }

    public List<First> query(String where, String... params) {
        return firstDao.queryRaw(" WHERE "+where, params);
    }

    public long save(First first) {
        return firstDao.insertOrReplace(first);
    }

    public void save(final List<First> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        firstDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    First first = list.get(i);
                    firstDao.insertOrReplace(first);
                }
            }
        });
    }

    public void delete(String id) {
        firstDao.deleteByKey(id);
    }

    public void delete(First first) {
        firstDao.delete(first);
    }

    public void deleteAll() {
        firstDao.deleteAll();
    }
}
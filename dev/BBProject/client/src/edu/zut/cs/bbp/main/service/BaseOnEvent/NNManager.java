package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class NNManager {

    private static final String TAG = NNManager.class.getSimpleName();
    
    private static NNManager nnManager;
    private static Context context;
    private DaoSession daoSession;
    private NNDao nnDao;

    private static User user;

    private NNManager() {
    }

    public static NNManager getInstance(Context context, final User user) {
        if (nnManager == null) {
            nnManager = new NNManager();
            if (NNManager.context == null) {
                NNManager.context = context.getApplicationContext();
            }
            nnManager.daoSession = BaseApplication.getDaoSession(context);
            nnManager.nnDao = nnManager.daoSession.getNNDao();
        }

        NNManager.user = user;
        return nnManager;
    }

    public NN findById(String id) {
        return nnDao.load(id);
    }

    // 得到所有状态的实体
    public List<NN> findAll(){
        return nnDao.queryBuilder()
                .where(NNDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<NN> findAll(Integer state){
        return nnDao.queryBuilder()
                .where(NNDao.Properties.UserId.eq(user.getId()), NNDao.Properties.State.eq(state))
                .list();
    }

    public List<NN> findAll(Integer state, final Event event){
        return nnDao.queryBuilder()
                .where(NNDao.Properties.UserId.eq(user.getId()),
                        NNDao.Properties.State.eq(state),
                        NNDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public List<NN> query(String where, String... params) {
        return nnDao.queryRaw(" WHERE "+where, params);
    }

    public long save(NN nn) {
        return nnDao.insertOrReplace(nn);
    }

    public void save(final List<NN> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        nnDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    NN nn = list.get(i);
                    nnDao.insertOrReplace(nn);
                }
            }
        });
    }

    public void delete(String id) {
        nnDao.deleteByKey(id);
    }

    public void delete(NN nn) {
        nnDao.delete(nn);
    }

    public void deleteAll() {
        nnDao.deleteAll();
    }
}
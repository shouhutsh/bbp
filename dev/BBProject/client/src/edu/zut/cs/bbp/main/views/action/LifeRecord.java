package edu.zut.cs.bbp.main.views.action;

/**
 * Created by kxp on 2014/6/25.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.utils.LoginUser;

import java.util.LinkedList;
import java.util.List;

public class LifeRecord extends Activity {

    private static User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liferecord);

        LoginUser loginUser = (LoginUser) getApplication();
        user = loginUser.getLogin();
    }

    @Override
    public void onResume() {
        super.onResume();

        final List<Event> firsts = EventManager.getInstance(this, user).getRoot();
        final List<Event> seconds = new LinkedList<Event>();
        final List<Event> thirds = new LinkedList<Event>();
        final Event[] event = {null, null, null};
/**
 * 注意：
 * 		这里是下拉列表
 */
        Spinner firstChose = (Spinner) findViewById(R.id.firstChose);
        final Spinner secondChose = (Spinner) findViewById(R.id.secondChose);
        final Spinner thirdChose = (Spinner) findViewById(R.id.thirdChose);

        /**
         * 这里是下拉列表的监听事件
         */
        final ArrayAdapter<Event> firstAdapter = new ArrayAdapter<Event>(getApplicationContext(),
                android.R.layout.simple_spinner_item, firsts);
        firstChose.setAdapter(firstAdapter);

        final ArrayAdapter<Event> secondAdapter = new ArrayAdapter<Event>(getApplicationContext(),
                android.R.layout.simple_spinner_item, seconds);
        secondChose.setAdapter(secondAdapter);

        final ArrayAdapter<Event> thirdAdapter = new ArrayAdapter<Event>(getApplicationContext(),
                android.R.layout.simple_spinner_item, thirds);
        thirdChose.setAdapter(thirdAdapter);

        /**
         * 这里是下拉框条目点击事件
         */
        firstChose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                event[0] = firstAdapter.getItem(i);

                List<Event> events = EventManager.getInstance(getApplicationContext(), user)
                        .getChildrens(firstAdapter.getItem(i));

                seconds.clear();
                thirds.clear();

                for (Event e : events) {
                    seconds.add(e);
                }
                secondChose.setAdapter(secondAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        secondChose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                event[1] = secondAdapter.getItem(i);
                List<Event> events = EventManager.getInstance(getApplicationContext(), user)
                        .getChildrens(secondAdapter.getItem(i));

                thirds.clear();

                for (Event e : events) {
                    thirds.add(e);
                }
                thirdChose.setAdapter(thirdAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        thirdChose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                event[2] = secondAdapter.getItem(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
/**
 * 注意：
 * 		这里是按钮
 */

        Button addSecond = (Button) findViewById(R.id.addSecond);
        Button addThird = (Button) findViewById(R.id.addThird);
        Button submit = (Button) findViewById(R.id.submit);
        /**
         * 这里是增添按钮的监听事件
         */
        addSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                if (event[0] == null && event[0].getParentId() == null) {
                    Toast.makeText(getApplicationContext(), "请先选择上一级类目！", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("f_id", event[0].getId());
                intent.setClass(getApplicationContext(), Second.class);
                startActivity(intent);
            }
        });
        addThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //To change body of implemented methods use File | Settings | File Templates.
                if (event[1] == null && event[1].getParentId() == null) {
                    Toast.makeText(getApplicationContext(), "请先选择上一级类目！", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra("s_id", event[1].getId());

                intent.setClass(getApplicationContext(), Third.class);
                startActivity(intent);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();

                if (event[0] != null)
                    intent.putExtra("f_id", event[0].getId());
                if (event[1] != null)
                    intent.putExtra("s_id", event[1].getId());
                if (event[2] != null)
                    intent.putExtra("t_id", event[2].getId());
                
                intent.setClass(getApplicationContext(), Submit.class);
                startActivity(intent);
            }
        });
    }
}
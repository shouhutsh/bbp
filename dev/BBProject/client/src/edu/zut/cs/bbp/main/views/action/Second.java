package edu.zut.cs.bbp.main.views.action;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.utils.IdCreater;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.utils.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 13-12-29
 * Time: 下午3:58
 * To change this template use File | Settings | File Templates.
 */
public class Second extends Activity {
	/**
	 * Called when the activity is first created.
	 */
	private List<String> list = new ArrayList<String>();
	private EditText editText;
	private Button bt1;
	private String str1;
	private static User user;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second);

		LoginUser loginUser = (LoginUser) getApplication();
		user = loginUser.getLogin();
		editText = (EditText) findViewById(R.id.editText);
		bt1 = (Button) findViewById(R.id.button1);
		bt1.setOnClickListener(new View.OnClickListener() {

			Intent intent = getIntent();

			@Override
			public void onClick(View v) {

				str1 = editText.getText().toString();
				if (!str1.equals("")) {
					Event event=new Event();
					event.setId(IdCreater.get(user));
					event.setUser(user);
					event.setName(str1);
					event.setState(State.NEW);
					event.setParentId(intent.getStringExtra("f_id"));
					EventManager.getInstance(getApplicationContext(), user).save(event);

					Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
					finish();
				} else {
					Toast.makeText(getApplicationContext(), "事件不能为空！", Toast.LENGTH_SHORT).show();
				}


			}

		});
	}
}

package edu.zut.cs.bbp.main.views.action;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import edu.zut.cs.bbp.main.R;

@SuppressWarnings("deprecation")
public class HealthRecord extends TabActivity {

    @Override
    protected void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView(R.layout.health_record);

    }
	public void health_life(View v) {
		Intent intent = new Intent();
		intent.setClass(HealthRecord.this,LifeRecord.class);
		startActivity(intent);
		//this.finish();
	}
	public void health_grow(View v) {
		Intent intent = new Intent();
		intent.setClass(HealthRecord.this, GrowRecord.class);
		startActivity(intent);
	}
}

package edu.zut.cs.bbp.main.views.loginRegister;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.views.action.MainActivity;

/**
 * Created with IntelliJ IDEA.
 * User: ASUS
 * Date: 14-6-24
 * Time: 下午12:04
 * To change this template use File | Settings | File Templates.
 */
public class UserMain extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user_main);

        final Intent intent = new Intent(this, MainActivity.class);
        //系统会为需要启动的activity寻找与当前activity不同的task;
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //创建一个新的线程来显示欢迎动画，指定时间后结束，跳转至指定界面
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                   /* //获取应用的上下文，生命周期是整个应用，应用结束才会结束
                    getApplicationContext().startActivity(intent);*//**/
                    Log.i("UserLogin", "BBBBB");
                    finish();
                }
            }
        }).start();
    }
}

package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class IntellectManager {

    private static final String TAG = IntellectManager.class.getSimpleName();
    
    private static IntellectManager intellectManager;
    private static Context context;
    private DaoSession daoSession;
    private IntellectDao intellectDao;

    private static User user;

    private IntellectManager() {
    }

    public static IntellectManager getInstance(Context context, final User user) {
        if (intellectManager == null) {
            intellectManager = new IntellectManager();
            if (IntellectManager.context == null) {
                IntellectManager.context = context.getApplicationContext();
            }
            intellectManager.daoSession = BaseApplication.getDaoSession(context);
            intellectManager.intellectDao = intellectManager.daoSession.getIntellectDao();
        }

        IntellectManager.user = user;
        return intellectManager;
    }

    public Intellect findById(String id) {
        return intellectDao.load(id);
    }

    public List<Intellect> findAll(){
        return intellectDao.queryBuilder()
                .where(IntellectDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Intellect> findAll(Integer state){
        return intellectDao.queryBuilder()
                .where(IntellectDao.Properties.UserId.eq(user.getId()), IntellectDao.Properties.State.eq(state))
                .list();
    }

    public List<Intellect> query(String where, String... params) {
        return intellectDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Intellect intellect) {
        return intellectDao.insertOrReplace(intellect);
    }

    public void save(final List<Intellect> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        intellectDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Intellect intellect = list.get(i);
                    intellectDao.insertOrReplace(intellect);
                }
            }
        });
    }

    public void delete(String id) {
        intellectDao.deleteByKey(id);
    }

    public void delete(Intellect intellect) {
        intellectDao.delete(intellect);
    }

    public void deleteAll() {
        intellectDao.deleteAll();
    }
}
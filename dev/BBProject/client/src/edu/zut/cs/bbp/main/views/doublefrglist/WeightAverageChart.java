package edu.zut.cs.bbp.main.views.doublefrglist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Average temperature demo chart.
 */
public  class WeightAverageChart extends AbstractChart {


	/**
	 * Returns the chart name.
	 *
	 * @return the chart name
	 */
	public String getName() {
		return "Average weight";
	}

	/**
	 * Returns the chart description.
	 *
	 * @return the chart description
	 */
	public String getDesc() {
		return "The average weight in 4 Greek islands (line chart)";
	}

	/**
	 * Executes the chart demo.
	 *
	 * @param context the context
	 * @return the built intent
	 */
	public Intent execute(Context context) {
		String[] titles = new String[]{"发育较快", "平均体重", "发育较慢","宝贝曲线"};
		List<double[]> x = new ArrayList<double[]>();
		for (int i = 0; i < titles.length-1; i++) {
			x.add(new double[]{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24});
		}
		x.add(new double[]{1, 2, 5, 6, 7, 10, 12, 14, 16, 18});
		List<double[]> values = new ArrayList<double[]>();
		values.add(new double[]{3.8, 6.0, 7.6, 8.8, 9.8, 10.6, 11.3, 11.7, 12.3, 12.7, 13.0, 13.7,14.2});
		values.add(new double[]{3.35, 5.1, 6.3, 7.8, 8.8, 9.6, 10.2, 10.5, 11.1, 11.5, 11.8, 12.3,13.5});
		values.add(new double[]{2.9, 4.3, 5.7, 6.9, 7.8, 8.6, 9.1, 9.4, 10.0, 10.3, 10.6, 11.0,12.8});
		values.add(new double[]{3.1, 4.8, 6.5, 8.0, 9.6, 11,11.5, 12, 12.5,13,13.5});

		int[] colors = new int[]{Color.DKGRAY, Color.CYAN, Color.YELLOW, Color.BLUE};
		PointStyle[] styles = new PointStyle[]{PointStyle.DIAMOND, PointStyle.CIRCLE,
				PointStyle.DIAMOND,PointStyle.TRIANGLE};
		XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles);
		int length = renderer.getSeriesRendererCount();
		for (int i = 0; i < length; i++) {
			((XYSeriesRenderer) renderer.getSeriesRendererAt(i)).setFillPoints(true);
		}

		setChartSettings(renderer, "Average ", "Month", "Weight", 0, 25, 0, 30,
				Color.LTGRAY, Color.LTGRAY);
		renderer.setXLabels(15);
		renderer.setYLabels(40);//设置网格间距
		renderer.setShowGrid(true);
		renderer.setXLabelsAlign(Align.RIGHT);
		renderer.setYLabelsAlign(Align.RIGHT);
		renderer.setZoomButtonsVisible(true);
		renderer.setPanLimits(new double[]{0, 60, 0, 50});//设置移动范围
		renderer.setZoomLimits(new double[]{0,20,0,80});//设置放大缩小时X轴Y轴允许的最大最小值.
		renderer.setApplyBackgroundColor(true);//设置背景颜色
		renderer.setBackgroundColor(Color.GRAY);
		renderer.setPointSize(3);
//		renderer.setPanEnabled(true, true);

		XYMultipleSeriesDataset dataset = buildDataset(titles, x, values);
		XYSeries series = dataset.getSeriesAt(0);
		series.addAnnotation("Vacation", 16, 24);//设置Vacation位置
		Intent intent = ChartFactory.getLineChartIntent(context, dataset, renderer,
				"体重曲线");
		return intent;
	}

}


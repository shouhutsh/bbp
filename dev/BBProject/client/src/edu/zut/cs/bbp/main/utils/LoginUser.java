package edu.zut.cs.bbp.main.utils;

import android.app.Application;
import edu.zut.cs.bbp.main.User;

/**
 * Created by Qi_2 on 2014/6/29 0029.
 */
public class LoginUser extends Application {
    private User login;

    public User getLogin() {
        return login;
    }

    public void setLogin(User login) {
        this.login = login;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        login = null;
    }
}

package edu.zut.cs.bbp.main.views.loginRegister;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.controller.EventController;
import edu.zut.cs.bbp.main.controller.UserController;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.views.action.MainActivity;

/**
 * Created with IntelliJ IDEA.
 * User: ASUS
 * Date: 14-6-24
 * Time: 下午12:04
 * To change this template use File | Settings | File Templates.
 */
public class UserLogin extends Activity implements View.OnClickListener {

    public static int REMEMBER_PSW = 1;
    public static int NAME_LIST = 0;

    private EditText userID;      //输入用户ID
    private EditText userPsw;       //输入密码
    private CheckBox rememberPsw;   //记住密码
    private Button loginToRegister; //注册新用户
    private Button login_btn;       //登录

    private Long id;
    private String passWord;

    public static final String PREFS_NAME = "MyPrefsFile";
    private boolean silent;

    private LoginUser loginUser;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user_login);
        // 初始化LoginUser
        loginUser = (LoginUser) getApplication();

        initView();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        silent = settings.getBoolean("silentMode", false);

        if (silent == false) {
            Toast.makeText(this, "第一次执行该应用！", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "非第一次执行该应用！", Toast.LENGTH_LONG).show();
        }
    }

    protected void onStop() {
        super.onStop();
        //通过名称，得到一个SharedPreferences,修改SharedPreferences中的存储值
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        //新建SharedPreferences编辑器
        SharedPreferences.Editor editor = settings.edit();
        //设置Boolean值
        editor.putBoolean("silentMode", true);
        //提交修改
        editor.commit();
    }

    private void initView() {
        userID = (EditText) findViewById(R.id.user_name);
        userPsw = (EditText) findViewById(R.id.user_psw);
        rememberPsw = (CheckBox) findViewById(R.id.remember_psw);

        loginToRegister = (Button) findViewById(R.id.login_regist);
        loginToRegister.setOnClickListener(this);

        login_btn = (Button) findViewById(R.id.btn_login);
        login_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.remember_psw:
                Drawable drawable_psw;
                if (REMEMBER_PSW == 1) {            //REMEMBER_PSW为 1 时，记住密码
                    drawable_psw = this.getResources().getDrawable(R.drawable.check_selected);
                    drawable_psw.setBounds(0, 0, drawable_psw.getMinimumWidth(), drawable_psw.getMinimumHeight());
                    rememberPsw.setCompoundDrawables(drawable_psw, null, null, null);
                } else if (REMEMBER_PSW == 0) {     //REMEMBER_PSW为 0 时，不记住密码
                    drawable_psw = this.getResources().getDrawable(R.drawable.check_unselected);
                    drawable_psw.setBounds(0, 0, drawable_psw.getMinimumWidth(), drawable_psw.getMinimumHeight());
                    rememberPsw.setCompoundDrawables(drawable_psw, null, null, null);
                }
                REMEMBER_PSW = 1 - REMEMBER_PSW;
                break;

            case R.id.login_regist:
                Intent intent2 = new Intent(UserLogin.this, UserRegister.class);
                startActivity(intent2);
                Toast.makeText(this, "注册新用户", Toast.LENGTH_LONG).show();
                break;

            case R.id.btn_login:
                if (null != getLoginUser()) {
                    Intent intent = new Intent(UserLogin.this, UserRegister.class);
                    startActivity(intent);
                    Toast.makeText(this, id + "登录成功", Toast.LENGTH_LONG).show();

                    intent.setClass(this, MainActivity.class);
                    startActivity(intent);

                    finish();
                    break;
                }
        }
    }

    public User getLoginUser() {
        /**
         * 实际连服务器使用时需要使用下面的
         */
        id = Long.valueOf(userID.getText().toString().trim());
        passWord = userPsw.getText().toString().trim();
        User user = new User();
        user.setId(id);
        user.setPassword(passWord);
        loginUser.setLogin(new UserController(this).login(user));

		loginUser.setLogin(user);
        return loginUser.getLogin();
    }
}

package edu.zut.cs.bbp.main.views.action;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.utils.IdCreater;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.utils.State;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 14-7-2
 * Time: 下午4:07
 * To change this template use File | Settings | File Templates.
 */
public class Submit extends Activity {

	private Button saveButton;
	private EditText amountEdit;
	private EditText remarkEdit;
	private TextView timeText;
	private String amount, remark;
	private User user;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.submit);

		user = ((LoginUser) getApplication()).getLogin();

		saveButton = (Button) findViewById(R.id.button1);
		amountEdit =(EditText) findViewById(R.id.amount);
		remarkEdit =(EditText) findViewById(R.id.remark);
		timeText =(TextView)findViewById(R.id.time);
		timeText.setText(new Date().toString());


		saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = getIntent();

                amount = amountEdit.getText().toString();
                remark = remarkEdit.getText().toString();
                Long time = System.currentTimeMillis();

                String f_id = intent.getStringExtra("f_id");
                String s_id = intent.getStringExtra("s_id");
                String t_id = intent.getStringExtra("t_id");

                String event_id = t_id==null ? s_id==null ? f_id : s_id : t_id;

                Event e = EventManager.getInstance(getApplicationContext(), user).findById(f_id);

                if (e.getName().equals("吃")) {
                    Eat eat = new Eat();
                    eat.setId(IdCreater.get(user));
                    eat.setUser(user);
                    eat.setEventId(event_id);
                    eat.setAmount(Long.valueOf(amount));
                    eat.setRemark(remark);
                    eat.setState(State.NEW);
                    eat.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (e.getName().equals("喝")) {
                    Drink drink = new Drink();
                    drink.setId(IdCreater.get(user));
                    drink.setUser(user);
                    drink.setEventId(event_id);
                    drink.setAmount(Long.valueOf(amount));
                    drink.setRemark(remark);
                    drink.setState(State.NEW);
                    drink.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (e.getName().equals("拉")) {
                    BB bb = new BB();
                    bb.setId(IdCreater.get(user));
                    bb.setUser(user);
                    bb.setEventId(event_id);
                    bb.setState(State.NEW);
                    bb.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (e.getName().equals("撒")) {
                    NN nn = new NN();
                    nn.setId(IdCreater.get(user));
                    nn.setUser(user);
                    nn.setEventId(event_id);
                    nn.setState(State.NEW);
                    nn.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (e.getName().equals("睡")) {
                    Sleep sleep = new Sleep();
                    sleep.setId(IdCreater.get(user));
                    sleep.setUser(user);
                    sleep.setEventId(event_id);
                    sleep.setState(State.NEW);
                    sleep.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (e.getName().equals("身高")) {
                    Height height = new Height();
                    height.setId(IdCreater.get(user));
                    height.setUser(user);
                    height.setEventId(event_id);
                    height.setHeight(Long.valueOf(amount));
                    height.setState(State.NEW);
                    height.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                } else if (e.getName().equals("体重")) {
                    Weight weight = new Weight();
                    weight.setId(IdCreater.get(user));
                    weight.setUser(user);
                    weight.setEventId(event_id);
                    weight.setWeight(Float.valueOf(amount));
                    weight.setState(State.NEW);
                    weight.setTime(time);

                    EventManager.getInstance(getApplicationContext(), user).save(e);
                    Toast.makeText(getApplicationContext(), "事件添加成功！", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
	}
}

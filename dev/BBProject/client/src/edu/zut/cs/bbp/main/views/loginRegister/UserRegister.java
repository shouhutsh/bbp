package edu.zut.cs.bbp.main.views.loginRegister;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.controller.UserController;

/**
 * Created with IntelliJ IDEA.
 * User: ASUS
 * Date: 14-6-24
 * Time: 下午12:04
 * To change this template use File | Settings | File Templates.
 */
public class UserRegister extends Activity implements View.OnClickListener {
    private Button backToLogin;      //返回到登录
    private Button register_btn;     //注册
    private TextView tv_top_title;   //注册页面标题
    private EditText inputName;      //输入用户名
    private EditText inputPsw;       //输入密码
    private EditText confirmPsw;     //确认密码
    private CheckBox checkBox;       //是否同意服务条款
    private TextView bbpSever;       //服务条款

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.user_register);
        initView();
    }

    private void initView() {
        backToLogin = (Button) findViewById(R.id.btn_title_left);
        backToLogin.setOnClickListener(this);

        register_btn = (Button) findViewById(R.id.button_register);
        register_btn.setOnClickListener(this);

        tv_top_title = (TextView) findViewById(R.id.tv_top_title);
        tv_top_title.setText("宝贝计划注册");

        inputName = (EditText) findViewById(R.id.intput_name);
        inputPsw = (EditText) findViewById(R.id.input_psw);
        confirmPsw = (EditText) findViewById(R.id.confirm_psw);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        bbpSever = (TextView) findViewById(R.id.bbp_Server);
    }

    @Override
    public void onClick(View view) {
        String name = inputName.getText().toString();
        String password = inputPsw.getText().toString();
        String password2 = confirmPsw.getText().toString();

        switch (view.getId()) {
            case R.id.btn_title_left:
                UserRegister.this.finish();
                break;

            case R.id.button_register:
                if (checkBox.isChecked() == false)
                    Toast.makeText(this, "请确认是否已经阅读《宝贝计划服务条款》", Toast.LENGTH_LONG).show();
                else if (!name.equals("") &&
                        password.equals(password2) &&
                        !password.equals("")) {

                    /*这里用户密码需要经过MD5加密*/

                    User user = new User();
                    user.setName(name);
                    user.setPassword(password);

                    UserController userController = new UserController(getApplicationContext());
                    user = userController.register(user);

                    if (null != user) {
                        Toast.makeText(this, inputName.getText().toString().trim() + "注册成功" + user.getId(), Toast.LENGTH_LONG).show();

                        finish();
                    }else {
                        Toast.makeText(this, inputName.getText().toString().trim() + "注册失败", Toast.LENGTH_LONG).show();
                    }
                } else if (name.equals("")) {
                    Toast.makeText(this, "用户名不能为空！", Toast.LENGTH_LONG).show();
                } else if (!password.equals(password2)) {
                    Toast.makeText(this, "密码输入有误！", Toast.LENGTH_LONG).show();
                }
        }
    }
}

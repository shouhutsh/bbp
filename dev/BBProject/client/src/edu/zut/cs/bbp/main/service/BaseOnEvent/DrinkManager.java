package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class DrinkManager {

    private static final String TAG = DrinkManager.class.getSimpleName();
    
    private static DrinkManager drinkManager;
    private static Context context;
    private DaoSession daoSession;
    private DrinkDao drinkDao;

    private static User user;

    private DrinkManager() {
    }

    public static DrinkManager getInstance(Context context, final User user) {
        if (drinkManager == null) {
            drinkManager = new DrinkManager();
            if (DrinkManager.context == null) {
                DrinkManager.context = context.getApplicationContext();
            }
            drinkManager.daoSession = BaseApplication.getDaoSession(context);
            drinkManager.drinkDao = drinkManager.daoSession.getDrinkDao();
        }

        DrinkManager.user = user;
        return drinkManager;
    }

    public Drink findById(String id) {
        return drinkDao.load(id);
    }

    public List<Drink> findAll(){
        return drinkDao.queryBuilder()
                .where(DrinkDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Drink> findAll(Integer state){
        return drinkDao.queryBuilder()
                .where(DrinkDao.Properties.UserId.eq(user.getId()), DrinkDao.Properties.State.eq(state))
                .list();
    }

    public List<Drink> findAll(Integer state, final Event event){
        return drinkDao.queryBuilder()
                .where(DrinkDao.Properties.UserId.eq(user.getId()),
                        DrinkDao.Properties.State.eq(state),
                        DrinkDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public List<Drink> query(String where, String... params) {
        return drinkDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Drink drink) {
        return drinkDao.insertOrReplace(drink);
    }

    public void save(final List<Drink> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        drinkDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Drink drink = list.get(i);
                    drinkDao.insertOrReplace(drink);
                }
            }
        });
    }

    public void delete(String id) {
        drinkDao.deleteByKey(id);
    }

    public void delete(Drink drink) {
        drinkDao.delete(drink);
    }

    public void deleteAll() {
        drinkDao.deleteAll();
    }
}
package edu.zut.cs.bbp.main.views.action;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.*;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.service.BaseOnUser.*;
import edu.zut.cs.bbp.main.utils.FileUtils;
import edu.zut.cs.bbp.main.utils.IdCreater;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.utils.State;
import android.widget.AdapterView.OnItemClickListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ASUS
 * Date: 14-6-30
 * Time: 上午10:20
 * To change this template use File | Settings | File Templates.
 */
public class LogRecord extends Activity {
    private EditText logTitle;
    private EditText logContent;
    private Button logSave;
    private Button logFirst;
    private Button logMileStone;
    private Button logSound;
    private Button logPicture;
    private Button logVideo;
    private ListView firstList;
    private ListView mileList;

    private User user;

    List<First> firsts = new ArrayList<First>();
    List<Milestone> miles = new ArrayList<Milestone>();
    List<Media> medias = new ArrayList<Media>();

    private MediaRecorder mr;// 创建录音对象
    private String[] firstArr = {"talk", "eat", "walk", "run"};
    private String[] mileArr = {"说", "吃", "走", "跑"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_record);

        logTitle = (EditText) findViewById(R.id.log_title);
        logContent = (EditText) findViewById(R.id.log_content);

        LoginUser loginUser = (LoginUser) getApplication();
        user = loginUser.getLogin();

        logSave = (Button) findViewById(R.id.log_save);
        logSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        save();
                    }
                }).start();
                Toast.makeText(LogRecord.this, "已保存", 1000).show();
            }
        });

        logFirst = (Button) findViewById(R.id.log_first);
        logFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (firstList.getVisibility() == View.GONE) {
                    if (mileList.getVisibility() == View.VISIBLE) {
                        mileList.setVisibility(View.GONE);
                    }
                    firstListView(firstArr);
                    firstList.setVisibility(View.VISIBLE);
                } else
                    firstList.setVisibility(View.GONE);
            }
        });

        firstList = (ListView) findViewById(R.id.log_firstlist);
        firstList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                if (firstArr[position] != null) {
                    String fname = firstArr[position].toString();

                    Toast.makeText(LogRecord.this, fname, 1000).show();

                    First first = new First();
                    first.setId(IdCreater.get(user));
                    first.setName(fname);
                    first.setState(State.NEW);
                    first.setUserId(user.getId());

                    firsts.add(first);
                }
            }
        });

        logMileStone = (Button) findViewById(R.id.log_mileStone);
        logMileStone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mileList.getVisibility() == View.GONE) {
                    if (firstList.getVisibility() == View.VISIBLE) {
                        firstList.setVisibility(View.GONE);
                    }
                    mileListView(mileArr);
                    mileList.setVisibility(View.VISIBLE);
                } else
                    mileList.setVisibility(View.GONE);
            }
        });

        mileList = (ListView) findViewById(R.id.log_milelist);
        mileList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mileArr[i] != null) {
                    String mname = mileArr[i].toString();

                    Toast.makeText(LogRecord.this, mname, 1000).show();

                    Milestone milestone = new Milestone();
                    milestone.setId(IdCreater.get(user));
                    milestone.setName(mname);
                    milestone.setState(State.NEW);
                    milestone.setUserId(user.getId());

                    miles.add(milestone);
                }
            }
        });

        final int[] FLAG = {1};
        logSound = (Button) findViewById(R.id.log_sound);
        logSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (FLAG[0] == 1) {
                    try {
                        mr = new MediaRecorder();
                        mr.setAudioSource(MediaRecorder.AudioSource.DEFAULT);// 从麦克风源进行录音
                        mr.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);// 设置输出格式
                        mr.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);// 设置编码格式

                        File file = new File(buildFileName() + ".m4a");
                        mr.setOutputFile(file.getAbsolutePath());// 设置输出文件
                        file.createNewFile();   // 创建文件
                        mr.prepare();           // 准备录制
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mr.start();
                    logSound.setText("Recording");
                } else if (FLAG[0] == 0) {
                    if (mr != null) {
                        mr.stop();
                    }
                    mr.release();
                    logSound.setText("语音");
                }
                FLAG[0] = 1 - FLAG[0];
            }
        });

        logPicture = (Button) findViewById(R.id.log_picture);
        logPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pictureAdr = buildFileName() + ".jpg";

                Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(pictureAdr)));
                startActivity(pictureIntent);
            }
        });

        logVideo = (Button) findViewById(R.id.log_video);
        logVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String videoAdr = buildFileName() + ".mp4";

                Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                videoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(videoAdr)));
                videoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);//0的时候画面质量低,1的时候1秒钟消耗2M存储空间
                videoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 8);//设置时间长度限制8s
                startActivity(videoIntent);
            }
        });
    }

    public void save() {

        String strContent = logContent.getText().toString().trim();
        String strTitle = logTitle.getText().toString().trim();

        Diary diary = new Diary();
        diary.setId(IdCreater.get(user));
        diary.setContent(strContent);
        diary.setTitle(strTitle);
        diary.setState(State.NEW);
        diary.setUserId(user.getId());

        DiaryManager.getInstance(this, user).save(diary);
        FirstManager.getInstance(this, user).save(firsts);
        MilestoneManager.getInstance(this, user).save(miles);

        for (First f : firsts) {
            First2Diary f2d = new First2Diary();
            f2d.setId(IdCreater.get(user));
            f2d.setUser(user);
            f2d.setFirst(f);
            f2d.setDiary(diary);

            First2DiaryManager.getInstance(this, user).save(f2d);
        }

        for (Milestone m : miles) {
            Mile2Diary m2d = new Mile2Diary();
            m2d.setId(IdCreater.get(user));
            m2d.setUser(user);
            m2d.setMilestone(m);
            m2d.setDiary(diary);

            Mile2DiaryManager.getInstance(this, user).save(m2d);
        }
    }

    /*创建存储文件*/
    private String buildFileName() {

        FileUtils fileUtils = new FileUtils();  //得到当前外部存储设备的目录
        if (!fileUtils.isFileExist("Media")) {
            fileUtils.creatSDDir("Media");
            Log.i("creatSDDir1", "no exist");
        }
        Log.i("creatSDDir2", "already exist");

        Date now = new Date();
        SimpleDateFormat formater = new SimpleDateFormat("yyMMdd_HHmm");

        String fileAddress = (fileUtils.creatSDDir("Media") + "/" + formater.format(now)).toString();
        Log.i("fileAddress", fileAddress);
        return fileAddress;
    }

    private void firstListView(String[] arr) {
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < arr.length; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            listItem.put("first_name", arr[i]);
            SimpleAdapter simpleAdapter = new SimpleAdapter(this, listItems,
                    R.layout.first_list_item, new String[]{"first_name"}, new int[]{R.id.first_name});
            firstList.setAdapter(simpleAdapter);
            listItems.add(listItem);
        }
    }

    private void mileListView(String[] milesArr) {
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < milesArr.length; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            listItem.put("mile_name", milesArr[i]);
            SimpleAdapter simpleAdapter = new SimpleAdapter(this, listItems,
                    R.layout.mile_list_item, new String[]{"mile_name"}, new int[]{R.id.mile_name});
            mileList.setAdapter(simpleAdapter);
            listItems.add(listItem);
        }
    }
}




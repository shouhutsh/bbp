package edu.zut.cs.bbp.main.views.action;

import android.os.Bundle;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.widget.TabHost;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.service.Base.UserManager;
import edu.zut.cs.bbp.main.utils.IdCreater;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.utils.State;
import edu.zut.cs.bbp.main.views.doublefrglist.DoubleFrgListActivity;
import edu.zut.cs.bbp.main.views.loginRegister.UserMain;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {
    private TabHost tabHost;

    private LoginUser loginUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tabHost=getTabHost();
        Resources localResources = getResources();

        loginUser= (LoginUser) getApplication();

        if(loginUser.getLogin() == null){

            UserManager um = UserManager.getInstance(this);
            User def = um.findById(1L);
            if(def == null){
                def = new User();

                def.setId(1L);
                def.setName("Test");
                def.setPassword("Test");

                um.save(def);

                String[] firsts = { "吃", "喝", "拉", "撒", "睡", "身高", "体重"};
                for(String s : firsts){
                    Event e = new Event();
                    e.setId(IdCreater.get(def));
                    e.setName(s);
                    e.setUser(def);
                    e.setState(State.CLEAN);

                    EventManager.getInstance(this, def).save(e);
                }
            }

            loginUser.setLogin(def);
        }

        Intent localIntent1 = new Intent();
        localIntent1.setClass(this, HealthRecord.class);
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.health_record))
                .setIndicator(getString(R.string.health_record),localResources.getDrawable(R.drawable.zhangdanchaxun))
                .setContent(localIntent1));

        Intent localIntent2 = new Intent();
        localIntent2.setClass(this, LogRecord.class);
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.log_record))
                .setIndicator(getString(R.string.log_record), localResources.getDrawable(R.drawable.tianjiazhangdan))
                .setContent(localIntent2));

        Intent localIntent3 = new Intent();
        localIntent3.setClass(this,  DoubleFrgListActivity.class);
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.report_forms))
                .setIndicator(getString(R.string.report_forms),localResources.getDrawable(R.drawable.fenleichaxun))
                .setContent(localIntent3));

        Intent localIntent4 = new Intent();
        localIntent4.setClass(this, Setting.class);
        tabHost.addTab(tabHost.newTabSpec(getString(R.string.setting))
                .setIndicator(getString(R.string.setting),localResources.getDrawable(R.drawable.tongjibaobiao))
                .setContent(localIntent4));
    }
}

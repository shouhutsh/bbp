package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class SleepManager {

    private static final String TAG = SleepManager.class.getSimpleName();
    
    private static SleepManager sleepManager;
    private static Context context;
    private DaoSession daoSession;
    private SleepDao sleepDao;

    private static User user;

    private SleepManager() {
    }

    public static SleepManager getInstance(Context context, final User user) {
        if (sleepManager == null) {
            sleepManager = new SleepManager();
            if (SleepManager.context == null) {
                SleepManager.context = context.getApplicationContext();
            }
            sleepManager.daoSession = BaseApplication.getDaoSession(context);
            sleepManager.sleepDao = sleepManager.daoSession.getSleepDao();
        }
        return sleepManager;
    }

    public Sleep findById(String id) {
        return sleepDao.load(id);
    }

    // 得到所有状态的实体
    public List<Sleep> findAll(){
        return sleepDao.queryBuilder()
                .where(SleepDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Sleep> findAll(Integer state){
        return sleepDao.queryBuilder()
                .where(SleepDao.Properties.UserId.eq(user.getId()), SleepDao.Properties.State.eq(state))
                .list();
    }

    public List<Sleep> findAll(Integer state, final Event event){
        return sleepDao.queryBuilder()
                .where(SleepDao.Properties.UserId.eq(user.getId()),
                        SleepDao.Properties.State.eq(state),
                        SleepDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public List<Sleep> query(String where, String... params) {
        return sleepDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Sleep sleep) {
        return sleepDao.insertOrReplace(sleep);
    }

    public void save(final List<Sleep> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        sleepDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Sleep sleep = list.get(i);
                    sleepDao.insertOrReplace(sleep);
                }
            }
        });
    }

    public void delete(String id) {
        sleepDao.deleteByKey(id);
    }

    public void delete(Sleep sleep) {
        sleepDao.delete(sleep);
    }

    public void deleteAll() {
        sleepDao.deleteAll();
    }
}
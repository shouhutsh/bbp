package edu.zut.cs.bbp.test.service;

import android.test.AndroidTestCase;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.UserManager;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2014/6/26.
 */
public class UserManagerTest extends AndroidTestCase {

    private final String TAG = this.getClass().getSimpleName();

    ObjectMapper mapper = new ObjectMapper();

    public void testSave(){
        for(int i = 0; i < 5; ++i){
            User user = new User();
            user.setName("Name_" + i);
            user.setPassword("Password_" + i);

            UserManager.getInstance(getContext()).save(user);

            assertNotNull(user.getId());
        }
    }
//
//    public void testSaveAll(){
//        List<User> users = new ArrayList<User>();
//
//        for(int i = 0; i < 5; ++i){
//            User user = new User();
//            user.setName("Name_" + i);
//            user.setPassword("Password_" + i);
//
//            users.add(user);
//        }
//        UserManager.getInstance(getContext()).save(users);
//    }
//
//    public void testFindById(){
//        User user = UserManager.getInstance(getContext()).findById(1L);
//    }
//
    public void testFindAll() throws IOException {
        List<User> users = UserManager.getInstance(getContext()).findAll();

        String s = mapper.writeValueAsString(users);

        assertNotNull(s);
    }
//
//    public void testQuery(){
//        List<User> users = UserManager.getInstance(getContext()).query(" WHERE _ID=?", "1");
//    }
//
//    public void testDelete(){
//        UserManager.getInstance(getContext()).delete(1L);
//        User u = UserManager.getInstance(getContext()).findById(1L);
//
//        assertEquals(null, u);
//    }
//
//    public void testDeleteAll(){
//        UserManager.getInstance(getContext()).deleteAll();
//
//        List<User> users = UserManager.getInstance(getContext()).findAll();
//
//        assertEquals(0, users.size());
//    }
}

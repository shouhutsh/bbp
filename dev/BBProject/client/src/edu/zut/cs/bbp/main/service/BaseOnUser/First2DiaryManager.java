package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class First2DiaryManager {

    private static final String TAG = First2DiaryManager.class.getSimpleName();
    
    private static First2DiaryManager first2DiaryManager;
    private static Context context;
    private DaoSession daoSession;
    private First2DiaryDao first2DiaryDao;

    private static User user;

    private First2DiaryManager() {
    }

    public static First2DiaryManager getInstance(Context context, final User user) {
        if (first2DiaryManager == null) {
            first2DiaryManager = new First2DiaryManager();
            if (First2DiaryManager.context == null) {
                First2DiaryManager.context = context.getApplicationContext();
            }
            first2DiaryManager.daoSession = BaseApplication.getDaoSession(context);
            first2DiaryManager.first2DiaryDao = first2DiaryManager.daoSession.getFirst2DiaryDao();
        }

        First2DiaryManager.user = user;
        return first2DiaryManager;
    }

    public First2Diary findById(String id) {
        return first2DiaryDao.load(id);
    }

    public List<First2Diary> findAll(){
        return first2DiaryDao.queryBuilder()
                .where(First2DiaryDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<First2Diary> findAll(Integer state){
        return first2DiaryDao.queryBuilder()
                .where(First2DiaryDao.Properties.UserId.eq(user.getId()), First2DiaryDao.Properties.State.eq(state))
                .list();
    }

    public List<First2Diary> query(String where, String... params) {
        return first2DiaryDao.queryRaw(" WHERE "+where, params);
    }

    public long save(First2Diary first2Diary) {
        return first2DiaryDao.insertOrReplace(first2Diary);
    }

    public void save(final List<First2Diary> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        first2DiaryDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    First2Diary first2Diary = list.get(i);
                    first2DiaryDao.insertOrReplace(first2Diary);
                }
            }
        });
    }

    public void delete(String id) {
        first2DiaryDao.deleteByKey(id);
    }

    public void delete(First2Diary first2Diary) {
        first2DiaryDao.delete(first2Diary);
    }

    public void deleteAll() {
        first2DiaryDao.deleteAll();
    }
}
package edu.zut.cs.bbp.main.service.Base;

import android.content.Context;
import edu.zut.cs.bbp.main.DaoSession;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.UserDao;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

/**
 * Created by Administrator on 2014/6/26.
 */
public class UserManager {

    private static final String TAG = UserManager.class.getSimpleName();
    
    private static UserManager userManager;
    private static Context context;
    private DaoSession daoSession;
    private UserDao userDao;


    private UserManager() {
    }

    public static UserManager getInstance(Context context) {
        if (userManager == null) {
            userManager = new UserManager();
            if (UserManager.context == null) {
                UserManager.context = context.getApplicationContext();
            }
            userManager.daoSession = BaseApplication.getDaoSession(context);
            userManager.userDao = userManager.daoSession.getUserDao();
        }
        return userManager;
    }

    public User findById(long id) {
        return userDao.load(id);
    }

    public List<User> findAll() {
        return userDao.loadAll();
    }

    /**
     * query list with where clause
     * ex: begin_date_time >= ? AND end_date_time <= ?
     *
     * @param where  where clause, include 'where' word
     * @param params query parameters
     * @return
     */
    public List<User> query(String where, String... params) {
        return userDao.queryRaw(" WHERE "+where, params);
    }

    /**
     * insert or update user
     *
     * @param user
     * @return insert or update user id
     */
    public long save(User user) {
        return userDao.insertOrReplace(user);
    }

    /**
     * insert or update userList use transaction
     *
     * @param list
     */
    public void save(final List<User> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        userDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    User user = list.get(i);
                    userDao.insertOrReplace(user);
                }
            }
        });

    }

    /**
     * delete user by id
     *
     * @param id
     */
    public void delete(long id) {
        userDao.deleteByKey(id);
    }

    public void delete(User user) {
        userDao.delete(user);
    }

    /**
     * delete all user
     */
    public void deleteAll() {
        userDao.deleteAll();
    }

}
package edu.zut.cs.bbp.main.utils;

import android.os.Environment;

import java.io.*;

/**
 * Created by Qi_2 on 2014/6/21 0021.
 */
public class FileUtils {

    private String SDPATH;

    public String getSDPATH() {
        return SDPATH;
    }

    public FileUtils() {
        //得到当前外部存储设备的目录
        // /SDCARD
        SDPATH = Environment.getExternalStorageDirectory() + "/";
    }

    public FileUtils(String basePath) {
        SDPATH = basePath;
    }

    /**
     * 在SD卡上创建文件
     *
     * @throws IOException
     */
    public File creatSDFile(String fileName) throws IOException {
        File file = new File(SDPATH + fileName);
        file.createNewFile();
        return file;
    }

    /**
     * 在SD卡上创建目录
     *
     * @param dirName
     */
    public File creatSDDir(String dirName) {
        File dir = new File(SDPATH + dirName);
        dir.mkdir();
        return dir;
    }

    /**
     * 判断SD卡上的文件夹是否存在
     */
    public boolean isFileExist(String fileName) {
        File file = new File(SDPATH + fileName);
        return file.exists();
    }

    /**
     * 将一个InputStream里面的数据写入到SD卡中
     */
    public File write2SDFromInput(String path, String fileName, InputStream input) {
        File file = null;
        OutputStream out = null;
        try {
            int i;
            creatSDDir(path);
            file = creatSDFile(path + fileName);
            out = new FileOutputStream(file);
            byte buffer[] = new byte[4 * 1024];
            while ((i = input.read(buffer)) != -1) {
                out.write(buffer, 0, i);
            }
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != out)
                    out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
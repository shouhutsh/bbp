package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class EatManager {

    private static final String TAG = EatManager.class.getSimpleName();
    
    private static EatManager eatManager;
    private static Context context;
    private DaoSession daoSession;
    private EatDao eatDao;

    private static User user;

    private EatManager() {
    }

    public static EatManager getInstance(Context context, final User user) {
        if (eatManager == null) {
            eatManager = new EatManager();
            if (EatManager.context == null) {
                EatManager.context = context.getApplicationContext();
            }
            eatManager.daoSession = BaseApplication.getDaoSession(context);
            eatManager.eatDao = eatManager.daoSession.getEatDao();
        }

        EatManager.user = user;
        return eatManager;
    }

    // 得到所有状态的实体
    public List<Eat> findAll(){
        return eatDao.queryBuilder()
                .where(EatDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Eat> findAll(Integer state){
        return eatDao.queryBuilder()
                .where(EatDao.Properties.UserId.eq(user.getId()), EatDao.Properties.State.eq(state))
                .list();
    }

    public List<Eat> findAll(Integer state, Event event){
        return eatDao.queryBuilder()
                .where(EatDao.Properties.UserId.eq(user.getId()),
                        EatDao.Properties.State.eq(state),
                        EatDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public Eat findById(String id) {
        return eatDao.load(id);
    }


    public List<Eat> query(String where, String... params) {
        return eatDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Eat eat) {
        return eatDao.insertOrReplace(eat);
    }

    public void save(final List<Eat> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        eatDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Eat eat = list.get(i);
                    eatDao.insertOrReplace(eat);
                }
            }
        });
    }

    public void delete(String id) {
        eatDao.deleteByKey(id);
    }

    public void delete(Eat eat) {
        eatDao.delete(eat);
    }

    public void deleteAll() {
        eatDao.deleteAll();
    }
}
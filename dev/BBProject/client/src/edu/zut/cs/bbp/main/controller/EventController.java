package edu.zut.cs.bbp.main.controller;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.utils.NetTemplate;
import edu.zut.cs.bbp.main.utils.State;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2014/6/29.
 */

/**
 * FIXME 同步问题还是有待考虑，另外Jackson包解析时候@JsonManagedReference还是有问题，服务器那边也需要再考虑
 */
public class EventController {

    private static final String TAG = UserController.class.getSimpleName();

    private static ObjectMapper mapper;
    private Context context;

    LoginUser loginUser;

    public EventController(Context context, LoginUser loginUser) {
        mapper = new ObjectMapper();
        this.context = context;
        this.loginUser = loginUser;
    }


    public void insert(final List<Event> events){

        if(events.size() == 0)
            return;

        final EventManager eventManager = EventManager.getInstance(context, events.get(0).getUser());

        new NetTemplate(){

            @Override
            protected String getUrl() {
                return context.getString(R.string.serverBaseUrl) + "event/insert.json";
            }

            @Override
            protected String getMethod() {
                return "POST";
            }

            @Override
            protected void setHeader(HttpURLConnection connection){
                super.setHeader(connection);

                connection.setUseCaches(false);
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            }

            @Override
            protected void upload(HttpURLConnection connection){
                try {
                    mapper.writeValue(connection.getOutputStream(), events);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void download(HttpURLConnection connection){
                for(Event e : events){
                    e.setState(State.CLEAN);
                    eventManager.save(e);
                }
            }
        }.execute();
    }

    public void delete(final List<Event> events) {

        if (events.size() == 0)
            return;

        final EventManager eventManager = EventManager.getInstance(context, events.get(0).getUser());

        for (final Event e : events) {
            new NetTemplate() {

                @Override
                protected String getUrl() {
                    return context.getString(R.string.serverBaseUrl) + "event/" + e.getId() + ".json";
                }

                @Override
                protected String getMethod() {
                    return "DELETE";
                }

                @Override
                protected void setHeader(HttpURLConnection connection) {
                    super.setHeader(connection);

                    connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                }

                @Override
                protected void download(HttpURLConnection connection) {
                    eventManager.delete(e);
                }
            }.execute();
        }
    }


    public void update(final List<Event> events){

        if(events.size() == 0)
            return;

        final EventManager eventManager = EventManager.getInstance(context, events.get(0).getUser());

        for(final Event e : events) {
            new NetTemplate() {

                @Override
                protected String getUrl() {
                    return context.getString(R.string.serverBaseUrl) + "event/"+e.getId()+".json";
                }

                @Override
                protected String getMethod() {
                    return "PUT";
                }

                @Override
                protected void setHeader(HttpURLConnection connection){
                    super.setHeader(connection);

                    connection.setUseCaches(false);
                    connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                    connection.setRequestProperty("Accept", "application/json");
                }

                @Override
                protected void upload(HttpURLConnection connection){
                    try {
                        mapper.writeValue(connection.getOutputStream(), e);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

                @Override
                protected void download(HttpURLConnection connection) {
                    try {
                        Event e = mapper.readValue(connection.getInputStream(), Event.class);
                        e.setState(State.CLEAN);
                        eventManager.save(e);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }.execute();
        }
    }

    public List<Event> selectAll(){

        final List<Event> events = new ArrayList<Event>();

        final EventManager eventManager = EventManager.getInstance(context, loginUser.getLogin());
        // FIXME limit可能需要用户指定，默认为10
        final Integer[] page = {1};
        final int limit = 10;

        while (0 < page[0]) {
            new NetTemplate() {

                @Override
                protected String getUrl() {
                    return context.getString(R.string.serverBaseUrl) + "event/.json?" + "page=" + page[0] + "&limit=" + limit + "&uid=" + loginUser.getLogin().getId();
                }

                @Override
                protected String getMethod() {
                    return "GET";
                }

                @Override
                protected void setHeader(HttpURLConnection connection) {
                    super.setHeader(connection);

                    connection.setUseCaches(false);
                    connection.setRequestProperty("Accept", "application/json");
                }

                @Override
                protected void download(HttpURLConnection connection) {
                    try {
                        JsonNode node = mapper.readTree(connection.getInputStream()).get("content");
                        Event[] es = mapper.readValue(node, Event[].class);

                        if(0 == es.length)
                            page[0] = 0;
                        else
                            ++page[0];

                        for(Event e : es){
                            eventManager.save(e);
                            events.add(e);
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }.execute();
        }

        return events;
    }

    public Event selectOne(final Event event){
        final EventManager eventManager = EventManager.getInstance(context, loginUser.getLogin());

        final Event[] e = new Event[1];
        new NetTemplate() {

            @Override
            protected String getUrl() {
                return context.getString(R.string.serverBaseUrl) + "event/"+event.getId()+".json";
            }

            @Override
            protected String getMethod() {
                return "GET";
            }

            @Override
            protected void setHeader(HttpURLConnection connection) {
                super.setHeader(connection);

                connection.setUseCaches(false);
                connection.setRequestProperty("Accept", "application/json");
            }

            @Override
            protected void download(HttpURLConnection connection) {
                try {
                    e[0] = mapper.readValue(connection.getInputStream(), Event.class);
                    e[0].setState(State.CLEAN);
                    eventManager.save(e[0]);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }.execute();

        return e[0];
    }
}

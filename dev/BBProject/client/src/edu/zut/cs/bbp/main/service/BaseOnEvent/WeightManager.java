package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class WeightManager {

    private static final String TAG = WeightManager.class.getSimpleName();
    
    private static WeightManager weightManager;
    private static Context context;
    private DaoSession daoSession;
    private WeightDao weightDao;

    private static User user;

    private WeightManager() {
    }

    public static WeightManager getInstance(Context context, final User user) {
        if (weightManager == null) {
            weightManager = new WeightManager();
            if (WeightManager.context == null) {
                WeightManager.context = context.getApplicationContext();
            }
            weightManager.daoSession = BaseApplication.getDaoSession(context);
            weightManager.weightDao = weightManager.daoSession.getWeightDao();
        }

        WeightManager.user = user;
        return weightManager;
    }

    public Weight findById(String id) {
        return weightDao.load(id);
    }

    public List<Weight> findAll(){
        return weightDao.queryBuilder()
                .where(WeightDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Weight> findAll(Integer state){
        return weightDao.queryBuilder()
                .where(WeightDao.Properties.UserId.eq(user.getId()), WeightDao.Properties.State.eq(state))
                .list();
    }

    public List<Weight> findAll(Integer state, final Event event){
        return weightDao.queryBuilder()
                .where(WeightDao.Properties.UserId.eq(user.getId()),
                        WeightDao.Properties.State.eq(state),
                        WeightDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public List<Weight> query(String where, String... params) {
        return weightDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Weight weight) {
        return weightDao.insertOrReplace(weight);
    }

    public void save(final List<Weight> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        weightDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Weight weight = list.get(i);
                    weightDao.insertOrReplace(weight);
                }
            }
        });
    }

    public void delete(String id) {
        weightDao.deleteByKey(id);
    }

    public void delete(Weight weight) {
        weightDao.delete(weight);
    }

    public void deleteAll() {
        weightDao.deleteAll();
    }
}
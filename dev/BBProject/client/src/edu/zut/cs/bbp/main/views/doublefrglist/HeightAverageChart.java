package edu.zut.cs.bbp.main.views.doublefrglist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint.Align;
import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Average temperature demo chart.
 */
public class HeightAverageChart extends AbstractChart {
	/**
	 * Returns the chart name.
	 *
	 * @return the chart name
	 */
	public String getName() {
		return "Average height";
	}

	/**
	 * Returns the chart description.
	 *
	 * @return the chart description
	 */
	public String getDesc() {
		return "The average temperature in 4 Greek islands (line chart)";
	}

	/**
	 * Executes the chart demo.
	 *
	 * @param context the context
	 * @return the built intent
	 */
	public Intent execute(Context context) {
		String[] titles = new String[]{"发育较快", "平均身高", "发育较慢","宝贝曲线"};
		List<double[]> x = new ArrayList<double[]>();
		for (int i = 0; i < titles.length-1; i++) {
			x.add(new double[]{1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22});
		}

		x.add(new double[]{0, 2, 5, 6, 7, 10, 12, 14, 16, 18});
		List<double[]> values = new ArrayList<double[]>();
		values.add(new double[]{52, 55, 59, 68, 71, 74, 77, 79, 81, 84, 86, 88});
		values.add(new double[]{50, 52, 57, 65, 68.5, 71.5, 74, 76, 78, 81, 82.5, 84.5});
		values.add(new double[]{47, 51, 55, 63, 66, 69, 71, 73, 75, 78, 79, 81});
		values.add(new double[]{30, 35, 40, 42, 45, 47, 50, 51,54,56});

		int[] colors = new int[]{Color.DKGRAY, Color.CYAN, Color.YELLOW, Color.BLUE};
		PointStyle[] styles = new PointStyle[]{PointStyle.DIAMOND, PointStyle.CIRCLE,
				PointStyle.DIAMOND,PointStyle.TRIANGLE};
		XYMultipleSeriesRenderer renderer = buildRenderer(colors, styles);
		int length = renderer.getSeriesRendererCount();
		for (int i = 0; i < length; i++) {
			((XYSeriesRenderer) renderer.getSeriesRendererAt(i)).setFillPoints(true);
		}

		setChartSettings(renderer, "Average ", "Month", "Height", 0, 25, 20, 120,
				Color.LTGRAY, Color.LTGRAY);
		renderer.setXLabels(15);
		renderer.setYLabels(40);//设置网格间距
		renderer.setShowGrid(true);
		renderer.setXLabelsAlign(Align.RIGHT);
		renderer.setYLabelsAlign(Align.RIGHT);
		renderer.setZoomButtonsVisible(true);
		renderer.setPanLimits(new double[]{0, 60, 20, 120});//设置移动范围
		renderer.setZoomLimits(new double[]{0,40,20,100});//设置放大缩小时X轴Y轴允许的最大最小值.
		renderer.setApplyBackgroundColor(true);//设置背景颜色
		renderer.setBackgroundColor(Color.GRAY);
		renderer.setPointSize(3);
//		renderer.setPanEnabled(true, true);

		XYMultipleSeriesDataset dataset = buildDataset(titles, x, values);
		XYSeries series = dataset.getSeriesAt(0);
		series.addAnnotation("Vacation", 16, 24);//设置Vacation位置
		Intent intent = ChartFactory.getLineChartIntent(context, dataset, renderer,
				"体重曲线");
		return intent;
	}




}


package edu.zut.cs.bbp.main.views.action;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import edu.zut.cs.bbp.main.R;

import java.io.File;

public class Setting extends Activity {
    private Button fileExplorer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);

        fileExplorer = (Button) findViewById(R.id.file_explorer);
        fileExplorer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("1","AAAAAA");
                Intent intent = new Intent(Setting.this,FileExplorer.class);
                startActivity(intent);
                Log.e("2","BBBBBB");
            }
        });

    }
}
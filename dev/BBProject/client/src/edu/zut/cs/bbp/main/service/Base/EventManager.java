package edu.zut.cs.bbp.main.service.Base;

import android.content.Context;
import edu.zut.cs.bbp.main.DaoSession;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.EventDao;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.utils.BaseApplication;
import edu.zut.cs.bbp.main.utils.LoginUser;
import edu.zut.cs.bbp.main.views.action.GrowRecord;

import java.util.List;

public class EventManager {

    private static final String TAG = EventManager.class.getSimpleName();

    private static EventManager eventManager;
    private static Context context;
    private DaoSession daoSession;
    private EventDao eventDao;

    private static User user;

    private EventManager() {
    }

    public static EventManager getInstance(Context context, final User user) {
        if (eventManager == null) {
            eventManager = new EventManager();
            if (EventManager.context == null) {
                EventManager.context = context.getApplicationContext();
            }
            eventManager.daoSession = BaseApplication.getDaoSession(context);
            eventManager.eventDao = eventManager.daoSession.getEventDao();
        }
        EventManager.user = user;
        return eventManager;
    }

    public Event findById(String id) {
        return eventDao.load(id);
    }

    // 得到所有状态的实体
    public List<Event> findAll() {
        return eventDao.queryBuilder()
                .where(EventDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Event> findAll(Integer state) {
        return eventDao.queryBuilder()
                .where(EventDao.Properties.UserId.eq(user.getId()), EventDao.Properties.State.eq(state))
                .list();
    }

    public List<Event> query(String where, String... params) {
        return eventDao.queryRaw(" WHERE " + where, params);
    }

    public long save(Event event) {
        return eventDao.insertOrReplace(event);
    }

    public void save(final List<Event> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        eventDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Event event = list.get(i);
                    eventDao.insertOrReplace(event);
                }
            }
        });
    }

    public void delete(String id) {
        eventDao.deleteByKey(id);
    }

    public void delete(Event event) {
        eventDao.delete(event);
    }

    public void deleteAll() {
        eventDao.deleteAll();
    }

    public List<Event> getRoot(){
        return eventDao.queryBuilder()
                .where(EventDao.Properties.UserId.eq(user.getId()), EventDao.Properties.ParentId.isNull())
                .list();
    }

    public List<Event> getChildrens(Event parent){
        return eventDao.queryBuilder()
                .where(EventDao.Properties.UserId.eq(user.getId()), EventDao.Properties.ParentId.eq(parent.getId()))
                .list();
    }
}
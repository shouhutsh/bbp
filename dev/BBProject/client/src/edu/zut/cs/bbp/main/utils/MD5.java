package edu.zut.cs.bbp.main.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created with IntelliJ IDEA.
 * User: ASUS
 * Date: 14-6-27
 * Time: 下午4:44
 * To change this template use File | Settings | File Templates.
 */
public class MD5 {

    public String MD5(byte[] bytes){
        return toMd5(bytes);
    }

    private static String toMd5(byte[] bytes) {
        MessageDigest algorithm = null;
        try {
            algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(bytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return toHexString(algorithm.digest(), "");
    }

    private static String toHexString(byte[] bytes, String separator) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : bytes) {
            String hex = Integer.toHexString(0xFF & b);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex).append(separator);
        }
        return hexString.toString();
    }
}

package edu.zut.cs.bbp.main.views.action;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.*;
import edu.zut.cs.bbp.main.R;
import android.widget.AdapterView.OnItemClickListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ASUS
 * Date: 14-7-2
 * Time: 下午4:48
 * To change this template use File | Settings | File Templates.
 */
public class FileExplorer extends Activity {
    private String rootPath = "/storage/";
    private String fileName;    //文件的名称
    private String fileAddr;

    private ListView listView;
    private TextView textView;

    // 记录当前的父文件夹
    private File currentParent;
    // 记录当前路径下的所有文件的文件数组
    private File[] currentFiles;

    private String suffix;//文件后缀

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filepreview);

        //获取列出全部文件的ListView
        listView = (ListView) findViewById(R.id.list);
        textView = (TextView) findViewById(R.id.path);

        //获取系统的SD卡目录
        File root = new File(rootPath);

        //如果extSdCard卡存在
        if (root.exists()) {
            currentParent = root;
            currentFiles = root.listFiles();
            //使用当前目录下的全部文件、文件夹来填充ListView
            inflateListView(currentFiles);
        }
        // 为ListView的列表项的单击事件绑定监听器
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // 用户单击了文件，fileName-文件名；fileAddr-文件的绝对路径
                if (currentFiles[position].isFile()) {
                    fileName = currentFiles[position].getName();
                    fileAddr = fileAddr + "/" + fileName;

                    Log.i("fileName", fileName);
                }
                // 获取用户点击的文件夹下的所有文件
                File[] tmp = currentFiles[position].listFiles();

                if (tmp == null) {
                    //suffix-文件的后缀名
                    suffix = fileName.substring(fileName.lastIndexOf("."));

                    Log.i("suffix", suffix);

                    //根据文件后缀名选择音乐播放器或者是视频播放器
                    if (suffix.equals(".mp3") || suffix.equals(".m4a") || suffix.equals(".aac")) {
                        Intent audio_intent = new Intent(Intent.ACTION_VIEW);
                        audio_intent.setDataAndType(Uri.parse(fileAddr), "audio/*");
                        startActivity(audio_intent);
                    } else if (suffix.equals(".mp4") || suffix.equals(".avi") || suffix.equals(".3gp")) {
                        Intent video_intent = new Intent(Intent.ACTION_VIEW);
                        video_intent.setDataAndType(Uri.parse(fileAddr), "video/*");
                        startActivity(video_intent);
                    } else {
                        Toast.makeText(FileExplorer.this, "该文件无法打开", Toast.LENGTH_LONG).show();
                    }
                } else {
                    //获取用户单击的列表项对应的文件夹，设为当前的父文件夹
                    currentParent = currentFiles[position];
                    //保存当前的父文件夹内的全部文件和文件夹
                    currentFiles = tmp;
                    // 再次更新ListView
                    inflateListView(currentFiles);
                }
            }
        });
        // 获取上一级目录的按钮
        Button parent = (Button) findViewById(R.id.parent);
        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View source) {
                try {
                    if (!currentParent.getCanonicalPath().equals("/storage")) {
                        // 获取上一级目录
                        currentParent = currentParent.getParentFile();
                        // 列出当前目录下所有文件
                        currentFiles = currentParent.listFiles();
                        Log.i("currentParent", currentParent.getCanonicalPath());
                        // 再次更新ListView
                        inflateListView(currentFiles);
                    } else if (currentParent.getCanonicalPath().equals("/storage")) {
                        Intent it = new Intent(FileExplorer.this, MainActivity.class);
                        startActivity(it);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void inflateListView(File[] files) {
        // 创建一个List集合，List集合的元素是Map
        List<Map<String, Object>> listItems = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < files.length; i++) {
            Map<String, Object> listItem = new HashMap<String, Object>();
            //如果当前File文件夹，使用folder图标；否则使用file图标
            if (files[i].isDirectory()) {
                listItem.put("icon", R.drawable.folder);
            } else {
                listItem.put("icon", R.drawable.file);
            }
            listItem.put("fileName", files[i].getName());
            //添加List项
            listItems.add(listItem);
        }
        // 创建一个SimpleAdapter
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, listItems,
                R.layout.line, new String[]{"icon", "fileName"}, new int[]{
                R.id.icon, R.id.file_name});
        // 为ListView设置Adapter
        listView.setAdapter(simpleAdapter);
        try {
            textView.setText("当前路径为：" + currentParent.getCanonicalPath());

            fileAddr = currentParent.getCanonicalPath();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

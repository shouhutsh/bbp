package edu.zut.cs.bbp.test.service;

import android.test.AndroidTestCase;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.utils.IdCreater;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/6/26.
 */
public class EventManagerTest extends AndroidTestCase {

    private final String TAG = this.getClass().getSimpleName();

    public void testSave(){
        User u = new User();
        u.setId(1L);

        String[] first = new String[]{"吃", "喝", "身高", "体重"};
        String[] second= new String[]{"水果", "正餐"};
        String[] third = new String[]{"苹果", "梨"};
        long id;
        int i, j, k;
        for(i = 0; i < first.length; ++i){

            Event event = new Event();
            event.setId(i+1 + "");
            event.setUser(u);
            event.setName(first[i]);
            event.setState(1);

            id = EventManager.getInstance(getContext(), u).save(event);

            assertNotNull(event.getId());
        }
        for(j = 0; j < second.length; ++j){
            Event event = new Event();
            event.setId(i+j+"");
            event.setUser(u);
            event.setName(second[j]);
            event.setParentId("1");
            event.setState(1);

            id = EventManager.getInstance(getContext(), u).save(event);

            assertNotNull(event.getId());
        }
        for(k = 0; k < third.length; ++k){
            Event event = new Event();
            event.setId(i+j+k+"");
            event.setUser(u);
            event.setName(third[k]);
            event.setParentId(i+"");
            event.setState(1);

            id = EventManager.getInstance(getContext(), u).save(event);

            assertNotNull(event.getId());
        }
    }

//	public void testGetRoot(){
//        User u = new User();
//        u.setId(1L);
//
//		List<Event> events = new ArrayList<Event>();
//
//		events = EventManager.getInstance(getContext(), u).getRoot();
//
//		assertNotSame(0, events.size());
//	}
//
//	public void testGetChildrens(){
//		User u = new User();
//		u.setId(1L);
//
//		Event e = new Event();
//		e.setId("1");
//
//		List<Event> events = new ArrayList<Event>();
//
//		events = EventManager.getInstance(getContext(), u).getChildrens(e);
//
//		assertNotSame(0, events.size());
//
//	}
//
//    public void testSaveAll(){
//        User u = new User();
//        u.setId(1L);
//
//        List<Event> events = new ArrayList<Event>();
//
//        for(int i = 0; i < 5; ++i){
//            Event event = new Event();
//            event.setId(IdCreater.get(u));
//            event.setUserId(1L);
//            event.setName("Name_" + i);
//            event.setState(0);
//
//            events.add(event);
//        }
//        EventManager.getInstance(getContext(), u).save(events);
//    }
//
//    public void testFindById(){
//        User u = new User();
//        u.setId(1L);
//
//        Event event = EventManager.getInstance(getContext(), u).findById("1");
//    }
//
//    public void testFindAll(){
//        User u = new User();
//        u.setId(1L);
//
//        List<Event> events;
//        EventManager eventManager = EventManager.getInstance(getContext(), u);
//        events = eventManager.findAll();
//        events = eventManager.findAll(0);
//        events = eventManager.findAll(1);
//    }
//
//    public void testQuery(){
//        User u = new User();
//        u.setId(1L);
//
//        List<Event> events = EventManager.getInstance(getContext(), u).query("name=?", "Name_1");
//    }
//
//    public void testDelete(){
//        User u = new User();
//        u.setId(1L);
//
//        EventManager.getInstance(getContext(), u).delete("1");
//        Event e = EventManager.getInstance(getContext(), u).findById("1");
//
//        assertEquals(null, e);
//    }

}

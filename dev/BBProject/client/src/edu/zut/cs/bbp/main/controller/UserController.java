package edu.zut.cs.bbp.main.controller;

import android.content.Context;
import edu.zut.cs.bbp.main.R;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.utils.NetTemplate;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by Qi_2 on 2014/6/28 0028.
 */
public class UserController {

    private static final String TAG = UserController.class.getSimpleName();

    private static ObjectMapper mapper;
    private Context context;

    public UserController(Context context) {
        mapper = new ObjectMapper();
        this.context = context;
    }

    public User register(final User user) {
        final User[] u = {null};

        new NetTemplate() {
            @Override
            protected String getUrl() {
                return context.getString(R.string.serverBaseUrl) + "user/0.json";
            }

            @Override
            protected String getMethod() {
                return "POST";
            }

            @Override
            protected void setHeader(HttpURLConnection connection){
                super.setHeader(connection);

                connection.setUseCaches(false);
                connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                connection.setRequestProperty("Accept", "application/json");
            }

            @Override
            protected void upload(HttpURLConnection connection){
                try {
                    mapper.writeValue(connection.getOutputStream(), user);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected void download(HttpURLConnection connection){
                try {
                    u[0] = mapper.readValue(connection.getInputStream(), User.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.execute();

        return u[0];
    }

    public User login(final User user){
        final User[] u = {null};
        new NetTemplate(){

            @Override
            protected String getUrl() {
                return context.getString(R.string.serverBaseUrl) + "user/" + user.getId() + ".json";
            }

            @Override
            protected String getMethod() {
                return "GET";
            }

            @Override
            protected void setHeader(HttpURLConnection connection){
                super.setHeader(connection);

                connection.setRequestProperty("Accept", "application/json");
            }

            @Override
            protected void download(HttpURLConnection connection) {
                try {
                    u[0] = mapper.readValue(connection.getInputStream(), User.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }.execute();

        if(null!=u[0]
                && u[0].getPassword().equals(user.getPassword())){
            return u[0];
        }
        return null;
    }
}

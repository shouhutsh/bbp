package edu.zut.cs.bbp.test.service;

import android.test.AndroidTestCase;
import edu.zut.cs.bbp.main.BB;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import edu.zut.cs.bbp.main.service.BaseOnEvent.BBManager;
import edu.zut.cs.bbp.main.utils.IdCreater;
import edu.zut.cs.bbp.main.utils.State;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/6/30.
 */
public class BBManagerTest extends AndroidTestCase {

    public void testSave(){
        User u = new User();
        u.setId(1L);

        Event e = new Event();
        e.setId("14040438941800000000000001000000");

        long id;
        for(int i = 0; i < 5; ++i){
            BB bb = new BB();
            bb.setId(IdCreater.get(u));
            bb.setUser(u);
            bb.setEvent(e);
            bb.setState(1);

            id = BBManager.getInstance(getContext(), u).save(bb);

            assertNotNull(bb.getId());
        }
    }

    public void testSaveAll(){
        User u = new User();
        u.setId(1L);

        Event e = new Event();
        e.setId("14040439763210000000000001000000");

        List<BB> bbs = new ArrayList<BB>();

        long id;
        for(int i = 0; i < 5; ++i){
            BB bb = new BB();
            bb.setId(IdCreater.get(u));
            bb.setUser(u);
            bb.setEvent(e);
            bb.setState(State.CLEAN);

            bbs.add(bb);
        }

        BBManager.getInstance(getContext(), u).save(bbs);
    }

    public void testFindAll(){
        User u = new User();
        u.setId(1L);

        Event e = new Event();
        e.setId("14040438941800000000000001000000");
        BBManager bbManager = BBManager.getInstance(getContext(), u);

        List<BB> bbs = bbManager.findAll();
        bbs = bbManager.findAll(0);
        bbs = bbManager.findAll(1, e);
    }
}

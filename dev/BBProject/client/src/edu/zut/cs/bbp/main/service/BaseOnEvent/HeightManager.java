package edu.zut.cs.bbp.main.service.BaseOnEvent;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class HeightManager {

    private static final String TAG = HeightManager.class.getSimpleName();
    
    private static HeightManager heightManager;
    private static Context context;
    private DaoSession daoSession;
    private HeightDao heightDao;

    private static User user;

    private HeightManager() {
    }

    public static HeightManager getInstance(Context context, final User user) {
        if (heightManager == null) {
            heightManager = new HeightManager();
            if (HeightManager.context == null) {
                HeightManager.context = context.getApplicationContext();
            }
            heightManager.daoSession = BaseApplication.getDaoSession(context);
            heightManager.heightDao = heightManager.daoSession.getHeightDao();
        }

        HeightManager.user = user;
        return heightManager;
    }

    public Height findById(String id) {
        return heightDao.load(id);
    }

    // 得到所有状态的实体
    public List<Height> findAll(){
        return heightDao.queryBuilder()
                .where(HeightDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Height> findAll(Integer state){
        return heightDao.queryBuilder()
                .where(HeightDao.Properties.UserId.eq(user.getId()), HeightDao.Properties.State.eq(state))
                .list();
    }

    public List<Height> findAll(Integer state, final Event event){
        return heightDao.queryBuilder()
                .where(HeightDao.Properties.UserId.eq(user.getId()),
                        HeightDao.Properties.State.eq(state),
                        HeightDao.Properties.EventId.eq(event.getId()))
                .list();
    }

    public List<Height> query(String where, String... params) {
        return heightDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Height height) {
        return heightDao.insertOrReplace(height);
    }

    public void save(final List<Height> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        heightDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Height height = list.get(i);
                    heightDao.insertOrReplace(height);
                }
            }
        });
    }

    public void delete(String id) {
        heightDao.deleteByKey(id);
    }

    public void delete(Height height) {
        heightDao.delete(height);
    }

    public void deleteAll() {
        heightDao.deleteAll();
    }
}
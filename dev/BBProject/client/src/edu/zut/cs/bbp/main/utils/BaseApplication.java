package edu.zut.cs.bbp.main.utils;

import android.app.Application;
import android.content.Context;
import android.provider.SyncStateContract;
import edu.zut.cs.bbp.main.DaoMaster;
import edu.zut.cs.bbp.main.DaoSession;

/**
 * Created by Administrator on 2014/6/26.
 */
public class BaseApplication extends Application {

        private static BaseApplication mInstance;
        private static DaoMaster daoMaster;
        private static DaoSession daoSession;

        @Override
        public void onCreate() {
            super.onCreate();
            if(mInstance == null)
                mInstance = this;
        }

        /**
         * 取得DaoMaster
         *
         * @param context
         * @return
         */
        public static DaoMaster getDaoMaster(Context context) {
            if (daoMaster == null) {
                DaoMaster.OpenHelper helper = new DaoMaster.DevOpenHelper(context, Constants.DB_NAME, null);
                daoMaster = new DaoMaster(helper.getWritableDatabase());
            }
            return daoMaster;
        }

        /**
         * 取得DaoSession
         *
         * @param context
         * @return
         */
        public static DaoSession getDaoSession(Context context) {
            if (daoSession == null) {
                if (daoMaster == null) {
                    daoMaster = getDaoMaster(context);
                }
                daoSession = daoMaster.newSession();
            }
            return daoSession;
        }
    }

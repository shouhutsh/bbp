package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class MilestoneManager {

    private static final String TAG = MilestoneManager.class.getSimpleName();
    
    private static MilestoneManager milestoneManager;
    private static Context context;
    private DaoSession daoSession;
    private MilestoneDao milestoneDao;

    private static User user;

    private MilestoneManager() {
    }

    public static MilestoneManager getInstance(Context context, final User user) {
        if (milestoneManager == null) {
            milestoneManager = new MilestoneManager();
            if (MilestoneManager.context == null) {
                MilestoneManager.context = context.getApplicationContext();
            }
            milestoneManager.daoSession = BaseApplication.getDaoSession(context);
            milestoneManager.milestoneDao = milestoneManager.daoSession.getMilestoneDao();
        }

        MilestoneManager.user = user;
        return milestoneManager;
    }

    public Milestone findById(String id) {
        return milestoneDao.load(id);
    }

    public List<Milestone> findAll(){
        return milestoneDao.queryBuilder()
                .where(MilestoneDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Milestone> findAll(Integer state){
        return milestoneDao.queryBuilder()
                .where(MilestoneDao.Properties.UserId.eq(user.getId()), MilestoneDao.Properties.State.eq(state))
                .list();
    }

    public List<Milestone> query(String where, String... params) {
        return milestoneDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Milestone milestone) {
        return milestoneDao.insertOrReplace(milestone);
    }

    public void save(final List<Milestone> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        milestoneDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Milestone milestone = list.get(i);
                    milestoneDao.insertOrReplace(milestone);
                }
            }
        });
    }

    public void delete(String id) {
        milestoneDao.deleteByKey(id);
    }

    public void delete(Milestone milestone) {
        milestoneDao.delete(milestone);
    }

    public void deleteAll() {
        milestoneDao.deleteAll();
    }
}
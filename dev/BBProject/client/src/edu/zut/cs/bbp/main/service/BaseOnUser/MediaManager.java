package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class MediaManager {

    private static final String TAG = MediaManager.class.getSimpleName();
    
    private static MediaManager mediaManager;
    private static Context context;
    private DaoSession daoSession;
    private MediaDao mediaDao;

    private static User user;

    private MediaManager() {
    }

    public static MediaManager getInstance(Context context, final User user) {
        if (mediaManager == null) {
            mediaManager = new MediaManager();
            if (MediaManager.context == null) {
                MediaManager.context = context.getApplicationContext();
            }
            mediaManager.daoSession = BaseApplication.getDaoSession(context);
            mediaManager.mediaDao = mediaManager.daoSession.getMediaDao();
        }

        MediaManager.user = user;
        return mediaManager;
    }

    public Media findById(String id) {
        return mediaDao.load(id);
    }

    public List<Media> findAll(){
        return mediaDao.queryBuilder()
                .where(MediaDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Media> findAll(Integer state){
        return mediaDao.queryBuilder()
                .where(MediaDao.Properties.UserId.eq(user.getId()), MediaDao.Properties.State.eq(state))
                .list();
    }

    public List<Media> query(String where, String... params) {
        return mediaDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Media media) {
        return mediaDao.insertOrReplace(media);
    }

    public void save(final List<Media> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        mediaDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Media media = list.get(i);
                    mediaDao.insertOrReplace(media);
                }
            }
        });
    }

    public void delete(String id) {
        mediaDao.deleteByKey(id);
    }

    public void delete(Media media) {
        mediaDao.delete(media);
    }

    public void deleteAll() {
        mediaDao.deleteAll();
    }
}
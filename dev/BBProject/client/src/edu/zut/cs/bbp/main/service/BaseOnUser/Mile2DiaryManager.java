package edu.zut.cs.bbp.main.service.BaseOnUser;

import android.content.Context;
import edu.zut.cs.bbp.main.*;
import edu.zut.cs.bbp.main.utils.BaseApplication;

import java.util.List;

public class Mile2DiaryManager {

    private static final String TAG = Mile2DiaryManager.class.getSimpleName();
    
    private static Mile2DiaryManager mile2DiaryManager;
    private static Context context;
    private DaoSession daoSession;
    private Mile2DiaryDao mile2DiaryDao;

    private static User user;

    private Mile2DiaryManager() {
    }

    public static Mile2DiaryManager getInstance(Context context, final User user) {
        if (mile2DiaryManager == null) {
            mile2DiaryManager = new Mile2DiaryManager();
            if (Mile2DiaryManager.context == null) {
                Mile2DiaryManager.context = context.getApplicationContext();
            }
            mile2DiaryManager.daoSession = BaseApplication.getDaoSession(context);
            mile2DiaryManager.mile2DiaryDao = mile2DiaryManager.daoSession.getMile2DiaryDao();
        }

        Mile2DiaryManager.user = user;
        return mile2DiaryManager;
    }

    public Mile2Diary findById(String id) {
        return mile2DiaryDao.load(id);
    }

    public List<Mile2Diary> findAll(){
        return mile2DiaryDao.queryBuilder()
                .where(Mile2DiaryDao.Properties.UserId.eq(user.getId()))
                .list();
    }

    public List<Mile2Diary> findAll(Integer state){
        return mile2DiaryDao.queryBuilder()
                .where(Mile2DiaryDao.Properties.UserId.eq(user.getId()), Mile2DiaryDao.Properties.State.eq(state))
                .list();
    }

    public List<Mile2Diary> query(String where, String... params) {
        return mile2DiaryDao.queryRaw(" WHERE "+where, params);
    }

    public long save(Mile2Diary mile2Diary) {
        return mile2DiaryDao.insertOrReplace(mile2Diary);
    }

    public void save(final List<Mile2Diary> list) {
        if (list == null || list.isEmpty()) {
            return;
        }
        mile2DiaryDao.getSession().runInTx(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
                    Mile2Diary mile2Diary = list.get(i);
                    mile2DiaryDao.insertOrReplace(mile2Diary);
                }
            }
        });
    }

    public void delete(String id) {
        mile2DiaryDao.deleteByKey(id);
    }

    public void delete(Mile2Diary mile2Diary) {
        mile2DiaryDao.delete(mile2Diary);
    }

    public void deleteAll() {
        mile2DiaryDao.deleteAll();
    }
}
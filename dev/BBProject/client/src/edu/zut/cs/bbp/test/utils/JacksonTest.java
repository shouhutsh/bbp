package edu.zut.cs.bbp.test.utils;

import android.test.AndroidTestCase;
import edu.zut.cs.bbp.main.Event;
import edu.zut.cs.bbp.main.User;
import edu.zut.cs.bbp.main.service.Base.EventManager;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created by Administrator on 2014/7/1.
 */
public class JacksonTest extends AndroidTestCase {

    ObjectMapper mapper = new ObjectMapper();

    public void test() throws IOException {

//        String s = "[{\"id\":\"1\",\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"state\":1,\"user\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"nickname\":null,\"name\":\"Name_0\",\"password\":\"Password_0\",\"lastUpload\":null,\"birthday\":null},\"name\":\"FIRST_1\",\"parent\":null,\"unit\":null},{\"id\":\"2\",\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"state\":1,\"user\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"nickname\":null,\"name\":\"Name_0\",\"password\":\"Password_0\",\"lastUpload\":null,\"birthday\":null},\"name\":\"FIRST_2\",\"parent\":null,\"unit\":null},{\"id\":\"3\",\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"state\":1,\"user\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"nickname\":null,\"name\":\"Name_0\",\"password\":\"Password_0\",\"lastUpload\":null,\"birthday\":null},\"name\":\"FIRST_3\",\"parent\":null,\"unit\":null},{\"id\":\"4\",\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"state\":1,\"user\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"nickname\":null,\"name\":\"Name_0\",\"password\":\"Password_0\",\"lastUpload\":null,\"birthday\":null},\"name\":\"FIRST_4\",\"parent\":null,\"unit\":null},{\"id\":\"5\",\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"state\":1,\"user\":{\"dateCreated\":null,\"dateModified\":null,\"deleted\":null,\"id\":1,\"nickname\":null,\"name\":\"Name_0\",\"password\":\"Password_0\",\"lastUpload\":null,\"birthday\":null},\"name\":\"FIRST_5\",\"parent\":null,\"unit\":null}]";
//
//        Event[] es;
//        es = mapper.readValue(s, Event[].class);
//
//        assertNotNull(es);

        User user = new User();
        user.setId(1L);

        Event e = EventManager.getInstance(getContext(), user).findById("12");

        String s = mapper.writeValueAsString(e);
        e = mapper.readValue(s, Event.class);

        System.out.print(s);
    }
}

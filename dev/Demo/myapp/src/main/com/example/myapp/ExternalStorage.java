package com.example.myapp;

import android.os.Environment;
import android.util.Log;

/**
 * Created by Administrator on 2014/6/17.
 */
public class ExternalStorage {

    public static void run() {
        boolean available = false;
        boolean writeable = false;
        String state = Environment.getExternalStorageState();

        if(state.equals(Environment.MEDIA_MOUNTED)){
            available = writeable = true;
        }else if(state.equals(Environment.MEDIA_MOUNTED_READ_ONLY)){
            available = true;
            writeable = false;
        }else{
            available = writeable = false;
        }

        Log.d("STORAGE",  "Available = " + available + ", Writeable = " + writeable);
    }
}

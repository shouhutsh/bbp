package com.example.myapp;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Administrator on 2014/6/17.
 */
public class MyMD5 {
    public String toMd5(byte[] bytes){
        try{
            MessageDigest algorithm = MessageDigest.getInstance("MD5");

            algorithm.reset();

            algorithm.update(bytes);

            return toHexString(algorithm.digest(), "");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String toHexString(byte[] bytes, String separator){
        StringBuilder hexString = new StringBuilder();

        for(byte b : bytes){
            String hex = Integer.toHexString(0xFF & b);
            if(hex.length() == 1){
                hexString.append('0');
            }
            hexString.append(hex).append(separator);
        }
        return hexString.toString();
    }

}

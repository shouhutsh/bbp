package com.example.myapp;

import android.util.Log;
import org.jivesoftware.smack.*;

import java.util.Collection;

/**
 * Created by Administrator on 2014/6/17.
 */
public class MyXMPP {

    private ConnectionConfiguration configuration;
    private XMPPConnection connection;

    public MyXMPP(){
        configuration = new ConnectionConfiguration("10.0.2.2", 5222);
        connection = new XMPPConnection(configuration);

        try {
            connection.connect();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    public void createUser(String name, String passwd){
        try {
            AccountManager manager = connection.getAccountManager();
            manager.createAccount(name, passwd);
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    public void login(String name, String passwd){
        try {
            connection.login(name, passwd);
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    public void logout(){
        connection.disconnect();
    }

    public void linkmanList(){
        Roster roster = connection.getRoster();

        Collection<RosterEntry> rg = roster.getEntries();
        for(RosterEntry r : rg){
            Log.d("LIST", "================LIST================" + r.getUser());
        }
    }

    public void linkmanState(String user){
        Roster roster = connection.getRoster();
        Log.d("STATE", "=================STATE================" + roster.getPresence(user));
    }
}

package com.example.myapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MyActivity extends Activity {

    public static final String PREFS_NAME = "myapp";

    MyXMPP xmpp = new MyXMPP();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean firstRun = settings.getBoolean("firstRun", true);      //默认为 true
        if(firstRun){
            Toast.makeText(this, "FIRST RUN", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "NOT FIRST RUN", Toast.LENGTH_SHORT).show();
        }

        ExternalStorage.run();

//        MyHttpClient httpClient = new MyHttpClient();
//        InputStream in = httpClient.getInputStreamFromUrl("http://10.0.2.2:9090");
//        httpClient.postData("http://10.0.2.2:9090/messageReply/testPost.html");

    }

    public void loginClick(View v){
        xmpp.login("abc", "abc");
    }
    public void logoutClick(View v){
        xmpp.logout();
    }
    public void createClick(View v){
        xmpp.createUser("shouhutsh", "123");
    }
    public void listClick(View v){
        xmpp.linkmanList();
    }
    public void stateClick(View v){
        xmpp.linkmanState("asd");
    }

    @Override
    protected void onStop(){
        super.onStop();

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean("firstRun", false);

        editor.commit();
    }
}

package com.example.myapp;

import android.test.AndroidTestCase;
import android.util.Log;

/**
 * Created by Administrator on 2014/6/17.
 */
public class MD5Test extends AndroidTestCase {
    MyMD5 md5;

    private void init() {
        md5 = new MyMD5();
    }

    public void test() {
        init();

        String s = md5.toMd5("hello".getBytes());
        Log.d("MD5", s);
    }
}
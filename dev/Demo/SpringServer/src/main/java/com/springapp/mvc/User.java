package com.springapp.mvc;

/**
 * Created by Qi_2 on 2014/5/6 0006.
 */
public class User {
    Long id;
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.stream.FileImageInputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class HelloController {
	@RequestMapping(value="/hello", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello ");
		return "hello";
	}
    @RequestMapping(value="/image/{imgId}", method = RequestMethod.GET, produces = "image/jpeg")
    public @ResponseBody byte[] image(@PathVariable  String imgId) {
        String path = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        File img = new File(path+"../image/cat.jpg");
        if(img.exists()){
            try {
                return FileCopyUtils.copyToByteArray(img);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    @RequestMapping(value = "/json", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody User json(){
        User u = new User();
        u.setId(123L);
        u.setName("hello");
        return u;
    }
}